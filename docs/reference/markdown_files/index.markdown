# Lightwood

liblightwood is library for widgets.It can also be extended to one's
requirements.

## Overview

Package provides very important widgets only for app development.
Liblightwood has all basic widgets like button,list,roller,webview
widgets,multiline textbox.etc. It is advisable to extend to one's
requirement.

Some of the widgets provides just abstract widgets(button,textbox)
and some are instantiable widgets(fixedroller, variable roller,flipper etc).

## Contact

[Mail the maintainers](mailto:maintainers@lists.apertis.org)
