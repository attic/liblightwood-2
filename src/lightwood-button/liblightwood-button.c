/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015, 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION:liblightwood-button
 * @Title:LightwoodButton
 * @Short_Description:LightwoodButton is an interface for generic buttons.
 * @See_Also: #LightwoodButtonBase #ClutterActor
 *
 * #LightwoodButton is an interface to create generic button widgets.
 * Supports properties like swipe actions,normal click,press,released events required for a generic
 * button. #LightwoodButtonBase implements this interface.
 *
 * Call g_object_unref() to free the widget.
 *
 * Since: UNRELEASED
 */

#include "liblightwood-button.h"
#include "lightwood-enumtypes.h"

G_DEFINE_INTERFACE (LightwoodButton, lightwood_button, G_TYPE_OBJECT);

enum
{
  SIGNAL_RELEASE,
  SIGNAL_PRESSED,
  SIGNAL_CLICKED,
  SIGNAL_LEAVE,
  SIGNAL_ENTER,
  SIGNAL_LONG_PRESS,
  SIGNAL_LAST
};

static guint signals[SIGNAL_LAST] = { 0, };

static void
lightwood_button_default_init (
    LightwoodButtonInterface *iface)
{

  /**
   * LightwoodButton:text-position:
   *
   * Position of the text for LIGHTWOOD_BUTTON_TEXT_IMAGE type.
   *
   * Since: UNRELEASED
   */
  g_object_interface_install_property (
      iface,
      g_param_spec_enum (
          "text-position", "text-position",
          "position of the text for LIGHTWOOD_BUTTON_TEXT_IMAGE type.",
          LIGHTWOOD_TYPE_BUTTON_TEXT_POSITION,
          5, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  /**
   * LightwoodButton:text:
   *
   * Text for the button.
   *
   * Since: UNRELEASED
   */
  g_object_interface_install_property (
      iface,
      g_param_spec_string ("text", "text",
                           "text for the button.", NULL,
                           G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  /**
   * LightwoodButton:icon:
   *
   * The #GIcon for the button.
   * The icon can be set using lightwood_button_set_icon ()
   *
   * Since: UNRELEASED
   */
  g_object_interface_install_property (
      iface,
      g_param_spec_string ("icon", "icon of type GIcon",
                           "get the icon of type GIcon.", NULL,
			   G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  /**
   * LightwoodButton:reset-duration:
   *
   * Reset duration for the click/swipe actions
   *
   * Since: UNRELEASED
   */
  g_object_interface_install_property (
      iface,
      g_param_spec_string ("reset-duration", "reset-duration",
                           "reset duration for the click/swipe actions", NULL,
			   G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  /**
   * LightwoodButton::released:
   * @iface: #LightwoodButton
   *
   * #LightwoodButton::released is emitted each
   * time a mouse button is released on button.
   *
   * Since: UNRELEASED
   */
  signals[SIGNAL_RELEASE] = g_signal_new (
      "released",
      G_TYPE_FROM_INTERFACE (iface),
      G_SIGNAL_RUN_LAST,
      G_STRUCT_OFFSET (LightwoodButtonInterface,
                       released),
      NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

  /**
   * LightwoodButton::pressed:
   * @iface: #LightwoodButton .
   *
   * #LightwoodButton::pressed is emitted each
   * time a mouse button is pressed on button.
   *
   * Since: UNRELEASED
   */
  signals[SIGNAL_PRESSED] = g_signal_new (
      "pressed",
      G_TYPE_FROM_INTERFACE (iface),
      G_SIGNAL_RUN_LAST,
      G_STRUCT_OFFSET (LightwoodButtonInterface,
                       pressed),
      NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

  /**
   * LightwoodButton::clicked:
   * @iface: #LightwoodButton .
   *
   * #LightwoodButton::clicked is emitted when the widget
   * should respond to a mouse pointer for the button press and release events.
   * This signal is emitted after the LightwoodButton::released signal.
   *
   * Since: UNRELEASED
   */
  signals[SIGNAL_CLICKED] = g_signal_new (
      "clicked",
      G_TYPE_FROM_INTERFACE (iface),
      G_SIGNAL_RUN_LAST,
      G_STRUCT_OFFSET (LightwoodButtonInterface,
                       clicked),
      NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

  /**
   * LightwoodButton::leave:
   * @iface: #LightwoodButton .
   *
   * #LightwoodButton::leave is emitted when
   * mouse pointer leaves over from widget.
   *
   * Since: UNRELEASED
   */
  signals[SIGNAL_LEAVE] = g_signal_new (
      "leave", G_TYPE_FROM_INTERFACE (iface), G_SIGNAL_RUN_LAST,
      G_STRUCT_OFFSET (LightwoodButtonInterface, leave),
      NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

  /**
   * LightwoodButton::enter:
   * @iface: #LightwoodButton .
   *
   * #LightwoodButton::enter is emitted when
   * mouse pointer enters/hover over to widget.
   *
   * Since: UNRELEASED
   */
  signals[SIGNAL_ENTER] = g_signal_new (
      "enter", G_TYPE_FROM_INTERFACE (iface), G_SIGNAL_RUN_LAST,
      G_STRUCT_OFFSET (LightwoodButtonInterface, enter),
      NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

  /**
   * LightwoodButton::long-pressed:
   * @iface: #LightwoodButton .
   *
   * #LightwoodButton::long-pressed is emitted
   * during the long press gesture handling.
   *
   * Since: UNRELEASED
   */
  signals[SIGNAL_LONG_PRESS] = g_signal_new (
      "long-pressed",
      G_TYPE_FROM_INTERFACE (iface),
      G_SIGNAL_RUN_LAST,
      G_STRUCT_OFFSET (LightwoodButtonInterface,
                       long_pressed),
      NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

}

/**
 * lightwood_button_get_text:
 * @self: A #LightwoodButton
 *
 * Helper function which returns the text set to the button.
 *
 * Returns: #LightwoodButton:text.
 *
 * Since: UNRELEASED
 */
const gchar *
lightwood_button_get_text (LightwoodButton *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_BUTTON (self), NULL);

  return (LIGHTWOOD_BUTTON_GET_IFACE (self)->get_text (self));
}

/**
 * lightwood_button_set_text:
 * @self: A #LightwoodButton
 * @text: the text to  set #LightwoodButton:text
 *
 * Sets the text for the button.
 *
 * Since: UNRELEASED
 */
void
lightwood_button_set_text (LightwoodButton *self, const gchar *text)
{
  g_return_if_fail (LIGHTWOOD_IS_BUTTON (self));

  LIGHTWOOD_BUTTON_GET_IFACE (self)->set_text (self, text);
}

/**
 * lightwood_button_set_icon:
 * @self: A #LightwoodButton
 * @icon: Icon of type #GIcon
 * @width: Width of the icon in pixels, must be >= 0
 * @height: Height of the image in pixels, must be >= 0
 *
 * Sets the path to icon of type #GIcon.
 *
 * Since: UNRELEASED
 */
void
lightwood_button_set_icon (LightwoodButton *self, GIcon *icon, gfloat width,
			   gfloat height)
{
  g_return_if_fail (LIGHTWOOD_IS_BUTTON (self));
  g_return_if_fail (width >= 0);
  g_return_if_fail (height >= 0);

  LIGHTWOOD_BUTTON_GET_IFACE (self)->set_icon (self, icon, width, height);
}

/**
 * lightwood_button_set_reset_duration:
 * @self: A #LightwoodButton
 * @duration: reset duration for the click/swipe actions
 *
 * Method which allows the user to set the reset-duration for the click/swipe actions
 *
 * Since: UNRELEASED
 */
void
lightwood_button_set_reset_duration (LightwoodButton *self, guint duration)
{
  g_return_if_fail (LIGHTWOOD_IS_BUTTON (self));

  LIGHTWOOD_BUTTON_GET_IFACE (self)->set_reset_duration (self, duration);
}


/**
 * lightwood_button_get_reset_duration:
 * @self: A #LightwoodButton
 *
 * Gets the reset duration for the click/swipe actions.
 *
 * Returns: #LightwoodButton:reset-duration
 *
 * Since: UNRELEASED
 */
guint
lightwood_button_get_reset_duration (LightwoodButton *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_BUTTON (self), 0);

  return (LIGHTWOOD_BUTTON_GET_IFACE (self)->get_reset_duration (self));
}
