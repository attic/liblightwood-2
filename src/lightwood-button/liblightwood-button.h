/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015, 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _LIBLIGHTWOOD_BUTTON_H
#define _LIBLIGHTWOOD_BUTTON_H

#include <glib-object.h>
#include <gio/gio.h>
#include <clutter/clutter.h>

G_BEGIN_DECLS

/**
 * LIGHTWOOD_TYPE_BUTTON:
 *
 * Get the #GType of the #LightwoodButton interface
 *
 * Since: UNRELEASED
 */
#define LIGHTWOOD_TYPE_BUTTON lightwood_button_get_type ()
G_DECLARE_INTERFACE (LightwoodButton, lightwood_button, LIGHTWOOD, BUTTON, GObject)

/**
 * LightwoodButtonTextPosition:
 * @LIGHTWOOD_BUTTON_TEXT_POSITION_LEFT: Text is placed on the left of icon
 * @LIGHTWOOD_BUTTON_TEXT_POSITION_RIGHT: Text is placed on the right of icon
 * @LIGHTWOOD_BUTTON_TEXT_POSITION_TOP: Text is placed above the icon
 * @LIGHTWOOD_BUTTON_TEXT_POSITION_BOTTOM: Text is placed below the icon
 * @LIGHTWOOD_BUTTON_TEXT_POSITION_CENTER: Text is placed over the icon
 * @LIGHTWOOD_BUTTON_TEXT_POSITION_UNKNOWN: Text position is Unknown
 *
 * The LightwoodButtonTextPosition enum which is used to
 * denote the position of the text.
 *
 * Since: UNRELEASED
 */
typedef enum {
  LIGHTWOOD_BUTTON_TEXT_POSITION_LEFT,
  LIGHTWOOD_BUTTON_TEXT_POSITION_RIGHT,
  LIGHTWOOD_BUTTON_TEXT_POSITION_TOP,
  LIGHTWOOD_BUTTON_TEXT_POSITION_BOTTOM,
  LIGHTWOOD_BUTTON_TEXT_POSITION_CENTER,
  LIGHTWOOD_BUTTON_TEXT_POSITION_UNKNOWN
} LightwoodButtonTextPosition;

/**
 * LightwoodButtonInterface:
 * @parent_iface: Parent of #LightwoodButton of type GTypeInterface
 *
 * Since: UNRELEASED
 */
struct _LightwoodButtonInterface
{
  GTypeInterface parent_iface;

  /*< public > */
  const gchar *
  (*get_text) (LightwoodButton *self);
  void
  (*set_text) (LightwoodButton *self, const gchar *text);
  void
  (*set_icon) (LightwoodButton *self, GIcon *icon, gfloat width, gfloat height);
  void
  (*set_reset_duration) (LightwoodButton *self, guint duration);
  guint
  (*get_reset_duration) (LightwoodButton *self);
  void
  (*released) (LightwoodButton *self);
  void
  (*pressed) (LightwoodButton *self);
  void
  (*clicked) (LightwoodButton *self);
  void
  (*leave) (LightwoodButton *self);
  void
  (*enter) (LightwoodButton *self);
  void
  (*long_pressed) (LightwoodButton *self);
};

const gchar *
lightwood_button_get_text (LightwoodButton *self);

void
lightwood_button_set_text (LightwoodButton *self, const gchar *text);

void
lightwood_button_set_icon (LightwoodButton *self, GIcon *icon, gfloat width,
			   gfloat height);

void
lightwood_button_set_reset_duration (LightwoodButton *self, guint duration);

guint
lightwood_button_get_reset_duration (LightwoodButton *self);

G_END_DECLS
#endif /* _LIBLIGHTWOOD_BUTTON_H */
