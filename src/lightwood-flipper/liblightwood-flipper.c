/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/******************************************************************************
 * Description of FIXES:
 * -----------------------------------------------------------------------------------
 *	Description												Date			Name
 *	----------												----			----
 * 1. Added "set-discrete-value" and "proceed-flip"
 * 	  properties to support flip of actors in discrete
 * 	  values between the initial and final values
 * 	  provided.														Dec 09 ,2013
 * 2. Get property provided for "set-discrete-value"				Mar 03 ,2014
 * 3. Actors Restored to normal states when model is removed		Mar 19 ,2014
 *******************************************************************************/
/**
 * SECTION:liblightwood-flipper
 * @Title:LightwoodFlipper
 * @Short_Description: Lightwood Flipper widget to flip pair of actors.
 * @See_Also: #ThornburyModel #ClutterActor
 *
 * #LightwoodFlipper will provide all generic properties required to animate a pair of actors in such
 * a way that two objects appears to be one behind the other. This  widget provides basic features
 * to  achieve the flip animation. The properties can be configured to achieve customized flip animation .
 *
 * The widget is capable of reversing the direction of animation at any point of time during the animation
 * on explicit request.No UI will be associated with the same as the purpose of this #LightwoodFlipper is to be a
 * generic widget which provides the basic properties and signals required to achieve flip animation of two objects.
 * A new object which inherits the liblightwood flipper can add different customized animation  based on its requirements.
 *
 * ## Feeding the Model.
 *      #LightwoodFlipper expects the actors to be flipped be provided through #ThornburyModel.
 *
 * Model structure :
 * %LIGHTWOOD_FLIPPER_COLUMN_FIRST_ACTOR of type #G_TYPE_OBJECT.
 * %LIGHTWOOD_FLIPPER_COLUMN_SECOND_ACTOR of type #G_TYPE_OBJECT.
 *
 * The model can contain a single actor , but the #LightwoodFlipper:flip-cycle must be
 * set appropriately. Actors can be swapped, removed or added via model. However
 * modifying the model during animation is not preferred.
 *
 * Note: The actors created must be within the same #ClutterStage.
 *
 *
 * ## Freeing the widget
 *      Call g_object_unref() to free the widget.
 *
 * ## Sample C Code
 * |[<!-- language="C" -->
 *
 * // create the stage
 * ClutterActor *pStage = clutter_stage_new ();
 * 
 * // create first actor
 * ClutterActor *pFirstActor = clutter_actor_new ();
 * clutter_actor_add_child (pStage,  pFirstActor);
 * clutter_actor_set_background_color ( pFirstActor, CLUTTER_COLOR_White);
 * clutter_actor_set_size( pFirstActor , 50.0 , 50.0 );
 * clutter_actor_set_position( pFirstActor ,0.0 , 0.0) ;
 * 
 * // create second actor
 * ClutterActor *pSecondActor = clutter_actor_new ();
 * clutter_actor_add_child (pStage,  pSecondActor); 
 * clutter_actor_set_background_color ( pSecondActor, CLUTTER_COLOR_DarkChameleon);
 * clutter_actor_set_size( pSecondActor , 50.0 , 50.0 );
 * clutter_actor_set_position( pSecondActor , 0.0 , 0.0);
 *
 * // Hide the actor which needs to be in foreground
 * clutter_actor_hide(pSecondActor);
 * 
 * 
 * // create model
 * ThornburyModel *model =  (ThornburyModel*)thornbury_list_model_new (LIGHTWOOD_FLIPPER_COLUUMN_LAST,
 *                      G_TYPE_OBJECT, NULL,
 *                      G_TYPE_OBJECT, NULL,
 *                      -1);
 *
 * // Create liblightwood flipper
 * LightwoodFlipper *pFlipper = g_object_new(LIGHTWOOD_TYPE_FLIPPER, "transition_time", TRANSITION_TIME,
 *                 "model",(ThornburyModel*)pFlipperModel,NULL);
 * 
 * // Start Animation.
 * lightwood_flipper_start_animation (pFlipper);
 *
 * ]|
 *
 *      Refer #ThornburyModel for model details.
 *
 * #LightwoodFlipper is available since liblightwood-1.0
 */



#include "liblightwood-flipper.h"


/* Enable this flag to provide time gap between transition to reduce flickering effect */
/*< private >*/
#define PROVIDE_ANIM_GAP 1

#define LIGHTWOOD_FLIPPER_DEFAULT_TRANS_DURATION 250
#define LIGHTWOOD_FLIPPER_DEFAULT_SCALE_X_MIN 0.4
#define LIGHTWOOD_FLIPPER_DEFAULT_SCALE_X_MAX 1.0
#define LIGHTWOOD_FLIPPER_DEFAULT_SCALE_Z_MAX  0.0
#define LIGHTWOOD_FLIPPER_DEFAULT_SCALE_Z_MIN  0.0
#define LIGHTWOOD_FLIPPER_DEFAULT_SCALE_Y_MIN 0.8
#define LIGHTWOOD_FLIPPER_DEFAULT_SCALE_Y_MAX 1.0
#define LIGHTWOOD_FLIPPER_DEFAULT_FWD_FINAL_ANGLE  90.0d
#define LIGHTWOOD_FLIPPER_DEFAULT_FWD_INITIAL_ANGLE  0.0d
#define LIGHTWOOD_FLIPPER_DEFAULT_BKWD_FINAL_ANGLE  360.0d
#define LIGHTWOOD_FLIPPER_DEFAULT_BKWD_INITIAL_ANGLE  270.0d
#define LIGHTWOOD_FLIPPER_MINIMUM_FLIP_DURATION 50
#define LIGHTWOOD_FLIPPER_DEFAULT_ANIMATION_MODE   CLUTTER_LINEAR /*CLUTTER_EASE_IN_QUAD*/
#define LIGHTWOOD_FLIPPER_DEFAULT_ANIMATION_MODE1  CLUTTER_LINEAR  /* CLUTTER_EASE_OUT_QUAD */
#define LIGHTWOOD_FLIPPER_DEFAULT_PIVOT_POINT_X 0.5
#define LIGHTWOOD_FLIPPER_DEFAULT_PIVOT_POINT_Y 0.5
#define LIGHTWOOD_FLIPPER_ANIMATION_TIME_GAP 150

#define LIGHTWOOD_ACTOR_ROTATION_ALONG_Y_AXIS 	"rotation-angle-y"
#define LIGHTWOOD_ACTOR_SCALE_ALONG_X_AXIS		"scale-x"
#define LIGHTWOOD_ACTOR_SCALE_ALONG_Y_AXIS		"scale-y"
#define LIGHTWOOD_ACTOR_SCALE_ALONG_Z_AXIS		"scale-z"
#define LIGHTWOOD_FLIPPER_FIRST_ACTOR_MARKER	"first-actor-marker"
#define LIGHTWOOD_FLIPPER_SECOND_ACTOR_MARKER	"second-actor-marker"

#define MODEL_LENGTH_ALLOWED 1
#define ROW_MODEL_DATA 0

/* State machine variable */
#define UPDATE_SMV(pLHSValue ,pRHSValue ) pLHSValue = pRHSValue

#define FWD_DESCRETE_VALUE(SCALE , START , END) ( START - (2 * ( (gdouble)SCALE * (START-END) ) ) )
#define FWD_DESCRETE_ANGLE(SCALE , START , END) ( ( 2 * (gdouble)SCALE * (END - START) ) + START)
#define BKD_DESCRETE_VALUE(SCALE , END ,START ) (START + ( 2 * ( (gdouble)SCALE - 0.5 ) * ( END - START) ) )
#define BKD_DESCRETE_ANGLE(SCALE , START , END) ( ( ( 2 * ((gdouble)SCALE - 0.5) )* (END - START)) + START)



/***
 * enFlipperSignals
 * @SIG_FLIPPER_ANIMATION_STARTED :
 * @SIG_FLIPPER_ANIMATION_HALF_DONE:
 * @SIG_FLIPPER_ANIMATION_COMPLETED:
 * @SIG_FLIPPER_ANIMATION_REVERSED:
 * @SIG_FLIPPER_ANIMATION_MARK_REACHED:
 *
 * The signals emitted by the liblightwood flipper
 */
typedef enum _enFlipperSignals enFlipperSignals;
enum _enFlipperSignals
{
	/*< private >*/
	SIG_FLIPPER_ANIMATION_STARTED,
	SIG_FLIPPER_ANIMATION_HALF_DONE,
	SIG_FLIPPER_ANIMATION_COMPLETED,
	SIG_FLIPPER_ANIMATION_REVERSED,
	SIG_FLIPPER_ANIMATION_MARK_REACHED,
	SIG_FLIPPER_LAST_SIGNAL
};

/**
 * enFlipperActors:
 * @LIGHTWOOD_FLIPPER_FIRST_ACTOR 	:  Indicates Front Actor to Flip
 * @LIGHTWOOD_FLIPPER_SECOND_ACTOR	:  Indicates Back Actor to Flip
 * @LIGHTWOOD_FLIPPER_BOTH_ACTOR	:  Consider both actors
 *
 * The enums to determine the object and their direction of rotation.
 */

typedef enum _enFlipperActors enFlipperActors;
enum _enFlipperActors
{
	/*< private >*/
	LIGHTWOOD_FLIPPER_FIRST_ACTOR,
	LIGHTWOOD_FLIPPER_SECOND_ACTOR,
	LIGHTWOOD_FLIPPER_BOTH_ACTOR,
	LIGHTWOOD_FLIPPER_ACTORS_NONE
};

/**
 * LightwoodFlipperSignalData:
 * @pLightwoodFlipper 	:  LightwoodFlipper Object pointer .
 * @enAnimateActor	:  enum of type enFlipperActors to identify the actor.
 *
 * The structure that can be used as user data while connecting to transition-stopped signal
 */
typedef struct _LightwoodFlipperSignalData LightwoodFlipperSignalData;
struct _LightwoodFlipperSignalData
{
	/*< private >*/
	LightwoodFlipper *pLightwoodFlipper ;
	enFlipperActors enAnimateActor;
	gint inHandlerId;
	guint uinMarkerTime ;
	gint inMarkerHandlerId ;
	gboolean bSetMarker ;
};


/**
 * enFlipperProperties:
 *
 *  flipper property enums
 */
typedef enum _enFlipperProperties enFlipperProperties;
enum _enFlipperProperties
{
	/*< private >*/
	PROP_FLIPPER_FIRST,
	PROP_FLIPPER_SCALE_X_MIN ,
	PROP_FLIPPER_SCALE_X_MAX ,
	PROP_FLIPPER_SCALE_Y_MIN ,
	PROP_FLIPPER_SCALE_Y_MAX ,
	PROP_FLIPPER_SCALE_Z_MIN ,
	PROP_FLIPPER_SCALE_Z_MAX ,
	PROP_FLIPPER_FORWARD_FLIP_START_ANGLE ,
	PROP_FLIPPER_FORWARD_FLIP_END_ANGLE ,
	PROP_FLIPPER_BACKWARD_FLIP_START_ANGLE ,
	PROP_FLIPPER_BACKWARD_FLIP_END_ANGLE,
	PROP_FLIPPER_TRANSITION_TIME ,
	PROP_FLIPPER_FIRST_ACTOR_MARK,
	PROP_FLIPPER_SECOND_ACTOR_MARK,
	PROP_FLIPPER_TRANSITION_DIRECTION,
	PROP_FLIPPER_ANIMATION_MODE ,
	PROP_FLIPPER_MODEL ,
	PROP_FLIPPER_ANIMATION_IN_PROGRESS,
	PROP_FLIPPER_ACTOR_BEING_ANIMATED ,
	PROP_FLIPPER_FLIP_DIRECTION,
	PROP_FLIPPER_FLIP_CYCLE,
	PROP_FLIPPER_X_PIVOT_POINT,
	PROP_FLIPPER_Y_PIVOT_POINT,
	PROP_FLIPPER_DISCRETE_FACTOR,
	PROP_FLIPPER_LAST
};

/**
 * LightwoodFlipperData:
 *
 *  Structure to hold the relevant data associated to either forward or backward Flip
 */

typedef struct _LightwoodFlipperData LightwoodFlipperData;
struct _LightwoodFlipperData
{
	/*< private >*/
	ClutterActor *pAnimateActor ;
	guint uinFlipDuration ;
	gdouble dScalingMinY ;
	gdouble dScalingMaxY ;
	gdouble dScalingMinX ;
	gdouble dScalingMaxX ;
	gdouble dScalingMinZ ;
	gdouble dScalingMaxZ ;
	gdouble dStartAngle;
	gdouble dStopAngle;

	/* Transition to be applied */
	ClutterTransition *pTransRotate   ;
	ClutterTransition *pTransScaleX   ;
	ClutterTransition *pTransScaleY   ;
	ClutterTransition *pTransScaleZ   ;

	/* InterMediate Values To be set on Interruption of Animation
	 * or on starting animation from scaled mode
	 */
	gdouble dScaleX;
	gdouble dScaleY;
	gdouble dScaleZ;
	gdouble dAngle;
	guint uinDuration ;

};




typedef enum _enFlipperControls enFlipperControls;
enum _enFlipperControls
{
	/*< private >*/
	DISABLE_TRANSTION_ON_FIRST_ACTOR,
	DISABLE_TRANSTION_ON_SECOND_ACTOR,
	ENABLE_TRANSTION_ON_FIRST_ACTOR ,
	ENABLE_TRANSTION_ON_SECOND_ACTOR,
	FLIPPER_ANIM_CONTROL_LAST

};

#define VALIDATE_LIGHTWOOD_FLIPPER_OBJECT(pFlipper) if (! LIGHTWOOD_IS_FLIPPER (pFlipper)) \
		{ \
	g_warning("invalid flipper pObject\n"); \
	return ; \
		} \

#define LIGHTWOOD_FLIPPER_FREE_MEM(pPointer) if(pPointer != NULL) \
		{\
	g_free(pPointer);\
	pPointer = NULL ;\
		}



#define LIGHTWOOD_FLIPPER_UNREF_TRANISTION(pTransition) if(NULL != pTransition) \
		{\
	g_object_unref(pTransition);\
	pTransition = NULL ;\
		}

#define RETURN_TRUE_IF_ANIMATION_IN_PROGRESS(pTransition)\
		if(TRUE == clutter_timeline_is_playing  (CLUTTER_TIMELINE(pTransition)) )\
		return TRUE ;


/**************** Debug Flags *******************/

/* debug flag settings */
guint  flipper_debug_flags = 0;

/* Set the environment variable in terminal to enable traces:
 *  export LIGHTWOOD_FLIPPER_DEBUG=lightwood-flipper */
enum _enFlipperDebugFlag
{
	LIGHTWOOD_FLIPPER_DEBUG = 1 << 0,

};
#define FLIPPER_HAS_DEBUG               ((flipper_debug_flags ) & 1)
#define LIGHTWOOD_FLIPPER_PRINT( a ...) \
		if (G_LIKELY (FLIPPER_HAS_DEBUG )) \
		{                            	\
			g_print(a);           \
		}
static const GDebugKey flipper_debug_keys[] =
{
		{ "lightwood-flipper",   LIGHTWOOD_FLIPPER_DEBUG }
};

/**************** signal Flags *******************/
/* Storage for the signals */
static guint32 flipper_signals[SIG_FLIPPER_LAST_SIGNAL] = {0,};


struct _LightwoodFlipperPrivate
{
	/*< private >*/
	ClutterActor *pFirstActor ;
	ClutterActor *pSecondActor ;
	gfloat fltPivotX;
	gfloat fltPivotY;
	gfloat fltDiscreteFactor;
	guint uinFlipDuration ;
	ClutterTimelineDirection uinFlipDirection ;
	ClutterAnimationMode uinFlipAnimType ;
	guint uinFlipAnimType1 ;
	ClutterTimelineDirection uinFlipType;
	LightwoodFlipCycle uinFlipCycle ;
	guint uinAnimGapTimeoutId;
	guint uinAnimControl;

	gdouble dScalingMinY ;
	gdouble dScalingMaxY ;
	gdouble dScalingMinX ;
	gdouble dScalingMaxX ;
	gdouble dScalingMinZ ;
	gdouble dScalingMaxZ ;
	gdouble dFirstActorAngleStart;
	gdouble dFirstActorAngleEnd;
	gdouble dSecondActorAngleStart;
	gdouble dSecondActorAngleEnd;
	ClutterTransition *pTransRotateA1   ;
	ClutterTransition *pTransScaleXA1   ;
	ClutterTransition *pTransScaleYA1   ;
	ClutterTransition *pTransScaleZA1   ;
	ClutterTransition *pTransRotateA2   ;
	ClutterTransition *pTransScaleXA2   ;
	ClutterTransition *pTransScaleYA2   ;
	ClutterTransition *pTransScaleZA2   ;
	gboolean bReversedAnimation ;
	/* This variable is used to judge whether the animation
	 * has been reset after completion and to avoid abnormal transition
	 * resulting from time lag effects on.
	 *
	 */
	gboolean bValuesReset ;
	gboolean uinAnimateActor;
	ThornburyModel *pFlipperModel ;
	LightwoodFlipperSignalData *pFirstActorData;
	LightwoodFlipperSignalData *pSecondActorData;
};

G_DEFINE_TYPE_WITH_CODE (LightwoodFlipper, lightwood_flipper, CLUTTER_TYPE_ACTOR,
                         G_ADD_PRIVATE (LightwoodFlipper))

#define LIGHTWOOD_FLIPPER_GET_PRIVATE(o) lightwood_flipper_get_instance_private((LightwoodFlipper *) o)


/*< public >*/

/***********************************************************
 * @local function declarations
 **********************************************************/
static void lightwood_flipper_restore_actor_to_normal_state(LightwoodFlipper *pLightwoodFlipper , enFlipperActors uinActor);
static void lightwood_flipper_disconnect_marker_signals(LightwoodFlipper *pLightwoodFlipper ,
		enFlipperActors  uinActors);
static void lightwood_flipper_update_animatable_objects(LightwoodFlipper *pLightwoodFlipper );
static gboolean b_lightwood_flipper_check_forward_animation_in_progress(LightwoodFlipperPrivate *pPriv);
static gboolean b_lightwood_flipper_check_backward_animation_in_progress(LightwoodFlipperPrivate *pPriv);
static void lightwood_flipper_add_animation_effects(LightwoodFlipper *pLightwoodFlipper ,
		gboolean bForwardFlip );
static gboolean b_lightwood_flipper_reverse_flip_direction(LightwoodFlipper *pLightwoodFlipper );
static void lightwood_flipper_change_backward_animation_direction(LightwoodFlipperPrivate *pPriv
		, gboolean bReset);
static void lightwood_flipper_change_forward_animation_direction(LightwoodFlipperPrivate *pPriv
		,  gboolean bReset);
static void lightwood_flipper_change_animation_direction(LightwoodFlipperPrivate *pPriv ,
		LightwoodFlipperData *pFlipData ,gboolean  bReset );
static void lightwood_flipper_define_transition_for_bkwd_direction(LightwoodFlipper *pLightwoodFlipper );
static void lightwood_flipper_define_transition_for_fwd_direction(LightwoodFlipper *pLightwoodFlipper );

/*  callback functions */
static void lightwood_flipper_row_added_cb (ThornburyModel *model, ThornburyModelIter *iter, gpointer pUserData);
static void lightwood_flipper_row_removed_cb (ThornburyModel *model, ThornburyModelIter *iter, gpointer pUserData);
static void lightwood_flipper_row_changed_cb (ThornburyModel *model, ThornburyModelIter *iter, gpointer pUserData);
static void lightwood_flipper_on_transition_completed (ClutterActor *pactor,
		const gchar  *pTransitionName,
		gboolean      bIsFinished , gpointer      pUserData);


#if PROVIDE_ANIM_GAP
gboolean b_lightwood_flipper_start_second_set_animation(gpointer pLightwoodFlipper);
#endif



/*****************************************************************************************************
 * Internal functions
 *****************************************************************************************************/

/********************************************************
 * Function :p_lightwood_flipper_extract_object_from_iter
 * Description: function to extract the Object pointer from the model .
 * Parameters: The ThornburyModelIter object reference, uinColumn to extract
 * Return value: Object pointer
 ********************************************************/
static gpointer p_lightwood_flipper_extract_object_from_iter(ThornburyModelIter *pModelIter , guint uinColumn)
{
	gpointer pPointer = NULL ;
	GValue value = { 0, };
	thornbury_model_iter_get_value(pModelIter,uinColumn, &value);
	pPointer = g_value_get_object (&value);
	g_value_unset (&value);
	return pPointer ;
}

/********************************************************
 * Function :lightwood_flipper_update_animatable_objects
 * Description: This function extracts the model information , validates condition and connects to signal  .
 * Parameters: The LightwoodFlipper object reference
 * Return value:
 ********************************************************/

static void lightwood_flipper_update_animatable_objects(LightwoodFlipper *pLightwoodFlipper )
{
	LightwoodFlipperPrivate *pPriv = LIGHTWOOD_FLIPPER_GET_PRIVATE (pLightwoodFlipper);
	ThornburyModelIter *pIter = thornbury_model_get_iter_at_row(pPriv->pFlipperModel,ROW_MODEL_DATA);
	if(NULL != pIter)
	{
		ClutterActor *pActor = NULL ;
		/* Extarct from Model row 0 column 0 data */
		pActor =  (ClutterActor*) p_lightwood_flipper_extract_object_from_iter(pIter ,
				LIGHTWOOD_FLIPPER_COLUMN_FIRST_ACTOR);
		if(NULL != pActor && pActor != pPriv->pFirstActor)
		{
			if(NULL != pPriv->pFirstActor && CLUTTER_IS_ACTOR(pPriv->pFirstActor))
			{
				/* if Model is being updated then remove the contents/signal
				 * associated with previous objects*/
				clutter_actor_remove_all_transitions( pPriv->pFirstActor);
				g_signal_handler_disconnect(pPriv->pFirstActor ,pPriv->pFirstActorData->inHandlerId ) ;
				if(-1 != pPriv->pFirstActorData->inMarkerHandlerId  )
				{
					g_signal_handler_disconnect(CLUTTER_TIMELINE(pPriv->pTransRotateA1) ,
							pPriv->pFirstActorData->inMarkerHandlerId  ) ;
					pPriv->pFirstActorData->inMarkerHandlerId = -1 ;
				}
			}
			pPriv->pFirstActor =  pActor ;
			pPriv->pFirstActorData->enAnimateActor = LIGHTWOOD_FLIPPER_FIRST_ACTOR ;

			/* connect to transition-stopped signal */
			pPriv->pFirstActorData->inHandlerId = g_signal_connect ( pPriv->pFirstActor, "transition-stopped",
					G_CALLBACK (lightwood_flipper_on_transition_completed),
					pPriv->pFirstActorData);

			/* set pivot point to 0.5 and 0.5 in order to rotate the object about its center */
			clutter_actor_set_pivot_point ( pActor, pPriv->fltPivotX, pPriv->fltPivotY);
		}
		else if(LIGHTWOOD_ENABLE_SECOND_ACTOR_FLIP_ONLY != pPriv->uinFlipCycle)
		{
			g_warning("Lightwood Flipper: Incorrect Model data for Colunm %d \n",
					LIGHTWOOD_FLIPPER_COLUMN_FIRST_ACTOR);
		}

		/* Extarct from Model row 0 column 1 data */
		pActor = (ClutterActor*) p_lightwood_flipper_extract_object_from_iter(pIter ,
				LIGHTWOOD_FLIPPER_COLUMN_SECOND_ACTOR);
		if(NULL != pActor && pActor != pPriv->pSecondActor)
		{
			if(NULL != pPriv->pSecondActor && CLUTTER_IS_ACTOR(pPriv->pSecondActor))
			{
				/* if Model is being updated then remove the contents/signal
				 * associated with previous objects*/
				clutter_actor_remove_all_transitions( pPriv->pSecondActor);
				g_signal_handler_disconnect(pPriv->pSecondActor ,pPriv->pSecondActorData->inHandlerId ) ;
				if(-1 != pPriv->pSecondActorData->inMarkerHandlerId  )
				{
					g_signal_handler_disconnect(CLUTTER_TIMELINE(pPriv->pTransRotateA2) ,
							pPriv->pSecondActorData->inMarkerHandlerId  ) ;
					pPriv->pSecondActorData->inMarkerHandlerId = -1 ;
				}
			}
			pPriv->pSecondActor = pActor ;
			pPriv->pSecondActorData->enAnimateActor = LIGHTWOOD_FLIPPER_SECOND_ACTOR ;

			/* connect to transition-stopped signal */
			pPriv->pSecondActorData->inHandlerId = g_signal_connect (pPriv->pSecondActor, "transition-stopped",
					G_CALLBACK (lightwood_flipper_on_transition_completed),
					pPriv->pSecondActorData);
			/* set pivot point to 0.5 and 0.5 in order to rotate the object about its center */
			clutter_actor_set_pivot_point ( pActor, pPriv->fltPivotX, pPriv->fltPivotY);
		}
		else if(LIGHTWOOD_ENABLE_FIRST_ACTOR_FLIP_ONLY != pPriv->uinFlipCycle)
		{
			g_warning("Lightwood Flipper: Incorrect Model data for Colunm %d \n",
					LIGHTWOOD_FLIPPER_COLUMN_SECOND_ACTOR);
		}
	}
}


/********************************************************
 * Function :lightwood_flipper_reset_and_swap_flip_objects
 * Description: This function resets transitions and swaps the objects.
 * Parameters: The LightwoodFlipperPrivate pointer
 * Return value:
 ********************************************************/
static void lightwood_flipper_reset_and_swap_flip_objects(LightwoodFlipperPrivate *pPriv)
{
	LIGHTWOOD_FLIPPER_PRINT("%s %d \n", __FUNCTION__ , __LINE__);
	lightwood_flipper_change_forward_animation_direction(pPriv , TRUE );
	lightwood_flipper_change_backward_animation_direction(pPriv , TRUE );
	UPDATE_SMV(pPriv->bReversedAnimation ,FALSE );
	UPDATE_SMV(pPriv->bValuesReset ,TRUE );

}




static inline void lightwood_flipper_anim_second_on_comp_of_first(
		LightwoodFlipperPrivate *pPriv ,LightwoodFlipperSignalData *pSignalUserData)
{
	clutter_actor_hide( pPriv->pFirstActor);
	/* emit the flip-animation-half-mark signal */
	g_signal_emit(LIGHTWOOD_FLIPPER(pSignalUserData->pLightwoodFlipper),
			flipper_signals[SIG_FLIPPER_ANIMATION_HALF_DONE],
			0, CLUTTER_TIMELINE_FORWARD);

#if PROVIDE_ANIM_GAP
	UPDATE_SMV( pPriv->uinAnimateActor ,LIGHTWOOD_FLIPPER_SECOND_ACTOR );
	if (pPriv->uinAnimGapTimeoutId >  0)
		g_source_remove(pPriv->uinAnimGapTimeoutId);

	pPriv->uinAnimGapTimeoutId =  g_timeout_add(LIGHTWOOD_FLIPPER_ANIMATION_TIME_GAP ,
			b_lightwood_flipper_start_second_set_animation , pSignalUserData->pLightwoodFlipper);
#else
	clutter_actor_show(pPriv->pSecondActor);
	lightwood_flipper_add_animation_effects(LIGHTWOOD_FLIPPER(pSignalUserData->pLightwoodFlipper) ,FALSE);
#endif
}


static inline void lightwood_flipper_anim_first_on_comp_of_second(
		LightwoodFlipperPrivate *pPriv ,LightwoodFlipperSignalData *pSignalUserData)
{
	LIGHTWOOD_FLIPPER_PRINT("%s %d \n", __FUNCTION__ , __LINE__);
	clutter_actor_hide(pPriv->pSecondActor );
	clutter_actor_remove_all_transitions( pPriv->pFirstActor);

	clutter_transition_set_from (pPriv->pTransScaleXA1 , G_TYPE_DOUBLE,pPriv->dScalingMinX);
	clutter_transition_set_to (pPriv->pTransScaleXA1, G_TYPE_DOUBLE, pPriv->dScalingMaxX );

	clutter_transition_set_from (pPriv->pTransScaleYA1, G_TYPE_DOUBLE,pPriv->dScalingMinY);
	clutter_transition_set_to (pPriv->pTransScaleYA1, G_TYPE_DOUBLE, pPriv->dScalingMaxY );

	clutter_transition_set_from (pPriv->pTransScaleZA1, G_TYPE_DOUBLE,pPriv->dScalingMinZ);
	clutter_transition_set_to (pPriv->pTransScaleZA1, G_TYPE_DOUBLE, pPriv->dScalingMaxZ );

	clutter_transition_set_from (pPriv->pTransRotateA1, G_TYPE_DOUBLE,pPriv->dFirstActorAngleEnd);
	clutter_transition_set_to (pPriv->pTransRotateA1, G_TYPE_DOUBLE, pPriv->dFirstActorAngleStart);

	/* emit the flip-animation-half-mark signal */
	g_signal_emit(LIGHTWOOD_FLIPPER(pSignalUserData->pLightwoodFlipper),
			flipper_signals[SIG_FLIPPER_ANIMATION_HALF_DONE],
			0, CLUTTER_TIMELINE_BACKWARD);

	UPDATE_SMV(pPriv->bReversedAnimation , TRUE );

#if PROVIDE_ANIM_GAP

	UPDATE_SMV( pPriv->uinAnimateActor ,LIGHTWOOD_FLIPPER_FIRST_ACTOR );
	if (pPriv->uinAnimGapTimeoutId >  0)
		g_source_remove(pPriv->uinAnimGapTimeoutId);
	g_timeout_add(LIGHTWOOD_FLIPPER_ANIMATION_TIME_GAP ,
			b_lightwood_flipper_start_second_set_animation , pSignalUserData->pLightwoodFlipper);
#else
	clutter_actor_show( pPriv->pFirstActor);
	lightwood_flipper_add_animation_effects(LIGHTWOOD_FLIPPER(pSignalUserData->pLightwoodFlipper) , TRUE);
#endif
}



static inline void lightwood_flipper_entire_flip_animation_completed(ClutterActor *pCompletedOnActor ,
		LightwoodFlipperPrivate *pPriv ,LightwoodFlipperSignalData *pSignalUserData)
{
	LIGHTWOOD_FLIPPER_PRINT("  %s %d  \n", __FUNCTION__ , __LINE__);
	lightwood_flipper_reset_and_swap_flip_objects(pPriv);
	/* emit the flip-animation-completed signal */
	g_signal_emit(LIGHTWOOD_FLIPPER(pSignalUserData->pLightwoodFlipper),
			flipper_signals[SIG_FLIPPER_ANIMATION_COMPLETED], 0, pCompletedOnActor);

	UPDATE_SMV(pPriv->fltDiscreteFactor ,0.0 );
	UPDATE_SMV (pPriv->uinAnimControl ,FLIPPER_ANIM_CONTROL_LAST );

	/* Disconnect marker signals if any */
	lightwood_flipper_disconnect_marker_signals(pSignalUserData->pLightwoodFlipper ,
			LIGHTWOOD_FLIPPER_BOTH_ACTOR);

}

/********************************************************
 * Function :lightwood_flipper_on_first_actor_transition_complete
 * Description: This function will handle the logic to animate the second actor on completion of first.
 * Parameters: The reference to  LightwoodFlipperSignalData
 * Return value:
 ********************************************************/
static void
lightwood_flipper_on_first_actor_transition_complete(LightwoodFlipperSignalData *pSignalUserData)
{
	LightwoodFlipperPrivate *pPriv = LIGHTWOOD_FLIPPER_GET_PRIVATE (pSignalUserData->pLightwoodFlipper);

	if(LIGHTWOOD_ENABLE_FIRST_ACTOR_FLIP_ONLY == pPriv->uinFlipCycle  )
	{
		LIGHTWOOD_FLIPPER_PRINT("Reversed Flip Direction   %s   \n " ,FALSE == pPriv->bReversedAnimation ? "FALSE" : "TRUE" );
		if(FALSE == pPriv->bReversedAnimation  && FALSE == pPriv->bValuesReset &&
				pPriv->uinAnimControl != DISABLE_TRANSTION_ON_SECOND_ACTOR )
		{
			/* Flip Animation on first actor completed (Half mark reached)  */
			lightwood_flipper_anim_second_on_comp_of_first(pPriv , pSignalUserData);
			UPDATE_SMV (pPriv->uinAnimControl ,FLIPPER_ANIM_CONTROL_LAST );
		}
		else
		{
			/* Check if the  flip-cycle is finished before the signal is received
			 * and avoid re-emitting the LIGHTWOOD_FLIP_COMPLETED signal
			 * */
			if(FALSE == pPriv->bValuesReset)
			{
				lightwood_flipper_entire_flip_animation_completed(pPriv->pFirstActor ,pPriv , pSignalUserData);
				/* restore the deformed second  actor to normal state except visibility*/
				lightwood_flipper_restore_actor_to_normal_state(pSignalUserData->pLightwoodFlipper ,
						LIGHTWOOD_FLIPPER_SECOND_ACTOR);
			}
		}
	}
	else if(LIGHTWOOD_ENABLE_FIRST_ACTOR_FLIP_ONLY == pPriv->uinFlipCycle  )
	{
		/* Reset the Animation Values to Defaults */
		lightwood_flipper_change_forward_animation_direction(pPriv , TRUE );
		/* emit the flip-animation-completed signal */
		g_signal_emit(LIGHTWOOD_FLIPPER(pSignalUserData->pLightwoodFlipper),
				flipper_signals[SIG_FLIPPER_ANIMATION_COMPLETED], 0, pPriv->pFirstActor);
		UPDATE_SMV(pPriv->bReversedAnimation , FALSE );
		UPDATE_SMV(pPriv->fltDiscreteFactor ,0.0 );

		/* Disconnect marker signals if any */
		lightwood_flipper_disconnect_marker_signals(pSignalUserData->pLightwoodFlipper ,
				LIGHTWOOD_FLIPPER_FIRST_ACTOR);

		/* Explicitly  hide the rotated actor and restore it to normal state */
		clutter_actor_hide( pPriv->pFirstActor);

		/* restore the deformed first actor to normal state except visibility*/
		lightwood_flipper_restore_actor_to_normal_state(pSignalUserData->pLightwoodFlipper ,
				LIGHTWOOD_FLIPPER_FIRST_ACTOR);

	}
}
/********************************************************
 * Function :lightwood_flipper_on_second_actor_transition_complete
 * Description: This function will handle the logic to animate the first actor on completion of second.
 * Parameters: The reference to  LightwoodFlipperSignalData
 * Return value:
 ********************************************************/

static void
lightwood_flipper_on_second_actor_transition_complete(LightwoodFlipperSignalData *pSignalUserData)
{
	LightwoodFlipperPrivate *pPriv = LIGHTWOOD_FLIPPER_GET_PRIVATE (pSignalUserData->pLightwoodFlipper);
	LIGHTWOOD_FLIPPER_PRINT("%s %d  bReversedAnimation = %s  \n", __FUNCTION__ , __LINE__ ,
			TRUE ==  pPriv->bReversedAnimation ? "TRUE" : "FALSE");

	if(LIGHTWOOD_ENABLE_FIRST_ACTOR_FLIP_ONLY == pPriv->uinFlipCycle  )
	{
		if((TRUE == pPriv->bReversedAnimation && FALSE == pPriv->bValuesReset )||
				pPriv->uinAnimControl == ENABLE_TRANSTION_ON_FIRST_ACTOR )
		{
			lightwood_flipper_anim_first_on_comp_of_second(pPriv , pSignalUserData);
			UPDATE_SMV (pPriv->uinAnimControl ,FLIPPER_ANIM_CONTROL_LAST );
		}
		else
		{
			/* Check if the  flip-cycle is finished before the signal is received
			 * and avoid re-emitting the LIGHTWOOD_FLIP_COMPLETED signal
			 * */
			if(FALSE == pPriv->bValuesReset)
			{
				lightwood_flipper_entire_flip_animation_completed(pPriv->pFirstActor ,pPriv , pSignalUserData);
				/* restore the deformed first actor to normal state except visibility*/
				lightwood_flipper_restore_actor_to_normal_state(pSignalUserData->pLightwoodFlipper ,
						LIGHTWOOD_FLIPPER_FIRST_ACTOR);
			}
		}
	}
	else if(LIGHTWOOD_ENABLE_SECOND_ACTOR_FLIP_ONLY == pPriv->uinFlipCycle  )
	{
		/* Reset the Animation Values to Defaults */
		lightwood_flipper_change_backward_animation_direction(pPriv , TRUE );

		/* emit the flip-animation-completed signal */
		g_signal_emit(LIGHTWOOD_FLIPPER(pSignalUserData->pLightwoodFlipper),
				flipper_signals[SIG_FLIPPER_ANIMATION_COMPLETED], 0, pPriv->pSecondActor);
		UPDATE_SMV(pPriv->bReversedAnimation , FALSE );
		UPDATE_SMV(pPriv->fltDiscreteFactor ,0.0 );

		/* Disconnect marker signals if any */
		lightwood_flipper_disconnect_marker_signals(pSignalUserData->pLightwoodFlipper ,
				LIGHTWOOD_FLIPPER_SECOND_ACTOR);

		/* Explicitly  hide or restoration should not be done for second actor */
	}
}


/********************************************************
 * Function :lightwood_flipper_on_marker_reached
 * Description: This callback function connected to signal "marker-reached".
 * Parameters: The ClutterTimeline pointer , Marker Name , guint uinElapsedTime , User data
 * Return value:
 ********************************************************/
static void lightwood_flipper_on_marker_reached(ClutterTimeline *pTimeline,
		gchar           *pMarkerName,
		gint             uinElapsedTime,
		gpointer         pUserData)
{
	LightwoodFlipperSignalData *pSignalUserData  = (LightwoodFlipperSignalData*)pUserData;
	LIGHTWOOD_FLIPPER_PRINT("%s %d \n", __FUNCTION__ , __LINE__);
	if(NULL != pSignalUserData)
	{
		LightwoodFlipperPrivate *pPriv = LIGHTWOOD_FLIPPER_GET_PRIVATE (pSignalUserData->pLightwoodFlipper);
		ClutterTimelineDirection enAnimationDirection;
		VALIDATE_LIGHTWOOD_FLIPPER_OBJECT(pSignalUserData->pLightwoodFlipper);

		enAnimationDirection = (pPriv->bReversedAnimation == FALSE ?
				CLUTTER_TIMELINE_BACKWARD : CLUTTER_TIMELINE_FORWARD);

		if(LIGHTWOOD_FLIPPER_FIRST_ACTOR ==   pSignalUserData->enAnimateActor)
		{
			LIGHTWOOD_FLIPPER_PRINT("%s %d \n", __FUNCTION__ , __LINE__);
			g_signal_emit(LIGHTWOOD_FLIPPER(pSignalUserData->pLightwoodFlipper),
					flipper_signals[SIG_FLIPPER_ANIMATION_MARK_REACHED],
					0, pPriv->pFirstActor , enAnimationDirection);
		}
		else
		{
			LIGHTWOOD_FLIPPER_PRINT("%s %d \n", __FUNCTION__ , __LINE__);
			g_signal_emit(LIGHTWOOD_FLIPPER(pSignalUserData->pLightwoodFlipper),
					flipper_signals[SIG_FLIPPER_ANIMATION_MARK_REACHED],
					0, pPriv->pSecondActor , enAnimationDirection);
		}
	}
}

#if PROVIDE_ANIM_GAP

gboolean b_lightwood_flipper_start_second_set_animation(gpointer pLightwoodFlipper)
{
	LightwoodFlipperPrivate *pPriv = LIGHTWOOD_FLIPPER_GET_PRIVATE (pLightwoodFlipper);
	if(LIGHTWOOD_FLIPPER_SECOND_ACTOR == pPriv->uinAnimateActor)
	{
		clutter_actor_show(pPriv->pSecondActor);
		lightwood_flipper_add_animation_effects(LIGHTWOOD_FLIPPER(pLightwoodFlipper) ,FALSE);
	}
	else if(LIGHTWOOD_FLIPPER_FIRST_ACTOR == pPriv->uinAnimateActor)
	{
		clutter_actor_show(pPriv->pFirstActor);
		lightwood_flipper_add_animation_effects(LIGHTWOOD_FLIPPER(pLightwoodFlipper) ,TRUE);
	}

	return FALSE ;
}
#endif


/********************************************************
 * Function :lightwood_flipper_on_transition_completed
 * Description: This callback function connected to signal "transition-stoped".
 * 				This function will handle the logic to animate the actor on completion of other.
 * Parameters: The ClutterActor pointer , Transition Name , boolean bIsFinished , User data
 * Return value:
 ********************************************************/
static void lightwood_flipper_on_transition_completed (ClutterActor *pactor,
		const gchar  *pTransitionName,
		gboolean      bIsFinished , gpointer      pUserData)
{
	LightwoodFlipperSignalData *pSignalUserData  = (LightwoodFlipperSignalData*)pUserData;
	LIGHTWOOD_FLIPPER_PRINT("%s %d \n", __FUNCTION__ , __LINE__);
	if(NULL != pSignalUserData)
	{
		LightwoodFlipperPrivate *pPriv = LIGHTWOOD_FLIPPER_GET_PRIVATE (pSignalUserData->pLightwoodFlipper);
		VALIDATE_LIGHTWOOD_FLIPPER_OBJECT(pSignalUserData->pLightwoodFlipper);

		if(LIGHTWOOD_FLIPPER_FIRST_ACTOR ==   pSignalUserData->enAnimateActor)
		{

			if( FALSE == b_lightwood_flipper_check_forward_animation_in_progress(pPriv)
					&& FALSE == b_lightwood_flipper_check_backward_animation_in_progress(pPriv) )
			{
				LIGHTWOOD_FLIPPER_PRINT("%s %d \n", __FUNCTION__ , __LINE__);
				LIGHTWOOD_FLIPPER_PRINT("ENTRY   LIGHTWOOD_FLIPPER_FIRST_ACTOR   : Visible %s \n", \
						((TRUE == CLUTTER_ACTOR_IS_VISIBLE(pactor))? "TRUE" :"FALSE"));
				lightwood_flipper_on_first_actor_transition_complete(pSignalUserData);
			}
		}
		else
		{
			if( FALSE == b_lightwood_flipper_check_backward_animation_in_progress(pPriv) &&
					FALSE == b_lightwood_flipper_check_backward_animation_in_progress(pPriv))
			{
				LIGHTWOOD_FLIPPER_PRINT("%s %d \n", __FUNCTION__ , __LINE__);
				LIGHTWOOD_FLIPPER_PRINT("ENTRY   LIGHTWOOD_FLIPPER_SECOND_ACTOR  : Visible %s \n",
						((TRUE == CLUTTER_ACTOR_IS_VISIBLE(pactor))? "TRUE" :"FALSE"));
				lightwood_flipper_on_second_actor_transition_complete(pSignalUserData);
			}
		}
	}
}


/********************************************************
 * Function :lightwood_flipper_fetch_actors_from_model
 * Description: This function will validate model length and updates the object reference.
 * Parameters: The reference to  LightwoodFlipper
 * Return value:
 ********************************************************/
static void lightwood_flipper_fetch_actors_from_model(LightwoodFlipper *pLightwoodFlipper)
{
	LightwoodFlipperPrivate *pPriv = LIGHTWOOD_FLIPPER_GET_PRIVATE (pLightwoodFlipper);
	VALIDATE_LIGHTWOOD_FLIPPER_OBJECT(pLightwoodFlipper);
	LIGHTWOOD_FLIPPER_PRINT("%s %d ",__FUNCTION__ , __LINE__);
	if(MODEL_LENGTH_ALLOWED == thornbury_model_get_n_rows (pPriv->pFlipperModel))
	{
		lightwood_flipper_update_animatable_objects(pLightwoodFlipper);
	}
	else
	{
		g_warning("Lightwood Flipper: Incorrect Model data : Model should have only one row \n");
	}
}


/********************************************************
 * Function :lightwood_flipper_row_added_cb
 * Description: This call function will handle the logic related to model row added.
 * Parameters: The reference to  ThornburyModel , ThornburyModelIter , user data
 * Return value:
 ********************************************************/
static void lightwood_flipper_row_added_cb (ThornburyModel *model,
		ThornburyModelIter *iter, gpointer pUserData)
{
	LIGHTWOOD_FLIPPER_PRINT("%s %d ",__FUNCTION__ , __LINE__);
	lightwood_flipper_fetch_actors_from_model((LightwoodFlipper *)pUserData);
}

/********************************************************
 * Function :lightwood_flipper_row_removed_cb
 * Description: This call function will handle the logic related to model row removed.
 * Parameters: The reference to  ThornburyModel , ThornburyModelIter , user data
 * Return value:
 ********************************************************/
static void lightwood_flipper_row_removed_cb (ThornburyModel *model,
		ThornburyModelIter *iter, gpointer pLightwoodFlipper)
{
	LightwoodFlipperPrivate *pPriv = LIGHTWOOD_FLIPPER_GET_PRIVATE (pLightwoodFlipper);
	LIGHTWOOD_FLIPPER_PRINT("%s %d ",__FUNCTION__ , __LINE__);
	VALIDATE_LIGHTWOOD_FLIPPER_OBJECT(pLightwoodFlipper);
	LIGHTWOOD_FLIPPER_PRINT("%s %d Model Length = %d  ",
			__FUNCTION__ , __LINE__ , thornbury_model_get_n_rows (pPriv->pFlipperModel));
	if(MODEL_LENGTH_ALLOWED == thornbury_model_get_n_rows (pPriv->pFlipperModel))
	{
		/* Remove the reference to the animatable objects */
		if(NULL != pPriv->pFirstActor)
		{
			/*  remove the contents/signal associated with first actor */
			clutter_actor_remove_all_transitions( pPriv->pFirstActor);
			g_signal_handler_disconnect(pPriv->pFirstActor ,pPriv->pFirstActorData->inHandlerId ) ;

			/* Disconnect marker signals if any */
			lightwood_flipper_disconnect_marker_signals( pLightwoodFlipper ,
					LIGHTWOOD_FLIPPER_FIRST_ACTOR);

			UPDATE_SMV(pPriv->pFirstActorData->bSetMarker , FALSE );
		}
		if(NULL != pPriv->pSecondActor)
		{
			/*  remove the contents/signal associated with second actor */
			clutter_actor_remove_all_transitions( pPriv->pSecondActor);
			g_signal_handler_disconnect(pPriv->pSecondActor ,pPriv->pSecondActorData->inHandlerId ) ;

			/* Disconnect marker signals if any */
			lightwood_flipper_disconnect_marker_signals( pLightwoodFlipper ,
					LIGHTWOOD_FLIPPER_SECOND_ACTOR);
			UPDATE_SMV(pPriv->pSecondActorData->bSetMarker , FALSE );
		}
		UPDATE_SMV(pPriv->fltDiscreteFactor ,0.0 );
		lightwood_flipper_restore_actor_to_normal_state(LIGHTWOOD_FLIPPER(pLightwoodFlipper) , LIGHTWOOD_FLIPPER_BOTH_ACTOR);
		pPriv->pFirstActor = NULL ;
		pPriv->pSecondActor = NULL ;

		/* Reset the state Variables to the Default states when model is empty */
		UPDATE_SMV(pPriv->bReversedAnimation , FALSE );
		UPDATE_SMV(pPriv->bValuesReset , FALSE );
	}
}



/********************************************************
 * Function :lightwood_flipper_row_changed_cb
 * Description: This call function will handle the logic related to model row changed.
 * Parameters: The reference to  ThornburyModel , ThornburyModelIter , user data
 * Return value:
 ********************************************************/
static void lightwood_flipper_row_changed_cb (ThornburyModel *model,
		ThornburyModelIter *iter, gpointer pLightwoodFlipper)
{
	LIGHTWOOD_FLIPPER_PRINT("%s %d ",__FUNCTION__ , __LINE__);
	lightwood_flipper_fetch_actors_from_model((LightwoodFlipper *)pLightwoodFlipper);
}

/**
 * lightwood_flipper_get_actor_being_animated:
 * @self: a #LightwoodFlipper
 *
 * Return the #LightwoodFlipper:actor-being-animated property
 *
 * Returns: (transfer none): the value of #LightwoodFlipper:actor-being-animated
 */
ClutterActor *
lightwood_flipper_get_actor_being_animated (LightwoodFlipper *self)
{
	ClutterActor *pAnimatedActor = NULL ;
	LightwoodFlipperPrivate *pPriv = LIGHTWOOD_FLIPPER_GET_PRIVATE (self);

	g_return_val_if_fail (LIGHTWOOD_IS_FLIPPER (self), NULL);
	LIGHTWOOD_FLIPPER_PRINT("%s %d ",__FUNCTION__ , __LINE__);

	/* validate model and objects to flip */
	if(!G_IS_OBJECT( pPriv->pFlipperModel) ||
			(MODEL_LENGTH_ALLOWED != thornbury_model_get_n_rows(pPriv->pFlipperModel) ))
	{
		/* check for the ongoing transitions */
		if(TRUE == b_lightwood_flipper_check_forward_animation_in_progress(pPriv))
		{
			pAnimatedActor = pPriv->pFirstActor ;
		}
		else if(TRUE == b_lightwood_flipper_check_backward_animation_in_progress(pPriv))
		{
			pAnimatedActor = pPriv->pSecondActor ;
		}
	}
	return pAnimatedActor;
}

static gboolean
get_animation_in_progress (LightwoodFlipper *self)
{
  if (b_lightwood_flipper_check_forward_animation_in_progress (self->priv) ||
      b_lightwood_flipper_check_backward_animation_in_progress (self->priv))
    return TRUE;

  return FALSE;
}

/********************************************************
 * Function : lightwood_flipper_get_property
 * Description: get a property value
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/
static void
lightwood_flipper_get_property (GObject    *pObject,
		guint       uinPropertyId,
		GValue     *pValue,
		GParamSpec *pSpec)
{
	LightwoodFlipper *pLightwoodFlipper = LIGHTWOOD_FLIPPER(pObject);
	LightwoodFlipperPrivate *pPriv = LIGHTWOOD_FLIPPER_GET_PRIVATE (pLightwoodFlipper);
	VALIDATE_LIGHTWOOD_FLIPPER_OBJECT(pObject);
	switch (uinPropertyId)
	{
	case PROP_FLIPPER_SCALE_X_MIN :
	{
		g_value_set_double(pValue, pPriv->dScalingMinX);
	}
	break;
	case  PROP_FLIPPER_SCALE_X_MAX:
	{
		g_value_set_double(pValue, pPriv->dScalingMaxX);
	}
	break;
	case  PROP_FLIPPER_SCALE_Y_MIN:
	{
		g_value_set_double(pValue, pPriv->dScalingMinY);
	}
	break;
	case PROP_FLIPPER_SCALE_Y_MAX :
	{
		g_value_set_double(pValue, pPriv->dScalingMaxY);
	}
	break;
	case PROP_FLIPPER_SCALE_Z_MIN :
	{
		g_value_set_double(pValue, pPriv->dScalingMinZ);
	}
	break;
	case  PROP_FLIPPER_SCALE_Z_MAX:
	{
		g_value_set_double(pValue, pPriv->dScalingMaxZ);
	}
	break;
	case  PROP_FLIPPER_FORWARD_FLIP_START_ANGLE:
	{
		g_value_set_double(pValue, pPriv->dFirstActorAngleStart);
	}
	break;
	case  PROP_FLIPPER_FORWARD_FLIP_END_ANGLE:
	{
		g_value_set_double(pValue, pPriv->dFirstActorAngleEnd);
	}
	break;
	case PROP_FLIPPER_BACKWARD_FLIP_START_ANGLE :
	{
		g_value_set_double(pValue, pPriv->dSecondActorAngleStart);
	}
	break;
	case  PROP_FLIPPER_BACKWARD_FLIP_END_ANGLE:
	{
		g_value_set_double(pValue, pPriv->dSecondActorAngleEnd);
	}
	break;
	case  PROP_FLIPPER_TRANSITION_TIME:
	{
		g_value_set_uint(pValue, pPriv->uinFlipDuration);
	}
	break;
	case PROP_FLIPPER_FIRST_ACTOR_MARK:
	{
		g_value_set_uint(pValue, pPriv->pFirstActorData->uinMarkerTime);
	}
	break;
	case PROP_FLIPPER_SECOND_ACTOR_MARK :
	{
		g_value_set_uint(pValue, pPriv->pSecondActorData->uinMarkerTime);
	}
	break;
	case PROP_FLIPPER_TRANSITION_DIRECTION :
	{
		g_value_set_enum (pValue, pPriv->uinFlipDirection);
	}
	break;
        case PROP_FLIPPER_ANIMATION_MODE:
	{
		g_value_set_uint(pValue, pPriv->uinFlipAnimType);
	}
	break;
	case PROP_FLIPPER_MODEL :
	{
		g_value_set_object (pValue, pPriv->pFlipperModel );

	}
	break;
	case PROP_FLIPPER_ANIMATION_IN_PROGRESS :
	{
		g_value_set_boolean (pValue, get_animation_in_progress (pLightwoodFlipper));
	}
	break ;
	case PROP_FLIPPER_ACTOR_BEING_ANIMATED :
	{
		g_value_set_object (pValue, lightwood_flipper_get_actor_being_animated (pLightwoodFlipper));
	}
	break ;
	case PROP_FLIPPER_FLIP_DIRECTION :
	{
		g_value_set_enum (pValue, pPriv->uinFlipType);
	}
	break ;
	case PROP_FLIPPER_FLIP_CYCLE:
	{
		g_value_set_enum (pValue, pPriv->uinFlipCycle);
	}
	break;
	case PROP_FLIPPER_X_PIVOT_POINT:
	{
		g_value_set_float(pValue, pPriv->fltPivotX);
	}
	break;
	case PROP_FLIPPER_Y_PIVOT_POINT:
	{
		g_value_set_float(pValue, pPriv->fltPivotY);
	}
	break;
        case PROP_FLIPPER_DISCRETE_FACTOR:
	{
		g_value_set_float(pValue , pPriv->fltDiscreteFactor);
	}
	break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyId, pSpec);
		break;
	}
}

static void
set_scale_x_min (LightwoodFlipper *self,
                 gdouble v)
{
  self->priv->dScalingMinX = v;
  clutter_transition_set_to (self->priv->pTransScaleXA1, G_TYPE_DOUBLE, self->priv->dScalingMinX);
  clutter_transition_set_from (self->priv->pTransScaleXA2, G_TYPE_DOUBLE, self->priv->dScalingMinX);
}

static void
set_scale_x_max (LightwoodFlipper *self,
                 gdouble v)
{
  self->priv->dScalingMaxX = v;
  clutter_transition_set_from (self->priv->pTransScaleXA1, G_TYPE_DOUBLE, self->priv->dScalingMaxX);
  clutter_transition_set_to (self->priv->pTransScaleXA2, G_TYPE_DOUBLE, self->priv->dScalingMaxX);
}

static void
set_scale_y_min (LightwoodFlipper *self,
                 gdouble v)
{
  self->priv->dScalingMinY = v;
  clutter_transition_set_to (self->priv->pTransScaleYA1, G_TYPE_DOUBLE, self->priv->dScalingMinY);
  clutter_transition_set_from (self->priv->pTransScaleYA2, G_TYPE_DOUBLE, self->priv->dScalingMinY);
}

static void
set_scale_y_max (LightwoodFlipper *self,
                 gdouble v)
{
  self->priv->dScalingMaxY = v;
  clutter_transition_set_from (self->priv->pTransScaleYA1, G_TYPE_DOUBLE, self->priv->dScalingMaxY);
  clutter_transition_set_to (self->priv->pTransScaleYA2, G_TYPE_DOUBLE, self->priv->dScalingMaxY);
}

static void
set_scale_z_min (LightwoodFlipper *self,
                 gdouble v)
{
  self->priv->dScalingMinZ = v;
  clutter_transition_set_to (self->priv->pTransScaleZA1, G_TYPE_DOUBLE, self->priv->dScalingMinZ);
  clutter_transition_set_from (self->priv->pTransScaleZA2, G_TYPE_DOUBLE, self->priv->dScalingMinZ);
}

static void
set_scale_z_max (LightwoodFlipper *self,
                 gdouble v)
{
  self->priv->dScalingMaxZ = v;
  clutter_transition_set_from (self->priv->pTransScaleZA1, G_TYPE_DOUBLE, self->priv->dScalingMaxZ);
  clutter_transition_set_to (self->priv->pTransScaleZA2, G_TYPE_DOUBLE, self->priv->dScalingMaxZ);
}

static void
set_forward_flip_start_angle (LightwoodFlipper *self,
                               gdouble v)
{
  self->priv->dFirstActorAngleStart = v;
  clutter_transition_set_from (self->priv->pTransRotateA1, G_TYPE_DOUBLE, self->priv->dFirstActorAngleStart);
}

static void
set_forward_flip_end_angle (LightwoodFlipper *self,
                            gdouble v)
{
  self->priv->dFirstActorAngleEnd = v;
  clutter_transition_set_to (self->priv->pTransRotateA1, G_TYPE_DOUBLE, self->priv->dFirstActorAngleEnd);
}

static void
set_backward_flip_start_angle (LightwoodFlipper *self,
                               gdouble v)
{
  self->priv->dSecondActorAngleStart = v;
  clutter_transition_set_from (self->priv->pTransRotateA2, G_TYPE_DOUBLE, self->priv->dSecondActorAngleStart);
}

static void
set_backward_flip_end_angle (LightwoodFlipper *self,
                             gdouble v)
{
  self->priv->dSecondActorAngleEnd = v;
  clutter_transition_set_to (self->priv->pTransRotateA2, G_TYPE_DOUBLE, self->priv->dSecondActorAngleEnd);
}

static void
set_first_actor_mark (LightwoodFlipper *self,
                      guint uinMarkerTime)
{
  if (uinMarkerTime == 0)
    {
      lightwood_flipper_disconnect_marker_signals (self, LIGHTWOOD_FLIPPER_FIRST_ACTOR);
      self->priv->pFirstActorData->inMarkerHandlerId = -1;
      UPDATE_SMV (self->priv->pFirstActorData->bSetMarker, FALSE);
    }
  else
    {
      self->priv->pFirstActorData->uinMarkerTime = uinMarkerTime;
      UPDATE_SMV (self->priv->pFirstActorData->bSetMarker, TRUE);
    }
}

static void
set_second_actor_mark (LightwoodFlipper *self,
                       guint uinMarkerTime)
{
  if (uinMarkerTime == 0)
    {
      lightwood_flipper_disconnect_marker_signals (self, LIGHTWOOD_FLIPPER_SECOND_ACTOR);
      self->priv->pSecondActorData->inMarkerHandlerId = -1 ;
      UPDATE_SMV (self->priv->pSecondActorData->bSetMarker , FALSE);
    }
  else
    {
      self->priv->pSecondActorData->uinMarkerTime = uinMarkerTime ;
      UPDATE_SMV (self->priv->pSecondActorData->bSetMarker, TRUE);
    }
}

static void
pivot_updated (LightwoodFlipper *self)
{
  if (self->priv->pFirstActor != NULL)
    clutter_actor_set_pivot_point (self->priv->pFirstActor, self->priv->fltPivotX, self->priv->fltPivotY);

  if (self->priv->pSecondActor != NULL)
    clutter_actor_set_pivot_point (self->priv->pSecondActor, self->priv->fltPivotX, self->priv->fltPivotY);
}

static void
set_x_pivot_point (LightwoodFlipper *self,
                   gfloat v)
{
  self->priv->fltPivotX = v;

  pivot_updated (self);
}

static void
set_y_pivot_point (LightwoodFlipper *self,
                   gfloat v)
{
  self->priv->fltPivotY = v;

  pivot_updated (self);
}

/********************************************************
 * Function : lightwood_flipper_set_property_when_no_animation
 * Description: set a property value when no animation is there
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/
static void
lightwood_flipper_set_property_when_no_animation (GObject      *pObject ,
		guint uinPropertyId ,
		const GValue *pValue , GParamSpec   *pSpec)
{
	LightwoodFlipper *pLightwoodFlipper = LIGHTWOOD_FLIPPER(pObject);
	LightwoodFlipperPrivate *pPriv = LIGHTWOOD_FLIPPER_GET_PRIVATE (pLightwoodFlipper);

	if(TRUE == b_lightwood_flipper_check_forward_animation_in_progress(pPriv) ||
			TRUE == b_lightwood_flipper_check_backward_animation_in_progress(pPriv))
	{
		g_warning(" Animation in progress: setting \" %s \"  property ignored \n" , pSpec->name );
	}

	switch (uinPropertyId)
	{
	case PROP_FLIPPER_SCALE_X_MIN :
	{
		set_scale_x_min (pLightwoodFlipper, g_value_get_double (pValue));
	}
	break;
	case  PROP_FLIPPER_SCALE_X_MAX:
	{
		set_scale_x_max (pLightwoodFlipper, g_value_get_double (pValue));
	}
	break;
	case  PROP_FLIPPER_SCALE_Y_MIN:
	{
		set_scale_y_min (pLightwoodFlipper, g_value_get_double (pValue));
	}
	break;
	case PROP_FLIPPER_SCALE_Y_MAX :
	{
		set_scale_y_max (pLightwoodFlipper, g_value_get_double (pValue));
	}
	break;
	case PROP_FLIPPER_SCALE_Z_MIN :
	{
		set_scale_z_min (pLightwoodFlipper, g_value_get_double (pValue));
	}
	break;
	case  PROP_FLIPPER_SCALE_Z_MAX:
	{
		set_scale_z_max (pLightwoodFlipper, g_value_get_double (pValue));
	}
	break;
	case  PROP_FLIPPER_FORWARD_FLIP_START_ANGLE:
	{
		set_forward_flip_start_angle (pLightwoodFlipper, g_value_get_double (pValue));
	}
	break;
	case  PROP_FLIPPER_FORWARD_FLIP_END_ANGLE:
	{
		set_forward_flip_end_angle (pLightwoodFlipper, g_value_get_double (pValue));
	}
	break;
	case PROP_FLIPPER_BACKWARD_FLIP_START_ANGLE :
	{
		set_backward_flip_start_angle (pLightwoodFlipper, g_value_get_double (pValue));
	}
	break;
	case  PROP_FLIPPER_BACKWARD_FLIP_END_ANGLE:
	{
		set_backward_flip_end_angle (pLightwoodFlipper, g_value_get_double (pValue));
	}
	break;
	case PROP_FLIPPER_X_PIVOT_POINT:
	{
		set_x_pivot_point (pLightwoodFlipper, g_value_get_float (pValue));
	}
	break;
	case PROP_FLIPPER_Y_PIVOT_POINT:
	{
		set_y_pivot_point (pLightwoodFlipper, g_value_get_float (pValue));
	}
	break;
	case PROP_FLIPPER_FIRST_ACTOR_MARK:
	{
		set_first_actor_mark (pLightwoodFlipper, g_value_get_uint (pValue));
	}
	break;
	case PROP_FLIPPER_SECOND_ACTOR_MARK:
	{
		set_second_actor_mark (pLightwoodFlipper, g_value_get_uint (pValue));
	}
	break;
        case PROP_FLIPPER_DISCRETE_FACTOR:
	{
		lightwood_flipper_set_discrete_factor(pLightwoodFlipper ,g_value_get_float(pValue));
	}
	break;
	default :
		G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyId, pSpec);
		break;
	}
}



/********************************************************
 * Function : lightwood_flipper_set_property
 * Description: set a property value
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/
static void
lightwood_flipper_set_property (GObject      *pObject,
		guint         uinPropertyId,
		const GValue *pValue,
		GParamSpec   *pSpec)
{
	LightwoodFlipper *pLightwoodFlipper = LIGHTWOOD_FLIPPER(pObject);
	VALIDATE_LIGHTWOOD_FLIPPER_OBJECT(pObject);
	switch (uinPropertyId)
	{
	case  PROP_FLIPPER_TRANSITION_TIME:
	{
		lightwood_flipper_set_transition_time(pLightwoodFlipper ,g_value_get_uint(pValue));
	}
	break;
	case PROP_FLIPPER_TRANSITION_DIRECTION :
	{
		lightwood_flipper_set_transition_direction (pLightwoodFlipper, g_value_get_enum (pValue));
	}
	break;
	case PROP_FLIPPER_ANIMATION_MODE:
	{
		lightwood_flipper_set_animation_mode (pLightwoodFlipper, g_value_get_uint (pValue));
	}
	break;
	case PROP_FLIPPER_MODEL:
	{
		lightwood_flipper_set_model(pLightwoodFlipper, g_value_get_object (pValue));
	}
	break ;
	case PROP_FLIPPER_FLIP_DIRECTION :
	{
		lightwood_flipper_set_flip_direction (pLightwoodFlipper, g_value_get_enum (pValue) );
	}
	break ;
	case PROP_FLIPPER_FLIP_CYCLE :
	{
		lightwood_flipper_set_flip_cycle(pLightwoodFlipper , g_value_get_enum (pValue));
	}
	break;
	default:
		/* set the properties only when animation is not in progress */
		lightwood_flipper_set_property_when_no_animation(pObject ,
				uinPropertyId , pValue , pSpec );
		break;
	}
}

static void
lightwood_flipper_dispose (GObject *pObject)
{
	LIGHTWOOD_FLIPPER_PRINT("%s %d \n" , __FUNCTION__ , __LINE__);
	G_OBJECT_CLASS (lightwood_flipper_parent_class)->dispose (pObject);
}

/********************************************************
 * Function : lightwood_flipper_finalize
 * Description: Finalise the LightwoodFlipper object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/

static void lightwood_flipper_finalize (GObject *pObject)
{
	LightwoodFlipperPrivate *pPriv = LIGHTWOOD_FLIPPER_GET_PRIVATE (pObject);
	LIGHTWOOD_FLIPPER_PRINT("%s %d \n" , __FUNCTION__ , __LINE__);
	VALIDATE_LIGHTWOOD_FLIPPER_OBJECT(pObject);
	if(NULL != pPriv)
	{
		if(NULL != pPriv->pFirstActor)
		{
			/*  remove the contents/signal associated with first actor */
			clutter_actor_remove_all_transitions( pPriv->pFirstActor);
			g_signal_handler_disconnect(pPriv->pFirstActor ,pPriv->pFirstActorData->inHandlerId ) ;
		}
		if(NULL != pPriv->pSecondActor)
		{
			/*  remove the contents/signal associated with second actor */
			clutter_actor_remove_all_transitions( pPriv->pSecondActor);
			g_signal_handler_disconnect(pPriv->pSecondActor ,pPriv->pSecondActorData->inHandlerId ) ;
		}
		LIGHTWOOD_FLIPPER_UNREF_TRANISTION(pPriv->pTransRotateA1);
		LIGHTWOOD_FLIPPER_UNREF_TRANISTION(pPriv->pTransRotateA2);
		LIGHTWOOD_FLIPPER_UNREF_TRANISTION(pPriv->pTransScaleXA1);
		LIGHTWOOD_FLIPPER_UNREF_TRANISTION(pPriv->pTransScaleXA2);
		LIGHTWOOD_FLIPPER_UNREF_TRANISTION(pPriv->pTransScaleYA1);
		LIGHTWOOD_FLIPPER_UNREF_TRANISTION(pPriv->pTransScaleYA2);
		LIGHTWOOD_FLIPPER_UNREF_TRANISTION(pPriv->pTransScaleZA1);
		LIGHTWOOD_FLIPPER_UNREF_TRANISTION(pPriv->pTransScaleZA2);
		lightwood_flipper_restore_actor_to_normal_state(LIGHTWOOD_FLIPPER(pObject) , LIGHTWOOD_FLIPPER_BOTH_ACTOR);
		LIGHTWOOD_FLIPPER_FREE_MEM(pPriv->pFirstActorData);
		LIGHTWOOD_FLIPPER_FREE_MEM(pPriv->pSecondActorData);
	}
	G_OBJECT_CLASS (lightwood_flipper_parent_class)->finalize (pObject);
}


/********************************************************
 * Function : lightwood_flipper_load_default_cofiguration
 * Description: This Function will load the transition values that are required for default animation.
 * Parameters: The LightwoodFlipperPrivate reference.
 * Return value: void
 ********************************************************/

static void lightwood_flipper_load_default_cofiguration(LightwoodFlipperPrivate *pPriv)
{
	pPriv->uinFlipDuration = LIGHTWOOD_FLIPPER_DEFAULT_TRANS_DURATION ;
	pPriv->dScalingMinY =  LIGHTWOOD_FLIPPER_DEFAULT_SCALE_Y_MIN;
	pPriv->dScalingMaxY  = LIGHTWOOD_FLIPPER_DEFAULT_SCALE_Y_MAX;
	pPriv->dScalingMinX  = LIGHTWOOD_FLIPPER_DEFAULT_SCALE_X_MIN;
	pPriv->dScalingMaxX  = LIGHTWOOD_FLIPPER_DEFAULT_SCALE_X_MAX;
	pPriv->dScalingMinZ =  LIGHTWOOD_FLIPPER_DEFAULT_SCALE_Z_MIN;
	pPriv->dScalingMaxZ =  LIGHTWOOD_FLIPPER_DEFAULT_SCALE_Z_MAX ;
	pPriv->dFirstActorAngleStart = LIGHTWOOD_FLIPPER_DEFAULT_FWD_INITIAL_ANGLE;
	pPriv->dFirstActorAngleEnd = LIGHTWOOD_FLIPPER_DEFAULT_FWD_FINAL_ANGLE;
	pPriv->dSecondActorAngleStart = LIGHTWOOD_FLIPPER_DEFAULT_BKWD_INITIAL_ANGLE;
	pPriv->dSecondActorAngleEnd = LIGHTWOOD_FLIPPER_DEFAULT_BKWD_FINAL_ANGLE;
	pPriv->uinFlipAnimType = LIGHTWOOD_FLIPPER_DEFAULT_ANIMATION_MODE ;
	pPriv->uinFlipAnimType1  = LIGHTWOOD_FLIPPER_DEFAULT_ANIMATION_MODE1;
	pPriv->fltPivotX = LIGHTWOOD_FLIPPER_DEFAULT_PIVOT_POINT_X ;
	pPriv->fltPivotY = LIGHTWOOD_FLIPPER_DEFAULT_PIVOT_POINT_Y ;

	UPDATE_SMV(pPriv->uinFlipDirection, CLUTTER_TIMELINE_FORWARD);
	UPDATE_SMV(pPriv->uinFlipType, CLUTTER_TIMELINE_FORWARD);
	UPDATE_SMV(pPriv->uinFlipCycle , LIGHTWOOD_ENABLE_FIRST_ACTOR_FLIP_ONLY );
}

/********************************************************
 * Function : b_lightwood_flipper_check_forward_animation_in_progress
 * Description: This Function will check whether forward animation is still in progress..
 * Parameters: The  reference to LightwoodFlipperPrivate .
 * Return value: True if transition is in progress
 ********************************************************/
static gboolean b_lightwood_flipper_check_forward_animation_in_progress(LightwoodFlipperPrivate *pPriv)
{
	RETURN_TRUE_IF_ANIMATION_IN_PROGRESS(pPriv->pTransRotateA1);
	RETURN_TRUE_IF_ANIMATION_IN_PROGRESS(pPriv->pTransScaleXA1);
	RETURN_TRUE_IF_ANIMATION_IN_PROGRESS(pPriv->pTransScaleYA1);
	RETURN_TRUE_IF_ANIMATION_IN_PROGRESS(pPriv->pTransScaleZA1);
	return FALSE ;
}

/********************************************************
 * Function : b_lightwood_flipper_check_backward_animation_in_progress
 * Description: This Function will check whether backward animation is still in progress..
 * Parameters: The  reference to LightwoodFlipperPrivate .
 * Return value: True if transition is in progress
 ********************************************************/
static gboolean b_lightwood_flipper_check_backward_animation_in_progress(LightwoodFlipperPrivate *pPriv)
{
	RETURN_TRUE_IF_ANIMATION_IN_PROGRESS(pPriv->pTransRotateA2);
	RETURN_TRUE_IF_ANIMATION_IN_PROGRESS(pPriv->pTransScaleXA2);
	RETURN_TRUE_IF_ANIMATION_IN_PROGRESS(pPriv->pTransScaleYA2);
	RETURN_TRUE_IF_ANIMATION_IN_PROGRESS(pPriv->pTransScaleZA2);
	return FALSE ;
}


/********************************************************
 * Function : lightwood_flipper_define_transition_for_fwd_direction
 * Description: This Function will define transitions in forward direction .
 * Parameters: The  reference to LightwoodFlipper .
 * Return value:
 ********************************************************/

static void lightwood_flipper_define_transition_for_fwd_direction(LightwoodFlipper *pLightwoodFlipper )
{
	LightwoodFlipperPrivate *pPriv = LIGHTWOOD_FLIPPER_GET_PRIVATE (pLightwoodFlipper);
	VALIDATE_LIGHTWOOD_FLIPPER_OBJECT(pLightwoodFlipper);

	/* define parameters for Rotating the Actor in Forward direction  */
	pPriv->pTransRotateA1 = clutter_property_transition_new (LIGHTWOOD_ACTOR_ROTATION_ALONG_Y_AXIS);
	clutter_timeline_set_progress_mode (CLUTTER_TIMELINE (pPriv->pTransRotateA1),
			pPriv->uinFlipAnimType);
	clutter_transition_set_from (pPriv->pTransRotateA1, G_TYPE_DOUBLE, pPriv->dFirstActorAngleStart);
	clutter_transition_set_to (pPriv->pTransRotateA1, G_TYPE_DOUBLE, pPriv->dFirstActorAngleEnd);
	//clutter_timeline_set_repeat_count (CLUTTER_TIMELINE (pPriv->pTransRotateA1), 0);
	clutter_timeline_set_duration (CLUTTER_TIMELINE (pPriv->pTransRotateA1), pPriv->uinFlipDuration);


	/* define parameters for scaling  the Actor along Y-direction during Forward flip  */
	pPriv->pTransScaleYA1 = clutter_property_transition_new (LIGHTWOOD_ACTOR_SCALE_ALONG_Y_AXIS);
	clutter_timeline_set_progress_mode (CLUTTER_TIMELINE (pPriv->pTransScaleYA1),
			pPriv->uinFlipAnimType);
	clutter_transition_set_from (pPriv->pTransScaleYA1, G_TYPE_DOUBLE,pPriv->dScalingMaxY);
	clutter_transition_set_to (pPriv->pTransScaleYA1, G_TYPE_DOUBLE, pPriv->dScalingMinY);
	//clutter_timeline_set_repeat_count (CLUTTER_TIMELINE (pPriv->pTransScaleYA1), 0);
	clutter_timeline_set_duration (CLUTTER_TIMELINE (pPriv->pTransScaleYA1), pPriv->uinFlipDuration);

	/* define parameters for scaling  the Actor along X-direction during Forward flip  */
	pPriv->pTransScaleXA1 = clutter_property_transition_new (LIGHTWOOD_ACTOR_SCALE_ALONG_X_AXIS);
	clutter_timeline_set_progress_mode (CLUTTER_TIMELINE (pPriv->pTransScaleXA1),
			pPriv->uinFlipAnimType);
	clutter_transition_set_from (pPriv->pTransScaleXA1, G_TYPE_DOUBLE,pPriv->dScalingMaxX);
	clutter_transition_set_to (pPriv->pTransScaleXA1, G_TYPE_DOUBLE, pPriv->dScalingMinX);
	//clutter_timeline_set_repeat_count (CLUTTER_TIMELINE (pPriv->pTransScaleXA1), 0);
	clutter_timeline_set_duration (CLUTTER_TIMELINE (pPriv->pTransScaleXA1),  pPriv->uinFlipDuration);

	/* define parameters for scaling  the Actor along Z-direction during Forward flip  */
	pPriv->pTransScaleZA1 = clutter_property_transition_new (LIGHTWOOD_ACTOR_SCALE_ALONG_Z_AXIS);
	clutter_timeline_set_progress_mode (CLUTTER_TIMELINE (pPriv->pTransScaleZA1),
			pPriv->uinFlipAnimType);
	clutter_transition_set_from (pPriv->pTransScaleZA1, G_TYPE_DOUBLE,pPriv->dScalingMaxZ);
	clutter_transition_set_to (pPriv->pTransScaleZA1, G_TYPE_DOUBLE, pPriv->dScalingMinZ);
	//clutter_timeline_set_repeat_count (CLUTTER_TIMELINE (pPriv->pTransScaleZA1), 0);
	clutter_timeline_set_duration (CLUTTER_TIMELINE (pPriv->pTransScaleZA1),  pPriv->uinFlipDuration);
}


/********************************************************
 * Function : lightwood_flipper_define_transition_for_bkwd_direction
 * Description: This Function will define transitions in backward direction .
 * Parameters: The  reference to LightwoodFlipper .
 * Return value:
 ********************************************************/

static void lightwood_flipper_define_transition_for_bkwd_direction(LightwoodFlipper *pLightwoodFlipper )
{
	LightwoodFlipperPrivate *pPriv = LIGHTWOOD_FLIPPER_GET_PRIVATE (pLightwoodFlipper);
	VALIDATE_LIGHTWOOD_FLIPPER_OBJECT(pLightwoodFlipper);

	/* define parameters for Rotating the Actor in Forward direction  */
	pPriv->pTransRotateA2 = clutter_property_transition_new (LIGHTWOOD_ACTOR_ROTATION_ALONG_Y_AXIS);

	clutter_timeline_set_progress_mode (CLUTTER_TIMELINE (pPriv->pTransRotateA2),
			pPriv->uinFlipAnimType1);
	clutter_transition_set_from (pPriv->pTransRotateA2, G_TYPE_DOUBLE, pPriv->dSecondActorAngleStart);
	clutter_transition_set_to (pPriv->pTransRotateA2, G_TYPE_DOUBLE, pPriv->dSecondActorAngleEnd);
	//clutter_timeline_set_repeat_count (CLUTTER_TIMELINE (pPriv->pTransRotateA2), 0);
	clutter_timeline_set_duration (CLUTTER_TIMELINE (pPriv->pTransRotateA2), pPriv->uinFlipDuration);

	/* define parameters for scaling  the Actor along Y-direction during backward flip  */
	pPriv->pTransScaleYA2 = clutter_property_transition_new (LIGHTWOOD_ACTOR_SCALE_ALONG_Y_AXIS);
	clutter_timeline_set_progress_mode (CLUTTER_TIMELINE (pPriv->pTransScaleYA2),
			pPriv->uinFlipAnimType1);
	clutter_transition_set_from (pPriv->pTransScaleYA2, G_TYPE_DOUBLE,pPriv->dScalingMinY );
	clutter_transition_set_to (pPriv->pTransScaleYA2, G_TYPE_DOUBLE, pPriv->dScalingMaxY);
	//clutter_timeline_set_repeat_count (CLUTTER_TIMELINE (pPriv->pTransScaleYA2), 0);
	clutter_timeline_set_duration (CLUTTER_TIMELINE (pPriv->pTransScaleYA2), pPriv->uinFlipDuration);

	/* define parameters for scaling  the Actor along X-direction during backward flip  */
	pPriv->pTransScaleXA2 = clutter_property_transition_new (LIGHTWOOD_ACTOR_SCALE_ALONG_X_AXIS);
	clutter_timeline_set_progress_mode (CLUTTER_TIMELINE (pPriv->pTransScaleXA2),
			pPriv->uinFlipAnimType1);
	clutter_transition_set_from (pPriv->pTransScaleXA2, G_TYPE_DOUBLE,pPriv->dScalingMinX);
	clutter_transition_set_to (pPriv->pTransScaleXA2, G_TYPE_DOUBLE, pPriv->dScalingMaxX );
	//clutter_timeline_set_repeat_count (CLUTTER_TIMELINE (pPriv->pTransScaleXA2), 0);
	clutter_timeline_set_duration (CLUTTER_TIMELINE (pPriv->pTransScaleXA2),  pPriv->uinFlipDuration);

	/* define parameters for scaling  the Actor along Z-direction during backward flip  */
	pPriv->pTransScaleZA2 = clutter_property_transition_new (LIGHTWOOD_ACTOR_SCALE_ALONG_Z_AXIS);
	clutter_timeline_set_progress_mode (CLUTTER_TIMELINE (pPriv->pTransScaleZA2),
			pPriv->uinFlipAnimType1);
	clutter_transition_set_from (pPriv->pTransScaleZA2, G_TYPE_DOUBLE,pPriv->dScalingMinZ);
	clutter_transition_set_to (pPriv->pTransScaleZA2, G_TYPE_DOUBLE, pPriv->dScalingMaxZ );
	//clutter_timeline_set_repeat_count (CLUTTER_TIMELINE (pPriv->pTransScaleZA2), 0);
	clutter_timeline_set_duration (CLUTTER_TIMELINE (pPriv->pTransScaleZA2),  pPriv->uinFlipDuration);
}



static void lightwood_flipper_redefine_transition(LightwoodFlipperData *pFlipData, gboolean bReverseAnimate )
{
	LIGHTWOOD_FLIPPER_PRINT(" Final X Value = %f \n" , ( bReverseAnimate == TRUE ? pFlipData->dScalingMaxX : pFlipData->dScalingMinX));
	clutter_transition_set_from (pFlipData->pTransScaleX, G_TYPE_DOUBLE,pFlipData->dScaleX);
	clutter_transition_set_to (pFlipData->pTransScaleX, G_TYPE_DOUBLE,
			( bReverseAnimate == TRUE ? pFlipData->dScalingMaxX : pFlipData->dScalingMinX));
	clutter_timeline_set_duration(CLUTTER_TIMELINE(pFlipData->pTransScaleX) ,  pFlipData->uinDuration ) ;

	clutter_transition_set_from (pFlipData->pTransScaleY, G_TYPE_DOUBLE,pFlipData->dScaleY);
	clutter_transition_set_to (pFlipData->pTransScaleY, G_TYPE_DOUBLE,
			( bReverseAnimate == TRUE ? pFlipData->dScalingMaxY : pFlipData->dScalingMinY));
	clutter_timeline_set_duration(CLUTTER_TIMELINE(pFlipData->pTransScaleY) , pFlipData->uinDuration ) ;


	clutter_transition_set_from (pFlipData->pTransScaleZ, G_TYPE_DOUBLE,pFlipData->dScaleZ);
	clutter_transition_set_to (pFlipData->pTransScaleZ, G_TYPE_DOUBLE,
			( bReverseAnimate == TRUE ? pFlipData->dScalingMaxZ : pFlipData->dScalingMinZ));
	clutter_timeline_set_duration(CLUTTER_TIMELINE(pFlipData->pTransScaleZ) ,  pFlipData->uinDuration ) ;

	LIGHTWOOD_FLIPPER_PRINT(" Final Angle Value = %f \n" , ( bReverseAnimate == TRUE ? pFlipData->dStartAngle : pFlipData->dStopAngle));

	clutter_transition_set_from (pFlipData->pTransRotate, G_TYPE_DOUBLE,pFlipData->dAngle);
	clutter_transition_set_to (pFlipData->pTransRotate, G_TYPE_DOUBLE,
			( bReverseAnimate == TRUE ? pFlipData->dStartAngle : pFlipData->dStopAngle ));
	clutter_timeline_set_duration(CLUTTER_TIMELINE(pFlipData->pTransRotate) ,  pFlipData->uinDuration ) ;
}

/********************************************************
 * Function : lightwood_flipper_change_animation_direction
 * Description: This Function will either reset or change the transitions based on the direction.
 * Parameters: The  reference to LightwoodFlipperPrivate , LightwoodFlipperData , boolean value to reset.
 * Return value:
 ********************************************************/

static void lightwood_flipper_change_animation_direction(LightwoodFlipperPrivate *pPriv ,
		LightwoodFlipperData *pFlipData ,gboolean  bReset )
{
	gboolean bReverseAnimate = pPriv->bReversedAnimation ;

	if(FALSE == bReset)
	{
		pFlipData->uinDuration = clutter_timeline_get_elapsed_time (CLUTTER_TIMELINE(pFlipData->pTransRotate)) ;
		if(pFlipData->uinDuration <= 0)
		{
			pFlipData->uinDuration = LIGHTWOOD_FLIPPER_MINIMUM_FLIP_DURATION ;
		}
		/* Explicitly Stop Transitions and read the current scaling and rotation factors */
		clutter_actor_remove_all_transitions( pFlipData->pAnimateActor);
		clutter_actor_get_scale( pFlipData->pAnimateActor , &pFlipData->dScaleX , &pFlipData->dScaleY);
		pFlipData->dScaleZ = clutter_actor_get_scale_z( pFlipData->pAnimateActor );
		pFlipData->dAngle = clutter_actor_get_rotation_angle( pFlipData->pAnimateActor , CLUTTER_Y_AXIS);
	}
	else
	{
		pFlipData->uinDuration = pFlipData->uinFlipDuration ;
		pFlipData->dScaleX = pFlipData->dScalingMaxX;
		pFlipData->dScaleY = pFlipData->dScalingMaxY ;
		pFlipData->dScaleZ = pFlipData->dScalingMaxZ;
		pFlipData->dAngle = pFlipData->dStartAngle ;
		bReverseAnimate = FALSE ;
		clutter_actor_remove_all_transitions(pFlipData->pAnimateActor);
	}
	lightwood_flipper_redefine_transition(pFlipData ,bReverseAnimate ) ;
}


/********************************************************
 * Function : lightwood_flipper_change_forward_animation_direction
 * Description: This Function will either reset or change the transitions for forward flip  direction.
 * Parameters: The  reference to LightwoodFlipperPrivate ,  boolean value to reset.
 * Return value:
 ********************************************************/

static void lightwood_flipper_change_forward_animation_direction(LightwoodFlipperPrivate *pPriv   ,  gboolean bReset)
{
	LightwoodFlipperData *pFlipData  = g_new0(LightwoodFlipperData , 1);
	pFlipData->pAnimateActor  = pPriv->pFirstActor ;
	pFlipData->uinFlipDuration =  pPriv->uinFlipDuration ;
	pFlipData->dScalingMinY  =   pPriv->dScalingMinY ;
	pFlipData->dScalingMaxY   =  pPriv->dScalingMaxY  ;
	pFlipData->dScalingMinX   = pPriv->dScalingMinX   ;
	pFlipData->dScalingMaxX   =   pPriv->dScalingMaxX  ;
	pFlipData->dScalingMinZ   =  pPriv->dScalingMinZ   ;
	pFlipData->dScalingMaxZ   =  pPriv->dScalingMaxZ   ;
	pFlipData->dStartAngle   =    pPriv->dFirstActorAngleStart;
	pFlipData->dStopAngle   = pPriv->dFirstActorAngleEnd   ;
	pFlipData->pTransRotate   =  pPriv->pTransRotateA1   ;
	pFlipData->pTransScaleX   =   pPriv->pTransScaleXA1 ;
	pFlipData->pTransScaleY   =  pPriv->pTransScaleYA1    ;
	pFlipData->pTransScaleZ =  pPriv->pTransScaleZA1  ;
	lightwood_flipper_change_animation_direction(pPriv ,pFlipData ,  bReset );
	g_free(pFlipData);
	pFlipData = NULL ;
}


/********************************************************
 * Function : lightwood_flipper_add_markers_to_first_actor
 * Description: This Function will add transition markers
 * 				to the first actor in model
 * Parameters: The  reference to LightwoodFlipperPrivate
 * Return value:
 ********************************************************/

static void lightwood_flipper_add_markers_to_first_actor(LightwoodFlipperPrivate *pPriv  , gboolean bCheckMarkers)
{
	LIGHTWOOD_FLIPPER_PRINT("\n\n%s %d \n\n\n" , __FUNCTION__ , __LINE__);

	if(pPriv->pFirstActorData->uinMarkerTime < pPriv->uinFlipDuration)
	{
		gchar *pDetailedSignal;
		if(TRUE == bCheckMarkers)
		{
			guint uinTransDuration = clutter_timeline_get_duration(CLUTTER_TIMELINE (pPriv->pTransRotateA1));
			if(pPriv->fltDiscreteFactor > 0.5 )
				return ;

			/* Get the scaled animation duration which is reset for transition */
			if( pPriv->pFirstActorData->uinMarkerTime > uinTransDuration)
			{
				g_warning("Lightwood-Flipper: Marker Not set for Actor-1. marker-time is more than transition time");
				return;
			}
		}
		LIGHTWOOD_FLIPPER_PRINT("\n\n%s %d \n\n\n" , __FUNCTION__ , __LINE__);
		clutter_timeline_add_marker_at_time(CLUTTER_TIMELINE(pPriv->pTransRotateA1),
				LIGHTWOOD_FLIPPER_FIRST_ACTOR_MARKER , pPriv->pFirstActorData->uinMarkerTime);
		pDetailedSignal = g_strdup_printf("marker-reached::%s",LIGHTWOOD_FLIPPER_FIRST_ACTOR_MARKER);

		/* connect to marker-reached::<marker-name> signal */
		pPriv->pFirstActorData->inMarkerHandlerId = g_signal_connect (CLUTTER_TIMELINE(pPriv->pTransRotateA1),
				pDetailedSignal, G_CALLBACK (lightwood_flipper_on_marker_reached),
				pPriv->pFirstActorData);

		LIGHTWOOD_FLIPPER_FREE_MEM(pDetailedSignal);
	}
	else
	{
		g_warning("Lightwood-Flipper : Invalid First actor marker time");
	}
}

/********************************************************
 * Function : lightwood_flipper_add_markers_to_second_actor
 * Description: This Function will add transition markers
 * 				to the second actor in model
 * Parameters: The  reference to LightwoodFlipperPrivate
 * Return value:
 ********************************************************/

static void lightwood_flipper_add_markers_to_second_actor(LightwoodFlipperPrivate *pPriv ,  gboolean bCheckMarkers )
{
	LIGHTWOOD_FLIPPER_PRINT("\n\n%s %d \n\n\n" , __FUNCTION__ , __LINE__);
	if(pPriv->pSecondActorData->uinMarkerTime < pPriv->uinFlipDuration)
	{
		gchar *pDetailedSignal;
		if(TRUE == bCheckMarkers)
		{
			/* Get the scaled animation duration which is reset for transition */
			guint uinTransDuration = clutter_timeline_get_duration(CLUTTER_TIMELINE (pPriv->pTransRotateA2));
			if( pPriv->pSecondActorData->uinMarkerTime > uinTransDuration)
			{
				g_warning("Lightwood-Flipper: Marker Not set for Actor-2. marker-time is more than transition time");
				return;
			}
		}
		LIGHTWOOD_FLIPPER_PRINT("\n\n%s %d \n\n\n" , __FUNCTION__ , __LINE__);
		clutter_timeline_add_marker_at_time(CLUTTER_TIMELINE(pPriv->pTransRotateA2),
				LIGHTWOOD_FLIPPER_SECOND_ACTOR_MARKER , pPriv->pSecondActorData->uinMarkerTime);
		pDetailedSignal = g_strdup_printf("marker-reached::%s",LIGHTWOOD_FLIPPER_SECOND_ACTOR_MARKER);

		/* connect to marker-reached::<marker-name> signal */
		pPriv->pSecondActorData->inMarkerHandlerId = g_signal_connect (CLUTTER_TIMELINE(pPriv->pTransRotateA2),
				pDetailedSignal, G_CALLBACK (lightwood_flipper_on_marker_reached),
				pPriv->pSecondActorData);

		LIGHTWOOD_FLIPPER_FREE_MEM(pDetailedSignal);
	}
	else
	{
		g_warning("Lightwood-Flipper : Invalid First actor marker time");
	}
}









/********************************************************
 * Function : lightwood_flipper_add_markers_to_transition
 * Description: This Function will add transition markers to the actor
 * Parameters: The  reference to LightwoodFlipperPrivate
 * Return value:
 ********************************************************/

static void lightwood_flipper_add_markers_to_transition(LightwoodFlipperPrivate *pPriv  , gboolean bCheckMarkers)
{
	if(LIGHTWOOD_ENABLE_FIRST_ACTOR_FLIP_ONLY == pPriv->uinFlipCycle ||
			LIGHTWOOD_ENABLE_FIRST_ACTOR_FLIP_ONLY == pPriv->uinFlipCycle )
	{
		LIGHTWOOD_FLIPPER_PRINT("\n\n%s %d \n\n\n" , __FUNCTION__ , __LINE__);
		if(-1 == pPriv->pFirstActorData->inMarkerHandlerId && TRUE == pPriv->pFirstActorData->bSetMarker )
		{
			lightwood_flipper_add_markers_to_first_actor(pPriv , bCheckMarkers);
		}
	}
	if(LIGHTWOOD_ENABLE_SECOND_ACTOR_FLIP_ONLY == pPriv->uinFlipCycle ||
			LIGHTWOOD_ENABLE_FIRST_ACTOR_FLIP_ONLY == pPriv->uinFlipCycle )
	{
		LIGHTWOOD_FLIPPER_PRINT("\n\n%s %d \n\n\n" , __FUNCTION__ , __LINE__);
		if(  -1 == pPriv->pSecondActorData->inMarkerHandlerId && TRUE == pPriv->pSecondActorData->bSetMarker )
		{
			lightwood_flipper_add_markers_to_second_actor(pPriv  , bCheckMarkers);
		}
	}
}


/********************************************************
 * Function : lightwood_flipper_change_backward_animation_direction
 * Description: This Function will either reset or change the transitions for backward flip  direction.
 * Parameters: The  reference to LightwoodFlipperPrivate ,  boolean value to reset.
 * Return value:
 ********************************************************/

static void lightwood_flipper_change_backward_animation_direction(LightwoodFlipperPrivate *pPriv  , gboolean bReset)
{
	LightwoodFlipperData *pFlipData  = g_new0(LightwoodFlipperData , 1);
	pFlipData->pAnimateActor  = pPriv->pSecondActor ;
	pFlipData->uinFlipDuration =  pPriv->uinFlipDuration ;
	pFlipData->dScalingMinY  =   pPriv->dScalingMaxY ;
	pFlipData->dScalingMaxY   =  pPriv->dScalingMinY  ;
	pFlipData->dScalingMinX   = pPriv->dScalingMaxX   ;
	pFlipData->dScalingMaxX   =   pPriv->dScalingMinX  ;
	pFlipData->dScalingMinZ   =  pPriv->dScalingMaxZ   ;
	pFlipData->dScalingMaxZ   =  pPriv->dScalingMinZ   ;
	pFlipData->dStartAngle   =    pPriv->dSecondActorAngleStart;
	pFlipData->dStopAngle   = pPriv->dSecondActorAngleEnd   ;
	pFlipData->pTransRotate   =  pPriv->pTransRotateA2   ;
	pFlipData->pTransScaleX   =   pPriv->pTransScaleXA2 ;
	pFlipData->pTransScaleY   =  pPriv->pTransScaleYA2    ;
	pFlipData->pTransScaleZ =  pPriv->pTransScaleZA2  ;
	lightwood_flipper_change_animation_direction(pPriv ,pFlipData ,  bReset );
	g_free(pFlipData);
	pFlipData = NULL ;
}


/********************************************************
 * Function : b_lightwood_flipper_reverse_flip_direction
 * Description: This Function will reverse flip  direction if animation is still in progress.
 * Parameters: The  reference to LightwoodFlipper.
 * Return value: TRUE if flip is reversed
 ********************************************************/
static gboolean b_lightwood_flipper_reverse_flip_direction(LightwoodFlipper *pLightwoodFlipper )
{
	LightwoodFlipperPrivate *pPriv;
	gboolean bAnimGapInProgress = FALSE ;
	if (! LIGHTWOOD_IS_FLIPPER (pLightwoodFlipper))
	{
		g_warning("invalid flipper pObject\n");
		return FALSE;
	}
	pPriv = LIGHTWOOD_FLIPPER_GET_PRIVATE (pLightwoodFlipper);
#if PROVIDE_ANIM_GAP
	if (pPriv->uinAnimGapTimeoutId >  0)
	{
		g_source_remove(pPriv->uinAnimGapTimeoutId);
		bAnimGapInProgress = TRUE ;
	}

#endif

	if(TRUE == b_lightwood_flipper_check_forward_animation_in_progress(pPriv) ||
			TRUE == b_lightwood_flipper_check_backward_animation_in_progress(pPriv) || TRUE == bAnimGapInProgress )
	{
		ClutterTimelineDirection enAnimationDirection = (pPriv->bReversedAnimation == FALSE ?
				CLUTTER_TIMELINE_BACKWARD : CLUTTER_TIMELINE_FORWARD) ;

		if(pPriv->uinFlipCycle != LIGHTWOOD_ENABLE_SECOND_ACTOR_FLIP_ONLY &&
				(TRUE == CLUTTER_ACTOR_IS_VISIBLE( pPriv->pFirstActor) || LIGHTWOOD_FLIPPER_SECOND_ACTOR == pPriv->uinAnimateActor ))
		{

			UPDATE_SMV(pPriv->bReversedAnimation , !pPriv->bReversedAnimation );
			lightwood_flipper_change_forward_animation_direction(pPriv , FALSE );
			lightwood_flipper_add_animation_effects(pLightwoodFlipper , TRUE);
			g_signal_emit(LIGHTWOOD_FLIPPER(pLightwoodFlipper),
					flipper_signals[SIG_FLIPPER_ANIMATION_REVERSED],
					0, pPriv->pFirstActor , enAnimationDirection);
		}
		else if(pPriv->uinFlipCycle != LIGHTWOOD_ENABLE_FIRST_ACTOR_FLIP_ONLY &&
				( TRUE == CLUTTER_ACTOR_IS_VISIBLE(pPriv->pSecondActor) || LIGHTWOOD_FLIPPER_FIRST_ACTOR == pPriv->uinAnimateActor ))
		{
			UPDATE_SMV(pPriv->bReversedAnimation , !pPriv->bReversedAnimation );
			lightwood_flipper_change_backward_animation_direction(pPriv , FALSE );
			lightwood_flipper_add_animation_effects(pLightwoodFlipper ,FALSE);
			g_signal_emit(LIGHTWOOD_FLIPPER(pLightwoodFlipper),
					flipper_signals[SIG_FLIPPER_ANIMATION_REVERSED],
					0, pPriv->pSecondActor , enAnimationDirection);
		}
		return TRUE ;
	}
	return FALSE ;
}


/********************************************************
 * Function : lightwood_flipper_add_animation_effects
 * Description: This Function will add transitions defined to the actor.
 * Parameters: The  reference to LightwoodFlipper, boolean to find ForwardFlip
 * Return value:
 ********************************************************/
static void
lightwood_flipper_add_animation_effects(LightwoodFlipper *pLightwoodFlipper , gboolean bForwardFlip )
{
	LightwoodFlipperPrivate *pPriv = LIGHTWOOD_FLIPPER_GET_PRIVATE (pLightwoodFlipper);
	VALIDATE_LIGHTWOOD_FLIPPER_OBJECT(pLightwoodFlipper);
	if(TRUE == bForwardFlip)
	{
		LIGHTWOOD_FLIPPER_PRINT("\n Animation Start for First Actor \n\n ");
		if(LIGHTWOOD_ENABLE_FIRST_ACTOR_FLIP_ONLY != pPriv->uinFlipCycle)
		{
			clutter_actor_set_rotation_angle (pPriv->pSecondActor ,
					CLUTTER_Y_AXIS , pPriv->dSecondActorAngleStart );
		}
		if(NULL != pPriv->pSecondActor )
			clutter_actor_hide(pPriv->pSecondActor);
		if(FALSE == CLUTTER_ACTOR_IS_VISIBLE(pPriv->pFirstActor))
		{
			clutter_actor_show(pPriv->pFirstActor);
		}
		clutter_actor_remove_all_transitions(pPriv->pFirstActor);
		clutter_actor_add_transition ( pPriv->pFirstActor, "flip", pPriv->pTransRotateA1 );
		clutter_actor_add_transition ( pPriv->pFirstActor, "scaley", pPriv->pTransScaleYA1 );
		clutter_actor_add_transition ( pPriv->pFirstActor, "scalex", pPriv->pTransScaleXA1);
		clutter_actor_add_transition ( pPriv->pFirstActor, "scalez", pPriv->pTransScaleZA1);

	}
	else
	{
		LIGHTWOOD_FLIPPER_PRINT("\n Animation Start for Second Actor \n\n ");

		if(NULL != pPriv->pFirstActor && TRUE == CLUTTER_ACTOR_IS_VISIBLE(pPriv->pFirstActor))
			clutter_actor_hide(pPriv->pFirstActor);

		if(FALSE == CLUTTER_ACTOR_IS_VISIBLE(pPriv->pSecondActor))
		{
			clutter_actor_show(pPriv->pSecondActor);
		}
		clutter_actor_remove_all_transitions(pPriv->pSecondActor);
		clutter_actor_add_transition (pPriv->pSecondActor, "flip2",  pPriv->pTransRotateA2);
		clutter_actor_add_transition (pPriv->pSecondActor, "scaley2", pPriv->pTransScaleYA2);
		clutter_actor_add_transition (pPriv->pSecondActor, "scalex2", pPriv->pTransScaleXA2);
		clutter_actor_add_transition (pPriv->pSecondActor, "scalez2", pPriv->pTransScaleZA2);
	}
}


/********************************************************
 * Function : lightwood_flipper_restore_actor_to_normal_state
 * Description: This Function will restore the deformed actor to normal state.
 * Parameters:
 * 				@pLightwoodFlipper :The  reference to LightwoodFlipper
 * 				@uinActorsToReset : enFlipperActors enum  stating actors to reset  .
 * Return value:
 ********************************************************/

static void lightwood_flipper_restore_actor_to_normal_state(LightwoodFlipper *pLightwoodFlipper ,
		enFlipperActors  uinActorsToReset)
{
	LightwoodFlipperPrivate *pPriv = LIGHTWOOD_FLIPPER_GET_PRIVATE (pLightwoodFlipper);
	if(NULL != pPriv->pFirstActor && uinActorsToReset != LIGHTWOOD_FLIPPER_SECOND_ACTOR	)
	{
		/* restore the deformed actor to normal state */
		clutter_actor_set_scale(pPriv->pFirstActor , pPriv->dScalingMaxX , pPriv->dScalingMaxY);
		clutter_actor_set_scale_z(pPriv->pFirstActor ,pPriv->dScalingMaxZ);
		clutter_actor_set_rotation_angle(pPriv->pFirstActor  ,CLUTTER_Y_AXIS, pPriv->dFirstActorAngleStart);
	}
	if(NULL != pPriv->pSecondActor && uinActorsToReset != LIGHTWOOD_FLIPPER_FIRST_ACTOR)
	{
		clutter_actor_set_scale(pPriv->pSecondActor , pPriv->dScalingMaxX , pPriv->dScalingMaxY);
		clutter_actor_set_scale_z(pPriv->pSecondActor ,pPriv->dScalingMaxZ);
		clutter_actor_set_rotation_angle(pPriv->pSecondActor  ,CLUTTER_Y_AXIS, pPriv->dSecondActorAngleEnd);
	}
#if PROVIDE_ANIM_GAP
	UPDATE_SMV( pPriv->uinAnimateActor ,LIGHTWOOD_FLIPPER_ACTORS_NONE );
#endif
}
/********************************************************
 * Function : lightwood_flipper_disconnect_marker_signals
 * Description: This Function will disconnect the internally connected
 * 				marker signals associated to actors timeline animation
 * Parameters:
 * 				@pLightwoodFlipper :The  reference to LightwoodFlipper
 * 				@uinActorsToReset : enFlipperActors enum  stating actors.
 * Return value:
 ********************************************************/

static void lightwood_flipper_disconnect_marker_signals(LightwoodFlipper *pLightwoodFlipper ,
		enFlipperActors  uinActors)
{
	LightwoodFlipperPrivate *pPriv = LIGHTWOOD_FLIPPER_GET_PRIVATE (pLightwoodFlipper);
	if(NULL != pPriv->pFirstActor && uinActors != LIGHTWOOD_FLIPPER_SECOND_ACTOR	)
	{
		if(-1 != pPriv->pFirstActorData->inMarkerHandlerId  )
		{
			clutter_timeline_remove_marker(CLUTTER_TIMELINE(pPriv->pTransRotateA1),
					LIGHTWOOD_FLIPPER_FIRST_ACTOR_MARKER) ;

			g_signal_handler_disconnect(CLUTTER_TIMELINE(pPriv->pTransRotateA1) ,
					pPriv->pFirstActorData->inMarkerHandlerId  ) ;
			pPriv->pFirstActorData->inMarkerHandlerId = -1 ;
		}
	}
	if(NULL != pPriv->pSecondActor && uinActors != LIGHTWOOD_FLIPPER_FIRST_ACTOR)
	{
		if(-1 != pPriv->pSecondActorData->inMarkerHandlerId  )
		{
			clutter_timeline_remove_marker(CLUTTER_TIMELINE(pPriv->pTransRotateA2),
					LIGHTWOOD_FLIPPER_SECOND_ACTOR_MARKER) ;

			g_signal_handler_disconnect(CLUTTER_TIMELINE(pPriv->pTransRotateA2) ,
					pPriv->pSecondActorData->inMarkerHandlerId  ) ;
			pPriv->pSecondActorData->inMarkerHandlerId = -1 ;
		}
	}
}



/********************************************************
 * Function : lightwood_flipper_class_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/
static void lightwood_flipper_class_init (LightwoodFlipperClass *pKlass)
{
	GObjectClass *pObjectClass = G_OBJECT_CLASS (pKlass);
	GParamSpec *pSpec = NULL;

	pObjectClass->get_property = lightwood_flipper_get_property;
	pObjectClass->set_property = lightwood_flipper_set_property;
	pObjectClass->dispose = lightwood_flipper_dispose;
	pObjectClass->finalize = lightwood_flipper_finalize;


	/**
	 * LightwoodFlipper:scale-x-min:
	 *
	 * Min scaling factor for the actor  which can attain along X direction
	 * Note :Trying  to set this property when animation is in progress , will be ignored.
	 */
	pSpec = g_param_spec_double ("scale-x-min",
			"scaling-factor-x",
			"min limit for scaling the actors along X direction",
			-G_MAXDOUBLE, G_MAXDOUBLE,
			1.0,
			G_PARAM_READWRITE);
	g_object_class_install_property (pObjectClass, PROP_FLIPPER_SCALE_X_MIN, pSpec);


	/**
	 * LightwoodFlipper:scale-x-max:
	 *
	 * Max scaling factor for the actor  which can attain along X direction
	 * Note :Trying  to set this property when animation is in progress , will be ignored.
	 */
	pSpec = g_param_spec_double ("scale-x-max",
			"scaling-factor-x",
			"max limit for scaling the actors along X direction",
			-G_MAXDOUBLE, G_MAXDOUBLE,
			1.0,
			G_PARAM_READWRITE);
	g_object_class_install_property (pObjectClass, PROP_FLIPPER_SCALE_X_MAX, pSpec);



	/**
	 * LightwoodFlipper:scale-y-min:
	 *
	 * Min scaling factor for the actor  which can attain along Y direction
	 * Note:Trying  to set this property when animation is in progress , will be ignored.
	 */
	pSpec = g_param_spec_double ("scale-y-min",
			"scaling-factor-y",
			"min limit for scaling the actors along Y direction",
			-G_MAXDOUBLE, G_MAXDOUBLE,
			1.0,
			G_PARAM_READWRITE);
	g_object_class_install_property (pObjectClass, PROP_FLIPPER_SCALE_Y_MIN, pSpec);


	/**
	 * LightwoodFlipper:scale-y-max:
	 *
	 * Max scaling factor for the actor  which can attain along Y direction
	 * Note:Trying  to set this property when animation is in progress , will be ignored.
	 */
	pSpec = g_param_spec_double ("scale-y-max",
			"scaling-factor-y",
			"max limit for scaling the actors along Y direction",
			-G_MAXDOUBLE, G_MAXDOUBLE,
			1.0,
			G_PARAM_READWRITE);
	g_object_class_install_property (pObjectClass, PROP_FLIPPER_SCALE_Y_MAX, pSpec);



	/**
	 * LightwoodFlipper:scale-z-min:
	 *
	 * Min scaling factor for the actor  which can attain along Z direction
	 * Note :Trying  to set this property when animation is in progress , will be ignored.
	 */
	pSpec = g_param_spec_double ("scale-z-min",
			"scaling-factor-z",
			"min limit for scaling the actors along Z direction",
			-G_MAXDOUBLE, G_MAXDOUBLE,
			1.0,
			G_PARAM_READWRITE);
	g_object_class_install_property (pObjectClass, PROP_FLIPPER_SCALE_Z_MIN, pSpec);


	/**
	 * LightwoodFlipper:scale-z-max:
	 *
	 * Max scaling factor for the actor  which can attain along Z direction
	 * Note :Trying  to set this property when animation is in progress , will be ignored.
	 */
	pSpec = g_param_spec_double ("scale-z-max",
			"scaling-factor-z",
			"max limit for scaling the actors along Z direction",
			-G_MAXDOUBLE, G_MAXDOUBLE,
			1.0,
			G_PARAM_READWRITE);
	g_object_class_install_property (pObjectClass, PROP_FLIPPER_SCALE_Z_MAX, pSpec);


	/**
	 * LightwoodFlipper:forward-flip-start-angle:
	 *
	 * Starting angle to flip the actor along forward direction.
	 * Note :Trying  to set this property when animation is in progress , will be ignored.
	 *
	 * Default: 0 degree
	 */
	pSpec = g_param_spec_double ("forward-flip-start-angle",
			"fwd-start-angle",
			"starting angle to flip the actor along forward direction ",
			-G_MAXDOUBLE, G_MAXDOUBLE,
			0.0,
			G_PARAM_READWRITE);
	g_object_class_install_property (pObjectClass, PROP_FLIPPER_FORWARD_FLIP_START_ANGLE, pSpec);

	/**
	 * LightwoodFlipper:forward-flip-end-angle:
	 *
	 * Final angle specifier to flip the actor along forward direction.
	 * Note :Trying  to set this property when animation is in progress , will be ignored.
	 * Default: 90.0 degree
	 */
	pSpec = g_param_spec_double ("forward-flip-end-angle",
			"fwd-end-angle",
			"final angle specifier to flip the actor along forward direction ",
			-G_MAXDOUBLE, G_MAXDOUBLE,
			0.0,
			G_PARAM_READWRITE);
	g_object_class_install_property (pObjectClass, PROP_FLIPPER_FORWARD_FLIP_END_ANGLE, pSpec);




	/**
	 * LightwoodFlipper:backward-flip-start-angle:
	 *
	 * Starting angle to flip the actor along backward direction.
	 * Note: Trying  to set this property when animation is in progress , will be ignored.
	 *
	 * Default: 270.0 degree
	 */
	pSpec = g_param_spec_double ("backward-flip-start-angle",
			"bkwd-start-angle",
			"starting angle to flip the actor along backward direction ",
			-G_MAXDOUBLE, G_MAXDOUBLE,
			0.0,
			G_PARAM_READWRITE);
	g_object_class_install_property (pObjectClass, PROP_FLIPPER_BACKWARD_FLIP_START_ANGLE, pSpec);

	/**
	 * LightwoodFlipper:backward-flip-end-angle:
	 *
	 * Final angle specifier to flip the actor along backward direction.
	 * Note :Trying  to set this property when animation is in progress , will be ignored.
	 *
	 * Default: 360.0 degree
	 */
	pSpec = g_param_spec_double ("backward-flip-end-angle",
			"bkwd-end-angle",
			"final angle specifier to flip the actor along backward direction ",
			-G_MAXDOUBLE, G_MAXDOUBLE,
			0.0,
			G_PARAM_READWRITE);
	g_object_class_install_property (pObjectClass, PROP_FLIPPER_BACKWARD_FLIP_END_ANGLE, pSpec);



	/**
	 * LightwoodFlipper:transition-time:
	 *
	 * Transition-time for flip animation. Transition time should be positive integers.
	 * Note :Trying  to set this property when animation is in progress , will be ignored.
	 */

	pSpec = g_param_spec_uint("transition-time", "transition_time",
			"transition-time for flip animation", 0, G_MAXUINT,
			0, G_PARAM_READWRITE);
	g_object_class_install_property(pObjectClass, PROP_FLIPPER_TRANSITION_TIME, pSpec);


	/**
	 * LightwoodFlipper:first-actor-mark:
	 *
	 * sets mark point in transitions for first actor in model
	 * which when met #LightwoodFlipper::flip-animation-mark-reached signal will be emitted.
	 *
	 * Marker will be un-set implicitly when model data is removed or changed .
	 * set this property to 0 to explicitly un-set the marker.
	 * Note :Trying  to set this property when animation is in progress , will be ignored.
	 *
	 * This property will be affected if the marker-time requested falls short of the
	 * scaled transition time , which is calculated when #LightwoodFlipper:discrete-factor
	 * is set between 0.0 and 1.0.
	 */

	pSpec = g_param_spec_uint("first-actor-mark", "first-actor-marker",
			"first-actor-mark for flip animation", 0, G_MAXUINT,
			0, G_PARAM_READWRITE);
	g_object_class_install_property(pObjectClass, PROP_FLIPPER_FIRST_ACTOR_MARK, pSpec);


	/**
	 * LightwoodFlipper:second-actor-mark:
	 *
	 * Sets mark point in transitions for second actor in model which when met
	 * #LightwoodFlipper::flip-animation-mark-reached signal will be emitted.
	 * Marker will be un-set implicitly when model data is removed or changed.
	 * set this property to 0 to explicitly un-set the marker.
	 * Note :Trying  to set this property when animation is in progress , will be ignored.
	 *
	 * This property will be affected if the marker-time requested falls short of the
	 * scaled transition time , which is calculated when #LightwoodFlipper:discrete-factor
	 * is set between 0.0 and 1.0.
	 */

	pSpec = g_param_spec_uint("second-actor-mark", "second-actor-marker",
			"second-actor-mark for flip animation", 0, G_MAXUINT,
			0, G_PARAM_READWRITE);
	g_object_class_install_property(pObjectClass, PROP_FLIPPER_SECOND_ACTOR_MARK, pSpec);



	/**
	 * LightwoodFlipper:transition-direction:
	 *
	 * Transition-direction for flip animation as defined in #ClutterTimelineDirection.
	 * Toggling this property  will internally reverse the start
	 * and end factors used for transition.Proper Start and end
	 * parameters may have to be set for achieving desired behavior.
	 *
	 * Note :Trying  to set this property when animation is in progress , will be ignored.
	 *
	 * Default : %CLUTTER_TIMELINE_FORWARD
	 */

	pSpec = g_param_spec_enum ("transition-direction", "transition_direction",
			"transition-direction for flip animation",
			CLUTTER_TYPE_TIMELINE_DIRECTION,
			CLUTTER_TIMELINE_FORWARD, G_PARAM_READWRITE);
	g_object_class_install_property(pObjectClass, PROP_FLIPPER_TRANSITION_DIRECTION, pSpec);


	/**
	 * LightwoodFlipper:animation-mode:
	 *
	 * Animation-mode for flip animation as a #ClutterAnimationMode.
	 * Note :Trying  to set #LightwoodFlipper:animation-mode property when animation is in progress, will be ignored.
	 *
	 * Default : %CLUTTER_LINEAR
	 */

	pSpec = g_param_spec_uint ("animation-mode", "animation_mode",
			"animation mode for flip animation", 0, G_MAXUINT,
			CLUTTER_LINEAR , G_PARAM_READWRITE);
	g_object_class_install_property(pObjectClass, PROP_FLIPPER_ANIMATION_MODE, pSpec);


	/**
	 * LightwoodFlipper:animation-in-progress:
	 *
	 * Read only property which can be used to determine idle/transition state
	 *
	 * Default : %FALSE
	 */
	pSpec = g_param_spec_boolean ("animation-in-progress",
			"animation_in_progress",
			"property to determine idle/transition state ",
			FALSE,
			G_PARAM_READABLE);
	g_object_class_install_property (pObjectClass, PROP_FLIPPER_ANIMATION_IN_PROGRESS, pSpec);



	/**
	 * LightwoodFlipper:actor-being-animated:
	 *
	 * Read only property which can be used to determine the current actor that is being animated.
	 * This Property returns the actor being animated only if animation is in progress.
	 * Return %NULL if no animation is in Progress .
	 */
	pSpec = g_param_spec_object ("actor-being-animated",
			"acotr_being_animated",
			"property to determine current actor that is being animated",
			G_TYPE_OBJECT,
			G_PARAM_READABLE);
	g_object_class_install_property (pObjectClass, PROP_FLIPPER_ACTOR_BEING_ANIMATED, pSpec);


	/**
	 * LightwoodFlipper:flip-direction:
	 *
	 * Direction of flip when animation started by calling lightwood_flipper_start_animation().
	 * This Property defines the direction of flip to be considered while animating
	 * one complete flip cycle (involving two actors).
	 *  Setting this Property will internally swap the start and final values of
	 *  certain parameters which defines transition.
	 *
	 * Note :Trying  to set #LightwoodFlipper:flip-direction property when animation is in
	 * progress ,will be ignored.Also, setting #LightwoodFlipper:flip-direction property
	 * has no effects if #LightwoodFlipper:discrete-factor is non-zero.
	 *
	 * Default : %CLUTTER_TIMELINE_FORWARD
	 *
	 */

	pSpec = g_param_spec_enum ("flip-direction", "flip_direction",
			"flip-direction for animation", CLUTTER_TYPE_TIMELINE_DIRECTION,
			CLUTTER_TIMELINE_FORWARD, G_PARAM_READWRITE);
	g_object_class_install_property(pObjectClass, PROP_FLIPPER_FLIP_DIRECTION, pSpec);

	/**
	 * LightwoodFlipper:flip-cycle:
	 *
	 * Type of Flip-cycle to be applied like Flip both actors ,Rotate only first actor
	 * or Flip only Second Actor. The model must contain the data according to the flip-cycle set.
	 * It is preferable to set this property appropriately before changing or appending the
	 * actors to model.Unless #LightwoodFlipper:flip-cycle property is set for %LIGHTWOOD_ENABLE_FIRST_ACTOR_FLIP_ONLY and first actor
	 * for animating single actors is passed via #ThornburyModel ,the other actor can be passed %NULL.
	 *
	 * Note: Trying  to set this property when animation is in progress , will be ignored.
	 * #LightwoodFlipper::flip-animation-half-mark signal will not be emitted in case other
	 * than %LIGHTWOOD_ENABLE_FIRST_ACTOR_FLIP_ONLY.
	 *
	 * Default : %LIGHTWOOD_ENABLE_FIRST_ACTOR_FLIP_ONLY
	 */

	pSpec = g_param_spec_enum ("flip-cycle", "flip_cycle",
			"flip-cycle to be followed", LIGHTWOOD_TYPE_FLIP_CYCLE,
			LIGHTWOOD_ENABLE_FIRST_ACTOR_FLIP_ONLY, G_PARAM_READWRITE);
	g_object_class_install_property(pObjectClass, PROP_FLIPPER_FLIP_CYCLE, pSpec);



	/**
	 * LightwoodFlipper:x-pivot-point:
	 *
	 * X direction Pivot point to perform scaling and rotation transformations on both actors.
	 * Note:Trying  to set this property when animation is in progress , will be ignored.
	 *
	 * Default: 0.5
	 * Range: min 0.0 and max 1.0
	 */
	pSpec = g_param_spec_float ("x-pivot-point",
			"x-pivot_point",
			"X direction Pivot point",
			-G_MAXFLOAT, G_MAXFLOAT,
			0.5,
			G_PARAM_READWRITE);
	g_object_class_install_property (pObjectClass, PROP_FLIPPER_X_PIVOT_POINT, pSpec);


	/**
	 * LightwoodFlipper:y-pivot-point:
	 *
	 * Y direction Pivot point to perform scaling and rotation transformations on both actors.
	 * Note :Trying  to set this property when animation is in progress, will be ignored.
	 *
	 * Default: 0.5
	 * Range:min 0.0 and max 1.0
	 */
	pSpec = g_param_spec_float ("y-pivot-point",
			"y-pivot_point",
			"Y direction Pivot point",
			-G_MAXFLOAT, G_MAXFLOAT,
			0.5,
			G_PARAM_READWRITE);
	g_object_class_install_property (pObjectClass, PROP_FLIPPER_Y_PIVOT_POINT, pSpec);


	/**
	 * LightwoodFlipper:discrete-factor:
	 *
	 * Setting #LightwoodFlipper:discrete-factor property with normalized values
	 * ranging from 0.0 to 1.0 of final scale and rotation values, will modify the actor
	 * to discrete flip frames.
	 *
	 * Note: Trying  to set this property when animation is in progress , will be ignored.
	 *
	 * Setting #LightwoodFlipper:flip-direction property has no effects if
	 * #LightwoodFlipper:discrete-factor is set to non-zero.
	 * Note: Setting #LightwoodFlipper:flip-direction is not recommended to be set simultaneously
	 * with the #LightwoodFlipper:discrete-factor property.
	 *
	 * Normalized Values : 0.0 < Value > 1.0
	 */
	pSpec = g_param_spec_float ("discrete-factor",
			"discrete-factor",
			"modify the actor to discrete flip frames",
			-G_MAXFLOAT, G_MAXFLOAT,
			0.0,
			G_PARAM_READWRITE);
	g_object_class_install_property (pObjectClass, PROP_FLIPPER_DISCRETE_FACTOR, pSpec);






	/**
	 * LightwoodFlipper:model:
	 *
	 * Model for the #LightwoodFlipper widget. Flipper at most can have only one Row .
	 * model content:
	 * 1. Object Pointer for First Actor to flip in forward direction.
	 * 2. Object Pointer for second Actor to flip in backward direction.
	 *
	 * Default: NULL
	 */
	pSpec = g_param_spec_object ("model",
			"Model",
			"model containing Object pointers",
			G_TYPE_OBJECT,
			G_PARAM_READWRITE);
	g_object_class_install_property (pObjectClass, PROP_FLIPPER_MODEL, pSpec);

	  /**
	  * LightwoodFlipper::flip-animation-started:
	  * @pFlipper: a #LightwoodFlipper.
	  * @pActorBeingAnimated: a #ClutterActor which is being animated.
	  *
	  * #LightwoodFlipper::flip-animation-started is emitted when flip animation
	  * begins for the visible first actor
	  */
	flipper_signals[SIG_FLIPPER_ANIMATION_STARTED] = g_signal_new ("flip-animation-started",
			G_TYPE_FROM_CLASS (pObjectClass),
			G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
			G_STRUCT_OFFSET (LightwoodFlipperClass, flip_animation_started),
			NULL, NULL,
			g_cclosure_marshal_generic,
			G_TYPE_NONE, 1, G_TYPE_OBJECT);

	/**
	 * LightwoodFlipper::flip-animation-half-mark:
	 * @pFlipper: a #LightwoodFlipper.
	 * @pActorInFocus: a #ClutterActor which is in focus.
	 * @inCompletedFlipDirection: a #ClutterTimelineDirection to indicate the transition direction
	 *                          upto half completion .
	 *
	 * #LightwoodFlipper::flip-animation-half-mark is emitted when flip animation ends for
	 *  the visible first actor and before starting animation for the second actor.
	 */

	flipper_signals[SIG_FLIPPER_ANIMATION_HALF_DONE] = g_signal_new ("flip-animation-half-mark",
			G_TYPE_FROM_CLASS (pObjectClass),
			G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
			G_STRUCT_OFFSET (LightwoodFlipperClass, flip_animation_half_done),
			NULL, NULL,
			g_cclosure_marshal_VOID__UINT,
			G_TYPE_NONE, 1, CLUTTER_TYPE_TIMELINE_DIRECTION);


	  /**
	     * LightwoodFlipper::flip-animation-mark-reached:
	     * @pFlipper: a #LightwoodFlipper.
	     * @pActorBeingAnimated: a #ClutterActor which is being animated.
	     * @inFlipDirection: a #ClutterTimelineDirection to indicate the transition direction.
	     *
	     * #LightwoodFlipper::flip-animation-mark-reached is emitted if  property
	     * #LightwoodFlipper:first-actor-mark  or #LightwoodFlipper:second-actor-mark is set.
	     * The Direction of flip and the actor being animated will be conveyed through this signal.
	     */
	flipper_signals[SIG_FLIPPER_ANIMATION_MARK_REACHED] = g_signal_new ("flip-animation-mark-reached",
			G_TYPE_FROM_CLASS (pObjectClass),
			G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
			G_STRUCT_OFFSET (LightwoodFlipperClass, flip_animation_mark_reached),
			NULL, NULL,
			g_cclosure_marshal_generic,
			G_TYPE_NONE, 2, G_TYPE_OBJECT, CLUTTER_TYPE_TIMELINE_DIRECTION);




	/**
	 * LightwoodFlipper::flip-animation-completed:
	 * @pFlipper: a #LightwoodFlipper.
	 * @pActorInFocus: a #ClutterActor which is in focus.
	 *
	 * #LightwoodFlipper::flip-animation-completed is emitted when flip animation completes.
	 */
	flipper_signals[SIG_FLIPPER_ANIMATION_COMPLETED] = g_signal_new ("flip-animation-completed",
			G_TYPE_FROM_CLASS (pObjectClass),
			G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
			G_STRUCT_OFFSET (LightwoodFlipperClass, flip_animation_completed),
			NULL, NULL,
			g_cclosure_marshal_generic,
			G_TYPE_NONE, 1, G_TYPE_OBJECT);

	/**
	 *  flip-animation-reversed:
	 *
	 * ::flip-animation-reversed  is emitted when flip animation is reversed in the mid way.
	 */
	flipper_signals[SIG_FLIPPER_ANIMATION_REVERSED] = g_signal_new ("flip-animation-reversed",
			G_TYPE_FROM_CLASS (pObjectClass),
			G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
			G_STRUCT_OFFSET (LightwoodFlipperClass, flip_animation_reversed),
			NULL, NULL,
			g_cclosure_marshal_generic,
			G_TYPE_NONE, 2, G_TYPE_OBJECT, CLUTTER_TYPE_TIMELINE_DIRECTION);

}




/********************************************************
 * Function : lightwood_flipper_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/
static void lightwood_flipper_init (LightwoodFlipper *pSelf)
{
	const char *pEnvString;

	pSelf->priv = LIGHTWOOD_FLIPPER_GET_PRIVATE (pSelf);
	pSelf->priv->pFirstActor = NULL  ;
	pSelf->priv->pSecondActor  = NULL  ;
	pSelf->priv->pTransRotateA1 = NULL  ;
	pSelf->priv->pTransScaleXA1  = NULL  ;
	pSelf->priv->pTransScaleYA1  = NULL  ;
	pSelf->priv->pTransScaleZA1  = NULL  ;
	pSelf->priv->pTransRotateA2  = NULL  ;
	pSelf->priv->pTransScaleXA2  = NULL  ;
	pSelf->priv->pTransScaleYA2  = NULL  ;
	pSelf->priv->pTransScaleZA2  = NULL  ;
	UPDATE_SMV(pSelf->priv->bReversedAnimation , FALSE );
	UPDATE_SMV(pSelf->priv->bValuesReset , FALSE );
	UPDATE_SMV(pSelf->priv->fltDiscreteFactor ,0.0 );
	UPDATE_SMV(pSelf->priv->uinAnimControl , FLIPPER_ANIM_CONTROL_LAST);

#if PROVIDE_ANIM_GAP
	UPDATE_SMV(pSelf->priv->uinAnimateActor ,LIGHTWOOD_FLIPPER_ACTORS_NONE );
	UPDATE_SMV(pSelf->priv->uinAnimGapTimeoutId , 0);
#endif
	pSelf->priv->pFlipperModel = NULL ;


	/* configure the widget to provide default animation */
	lightwood_flipper_load_default_cofiguration(pSelf->priv );

	/* check for env to enable traces */
	pEnvString = g_getenv ("LIGHTWOOD_FLIPPER_DEBUG");
	if (pEnvString != NULL)
	{
		flipper_debug_flags = g_parse_debug_string (pEnvString,
				flipper_debug_keys, G_N_ELEMENTS (flipper_debug_keys));
	}

	/* create transitions to be applied in forward Flip direction */
	lightwood_flipper_define_transition_for_fwd_direction(pSelf);
	/* create transitions to be applied in backward Flip direction */
	lightwood_flipper_define_transition_for_bkwd_direction(pSelf);

	/* data associated to callback function needed for connecting the objects to signal */
	pSelf->priv->pFirstActorData = g_new0(LightwoodFlipperSignalData , 1);
	pSelf->priv->pFirstActorData->pLightwoodFlipper = pSelf ;
	pSelf->priv->pFirstActorData->inMarkerHandlerId = -1 ;
	UPDATE_SMV(pSelf->priv->pFirstActorData->bSetMarker , FALSE );
	pSelf->priv->pSecondActorData = g_new0(LightwoodFlipperSignalData , 1);
	pSelf->priv->pSecondActorData->pLightwoodFlipper = pSelf ;
	pSelf->priv->pSecondActorData->inMarkerHandlerId = -1 ;
	UPDATE_SMV(pSelf->priv->pSecondActorData->bSetMarker , FALSE );
}



/**
 * lightwood_flipper_set_discrete_factor:
 * @pLightwoodFlipper: a #LightwoodFlipper
 * @fltDiscreteFactor: the new factor value
 *
 * Update the #LightwoodFlipper:discrete-factor property
 */
void
lightwood_flipper_set_discrete_factor (LightwoodFlipper *pLightwoodFlipper,
                                       gfloat fltDiscreteFactor)
{
	LightwoodFlipperPrivate *pPriv = LIGHTWOOD_FLIPPER_GET_PRIVATE (pLightwoodFlipper);
	LIGHTWOOD_FLIPPER_PRINT("%s %d fltDiscreteFactor = %f \n",__FUNCTION__ , __LINE__ , fltDiscreteFactor);

	/* Ignore if same value is requested */
	if(pPriv->fltDiscreteFactor == fltDiscreteFactor)
	{
		return;
	}

	if(fltDiscreteFactor >= 1.0 || fltDiscreteFactor <= 0.0 )
	{
		/* set the actor to initial values if 0.0 is requested */
		if(fltDiscreteFactor <= 0.0 && NULL != pPriv->pFirstActor)
		{
			/* Reinitialize the discrete factor to 0.0 */
			UPDATE_SMV(pPriv->fltDiscreteFactor ,0.0 );
			lightwood_flipper_restore_actor_to_normal_state(pLightwoodFlipper , LIGHTWOOD_FLIPPER_BOTH_ACTOR);
		}
		if(fltDiscreteFactor != 0.0)
		{
			g_message("LightwoodFlipper: Invalid Discrete value specified : %f ",fltDiscreteFactor);
		}
		return;
	}

	if(fltDiscreteFactor > 0.0 && fltDiscreteFactor <= 0.5 && NULL != pPriv->pFirstActor)
	{
		if(NULL != pPriv->pSecondActor && TRUE == CLUTTER_ACTOR_IS_VISIBLE( pPriv->pSecondActor))
			clutter_actor_hide(pPriv->pSecondActor);

		if( FALSE == CLUTTER_ACTOR_IS_VISIBLE( pPriv->pFirstActor))
			clutter_actor_show(pPriv->pFirstActor);


		UPDATE_SMV(pPriv->fltDiscreteFactor ,fltDiscreteFactor );
		LIGHTWOOD_FLIPPER_PRINT(" Scaling Factor:\n x = %f \n y = %f \n z = %f \n Rotation = %f \n",
				FWD_DESCRETE_VALUE (fltDiscreteFactor, pPriv->dScalingMaxX , pPriv->dScalingMinX) ,
				FWD_DESCRETE_VALUE (fltDiscreteFactor, pPriv->dScalingMaxY , pPriv->dScalingMinY),
				FWD_DESCRETE_VALUE (fltDiscreteFactor, pPriv->dScalingMaxZ , pPriv->dScalingMinZ),
				FWD_DESCRETE_ANGLE (fltDiscreteFactor, pPriv->dFirstActorAngleStart , pPriv->dFirstActorAngleEnd));

		/* restore the deformed actor to normal state */
		clutter_actor_set_scale(pPriv->pFirstActor ,
				FWD_DESCRETE_VALUE (fltDiscreteFactor, pPriv->dScalingMaxX , pPriv->dScalingMinX),
				FWD_DESCRETE_VALUE (fltDiscreteFactor, pPriv->dScalingMaxY , pPriv->dScalingMinY));
		clutter_actor_set_scale_z(pPriv->pFirstActor ,
				FWD_DESCRETE_VALUE (fltDiscreteFactor, pPriv->dScalingMaxZ , pPriv->dScalingMinZ));
		clutter_actor_set_rotation_angle(pPriv->pFirstActor  ,CLUTTER_Y_AXIS,
				FWD_DESCRETE_ANGLE (fltDiscreteFactor, pPriv->dFirstActorAngleStart  ,pPriv->dFirstActorAngleEnd  ));
	}
	else if(NULL != pPriv->pSecondActor)
	{
		if(NULL != pPriv->pFirstActor && TRUE == CLUTTER_ACTOR_IS_VISIBLE( pPriv->pFirstActor))
			clutter_actor_hide(pPriv->pFirstActor);

		if( FALSE == CLUTTER_ACTOR_IS_VISIBLE( pPriv->pSecondActor))
			clutter_actor_show(pPriv->pSecondActor);

		UPDATE_SMV(pPriv->fltDiscreteFactor ,fltDiscreteFactor );
		LIGHTWOOD_FLIPPER_PRINT(" Scaling Factor:\n x = %f \n y = %f \n z = %f \n Rotation = %f \n",
				BKD_DESCRETE_VALUE(fltDiscreteFactor, pPriv->dScalingMaxX , pPriv->dScalingMinX) ,
				BKD_DESCRETE_VALUE(fltDiscreteFactor, pPriv->dScalingMaxY , pPriv->dScalingMinY),
				BKD_DESCRETE_VALUE(fltDiscreteFactor, pPriv->dScalingMaxZ , pPriv->dScalingMinZ),
				BKD_DESCRETE_ANGLE(fltDiscreteFactor, pPriv->dSecondActorAngleStart  ,pPriv->dSecondActorAngleEnd  ));

		/* restore the deformed actor to normal state */
		clutter_actor_set_scale(pPriv->pSecondActor ,
				BKD_DESCRETE_VALUE(fltDiscreteFactor, pPriv->dScalingMaxX , pPriv->dScalingMinX),
				BKD_DESCRETE_VALUE(fltDiscreteFactor, pPriv->dScalingMaxY , pPriv->dScalingMinY));
		clutter_actor_set_scale_z(pPriv->pSecondActor ,
				BKD_DESCRETE_VALUE(fltDiscreteFactor, pPriv->dScalingMaxZ , pPriv->dScalingMinZ));
		clutter_actor_set_rotation_angle(pPriv->pSecondActor  ,CLUTTER_Y_AXIS,
				BKD_DESCRETE_ANGLE(fltDiscreteFactor, pPriv->dSecondActorAngleStart  ,pPriv->dSecondActorAngleEnd  ));

		LIGHTWOOD_FLIPPER_PRINT(" Rotation angle as set =  %lf \n",clutter_actor_get_rotation_angle(pPriv->pSecondActor  ,CLUTTER_Y_AXIS)  );
	}
}


static void
lightwood_flipper_change_animation_start_values (LightwoodFlipper *pLightwoodFlipper,
                                                 ClutterTimelineDirection uinFlipDirection)
{
	LightwoodFlipperPrivate *pPriv = LIGHTWOOD_FLIPPER_GET_PRIVATE (pLightwoodFlipper);
	LightwoodFlipperData *pFlipData  = g_new0(LightwoodFlipperData , 1);
	gboolean bApplyTransOnFirst = TRUE ;
	gboolean bRightToLeftFlip = FALSE ;
	gfloat fltTimeScaleFactor ;
	if(pPriv->fltDiscreteFactor <= 0.5)
		fltTimeScaleFactor = pPriv->fltDiscreteFactor * 2;
	else
	{
		fltTimeScaleFactor = (pPriv->fltDiscreteFactor*2 - 1 );
		bApplyTransOnFirst = FALSE ;
	}

	pFlipData->uinDuration = pPriv->uinFlipDuration - (guint)(pPriv->uinFlipDuration *fltTimeScaleFactor);
	LIGHTWOOD_FLIPPER_PRINT(" Flip Scaling Factor : Actual : %f  :::: Applicable : %f \n" ,pPriv->fltDiscreteFactor ,fltTimeScaleFactor  );

	if(bApplyTransOnFirst == TRUE )
	{
		/* Explicitly Stop Transitions and read the current scaling and rotation factors */
		clutter_actor_remove_all_transitions( pPriv->pFirstActor);
		clutter_actor_get_scale( pPriv->pFirstActor , &pFlipData->dScaleX , &pFlipData->dScaleY);
		pFlipData->dScaleZ = clutter_actor_get_scale_z( pPriv->pFirstActor );
		pFlipData->dAngle =  clutter_actor_get_rotation_angle(pPriv->pFirstActor , CLUTTER_Y_AXIS);

		pFlipData->pAnimateActor  = pPriv->pFirstActor ;
		pFlipData->uinFlipDuration = pPriv->uinFlipDuration ;
		pFlipData->dScalingMinY  =   pPriv->dScalingMinY ;
		pFlipData->dScalingMaxY   =  pPriv->dScalingMaxY  ;
		pFlipData->dScalingMinX   = pPriv->dScalingMinX   ;
		pFlipData->dScalingMaxX   =   pPriv->dScalingMaxX  ;
		pFlipData->dScalingMinZ   =  pPriv->dScalingMinZ   ;
		pFlipData->dScalingMaxZ   =  pPriv->dScalingMaxZ   ;
		pFlipData->pTransRotate   =  pPriv->pTransRotateA1   ;
		pFlipData->pTransScaleX   =   pPriv->pTransScaleXA1 ;
		pFlipData->pTransScaleY   =  pPriv->pTransScaleYA1    ;
		pFlipData->pTransScaleZ =  pPriv->pTransScaleZA1  ;
		pFlipData->dStopAngle   = pPriv->dFirstActorAngleEnd   ;
		pFlipData->dStartAngle   =    pPriv->dFirstActorAngleStart ;

		LIGHTWOOD_FLIPPER_PRINT(" Flip Direction Final : %d  Initial %d \n " ,uinFlipDirection ,pPriv->uinFlipType );

		if(uinFlipDirection != pPriv->uinFlipType )
		{
			UPDATE_SMV (pPriv->uinAnimControl ,DISABLE_TRANSTION_ON_SECOND_ACTOR );
			bRightToLeftFlip = TRUE ;
			pFlipData->uinDuration = pPriv->uinFlipDuration - pFlipData->uinDuration ;
		}

		LIGHTWOOD_FLIPPER_PRINT(" First Actor : Flip Duration = %d \n X = %f  \n Y = %f \n Z = %f \n Angle = %f \n" ,pFlipData->uinDuration ,
				pFlipData->dScaleX ,pFlipData->dScaleY ,pFlipData->dScaleZ ,
				pFlipData->dAngle
		);
	}
	else
	{
		/* Explicitly Stop Transitions and read the current scaling and rotation factors */
		clutter_actor_remove_all_transitions( pPriv->pSecondActor);
		clutter_actor_get_scale( pPriv->pSecondActor , &pFlipData->dScaleX , &pFlipData->dScaleY);
		pFlipData->dScaleZ = clutter_actor_get_scale_z( pPriv->pSecondActor );
		pFlipData->dAngle =  clutter_actor_get_rotation_angle(pPriv->pSecondActor , CLUTTER_Y_AXIS);

		pFlipData->pAnimateActor  = pPriv->pSecondActor ;
		pFlipData->uinFlipDuration = pPriv->uinFlipDuration ;
		pFlipData->dScalingMinY  =   pPriv->dScalingMaxY ;
		pFlipData->dScalingMaxY   =  pPriv->dScalingMinY  ;
		pFlipData->dScalingMinX   = pPriv->dScalingMaxX   ;
		pFlipData->dScalingMaxX   =   pPriv->dScalingMinX  ;
		pFlipData->dScalingMinZ   =  pPriv->dScalingMaxZ   ;
		pFlipData->dScalingMaxZ   =  pPriv->dScalingMinZ   ;
		pFlipData->pTransRotate   =  pPriv->pTransRotateA2   ;
		pFlipData->pTransScaleX   =   pPriv->pTransScaleXA2 ;
		pFlipData->pTransScaleY   =  pPriv->pTransScaleYA2    ;
		pFlipData->pTransScaleZ =  pPriv->pTransScaleZA2  ;
		pFlipData->dStartAngle   =    pPriv->dSecondActorAngleStart ;
		pFlipData->dStopAngle   = pPriv->dSecondActorAngleEnd   ;

		if(uinFlipDirection != pPriv->uinFlipType )
		{
			UPDATE_SMV (pPriv->uinAnimControl ,ENABLE_TRANSTION_ON_FIRST_ACTOR );
			bRightToLeftFlip = TRUE ;
			pFlipData->uinDuration = pPriv->uinFlipDuration - pFlipData->uinDuration ;
		}

		LIGHTWOOD_FLIPPER_PRINT(" Flip Duration = %d \n X = %f  \n Y = %f \n Z = %f \n Angle = %f \n" ,pFlipData->uinDuration ,
				pFlipData->dScaleX ,pFlipData->dScaleY ,pFlipData->dScaleZ ,
				pFlipData->dAngle
		);
	}
	/* check if the transition time is less than 0 */
	if(pFlipData->uinDuration <= 0)
		pFlipData->uinDuration = LIGHTWOOD_FLIPPER_MINIMUM_FLIP_DURATION ;

	/* Apply the changed values and re-configure transitions */
	lightwood_flipper_redefine_transition(pFlipData ,bRightToLeftFlip );

	LIGHTWOOD_FLIPPER_PRINT(" Flip Duration Final : %d  Initial %d \n " ,pFlipData->uinDuration , pPriv->uinFlipDuration );

	g_free(pFlipData);;
}

static void
lightwood_flipper_start_flip_animation (LightwoodFlipper *pLightwoodFlipper,
                                        ClutterTimelineDirection uinFlipDirection,
                                        gboolean change_animation_start_values)
{
	LightwoodFlipperPrivate *pPriv = LIGHTWOOD_FLIPPER_GET_PRIVATE (pLightwoodFlipper);
	gboolean bMarkersAffected = FALSE ;
	/* Remove any Previously applied Transitions to the Actors before start */

	if(LIGHTWOOD_ENABLE_FIRST_ACTOR_FLIP_ONLY == pPriv->uinFlipCycle ||
			LIGHTWOOD_ENABLE_FIRST_ACTOR_FLIP_ONLY == pPriv->uinFlipCycle)
	{
		if(NULL == pPriv->pFirstActor)
		{
			g_warning("Cannot perform flip on NULL/Invalid Object");
			return;
		}
	}
	if(LIGHTWOOD_ENABLE_SECOND_ACTOR_FLIP_ONLY == pPriv->uinFlipCycle ||
			LIGHTWOOD_ENABLE_FIRST_ACTOR_FLIP_ONLY == pPriv->uinFlipCycle)
	{
		if(NULL == pPriv->pSecondActor)
		{
			g_warning("Cannot perform flip on NULL/Invalid Object");
			return;
		}
	}

	if (change_animation_start_values)
	{
		lightwood_flipper_change_animation_start_values(pLightwoodFlipper , uinFlipDirection);
		bMarkersAffected = TRUE ;
	}
	/* add markers to timeline if any set be the user */
	lightwood_flipper_add_markers_to_transition(pPriv , bMarkersAffected);


	/* check if the animation is normal start or to proceed with the discrete flip state */
	if(bMarkersAffected == FALSE)
	{
		if(LIGHTWOOD_ENABLE_SECOND_ACTOR_FLIP_ONLY != pPriv->uinFlipCycle )
			lightwood_flipper_add_animation_effects(pLightwoodFlipper , TRUE );
		else
			lightwood_flipper_add_animation_effects(pLightwoodFlipper , FALSE );
	}
	else
	{
		/* if Discrete factor is less than 0.5 then apply trans on first actor */
		if(pPriv->fltDiscreteFactor <= 0.5)
			lightwood_flipper_add_animation_effects(pLightwoodFlipper , TRUE );
		else
			lightwood_flipper_add_animation_effects(pLightwoodFlipper , FALSE );
	}

	/* emit the flip-animation-started signal */
	g_signal_emit(pLightwoodFlipper, flipper_signals[SIG_FLIPPER_ANIMATION_STARTED],0 ,
			( LIGHTWOOD_ENABLE_SECOND_ACTOR_FLIP_ONLY != pPriv->uinFlipCycle  ?
					pPriv->pFirstActor : pPriv->pSecondActor));
	UPDATE_SMV(pPriv->bValuesReset , FALSE );

}








/*****************************************************************************************************
 * Exposed API's
 *****************************************************************************************************/


/**
 * lightwood_flipper_new :
 *
 * Creates a #LightwoodFlipper widget configured with default animation
 *
 * Returns: Lightwood Flipper widget
 * Since: 1.0
 *
 */
LightwoodFlipper *lightwood_flipper_new (void)
{
	return g_object_new (LIGHTWOOD_TYPE_FLIPPER, NULL);
}



/**
 * lightwood_flipper_set_model:
 * @pLightwoodFlipper :The  reference to #LightwoodFlipper
 * @pModel : The  reference to #ThornburyModel
 *
 * setter function for model
 */
void lightwood_flipper_set_model (LightwoodFlipper *pLightwoodFlipper , ThornburyModel *pModel)
{
	LightwoodFlipperPrivate *pPriv = LIGHTWOOD_FLIPPER_GET_PRIVATE (pLightwoodFlipper);
	VALIDATE_LIGHTWOOD_FLIPPER_OBJECT(pLightwoodFlipper);
	LIGHTWOOD_FLIPPER_PRINT("%s %d ",__FUNCTION__ , __LINE__);

	if (pPriv->pFlipperModel != NULL)
	{
		g_signal_handlers_disconnect_by_func (pPriv->pFlipperModel,
				G_CALLBACK (lightwood_flipper_row_added_cb),
				pLightwoodFlipper);
		g_signal_handlers_disconnect_by_func (pPriv->pFlipperModel,
				G_CALLBACK (lightwood_flipper_row_changed_cb),
				pLightwoodFlipper);
		g_signal_handlers_disconnect_by_func (pPriv->pFlipperModel,
				G_CALLBACK (lightwood_flipper_row_removed_cb),
				pLightwoodFlipper);
		g_object_unref (pPriv->pFlipperModel);

		pPriv->pFlipperModel = NULL;
	}

	if (pModel != NULL)
	{
		g_return_if_fail (G_IS_OBJECT (pModel));

		pPriv->pFlipperModel = g_object_ref (pModel);
		g_signal_connect (pPriv->pFlipperModel,
				"row-added",
				G_CALLBACK (lightwood_flipper_row_added_cb),
				pLightwoodFlipper);

		g_signal_connect (pPriv->pFlipperModel,
				"row-changed",
				G_CALLBACK (lightwood_flipper_row_changed_cb),
				pLightwoodFlipper);

		g_signal_connect (pPriv->pFlipperModel,
				"row-removed",
				G_CALLBACK (lightwood_flipper_row_removed_cb),
				pLightwoodFlipper);
	}
	if(MODEL_LENGTH_ALLOWED == thornbury_model_get_n_rows (pPriv->pFlipperModel))
	{
		/* Fetch the actors from model to apply flip */
		lightwood_flipper_fetch_actors_from_model(pLightwoodFlipper);
	}
}



/**
 * lightwood_flipper_set_transition_time:
 * @pLightwoodFlipper: The  reference to #LightwoodFlipper
 * @uinTransitionTime: Positive transition time of type #guint
 *
 * setter function for setting the transition time for all transition.
 * Transition time should be positive integers.
 */

void lightwood_flipper_set_transition_time(LightwoodFlipper *pLightwoodFlipper , guint uinTransitionTime  )
{
	LightwoodFlipperPrivate *pPriv = LIGHTWOOD_FLIPPER_GET_PRIVATE (pLightwoodFlipper);
	/* FIXME: this seems needed to have a smooth animation with ./tests/flipper/test_flipper */
	return;
	VALIDATE_LIGHTWOOD_FLIPPER_OBJECT(pLightwoodFlipper);
	LIGHTWOOD_FLIPPER_PRINT("%s %d ",__FUNCTION__ , __LINE__);

	if(TRUE == b_lightwood_flipper_check_forward_animation_in_progress(pPriv) ||
			TRUE == b_lightwood_flipper_check_backward_animation_in_progress(pPriv))
	{
		g_warning(" Animation in progress: setting \" transition-time \"  property ignored \n"   );
	}

	if(uinTransitionTime > 0)
	{
		pPriv->uinFlipDuration = uinTransitionTime ;
		clutter_timeline_set_duration (CLUTTER_TIMELINE (pPriv->pTransRotateA1), uinTransitionTime);
		clutter_timeline_set_duration (CLUTTER_TIMELINE (pPriv->pTransScaleXA1), uinTransitionTime);
		clutter_timeline_set_duration (CLUTTER_TIMELINE (pPriv->pTransScaleYA1), uinTransitionTime);
		clutter_timeline_set_duration (CLUTTER_TIMELINE (pPriv->pTransScaleZA1), uinTransitionTime);
		clutter_timeline_set_duration (CLUTTER_TIMELINE (pPriv->pTransRotateA2), uinTransitionTime);
		clutter_timeline_set_duration (CLUTTER_TIMELINE (pPriv->pTransScaleXA2), uinTransitionTime);
		clutter_timeline_set_duration (CLUTTER_TIMELINE (pPriv->pTransScaleYA2), uinTransitionTime);
		clutter_timeline_set_duration (CLUTTER_TIMELINE (pPriv->pTransScaleZA2), uinTransitionTime);
	}
	else
	{
		g_message("Invalid Transition Time :Transition time must be greater than zero \n");
	}
}






/**
 * lightwood_flipper_set_flip_direction:
 * @pLightwoodFlipper: The  reference to #LightwoodFlipper.
 * @uinFlipDirection: Flip direction as defined in #ClutterTimelineDirection .
 *
 * setter function for setting the flip direction .
 */

void
lightwood_flipper_set_flip_direction (LightwoodFlipper *pLightwoodFlipper,
                                      ClutterTimelineDirection uinFlipDirection)
{
	LightwoodFlipperPrivate *pPriv = LIGHTWOOD_FLIPPER_GET_PRIVATE (pLightwoodFlipper);
	gdouble dTemp ;
	VALIDATE_LIGHTWOOD_FLIPPER_OBJECT(pLightwoodFlipper);
	LIGHTWOOD_FLIPPER_PRINT("%s %d uinFlipDirection = %d  ",__FUNCTION__ , __LINE__ , uinFlipDirection);

	if(TRUE == b_lightwood_flipper_check_forward_animation_in_progress(pPriv) ||
			TRUE == b_lightwood_flipper_check_backward_animation_in_progress(pPriv))
	{
		g_warning(" Animation in progress: setting \" flip-direction \"  property ignored \n"   );
		return;
	}

	if(pPriv->fltDiscreteFactor != 0.0)
	{
		g_message("Lightwood-Flipper: Trying to set flip-direction during non-zero discrete-flip-value");
		return;
	}
	if(uinFlipDirection == pPriv->uinFlipType)
		return ;
	switch(uinFlipDirection)
	{
	case CLUTTER_TIMELINE_FORWARD:
	case CLUTTER_TIMELINE_BACKWARD :
		LIGHTWOOD_FLIPPER_PRINT("%s %d uinFlipDirection = %d  \n",__FUNCTION__ , __LINE__ , uinFlipDirection);
		dTemp = pPriv->dSecondActorAngleEnd ;
		pPriv->dSecondActorAngleEnd = pPriv->dFirstActorAngleStart ;
		pPriv->dFirstActorAngleStart = dTemp ;

		dTemp = pPriv->dSecondActorAngleStart ;
		pPriv->dSecondActorAngleStart = pPriv->dFirstActorAngleEnd ;
		pPriv->dFirstActorAngleEnd = dTemp ;


		clutter_transition_set_from (pPriv->pTransRotateA1,
				G_TYPE_DOUBLE, pPriv->dFirstActorAngleStart);
		clutter_transition_set_to (pPriv->pTransRotateA1,
				G_TYPE_DOUBLE, pPriv->dFirstActorAngleEnd);
		clutter_transition_set_from (pPriv->pTransRotateA2,
				G_TYPE_DOUBLE, pPriv->dSecondActorAngleStart);
		clutter_transition_set_to (pPriv->pTransRotateA2,
				G_TYPE_DOUBLE, pPriv->dSecondActorAngleEnd);

		UPDATE_SMV(pPriv->uinFlipType , uinFlipDirection );

		break;
	default:
		g_message("Invalid Flip Direction Specified:  \n");
		break;
	}
}


/**
 * lightwood_flipper_set_flip_cycle:
 * @pLightwoodFlipper: The  reference to #LightwoodFlipper
 * @uinFlipCycle: Flip Cycle  as defined in #LightwoodFlipCycle
 *
 * setter function for setting the flip cycle.
 */
void lightwood_flipper_set_flip_cycle(LightwoodFlipper *pLightwoodFlipper , LightwoodFlipCycle uinFlipCycle)
{
	LightwoodFlipperPrivate *pPriv = LIGHTWOOD_FLIPPER_GET_PRIVATE (pLightwoodFlipper);
	LIGHTWOOD_FLIPPER_PRINT("%s %d ",__FUNCTION__ , __LINE__);
	VALIDATE_LIGHTWOOD_FLIPPER_OBJECT(pLightwoodFlipper);
	if(TRUE == b_lightwood_flipper_check_forward_animation_in_progress(pPriv) ||
			TRUE == b_lightwood_flipper_check_backward_animation_in_progress(pPriv))
	{
		g_warning(" Animation in progress: setting \" flip-cycle \"  property ignored \n"   );
	}
	UPDATE_SMV(pPriv->uinFlipCycle , uinFlipCycle );
}




/**
 * lightwood_flipper_set_animation_mode:
 * @pLightwoodFlipper: The  reference to #LightwoodFlipper
 * @uinTransitionMode: Animation Mode as defined in #ClutterAnimationMode
 *
 * setter function for setting the animation mode for flip transition.
 */

void
lightwood_flipper_set_animation_mode (LightwoodFlipper *pLightwoodFlipper,
                                      ClutterAnimationMode uinTransitionMode)
{
	LightwoodFlipperPrivate *pPriv = LIGHTWOOD_FLIPPER_GET_PRIVATE (pLightwoodFlipper);
	LIGHTWOOD_FLIPPER_PRINT("%s %d ",__FUNCTION__ , __LINE__);
	VALIDATE_LIGHTWOOD_FLIPPER_OBJECT(pLightwoodFlipper);

	if(TRUE == b_lightwood_flipper_check_forward_animation_in_progress(pPriv) ||
			TRUE == b_lightwood_flipper_check_backward_animation_in_progress(pPriv))
	{
		g_warning(" Animation in progress: setting \" animation-mode \"  property ignored \n"   );
	}
	pPriv->uinFlipAnimType = uinTransitionMode ;
	clutter_timeline_set_progress_mode (CLUTTER_TIMELINE (pPriv->pTransRotateA1), pPriv->uinFlipAnimType);
	clutter_timeline_set_progress_mode (CLUTTER_TIMELINE (pPriv->pTransScaleXA1), pPriv->uinFlipAnimType);
	clutter_timeline_set_progress_mode (CLUTTER_TIMELINE (pPriv->pTransScaleYA1), pPriv->uinFlipAnimType);
	clutter_timeline_set_progress_mode (CLUTTER_TIMELINE (pPriv->pTransScaleZA1), pPriv->uinFlipAnimType);
	clutter_timeline_set_progress_mode (CLUTTER_TIMELINE (pPriv->pTransRotateA2), pPriv->uinFlipAnimType1);
	clutter_timeline_set_progress_mode (CLUTTER_TIMELINE (pPriv->pTransScaleXA2), pPriv->uinFlipAnimType1);
	clutter_timeline_set_progress_mode (CLUTTER_TIMELINE (pPriv->pTransScaleYA2), pPriv->uinFlipAnimType1);
	clutter_timeline_set_progress_mode (CLUTTER_TIMELINE (pPriv->pTransScaleZA2), pPriv->uinFlipAnimType1);
}



/**
 * lightwood_flipper_set_transition_direction:
 * @pLightwoodFlipper: The  reference to #LightwoodFlipper
 * @uinFlipDirection: a #ClutterTimelineDirection specifying the transition direction
 *
 * setter function to set the direction of the transitions.
 */
void
lightwood_flipper_set_transition_direction (LightwoodFlipper *pLightwoodFlipper,
                                            ClutterTimelineDirection uinFlipDirection)
{
	LightwoodFlipperPrivate *pPriv = LIGHTWOOD_FLIPPER_GET_PRIVATE (pLightwoodFlipper);
	LIGHTWOOD_FLIPPER_PRINT("%s %d \n ",__FUNCTION__ , __LINE__);

	if (CLUTTER_TIMELINE_FORWARD != uinFlipDirection &&
			CLUTTER_TIMELINE_BACKWARD != uinFlipDirection )
	{
		g_warning("Invalid animation direction \n");
		return ;
	}

	if(FALSE == b_lightwood_flipper_check_forward_animation_in_progress(pPriv) &&
			FALSE == b_lightwood_flipper_check_backward_animation_in_progress(pPriv))
	{
		pPriv->uinFlipDirection = uinFlipDirection;
		clutter_timeline_set_direction (CLUTTER_TIMELINE (pPriv->pTransRotateA1), pPriv->uinFlipDirection);
		clutter_timeline_set_direction (CLUTTER_TIMELINE (pPriv->pTransScaleXA1), pPriv->uinFlipDirection);
		clutter_timeline_set_direction (CLUTTER_TIMELINE (pPriv->pTransScaleYA1), pPriv->uinFlipDirection);
		clutter_timeline_set_direction (CLUTTER_TIMELINE (pPriv->pTransScaleZA1), pPriv->uinFlipDirection);
		clutter_timeline_set_direction (CLUTTER_TIMELINE (pPriv->pTransRotateA2), pPriv->uinFlipDirection);
		clutter_timeline_set_direction (CLUTTER_TIMELINE (pPriv->pTransScaleXA2), pPriv->uinFlipDirection);
		clutter_timeline_set_direction (CLUTTER_TIMELINE (pPriv->pTransScaleYA2), pPriv->uinFlipDirection);
		clutter_timeline_set_direction (CLUTTER_TIMELINE (pPriv->pTransScaleZA2), pPriv->uinFlipDirection);
	}
	else
	{
		g_warning("Animation in progress : \"transition-direction\" property ignored \n" );
	}
}




/**
 * lightwood_flipper_stop_animation:
 * @pLightwoodFlipper :The  reference to #LightwoodFlipper
 *
 * Function to stop flip animation on request.
 * All the transitions applied on the actor will be canceled
 * Immediately and the actor being animated will be restored to
 * their default states.
 */
void lightwood_flipper_stop_animation (LightwoodFlipper *pLightwoodFlipper)
{
	LightwoodFlipperPrivate *pPriv = LIGHTWOOD_FLIPPER_GET_PRIVATE (pLightwoodFlipper);
	if(!G_IS_OBJECT( pPriv->pFlipperModel) ||
			(MODEL_LENGTH_ALLOWED != thornbury_model_get_n_rows(pPriv->pFlipperModel) ||
					NULL == pPriv->pFirstActor || NULL == pPriv->pSecondActor )			)
	{
		g_warning("Parameters not Defined: Set Model to Flip : lightwood_flipper_stop_animation() ignored");
		return ;
	}

	/* check whether the animation is still in progress */
	if(TRUE == b_lightwood_flipper_check_forward_animation_in_progress(pPriv) ||
			TRUE == b_lightwood_flipper_check_backward_animation_in_progress(pPriv))
	{
		clutter_actor_remove_all_transitions( pPriv->pFirstActor);
		clutter_actor_remove_all_transitions( pPriv->pSecondActor);

		/* Disconnect marker signals if any */
		lightwood_flipper_disconnect_marker_signals( pLightwoodFlipper ,
				LIGHTWOOD_FLIPPER_BOTH_ACTOR);

		lightwood_flipper_restore_actor_to_normal_state(pLightwoodFlipper , LIGHTWOOD_FLIPPER_BOTH_ACTOR);
		UPDATE_SMV(pPriv->fltDiscreteFactor ,0.0 );
	}
}

/**
 * lightwood_flipper_reverse_animation:
 * @pLightwoodFlipper :The  reference to #LightwoodFlipper
 *
 *This Function will reverse flip animation direction
 *from same point provided animation is in progress.
 */

void lightwood_flipper_reverse_animation (LightwoodFlipper *pLightwoodFlipper)
{
	LightwoodFlipperPrivate *pPriv = LIGHTWOOD_FLIPPER_GET_PRIVATE (pLightwoodFlipper);
	gboolean bReverseAck;

	if(!G_IS_OBJECT( pPriv->pFlipperModel) ||
			(MODEL_LENGTH_ALLOWED != thornbury_model_get_n_rows(pPriv->pFlipperModel) ))
	{
		g_warning("Parameters not Defined: Set Model to Flip: lightwood_flipper_reverse_animation() ignored");
		return ;
	}
	bReverseAck = b_lightwood_flipper_reverse_flip_direction(pLightwoodFlipper);
	if(FALSE == bReverseAck)
	{
		g_warning("No Animation in progress: lightwood_flipper_reverse_animation() ignored");
	}
}

/**
 * lightwood_flipper_rewind_animation:
 * @pLightwoodFlipper :The  reference to #LightwoodFlipper
 *
 * This Function will rewind flip animation for the actor being animated
 * from start provided animation is in progress.
 */

void lightwood_flipper_rewind_animation (LightwoodFlipper *pLightwoodFlipper)
{

	LightwoodFlipperPrivate *pPriv = LIGHTWOOD_FLIPPER_GET_PRIVATE (pLightwoodFlipper);
	if(!G_IS_OBJECT( pPriv->pFlipperModel) ||
			(MODEL_LENGTH_ALLOWED != thornbury_model_get_n_rows(pPriv->pFlipperModel)  ))
	{
		g_warning("Parameters not Defined: Set Model to Flip : lightwood_flipper_rewind_animation() ignored");
		return ;
	}

	/* check whether the animation is still in progress */
	if(TRUE == b_lightwood_flipper_check_forward_animation_in_progress(pPriv) ||
			TRUE == b_lightwood_flipper_check_backward_animation_in_progress(pPriv))
	{
		/* rewind animation for the actor*/
		if(NULL != pPriv->pFirstActor && TRUE == CLUTTER_ACTOR_IS_VISIBLE( pPriv->pFirstActor))
		{
			clutter_timeline_rewind(CLUTTER_TIMELINE(pPriv->pTransRotateA1));
			clutter_timeline_rewind(CLUTTER_TIMELINE(pPriv->pTransScaleXA1));
			clutter_timeline_rewind(CLUTTER_TIMELINE(pPriv->pTransScaleYA1));
			clutter_timeline_rewind(CLUTTER_TIMELINE(pPriv->pTransScaleZA1));
		}
		else if(NULL != pPriv->pSecondActor && TRUE == CLUTTER_ACTOR_IS_VISIBLE( pPriv->pSecondActor))
		{
			clutter_timeline_rewind(CLUTTER_TIMELINE(pPriv->pTransRotateA2));
			clutter_timeline_rewind(CLUTTER_TIMELINE(pPriv->pTransScaleXA2));
			clutter_timeline_rewind(CLUTTER_TIMELINE(pPriv->pTransScaleYA2));
			clutter_timeline_rewind(CLUTTER_TIMELINE(pPriv->pTransScaleZA2));
		}
	}
}


/**
 * lightwood_flipper_start_animation:
 * @pLightwoodFlipper :The  reference to #LightwoodFlipper
 *
 * This Function will start flip from the first
 * actor provided animation is not in progress.
 *
 * Note: It is preferable not to apply any simultaneous transitions
 * 	on the specified objects during	flip animation to avoid
 * 	adverse transitions effects.
 */

void lightwood_flipper_start_animation (LightwoodFlipper *pLightwoodFlipper)
{
	LightwoodFlipperPrivate *pPriv = LIGHTWOOD_FLIPPER_GET_PRIVATE (pLightwoodFlipper);
	if(!G_IS_OBJECT( pPriv->pFlipperModel) ||
			(MODEL_LENGTH_ALLOWED != thornbury_model_get_n_rows(pPriv->pFlipperModel) ))
	{
		g_warning("Parameters not Defined: Set Model before starting Flip");
		return ;
	}
	if(FALSE == b_lightwood_flipper_check_forward_animation_in_progress(pPriv) &&
			FALSE == b_lightwood_flipper_check_backward_animation_in_progress(pPriv))
	{
		lightwood_flipper_start_flip_animation(pLightwoodFlipper, CLUTTER_TIMELINE_FORWARD, FALSE);
	}
	else
	{
		g_warning("Animation in progress: can't start flipping");
	}
}



/**
 * lightwood_flipper_proceed_flip_anim:
 * @pLightwoodFlipper :The  reference to #LightwoodFlipper
 * @uinFlipDirection: a #ClutterTimelineDirection specifying the flip-direction
 *
 * This Function will start flip for the first
 * actor from the scaled state provided
 * 1. 	Property #LightwoodFlipper:discrete-factor is set between 0.0 and 1.0.
 * 2.	animation is not in progress.
 *
 * Note: It is preferable not to apply any simultaneous transitions
 * 	on the specified objects during	flip animation to avoid
 * 	adverse transitions effects.
 */

void lightwood_flipper_proceed_flip_anim (LightwoodFlipper *pLightwoodFlipper,
                                          ClutterTimelineDirection uinFlipDirection)
{

	LightwoodFlipperPrivate *pPriv = LIGHTWOOD_FLIPPER_GET_PRIVATE (pLightwoodFlipper);
	if(!G_IS_OBJECT( pPriv->pFlipperModel) ||
			(MODEL_LENGTH_ALLOWED != thornbury_model_get_n_rows(pPriv->pFlipperModel) ))
	{
		g_warning("Parameters not Defined: Set Model before starting Flip");
		return ;
	}
	if(FALSE == b_lightwood_flipper_check_forward_animation_in_progress(pPriv) &&
			FALSE == b_lightwood_flipper_check_backward_animation_in_progress(pPriv))
	{
		if( pPriv->fltDiscreteFactor == 0.0)
		{
			g_message("Lightwood-Flipper: No proper scaling factor specified to proceed flip\n");
			return;
		}
		if(CLUTTER_TIMELINE_FORWARD != uinFlipDirection &&
				CLUTTER_TIMELINE_BACKWARD != uinFlipDirection )
		{
			g_message("Lightwood-Flipper: Invalid flip-direction specified to proceed flip\n");
			return;
		}
		lightwood_flipper_start_flip_animation (pLightwoodFlipper , uinFlipDirection, TRUE);
	}
	else
	{
		g_warning("Animation in progress : lightwood_flipper_proceed_flip_anim() ignored");
	}
}



/**
 * lightwood_flipper_restart_animation:
 * @pLightwoodFlipper :The  reference to #LightwoodFlipper
 *
 * This Function will restart flip from the first
 * actor provided animation is still in progress.
 */


void lightwood_flipper_restart_animation (LightwoodFlipper *pLightwoodFlipper)
{
	LightwoodFlipperPrivate *pPriv = LIGHTWOOD_FLIPPER_GET_PRIVATE (pLightwoodFlipper);
	if(!G_IS_OBJECT( pPriv->pFlipperModel) ||
			(MODEL_LENGTH_ALLOWED != thornbury_model_get_n_rows(pPriv->pFlipperModel) ))
	{
		g_warning("Parameters not Defined: Set Model before starting Flip");
		return ;
	}
	if(TRUE == b_lightwood_flipper_check_forward_animation_in_progress(pPriv) ||
			TRUE == b_lightwood_flipper_check_backward_animation_in_progress(pPriv))
	{
		/* Stop ongoing transitions and reset the Transitions to the initial Value */
		if(LIGHTWOOD_ENABLE_SECOND_ACTOR_FLIP_ONLY != pPriv->uinFlipCycle )
			lightwood_flipper_change_forward_animation_direction(pPriv , TRUE );
		if(LIGHTWOOD_ENABLE_FIRST_ACTOR_FLIP_ONLY != pPriv->uinFlipCycle )
			lightwood_flipper_change_backward_animation_direction(pPriv , TRUE );

		if(NULL != pPriv->pFirstActor && FALSE == CLUTTER_ACTOR_IS_VISIBLE(pPriv->pFirstActor))
		{
			clutter_actor_show(pPriv->pFirstActor);
		}
		/* restart the flip animation from the beginning */
		if(LIGHTWOOD_ENABLE_SECOND_ACTOR_FLIP_ONLY != pPriv->uinFlipCycle )
			lightwood_flipper_add_animation_effects(pLightwoodFlipper , TRUE);
		else
			lightwood_flipper_add_animation_effects(pLightwoodFlipper , FALSE);

		/* emit the flip-animation-started signal */
		g_signal_emit(pLightwoodFlipper, flipper_signals[SIG_FLIPPER_ANIMATION_STARTED],0,
				( LIGHTWOOD_ENABLE_SECOND_ACTOR_FLIP_ONLY != pPriv->uinFlipCycle  ?
						pPriv->pFirstActor : pPriv->pSecondActor));
	}
	else
	{
		g_warning("Animation in progress: can't start flipping");
	}
}

/**
 * lightwood_flipper_get_transition_time:
 * @self: a #LightwoodFlipper
 *
 * Return the #LightwoodFlipper:transition-time property
 *
 * Returns: the value of #LightwoodFlipper:transition-time
 */
guint
lightwood_flipper_get_transition_time (LightwoodFlipper *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_FLIPPER (self), 0);

  return self->priv->uinFlipDuration;
}

/**
 * lightwood_flipper_get_animation_mode:
 * @self: a #LightwoodFlipper
 *
 * Return the #LightwoodFlipper:animation-mode property
 *
 * Returns: the value of #LightwoodFlipper:animation-mode
 */
ClutterAnimationMode
lightwood_flipper_get_animation_mode (LightwoodFlipper *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_FLIPPER (self), CLUTTER_LINEAR);

  return self->priv->uinFlipAnimType;
}

/**
 * lightwood_flipper_get_animation_direction:
 * @self: a #LightwoodFlipper
 *
 * Return the #LightwoodFlipper:transition-direction property
 *
 * Returns: the value of #LightwoodFlipper:transition-direction
 */
ClutterTimelineDirection
lightwood_flipper_get_transition_direction (LightwoodFlipper *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_FLIPPER (self), CLUTTER_TIMELINE_FORWARD);

  return self->priv->uinFlipDirection;
}
/**
 * lightwood_flipper_get_model:
 * @self: a #LightwoodFlipper
 *
 * Return the #LightwoodFlipper:model property
 *
 * Returns: (transfer none): the value of #LightwoodFlipper:model
 */
ThornburyModel *
lightwood_flipper_get_model (LightwoodFlipper *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_FLIPPER (self), NULL);

  return self->priv->pFlipperModel;
}

/**
 * lightwood_flipper_get_flip_direction:
 * @self: a #LightwoodFlipper
 *
 * Return the #LightwoodFlipper:flip-direction property
 *
 * Returns: the value of #LightwoodFlipper:flip-direction
 */
ClutterTimelineDirection
lightwood_flipper_get_flip_direction (LightwoodFlipper *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_FLIPPER (self), CLUTTER_TIMELINE_FORWARD);

  return self->priv->uinFlipType;
}

/**
 * lightwood_flipper_get_flip_cycle:
 * @self: a #LightwoodFlipper
 *
 * Return the #LightwoodFlipper:flip-cycle property
 *
 * Returns: the value of #LightwoodFlipper:flip-cycle
 */
LightwoodFlipCycle
lightwood_flipper_get_flip_cycle (LightwoodFlipper *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_FLIPPER (self), LIGHTWOOD_ENABLE_FIRST_ACTOR_FLIP_ONLY);

  return self->priv->uinFlipCycle;
}

/**
 * lightwood_flipper_get_scale_x_min:
 * @self: a #LightwoodFlipper
 *
 * Return the #LightwoodFlipper:scale-x-min property
 *
 * Returns: the value of #LightwoodFlipper:scale-x-min
 */
gdouble
lightwood_flipper_get_scale_x_min (LightwoodFlipper *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_FLIPPER (self), 0);

  return self->priv->dScalingMinX;
}

/**
 * lightwood_flipper_set_scale_x_min:
 * @self: a #LightwoodFlipper
 * @scale: the new scale value
 *
 * Update the #LightwoodFlipper:scale-x-min property.
 */
void
lightwood_flipper_set_scale_x_min (LightwoodFlipper *self,
                                   gdouble scale)
{
  g_return_if_fail (LIGHTWOOD_IS_FLIPPER (self));

  set_scale_x_min (self, scale);
  g_object_notify (G_OBJECT (self), "scale-x-min");
}

/**
 * lightwood_flipper_get_scale_x_max:
 * @self: a #LightwoodFlipper
 *
 * Return the #LightwoodFlipper:scale-x-max property
 *
 * Returns: the value of #LightwoodFlipper:scale-x-max
 */
gdouble
lightwood_flipper_get_scale_x_max (LightwoodFlipper *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_FLIPPER (self), 0);

  return self->priv->dScalingMaxX;
}

/**
 * lightwood_flipper_set_scale_x_max:
 * @self: a #LightwoodFlipper
 * @scale: the new scale value
 *
 * Update the #LightwoodFlipper:scale-x-max property.
 */
void
lightwood_flipper_set_scale_x_max (LightwoodFlipper *self,
                                   gdouble scale)
{
  g_return_if_fail (LIGHTWOOD_IS_FLIPPER (self));

  set_scale_x_max (self, scale);
  g_object_notify (G_OBJECT (self), "scale-x-max");
}

/**
 * lightwood_flipper_get_scale_y_min:
 * @self: a #LightwoodFlipper
 *
 * Return the #LightwoodFlipper:scale-y-min property
 *
 * Returns: the value of #LightwoodFlipper:scale-y-min
 */
gdouble
lightwood_flipper_get_scale_y_min (LightwoodFlipper *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_FLIPPER (self), 0);

  return self->priv->dScalingMinY;
}

/**
 * lightwood_flipper_set_scale_y_min:
 * @self: a #LightwoodFlipper
 * @scale: the new scale value
 *
 * Update the #LightwoodFlipper:scale-y-min property.
 */
void
lightwood_flipper_set_scale_y_min (LightwoodFlipper *self,
                                   gdouble scale)
{
  g_return_if_fail (LIGHTWOOD_IS_FLIPPER (self));

  set_scale_y_min (self, scale);
  g_object_notify (G_OBJECT (self), "scale-y-min");
}

/**
 * lightwood_flipper_get_scale_y_max:
 * @self: a #LightwoodFlipper
 *
 * Return the #LightwoodFlipper:scale-y-max property
 *
 * Returns: the value of #LightwoodFlipper:scale-y-max
 */
gdouble
lightwood_flipper_get_scale_y_max (LightwoodFlipper *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_FLIPPER (self), 0);

  return self->priv->dScalingMaxY;
}

/**
 * lightwood_flipper_set_scale_y_max:
 * @self: a #LightwoodFlipper
 * @scale: the new scale value
 *
 * Update the #LightwoodFlipper:scale-y-max property.
 */
void
lightwood_flipper_set_scale_y_max (LightwoodFlipper *self,
                                   gdouble scale)
{
  g_return_if_fail (LIGHTWOOD_IS_FLIPPER (self));

  set_scale_y_max (self, scale);
  g_object_notify (G_OBJECT (self), "scale-y-max");
}

/**
 * lightwood_flipper_get_scale_z_min:
 * @self: a #LightwoodFlipper
 *
 * Return the #LightwoodFlipper:scale-z-min property
 *
 * Returns: the value of #LightwoodFlipper:scale-z-min
 */
gdouble
lightwood_flipper_get_scale_z_min (LightwoodFlipper *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_FLIPPER (self), 0);

  return self->priv->dScalingMinZ;
}

/**
 * lightwood_flipper_set_scale_z_min:
 * @self: a #LightwoodFlipper
 * @scale: the new scale value
 *
 * Update the #LightwoodFlipper:scale-z-min property.
 */
void
lightwood_flipper_set_scale_z_min (LightwoodFlipper *self,
                                   gdouble scale)
{
  g_return_if_fail (LIGHTWOOD_IS_FLIPPER (self));

  set_scale_z_min (self, scale);
  g_object_notify (G_OBJECT (self), "scale-z-min");
}

/**
 * lightwood_flipper_get_scale_z_max:
 * @self: a #LightwoodFlipper
 *
 * Return the #LightwoodFlipper:scale-z-max property
 *
 * Returns: the value of #LightwoodFlipper:scale-z-max
 */
gdouble
lightwood_flipper_get_scale_z_max (LightwoodFlipper *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_FLIPPER (self), 0);

  return self->priv->dScalingMaxZ;
}

/**
 * lightwood_flipper_set_scale_z_max:
 * @self: a #LightwoodFlipper
 * @scale: the new scale value
 *
 * Update the #LightwoodFlipper:scale-z-max property.
 */
void
lightwood_flipper_set_scale_z_max (LightwoodFlipper *self,
                                   gdouble scale)
{
  g_return_if_fail (LIGHTWOOD_IS_FLIPPER (self));

  set_scale_z_max (self, scale);
  g_object_notify (G_OBJECT (self), "scale-z-max");
}

/**
 * lightwood_flipper_get_forward_flip_start_angle:
 * @self: a #LightwoodFlipper
 *
 * Return the #LightwoodFlipper:forward-flip-start-angle property
 *
 * Returns: the value of #LightwoodFlipper:forward-flip-start-angle
 */
gdouble
lightwood_flipper_get_forward_flip_start_angle (LightwoodFlipper *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_FLIPPER (self), 0);

  return self->priv->dFirstActorAngleStart;
}

/**
 * lightwood_flipper_set_forward_flip_start_angle:
 * @self: a #LightwoodFlipper
 * @angle: the new angle value
 *
 * Update the #LightwoodFlipper:forward-flip-start-angle property
 */
void
lightwood_flipper_set_forward_flip_start_angle (LightwoodFlipper *self,
                                                gdouble angle)
{
  g_return_if_fail (LIGHTWOOD_IS_FLIPPER (self));

  set_forward_flip_start_angle (self, angle);
  g_object_notify (G_OBJECT (self), "forward-flip-start-angle");
}

/**
 * lightwood_flipper_get_forward_flip_end_angle:
 * @self: a #LightwoodFlipper
 *
 * Return the #LightwoodFlipper:forward-flip-end-angle property
 *
 * Returns: the value of #LightwoodFlipper:forward-flip-end-angle
 */
gdouble
lightwood_flipper_get_forward_flip_end_angle (LightwoodFlipper *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_FLIPPER (self), 0);

  return self->priv->dFirstActorAngleEnd;
}

/**
 * lightwood_flipper_set_forward_flip_end_angle:
 * @self: a #LightwoodFlipper
 * @angle: the new angle value
 *
 * Update the #LightwoodFlipper:forward-flip-end-angle property
 */
void
lightwood_flipper_set_forward_flip_end_angle (LightwoodFlipper *self,
                                              gdouble angle)
{
  g_return_if_fail (LIGHTWOOD_IS_FLIPPER (self));

  set_forward_flip_end_angle (self, angle);
  g_object_notify (G_OBJECT (self), "forward-flip-end-angle");
}

/**
 * lightwood_flipper_get_backward_flip_start_angle:
 * @self: a #LightwoodFlipper
 *
 * Return the #LightwoodFlipper:backward-flip-start-angle property
 *
 * Returns: the value of #LightwoodFlipper:backward-flip-start-angle
 */
gdouble
lightwood_flipper_get_backward_flip_start_angle (LightwoodFlipper *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_FLIPPER (self), 0);

  return self->priv->dSecondActorAngleStart;
}

/**
 * lightwood_flipper_set_backward_flip_start_angle:
 * @self: a #LightwoodFlipper
 * @angle: the new angle value
 *
 * Update the #LightwoodFlipper:backward-flip-start-angle property
 */
void
lightwood_flipper_set_backward_flip_start_angle (LightwoodFlipper *self,
                                                 gdouble angle)
{
  g_return_if_fail (LIGHTWOOD_IS_FLIPPER (self));

  set_backward_flip_start_angle (self, angle);
  g_object_notify (G_OBJECT (self), "backward-flip-start-angle");
}

/**
 * lightwood_flipper_get_backward_flip_end_angle:
 * @self: a #LightwoodFlipper
 *
 * Return the #LightwoodFlipper:backward-flip-end-angle property
 *
 * Returns: the value of #LightwoodFlipper:backward-flip-end-angle
 */
gdouble
lightwood_flipper_get_backward_flip_end_angle (LightwoodFlipper *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_FLIPPER (self), 0);

  return self->priv->dSecondActorAngleEnd;
}

/**
 * lightwood_flipper_set_backward_flip_end_angle:
 * @self: a #LightwoodFlipper
 * @angle: the new angle value
 *
 * Update the #LightwoodFlipper:backward-flip-end-angle property
 */
void
lightwood_flipper_set_backward_flip_end_angle (LightwoodFlipper *self,
                                               gdouble angle)
{
  g_return_if_fail (LIGHTWOOD_IS_FLIPPER (self));

  set_backward_flip_end_angle (self, angle);
  g_object_notify (G_OBJECT (self), "backward-flip-end-angle");
}

/**
 * lightwood_flipper_get_first_actor_mark:
 * @self: a #LightwoodFlipper
 *
 * Return the #LightwoodFlipper:first-actor-mark property
 *
 * Returns: the value of #LightwoodFlipper:first-actor-mark
 */
guint
lightwood_flipper_get_first_actor_mark (LightwoodFlipper *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_FLIPPER (self), 0);

  return self->priv->pFirstActorData->uinMarkerTime;
}

/**
 * lightwood_flipper_set_first_actor_mark:
 * @self: a #LightwoodFlipper
 * @mark: the new mark value
 *
 * Update the #LightwoodFlipper:first-actor-mark property
 */
void
lightwood_flipper_set_first_actor_mark (LightwoodFlipper *self,
                                        guint mark)
{
  g_return_if_fail (LIGHTWOOD_IS_FLIPPER (self));

  set_first_actor_mark (self, mark);
  g_object_notify (G_OBJECT (self), "first-actor-mark");
}

/**
 * lightwood_flipper_get_second_actor_mark:
 * @self: a #LightwoodFlipper
 *
 * Return the #LightwoodFlipper:second-actor-mark property
 *
 * Returns: the value of #LightwoodFlipper:second-actor-mark
 */
guint
lightwood_flipper_get_second_actor_mark (LightwoodFlipper *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_FLIPPER (self), 0);

  return self->priv->pSecondActorData->uinMarkerTime;
}

/**
 * lightwood_flipper_set_second_actor_mark:
 * @self: a #LightwoodFlipper
 * @mark: the new mark value
 *
 * Update the #LightwoodFlipper:second-actor-mark property
 */
void
lightwood_flipper_set_second_actor_mark (LightwoodFlipper *self,
                                        guint mark)
{
  g_return_if_fail (LIGHTWOOD_IS_FLIPPER (self));

  set_second_actor_mark (self, mark);
  g_object_notify (G_OBJECT (self), "second-actor-mark");
}

/**
 * lightwood_flipper_get_animation_in_progress:
 * @self: a #LightwoodFlipper
 *
 * Return the #LightwoodFlipper:animation-in-progress property
 *
 * Returns: the value of #LightwoodFlipper:animation-in-progress
 */
gboolean
lightwood_flipper_get_animation_in_progress (LightwoodFlipper *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_FLIPPER (self), FALSE);

  return get_animation_in_progress (self);
}

/**
 * lightwood_flipper_get_x_pivot_point:
 * @self: a #LightwoodFlipper
 *
 * Return the #LightwoodFlipper:x-pivot-point property
 *
 * Returns: the value of #LightwoodFlipper:x-pivot-point
 */
gfloat
lightwood_flipper_get_x_pivot_point (LightwoodFlipper *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_FLIPPER (self), 0.0);

  return self->priv->fltPivotX;
}

/**
 * lightwood_flipper_set_x_pivot_point:
 * @self: a #LightwoodFlipper
 * @pivot: the new pivot point value
 *
 * Update the #LightwoodFlipper:x-pivot-point property
 */
void
lightwood_flipper_set_x_pivot_point (LightwoodFlipper *self,
                                     gfloat pivot)
{
  g_return_if_fail (LIGHTWOOD_IS_FLIPPER (self));

  set_x_pivot_point (self, pivot);
  g_object_notify (G_OBJECT (self), "x-pivot-point");
}

/**
 * lightwood_flipper_get_y_pivot_point:
 * @self: a #LightwoodFlipper
 *
 * Return the #LightwoodFlipper:y-pivot-point property
 *
 * Returns: the value of #LightwoodFlipper:y-pivot-point
 */
gfloat
lightwood_flipper_get_y_pivot_point (LightwoodFlipper *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_FLIPPER (self), 0.0);

  return self->priv->fltPivotY;
}

/**
 * lightwood_flipper_set_y_pivot_point:
 * @self: a #LightwoodFlipper
 * @pivot: the new pivot point value
 *
 * Update the #LightwoodFlipper:y-pivot-point property
 */
void
lightwood_flipper_set_y_pivot_point (LightwoodFlipper *self,
                                     gfloat pivot)
{
  g_return_if_fail (LIGHTWOOD_IS_FLIPPER (self));

  set_y_pivot_point (self, pivot);
  g_object_notify (G_OBJECT (self), "y-pivot-point");
}

/**
 * lightwood_flipper_get_discrete_factor:
 * @self: a #LightwoodFlipper
 *
 * Return the #LightwoodFlipper:discrete-factor property
 *
 * Returns: the value of #LightwoodFlipper:discrete-factor
 */
gfloat
lightwood_flipper_get_discrete_factor (LightwoodFlipper *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_FLIPPER (self), 0.0);

  return self->priv->fltDiscreteFactor;
}
