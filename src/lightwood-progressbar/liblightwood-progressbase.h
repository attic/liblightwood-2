/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* liblightwood-progressbase.h */

#ifndef __LIGHTWOOD_PROGRESS_BASE_H__
#define __LIGHTWOOD_PROGRESS_BASE_H__

#include <glib-object.h>
#include<stdlib.h>
#include <clutter/clutter.h>

G_BEGIN_DECLS

#define LIGHTWOOD_TYPE_PROGRESS_BASE lightwood_progress_base_get_type ()
G_DECLARE_DERIVABLE_TYPE (LightwoodProgressBase, lightwood_progress_base, LIGHTWOOD, PROGRESS_BASE, ClutterActor)

struct _LightwoodProgressBaseClass
{
  ClutterActorClass parent_class;

  void (* seek_end) (LightwoodProgressBase *self,
                     gfloat seekPos);
  void (* seek_start) (LightwoodProgressBase *self,
                       gfloat seekPos);
  void (* play_requested) (LightwoodProgressBase *self);
  void (* pause_requested) (LightwoodProgressBase *self);
  void (* progress_updated) (LightwoodProgressBase *self,
                             gfloat seekPos);
  void (* tap_to_seek) (LightwoodProgressBase *self,
                        gfloat seekPos);
};

ClutterActor *lightwood_progress_base_get_play_pause_group (LightwoodProgressBase *self);
ClutterAction *lightwood_progress_base_get_drag_action (LightwoodProgressBase *self);

gboolean lightwood_progress_base_get_play_state (LightwoodProgressBase *self);
void lightwood_progress_base_set_play_state (LightwoodProgressBase *self,
                                             gboolean state);

gdouble lightwood_progress_base_get_current_duration (LightwoodProgressBase *self);
void lightwood_progress_base_set_current_duration (LightwoodProgressBase *self,
                                                   gdouble duration);

gdouble lightwood_progress_base_get_buffer_fill (LightwoodProgressBase *self);
void lightwood_progress_base_set_buffer_fill (LightwoodProgressBase *self,
                                              gdouble fill);

gdouble lightwood_progress_base_get_offset (LightwoodProgressBase *self);
void lightwood_progress_base_set_offset (LightwoodProgressBase *self,
                                         gdouble offset);

gdouble lightwood_progress_base_get_seek_button_size (LightwoodProgressBase *self);
void lightwood_progress_base_set_seek_button_size (LightwoodProgressBase *self,
                                                   gdouble size);

G_END_DECLS

#endif /* __LIGHTWOOD_PROGRESS_BASE_H__ */
