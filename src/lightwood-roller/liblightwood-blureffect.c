/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION:liblightwood-blureffect
 * @Title:LightwoodBlurEffect
 * @short_description: Blur effect implemented using #ClutterShaderEffect
 *
 * #LightwoodBlurEffect will give the blur effect to the text or Icon.The blur effect 
 * is applied using GLSL. It is advisable to apply a blur effect if required. And 
 * to make it more efficient it is better to instatiate in the class_init function.
 * A small code snippet used in #LightwoodRoller to illustrate the usage.
 * 
 * ## Sample C Code
 * |[<!-- language="C" -->
 *
 *static void
 *adjustment_value_notify_cb (MxAdjustment    *adjustment,
 *                            GParamSpec      *pspec,
 *                            LightwoodRoller       *roller)
 *{
 *	ROLLER_DEBUG("%s",__FUNCTION__);
 *  LightwoodRollerPrivate *priv = roller->priv;
 *
 *  if (priv->motion_blur_enabled)
 *  {
 *    gdouble start;
 *    guint blur_steps;
 *
 *    if (priv->blur_effect == NULL)
 *    {
 *      priv->blur_effect = g_object_ref_sink (lightwood_blur_effect_new ());
 *    }
 *
 *    start = mx_adjustment_get_value (adjustment);
 *    blur_steps = CLAMP (ABS (start - priv->previous_value) / 2, 0, 20);
 *    priv->previous_value = start;
 *
 *    lightwood_blur_effect_set_steps (LIGHTWOOD_BLUR_EFFECT (priv->blur_effect),
 *	blur_steps);
 *
 *    if (clutter_actor_get_effect (CLUTTER_ACTOR (roller),
 *	  BLUR_EFFECT_NAME) == NULL)
 *    {
 *      clutter_actor_add_effect_with_name (CLUTTER_ACTOR (roller),
 *	  BLUR_EFFECT_NAME,
 *	  priv->blur_effect);
 *    }
 *  }
 *
 *  clutter_actor_queue_redraw (CLUTTER_ACTOR (roller));
 *}
 *
 * // Finally, on disposing of your object:
 * g_clear_object (&priv->blur_effect);
 * ]|
 *
 */

#include "liblightwood-blureffect.h"

G_DEFINE_TYPE (LightwoodBlurEffect, lightwood_blur_effect, CLUTTER_TYPE_SHADER_EFFECT)

#define BLUR_EFFECT_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), LIGHTWOOD_TYPE_BLUR_EFFECT, LightwoodBlurEffectPrivate))

static gchar *shader_source =
"uniform sampler2D tex;\n"
"uniform float texheight;\n"
"uniform int steps;\n"
"\n"
"void main () {\n"
"    vec4 color = texture2D(tex, cogl_tex_coord_in[0].st);\n"
"\n"
"    for (int i = 1; i <= steps; i++) {\n"
"        float alpha = 0.5 / float (i);\n"
"        vec2 distance = vec2(0, float (i) / texheight);\n"
"\n"
"        // top\n"
"        color = mix(color,\n"
"                    texture2D(tex, cogl_tex_coord_in[0].st - distance),\n"
"                    alpha);\n"
"\n"
"        // bottom\n"
"        color = mix(color,\n"
"                    texture2D(tex, cogl_tex_coord_in[0].st + distance),\n"
"                    alpha);\n"
"    }\n"
"\n"
"    gl_FragColor = color;\n"
"}";

struct _LightwoodBlurEffectPrivate
{
  guint steps;
};

static gboolean
shader_pre_paint (ClutterEffect *effect)
{
  LightwoodBlurEffectPrivate *priv = LIGHTWOOD_BLUR_EFFECT (effect)->priv;
  ClutterShaderEffect *shader = CLUTTER_SHADER_EFFECT (effect);
  ClutterActor *actor = clutter_actor_meta_get_actor (CLUTTER_ACTOR_META (effect));
  float tex_height;

  if (!clutter_actor_meta_get_enabled (CLUTTER_ACTOR_META (effect)))
    return FALSE;

  clutter_shader_effect_set_shader_source (shader, shader_source);

  tex_height = clutter_actor_get_height (actor);

  clutter_shader_effect_set_uniform (shader, "tex", G_TYPE_INT, 1, 0);
  clutter_shader_effect_set_uniform (shader, "steps", G_TYPE_INT, 1, priv->steps);
  clutter_shader_effect_set_uniform (shader, "texheight", G_TYPE_FLOAT, 1, tex_height);

  return CLUTTER_EFFECT_CLASS (lightwood_blur_effect_parent_class)->pre_paint (effect);
}

/* ClutterOffscreenEffect implementation */

static CoglHandle
offscreen_defect_create_texture (ClutterOffscreenEffect *effect,
                                 gfloat                  width,
                                 gfloat                  height)
{
  ClutterActor *actor = clutter_actor_meta_get_actor (CLUTTER_ACTOR_META (effect));
  ClutterActorBox box;

  clutter_actor_get_allocation_box (actor, &box);

  return cogl_texture_new_with_size (box.x2 - box.x1,
                                     box.y2 - box.y1,
                                     COGL_TEXTURE_NO_SLICING,
                                     COGL_PIXEL_FORMAT_RGBA_8888_PRE);
}

static void
lightwood_blur_effect_class_init (LightwoodBlurEffectClass *klass)
{
  ClutterEffectClass *shader_class = CLUTTER_EFFECT_CLASS (klass);
  ClutterOffscreenEffectClass *offscreen_class = CLUTTER_OFFSCREEN_EFFECT_CLASS (klass);

  g_type_class_add_private (klass, sizeof (LightwoodBlurEffectPrivate));

  shader_class->pre_paint = shader_pre_paint;

  offscreen_class->create_texture = offscreen_defect_create_texture;
}

static void
lightwood_blur_effect_init (LightwoodBlurEffect *self)
{
  self->priv = BLUR_EFFECT_PRIVATE (self);
}

/**
 * lightwood_blur_effect_new:
 *
 * Create a new #LightwoodBlurEffect. Note that, as a #ClutterEffect, the
 * returned object has a floating reference.
 *
 * Returns: (transfer full): a newly allocated #LightwoodBlurEffect
 */
ClutterEffect *
lightwood_blur_effect_new (void)
{
  return g_object_new (LIGHTWOOD_TYPE_BLUR_EFFECT, NULL);
}

/**
 * lightwood_blur_effect_set_steps:
 * @blur_effect: A #LightwoodBlurEffect.
 * @steps: Number of texels to sample around each pixel
 *
 * Set how far the shader should go when blurring a pixel
 */
void
lightwood_blur_effect_set_steps (LightwoodBlurEffect *blur_effect, guint steps)
{
  LightwoodBlurEffectPrivate *priv = blur_effect->priv;
  priv->steps = steps;
}
