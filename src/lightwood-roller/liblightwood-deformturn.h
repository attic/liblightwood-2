/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


#ifndef _LIGHTWOOD_DEFORM_PAGE_TURN_H
#define _LIGHTWOOD_DEFORM_PAGE_TURN_H

#include <glib-object.h>
#include <clutter/clutter.h>

G_BEGIN_DECLS

#define LIGHTWOOD_TYPE_DEFORM_PAGE_TURN lightwood_deform_page_turn_get_type()

#define LIGHTWOOD_DEFORM_PAGE_TURN(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  LIGHTWOOD_TYPE_DEFORM_PAGE_TURN, LightwoodDeformPageTurn))

#define LIGHTWOOD_DEFORM_PAGE_TURN_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  LIGHTWOOD_TYPE_DEFORM_PAGE_TURN, LightwoodDeformPageTurnClass))

#define LIGHTWOOD_IS_DEFORM_PAGE_TURN(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  LIGHTWOOD_TYPE_DEFORM_PAGE_TURN))

#define LIGHTWOOD_IS_DEFORM_PAGE_TURN_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  LIGHTWOOD_TYPE_DEFORM_PAGE_TURN))

#define LIGHTWOOD_DEFORM_PAGE_TURN_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  LIGHTWOOD_TYPE_DEFORM_PAGE_TURN, LightwoodDeformPageTurnClass))

typedef struct _LightwoodDeformPageTurn LightwoodDeformPageTurn;
typedef struct _LightwoodDeformPageTurnClass LightwoodDeformPageTurnClass;
typedef struct _LightwoodDeformPageTurnPrivate LightwoodDeformPageTurnPrivate;

struct _LightwoodDeformPageTurn
{
  /*< private >*/
  ClutterDeformEffect parent;

  LightwoodDeformPageTurnPrivate *priv;
};

struct _LightwoodDeformPageTurnClass
{
  /*< private >*/
  ClutterDeformEffectClass parent_class;
};

GType lightwood_deform_page_turn_get_type (void) G_GNUC_CONST;

LightwoodDeformPageTurn *lightwood_deform_page_turn_new (void);

void lightwood_deform_page_turn_bend_left_corner (LightwoodDeformPageTurn *effect, gboolean enabled);
void lightwood_deform_page_turn_bend_right_corner (LightwoodDeformPageTurn *effect, gboolean enabled);

/* Properties */

gdouble lightwood_deform_page_turn_get_angle (LightwoodDeformPageTurn *self);
void lightwood_deform_page_turn_set_angle (LightwoodDeformPageTurn *self,
                                           gdouble angle);

gboolean lightwood_deform_page_turn_get_inverted (LightwoodDeformPageTurn *self);
void lightwood_deform_page_turn_set_inverted (LightwoodDeformPageTurn *self,
                                              gboolean inverted);

G_END_DECLS

#endif /* _LIGHTWOOD_DEFORM_PAGE_TURN_H */
