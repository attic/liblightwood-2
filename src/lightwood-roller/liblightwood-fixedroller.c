/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


/**
 * SECTION:liblightwood-fixedroller
 * @Title:LightwoodFixedRoller
 * @short_description: #LightwoodRoller subclass that provides high scalability by
 * considering that all elements have the same height.
 *
 * #LightwoodFixedRoller is very efficient roller as it creates very less items 
 * even when there are many roller items. It will create required to show in the viewport.
 * Fixed roller can add some additional effect like blur effect and cylindrical distortion 
 * using #LightwoodRoller:blur-effect,#LightwoodRoller:cyndrical-distortion. If #LightwoodRoller:roll-over 
 * is enabled 
 * One item can be expanded at a time and, by #LightwoodFixedRoller:flow-layout, more
 * than one item can be made to fit in a single line.
 *
 * ## Sample C Code
 * |[<!-- language="C" -->
 *static ClutterActor *
 *create_roller (gboolean video, gboolean toggle, gboolean no_background,
 *               gboolean motion_blur, ClutterModel *model)
 *{
 *  ClutterActor *roller;
 *  SampleItemFactory *item_factory;
 *  GType item_type;
 *
 *  if (video)
 *    item_type = TYPE_SAMPLE_ITEM_VIDEO;//Any type
 *  else if (toggle)
 *    item_type = TYPE_SAMPLE_ITEM_TOGGLE; 
 *  else
 *    item_type = TYPE_SAMPLE_ITEM;
 *
 *  item_factory = g_object_new (TYPE_SAMPLE_ITEM_FACTORY,
 *                               "item-type", item_type,
 *                               "model", model,
 *                               NULL);
 *
 *  roller = g_object_new (LIGHTWOOD_TYPE_FIXED_ROLLER,
 *                         "roll-over", TRUE,
 *                         "motion-blur", motion_blur,
 *                         "model", model,
 *                         "factory", item_factory,
 *                         "name", "roller",
 *                         "reactive", TRUE,
 *                         "clamp-duration", 1250,
 *                         "clamp-mode", get_elastic_mode (),
 *                         NULL);
 *
 *  lightwood_roller_add_attribute (LIGHTWOOD_ROLLER (roller), "name", COLUMN_NAME);
 *  lightwood_roller_add_attribute (LIGHTWOOD_ROLLER (roller), "label", COLUMN_LABEL);
 *
 *  if (video)
 *    {
 *      lightwood_roller_add_attribute (LIGHTWOOD_ROLLER (roller), "video", COLUMN_VIDEO);
 *    }
 *  else if (toggle)
 *    {
 *      lightwood_roller_add_attribute (LIGHTWOOD_ROLLER (roller), "icon", COLUMN_ICON);
 *      lightwood_roller_add_attribute (LIGHTWOOD_ROLLER (roller), "toggle", COLUMN_TOGGLE);
 *    }
 * else
 *    {
 *      lightwood_roller_add_attribute (LIGHTWOOD_ROLLER (roller), "icon", COLUMN_ICON);
 *    }
 *
 *  return roller;
 *}
 *int main()
 *{
 *  ClutterActor *scroll_view;
 *  ThornburyModel *model;
 *  qMxSettings *settings;
 *  //do something ....
 *
 *
 *  model = create_model (NUM_OF_ITEMS);
 *  roller = create_roller (video, toggle, no_background, motion_blur, model);
 *  scroll_view = create_scroll_view ();
 *  settings = mx_settings_get_default ();
 *  g_object_set (G_OBJECT (settings), "drag-threshold", 20, NULL);
 *  g_signal_connect (G_OBJECT (roller), "item-activated",
 *                    G_CALLBACK (roller_item_activated_cb), NULL);
 *
 *  g_signal_connect (vadjustment, "notify::value",
 *                    G_CALLBACK (adjustment_value_notify3_cb),
 *                    roller);
 *  // add roller ,settings and scrollview to the container and finally add container to the stage
 *
 *}
 *]|
 * creating item type 
 * ## Sample C Code
 * |[<!-- language="C" -->
 *
 *G_DEFINE_TYPE (SingleItem, single_item, CLUTTER_TYPE_GROUP)
 *
 *#define ROLLER_FONT(size) "abcSansCondRegular " #size "px"
 *
 *enum
 *{
 *    PROP_0,
 *    PROP_LABEL,
 *    PROP_RIGHT_TEXT,
 *    PROP_BOTTOM_TEXT,
 *    PROP_MODEL,
 *    PROP_TEXT_COLOUR,
 *    PROP_TEXT_X,
 *    PROP_TEXT_Y,
 *    PROP_TEXT_FONT,
 *    PROP_FOCUSED,
 *    PROP_ROW
 *};
 *
 *
 *static void
 *single_item_get_property (GObject    *object,
 *                          guint       property_id,
 *                          GValue     *value,
 *                          GParamSpec *pspec)
 *{
 *    SingleItem *item = SINGLE_ITEM (object);
 *    switch (property_id)
 *{
 *    case PROP_LABEL:
 *        g_value_set_string (value,
 *                            g_strdup (clutter_text_get_text (CLUTTER_TEXT (item->label))));
 *        break;
 *    case PROP_RIGHT_TEXT:
 *        g_value_set_string (value,g_strdup (clutter_text_get_text (CLUTTER_TEXT (item->pRightText))));
 *        break;
 *    case PROP_BOTTOM_TEXT:
 *        g_value_set_string (value,g_strdup (clutter_text_get_text (CLUTTER_TEXT (item->pBottomText))));
 *        break;
 *    default:
 *        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
 *    }
 *}
 *
 *static void on_notify_language ( GObject    *object,
 *                                 GParamSpec *pspec,
 *                                 gpointer    user_data)
 *{
 *  SingleItem *self = SINGLE_ITEM (user_data);
 *  clutter_text_set_text(CLUTTER_TEXT (self->label),get_text(self->text));
 *}
 *
 *static void
 *single_item_set_property (GObject      *object,
 *                          guint         property_id,
 *                          const GValue *value,
 *                          GParamSpec   *pspec)
 *{
 *    SingleItem *self = SINGLE_ITEM (object);
 *    switch (property_id)
 *    {
 *    case PROP_LABEL:
 *        clutter_text_set_text (CLUTTER_TEXT (self->label),
 *                               get_text((gchar *)g_value_get_string (value)));
 *        self->text = g_strdup(g_value_get_string (value));
 *        break;
 *    case PROP_RIGHT_TEXT:
 *        clutter_text_set_text(CLUTTER_TEXT (self->pRightText),get_text((gchar *)g_value_get_string (value)));
 *        break;
 *    case PROP_BOTTOM_TEXT:
 *        clutter_text_set_text(CLUTTER_TEXT (self->pBottomText),get_text((gchar *)g_value_get_string (value)));
 *        break;
 *    case PROP_MODEL:
 *        self->model = g_value_get_object (value);
 *        break;
 *    case PROP_TEXT_COLOUR:
 *        ClutterColor *color=(ClutterColor *)g_value_get_pointer(value);
 *        clutter_text_set_color (CLUTTER_TEXT (self->label),color);
 *        break;
 *    case PROP_TEXT_X:
 *        clutter_actor_set_x(self->label,g_value_get_float(value));
 *        break;
 *    case PROP_TEXT_Y:
 *        clutter_actor_set_y(self->label,g_value_get_float(value));
 *        break;
 *    case PROP_FOCUSED:
 *        {
              if (g_value_get_boolean (value))
 *            {
 *                if (clutter_actor_get_effect (self->label, "glow") != NULL)
 *                    return;
 *
                  clutter_actor_add_effect_with_name (self->label, "glow", self->glow_effect_1);
 *
 *             }
 *            else
 *            {
 *                if (clutter_actor_get_effect (self->label, "glow") == NULL)
 *                    return;
 *
 *                clutter_actor_remove_effect_by_name (self->label, "glow");
 *            }
 *        }
 *        break;
 *    case PROP_TEXT_FONT:
 *        {
 *            gchar *fontname = g_strdup_printf("abcSansCondRegular %dpx",g_value_get_int(value));
 *            clutter_text_set_font_name (CLUTTER_TEXT (self->label), fontname);
 *            if (fontname)
 *            {
 *                g_free(fontname);
 *                fontname=NULL;
 *            }
 *        }
 *        break;
 *    case PROP_ROW:
 *        {
 *            ClutterActor *parent = clutter_actor_get_parent (CLUTTER_ACTOR (object));
 *
 *            if(FALSE == self->row_called)
 *            {
 *                g_signal_connect (parent,
 *                                  "notify::language",
 *                                  G_CALLBACK (on_notify_language),
 *                                  self);
 *            }
 *
 *            self->row_called = TRUE;
 *        }
 *        break;
 *
 *    default:
 *        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
 *    }
 *}
 *
 *static void
 *single_item_class_init (SingleItemClass *klass)
 *{
 *    GObjectClass *object_class = G_OBJECT_CLASS (klass);
 *    ClutterActorClass *actor_class = CLUTTER_ACTOR_CLASS (klass);
 *
 *    object_class->get_property = single_item_get_property;
 *    object_class->set_property = single_item_set_property;
 *    GParamSpec *pspec;
 *    pspec = g_param_spec_string ("label",
 *                                 "label",
 *                                 "Text for the label",
 *                                 NULL,
 *                                 G_PARAM_READWRITE);
 *    g_object_class_install_property (object_class, PROP_LABEL, pspec);
 *
 *    pspec = g_param_spec_string ("right-text",
 *                                     "right-text",
 *                                     "Text for the right label",
 *                                     NULL,
 *                                     G_PARAM_READWRITE);
 *    g_object_class_install_property (object_class, PROP_RIGHT_TEXT, pspec);
 *
 *    pspec = g_param_spec_string ("bottom-text",
 *                                 "bottom-text",
 *                                  "Text for the bottom label",
 *                                         NULL,
 *                                         G_PARAM_READWRITE);
 *    g_object_class_install_property (object_class, PROP_BOTTOM_TEXT, pspec);
 *
 *    pspec = g_param_spec_object ("model",
 *                                 "ThornburyModel",
 *                                 "ThornburyModel",
 *                                 G_TYPE_OBJECT,
 *                                 G_PARAM_READWRITE);
 *    g_object_class_install_property (object_class, PROP_MODEL, pspec);
 *
 *
 *    pspec = g_param_spec_pointer("text-color",
 *                                 "text-color",
 *                                 "text-color",
 *                                 G_PARAM_WRITABLE);
 *    g_object_class_install_property (object_class, PROP_TEXT_COLOUR, pspec);
 *
 *    pspec = g_param_spec_float ("text-x",
 *                                "text-x",
 *                                "x position of text",
 *                                0,
 *                                728,
 *                                20.0,
 *                                G_PARAM_WRITABLE);
 *    g_object_class_install_property (object_class, PROP_TEXT_X, pspec);
 *
 *    pspec = g_param_spec_float ("text-y",
 *                                "text-y",
 *                                "y position of text",
 *                                0,
 *                                64,
 *                                18.0,
 *                                G_PARAM_WRITABLE);
 *    g_object_class_install_property (object_class, PROP_TEXT_Y, pspec);
 *
 *    pspec = g_param_spec_int("font-size",
 *                             "font-size",
 *                             "font-size of text",
 *                             0,
 *                             64,
 *                             24.0,
 *                             G_PARAM_WRITABLE);
 *    g_object_class_install_property (object_class, PROP_TEXT_FONT, pspec);
 *
 *    pspec = g_param_spec_boolean ("focused",
 *                                  "focused",
 *                                  "Whether this actor should be rendered as focused",
 *                                  FALSE,
 *                                  G_PARAM_WRITABLE);
 *    g_object_class_install_property (object_class, PROP_FOCUSED, pspec);
 *
 *    pspec = g_param_spec_uint ("row",
 *                        "row number",
 *                        "row number",
 *                        0, G_MAXUINT,
 *                        0,
 *                        G_PARAM_WRITABLE);
 *        g_object_class_install_property (object_class, PROP_ROW, pspec);
 *}
 *
 *static void
 *single_item_init (SingleItem *self)
 *{
 *    ClutterColor fontColor = {0x98, 0xA9, 0xB2, 0xFF};
 *    ClutterColor line_horizon_color = {0xFF, 0xFF, 0xFF, 0x33};

 *    self->row_called = FALSE;

 *    self->label = clutter_text_new ();
 *    clutter_text_set_color (CLUTTER_TEXT (self->label), &fontColor);
 *    clutter_text_set_font_name (CLUTTER_TEXT (self->label), ROLLER_FONT(24));
 *    clutter_actor_set_position (self->label, 20.0, 10.0);
 *    clutter_actor_add_child (CLUTTER_ACTOR (self), self->label);
 *    clutter_actor_show (self->label);

 *    self->pRightText = clutter_text_new ();
 *    clutter_text_set_color (CLUTTER_TEXT (self->pRightText),&fontColor);
 *    clutter_text_set_font_name (CLUTTER_TEXT (self->pRightText), ROLLER_FONT(18));
 *    clutter_actor_set_position (self->pRightText, 165.0, 37.0);
 *    clutter_actor_add_child (CLUTTER_ACTOR (self), self->pRightText);
 *    clutter_actor_show (self->pRightText);

 *    self->pBottomText = clutter_text_new ();
 *    clutter_text_set_color (CLUTTER_TEXT (self->pBottomText),&fontColor);
 *    clutter_text_set_font_name (CLUTTER_TEXT (self->pBottomText), ROLLER_FONT(18));
 *    clutter_actor_set_position (self->pBottomText, 20.0, 37.0);
 *    clutter_actor_add_child (CLUTTER_ACTOR (self), self->pBottomText);
 *    clutter_actor_show (self->pBottomText);

 *    ClutterActor *line1 = clutter_actor_new();
 *    clutter_actor_set_background_color(line1, &line_horizon_color);
 *    clutter_actor_set_size (line1, 728, 1);
 *    clutter_actor_set_position (line1, 2, 64);
 *    clutter_actor_add_child (CLUTTER_ACTOR(self),line1);
 *    clutter_actor_show (line1);

 *    ClutterActor *line2 = clutter_actor_new();
 *    clutter_actor_set_background_color(line2, &line_horizon_color);
 *    clutter_actor_set_size (line2, 728, 1);
 *    clutter_actor_set_position (line2, 2, 0);
 *    clutter_actor_add_child (CLUTTER_ACTOR (self), line2);
 *    clutter_actor_show (line2);

 *    clutter_actor_set_reactive (CLUTTER_ACTOR (self), TRUE);
 *    g_object_set(self,"height",64.0,NULL);

 *    self->glow_effect_1 = g_object_ref (sample_glow_shader_new ());
 *}
 * ]|
 *
 */


/*******************************************************************************
 * Fixes
 * Description                               Date                        Author

 *1) Height of the Roller                    29-oct-2012             Ramya Ajiri
 *2) Animation Issue                         06-nov-2012             Ramya Ajiri
 *3) List Roller support added		     21-dec-2012	     Ramya Ajiri
 *4) Changes to support thumbroller	     3-Jan-2012		     Ramya Ajiri
     if items are less
 *5) Removed deprecated API's and 			25-feb-2012			Ramya Ajiri
	 replaced with new API's
 *6) To improve textuire paint, changes		28-feb-2012		Ramya Ajiri
    in actor_apply_transform() v-func
 *7) Connected to ThornburyModel::sort-changed	7-Mar-2013		Abhiruchi
	signal
 *8) signal LightwoodFixedRoller::item-expanded   04-May-2013		Nitin	 
     added
 *9) expanding items in short roller solved	06-May-2013	Ramya Ajiri
 *10) Added Property "refresh", so that user 	01-May-2013	Ramya Ajiri
	can update the ui if roller is not 
	updating.				
*11) cc resolved 				|27-Aug-2013		|Ramya
* 12) to update the roller when changing        |18-dec-2013		Ramya
* the model consistently 
********************************************************************************/

#include "liblightwood-fixedroller.h"
#include "roller-private.h"
#include "roller-internal.h"

#include <math.h>

G_DEFINE_TYPE(LightwoodFixedRoller, lightwood_fixed_roller, LIGHTWOOD_TYPE_ROLLER)

#define FIXED_ROLLER_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), LIGHTWOOD_TYPE_FIXED_ROLLER, LightwoodFixedRollerPrivate))

#define MIN_VISIBLE_ACTORS 20;

struct _LightwoodFixedRollerPrivate
{
  GSList *visible_children;
  gint first_visible_row;
/* Fix Bug 1
Height of the roller */
  gfloat height;
  gboolean children_update_needed;
  gboolean flow_layout;
  gint min_visible_actors;

  gint expanded_row;
  gint previously_expanded_row;

  /* For calculating sizes */
  ClutterActor *expanded_actor;
  ClutterActor *previously_expanded_actor;
  ClutterActor *unexpanded_actor;

  gdouble translation_y;
  gdouble last_adjustment;

  ClutterActorBox allocation;
};

enum {
  PROP_0,
  /* Fix Bug 1
     Height of the roller */
  PROP_HEIGHT,
  PROP_FLOW_LAYOUT,
  PROP_CHILD_HEIGHT,
  PROP_CHILD_WIDTH,
  PROP_REFRESH,
};

enum
{
	ITEM_EXPANDED,

	LAST_SIGNAL
};
static guint signals[LAST_SIGNAL] = { 0, };

static ClutterActor *get_actor_for_sizing (LightwoodFixedRoller *roller);
/* Fix Bug 1
Height of the roller */
void lightwood_fixed_roller_set_height(LightwoodFixedRoller *roller,gfloat height);

static void
get_property (GObject    *object,
              guint       property_id,
              GValue     *value,
              GParamSpec *pspec)
{
	ClutterActor *child;

	switch (property_id)
	{
		case PROP_FLOW_LAYOUT:
			g_value_set_boolean (value, LIGHTWOOD_FIXED_ROLLER (object)->priv->flow_layout);
			break;
			/* Fix Bug 1
			   Height of the roller */
		case PROP_HEIGHT:
			g_value_set_float (value, LIGHTWOOD_FIXED_ROLLER (object)->priv->height);
			break;
		case PROP_CHILD_HEIGHT:
			child = get_actor_for_sizing (LIGHTWOOD_FIXED_ROLLER (object));
			g_value_set_float (value, clutter_actor_get_height (child));
			break;

		case PROP_CHILD_WIDTH:
			child = get_actor_for_sizing (LIGHTWOOD_FIXED_ROLLER (object));
			g_value_set_float (value, clutter_actor_get_width (child));
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
	}
}

static void
set_property (GObject      *object,
                               guint         property_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  ClutterActor *child;

  switch (property_id)
    {
    case PROP_FLOW_LAYOUT:
      lightwood_fixed_roller_set_flow_layout (LIGHTWOOD_FIXED_ROLLER (object),
                                        g_value_get_boolean (value));
      break;
    /* Fix Bug 1
    Height of the roller */
	case PROP_HEIGHT:
	{
	lightwood_fixed_roller_set_height(LIGHTWOOD_FIXED_ROLLER(object),
					g_value_get_float (value));

	}
	break;

    case PROP_CHILD_HEIGHT:
      child = get_actor_for_sizing (LIGHTWOOD_FIXED_ROLLER (object));
      clutter_actor_set_height (child, g_value_get_float (value));
      break;

    case PROP_CHILD_WIDTH:
      child = get_actor_for_sizing (LIGHTWOOD_FIXED_ROLLER (object));
      clutter_actor_set_width (child, g_value_get_float (value));
      break;

	case PROP_REFRESH:
        lightwood_fixed_roller_refresh(LIGHTWOOD_FIXED_ROLLER(object));
        break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

/* Auxiliary functions for calculating positions */

static void
update_actor (LightwoodFixedRoller *roller, ClutterActor *actor, guint row)
{
  ThornburyModelIter *iter;
  GSList *p;
  GSList *attributes;
  ThornburyModel *model = lightwood_roller_get_model (LIGHTWOOD_ROLLER (roller));

	ROLLER_DEBUG("%s",__FUNCTION__);

  if(!G_IS_OBJECT (model))
  {
    g_warning("model not part of the roller \n");
    return;
  }

  g_object_freeze_notify (G_OBJECT (actor));

  if (g_object_class_find_property (G_OBJECT_GET_CLASS (actor),
                                    "row") != NULL)
    g_object_set (G_OBJECT (actor), "row", row, NULL);

  iter = thornbury_model_get_iter_at_row (model, row);
  attributes = lightwood_roller_get_attributes (LIGHTWOOD_ROLLER (roller));

  for (p = attributes; p; p = p->next)
    {
      GValue value = { 0, };
      AttributeData *attr = p->data;

      if (g_object_class_find_property (G_OBJECT_GET_CLASS (actor),
                                        attr->name) == NULL)
        continue;

      thornbury_model_iter_get_value (iter, attr->column, &value);
      g_object_set_property (G_OBJECT (actor), attr->name, &value);
      g_value_unset (&value);
    }

  g_object_unref (iter);

  g_object_thaw_notify (G_OBJECT (actor));
}

static ClutterActor *
get_actor_for_sizing (LightwoodFixedRoller *roller)
{
  LightwoodFixedRollerPrivate *priv = LIGHTWOOD_FIXED_ROLLER (roller)->priv;
  MxItemFactory *item_factory = lightwood_roller_get_factory (LIGHTWOOD_ROLLER (roller));

	ROLLER_DEBUG("%s",__FUNCTION__);

  g_return_val_if_fail (item_factory != NULL, NULL);

  if (priv->unexpanded_actor == NULL)
    {
      priv->unexpanded_actor = mx_item_factory_create (item_factory);
/*  priv->unexpanded_actor is causing a crash while destroyinng roller,
    hence it will not be part of the roller object */
      //update_actor (roller, priv->unexpanded_actor, 0);
      //clutter_actor_set_parent (priv->unexpanded_actor, CLUTTER_ACTOR (roller));
    }

  return priv->unexpanded_actor;
}

static gfloat
get_item_height (LightwoodFixedRoller *roller)
{
  ClutterActor *child;
  ThornburyModel *model = lightwood_roller_get_model (LIGHTWOOD_ROLLER (roller));

	ROLLER_DEBUG("%s",__FUNCTION__);

  g_return_val_if_fail (model != NULL, 0);

  if (thornbury_model_get_n_rows (model) == 0)
    return 0;

  child = get_actor_for_sizing (roller);

  return clutter_actor_get_height (child);
}

static guint
get_n_columns (LightwoodFixedRoller *roller, gfloat forced_roller_width,
               gfloat forced_item_width)
{
  LightwoodFixedRollerPrivate *priv = LIGHTWOOD_FIXED_ROLLER (roller)->priv;
  ClutterActor *child;
  gfloat available_width, item_width;

	ROLLER_DEBUG("%s",__FUNCTION__);

  if (!priv->flow_layout)
    return 1;

  if (forced_roller_width == -1)
    available_width = priv->allocation.x2 - priv->allocation.x1;
  else
    available_width = forced_roller_width;

  if (forced_item_width == -1)
    {
      child = get_actor_for_sizing (roller);
      item_width = clutter_actor_get_width (child);
    }
  else
    item_width = forced_item_width;

  return MAX (floorf (available_width / item_width), 1);
}

static gint
get_number_of_needed_children (LightwoodFixedRoller *roller)
{
  LightwoodFixedRollerPrivate *priv = roller->priv;
  ThornburyModel *model = lightwood_roller_get_model (LIGHTWOOD_ROLLER (roller));
  gfloat item_height, content_height;
  gint needed_children;

	ROLLER_DEBUG("%s",__FUNCTION__);

  content_height = lightwood_roller_get_content_height (LIGHTWOOD_ROLLER (roller));
  item_height = get_item_height (roller);

  needed_children = ceilf (content_height / item_height) + 1;
  needed_children *= get_n_columns (roller, -1, -1);
  needed_children *= 2; /* To reduce relayouts */
  needed_children = MAX (needed_children, priv->min_visible_actors);
  needed_children = MIN (needed_children, thornbury_model_get_n_rows (model));

  return needed_children;
}

static gint
get_row_from_visible_child_index (LightwoodFixedRoller *roller, gint index, gboolean rollover)
{
	LightwoodFixedRollerPrivate *priv = LIGHTWOOD_FIXED_ROLLER (roller)->priv;
	gdouble start;
	gfloat item_height;
	gint n_items, row, offset, onscreen_children, offscreen_children;
	ThornburyModel *model = lightwood_roller_get_model (LIGHTWOOD_ROLLER (roller));
	MxAdjustment *vadjust;
	gfloat content_height;

	ROLLER_DEBUG("%s",__FUNCTION__);

	mx_scrollable_get_adjustments (MX_SCROLLABLE (roller), NULL, &vadjust);
	if (vadjust != NULL)
		start = mx_adjustment_get_value (vadjust);
	else
		start = 0.0;

	start -= priv->translation_y;

	if (start < 0.0)
		start = floor (start);
	else
		start = ceil (start);

	item_height = get_item_height (roller);
	content_height = lightwood_roller_get_content_height (LIGHTWOOD_ROLLER (roller));
	n_items = thornbury_model_get_n_rows (model);
	offset = floorf (start / item_height);
	offset *= get_n_columns (roller, -1, -1);

	/* Substract the number of items that we keep offscreen for animations */
	if (lightwood_roller_get_effective_rollover_enabled (LIGHTWOOD_ROLLER (roller)))
	{
		onscreen_children = ceilf (content_height / item_height) + 1;
		onscreen_children *= get_n_columns (roller, -1, -1);
		offscreen_children = get_number_of_needed_children (roller) -
			onscreen_children;
		if (offscreen_children > 0)
			offset -= offscreen_children / 2;
	}

	/* Compensate for expanded rows */
	if (priv->expanded_row != -1 && offset > priv->expanded_row)
	{
		gfloat expanded_item_height;

		clutter_actor_get_preferred_height (priv->expanded_actor, -1, NULL,
				&expanded_item_height);

		offset = floorf ((start - expanded_item_height) / item_height) + 1;
	}
	/* to get rid of crash due to, no item in the model */
	if(n_items != 0)
		row = (offset + index) % n_items;
	if(n_items  == 0)
		row = 0;

	/* Fix modulo for negative numbers */
	if (row < 0 && rollover)
	{
		row += n_items;
		g_assert (row >= 0);
	}

	//g_assert (row < n_items);

	/* Rollover effect has to be disabled,for list roller */
	if (!lightwood_roller_get_scrolling_required (LIGHTWOOD_ROLLER (roller)))
	{
		row = index;
	}

	if(lightwood_roller_get_effective_rollover_enabled (LIGHTWOOD_ROLLER (roller)) == FALSE)
	{
		if ((row < 0))
			row = 0;

	}

	return row;
}

static guint
get_visible_child_index_from_row (LightwoodFixedRoller *roller, gint row)
{
  LightwoodFixedRollerPrivate *priv = LIGHTWOOD_FIXED_ROLLER (roller)->priv;
  ThornburyModel *model = lightwood_roller_get_model (LIGHTWOOD_ROLLER (roller));
  guint n_items = thornbury_model_get_n_rows (model);

	ROLLER_DEBUG("%s",__FUNCTION__);

  if (row - priv->first_visible_row >= 0)
    return row - priv->first_visible_row;
  else
    return row - priv->first_visible_row + n_items;
}

static gfloat
get_coord_for_first_visible_child (LightwoodFixedRoller *roller)
{
  LightwoodFixedRollerPrivate *priv = roller->priv;
  gdouble start;
  gfloat item_height, y, offset;
  gint row;
  gint col;

	ROLLER_DEBUG("%s",__FUNCTION__);

	/* Rollover effect has to be disabled,for list roller */
	row = get_row_from_visible_child_index (roller, 0, lightwood_roller_get_effective_rollover_enabled(LIGHTWOOD_ROLLER(roller)));
	item_height = get_item_height (roller);
	offset = row;
	col = get_n_columns (roller, -1, -1);
	offset = floor (row / col);

	ROLLER_DEBUG ("%s, %p: row = %i, col = %i, item_height = %f, "
	              "offset = %f, effective-rollover-enabled = %s, "
	              "scrolling-required = %s", G_STRFUNC, roller, row, col,
	              item_height, offset,
	              lightwood_roller_get_effective_rollover_enabled (LIGHTWOOD_ROLLER (roller)) ? "yes" : "no",
	              lightwood_roller_get_scrolling_required (LIGHTWOOD_ROLLER (roller)) ? "yes" : "no");

	/* Animation Issue */
	if (lightwood_roller_get_scrolling_required (LIGHTWOOD_ROLLER (roller)))
		offset *= item_height;

  start = lightwood_roller_get_normalized_adjustment_position (LIGHTWOOD_ROLLER (roller));
  y = offset - start;

	ROLLER_DEBUG ("%s, %p: offset = %f, start = %f, y = %f, y2 - y1 = %f",
	              G_STRFUNC, roller, offset, start, y,
	              priv->allocation.y2 - priv->allocation.y1);

	if (y > priv->allocation.y2 - priv->allocation.y1)
	{
		guint n_items;

		ThornburyModel *model = lightwood_roller_get_model (LIGHTWOOD_ROLLER (roller));
		n_items = thornbury_model_get_n_rows (model);

      y -= n_items * item_height / get_n_columns (roller, -1, -1);

		ROLLER_DEBUG ("%s, %p: n_items = %u, n_columns = %u, y = %f",
		              G_STRFUNC, roller, n_items,
		              get_n_columns (roller, -1, -1), y);
    }

  return y;
}

static void 
seperate_valid_invalid_child(LightwoodFixedRoller *roller,GSList **invalid_actors,GHashTable **valid_actors,gint *row,gint *first_row,gint needed_children)
{
	LightwoodFixedRollerPrivate *priv = roller->priv;
	GSList *iter;
	ThornburyModel *model = lightwood_roller_get_model (LIGHTWOOD_ROLLER (roller));
	ClutterActor *child;
	gint i;

	ROLLER_DEBUG("%s",__FUNCTION__);

	for (iter = priv->visible_children, i = 0; iter != NULL; i++)
	{
		GSList *iter2;

		child = (ClutterActor *) iter->data;
		*row = priv->first_visible_row + i;
		if (lightwood_roller_get_effective_rollover_enabled (LIGHTWOOD_ROLLER (roller)))
		{
			if (*row >= (gint) thornbury_model_get_n_rows (model))
				*row -= thornbury_model_get_n_rows (model);
			else if (*first_row < 0 && *row > *first_row + needed_children - 1)
				*row -= thornbury_model_get_n_rows (model);
		}

		if (*row < *first_row || *row > *first_row + needed_children - 1)
			*invalid_actors = g_slist_append (*invalid_actors, child);
		else
			g_hash_table_insert (*valid_actors, (gpointer) *row, child);

		iter2 = iter->next;
		priv->visible_children = g_slist_remove_link (priv->visible_children, iter);
		iter = iter2;
	}
}
static ClutterActor *
_check_if_update_needed(LightwoodFixedRoller *roller,GHashTable **valid_actors,GSList **invalid_actors,gint **row)
{
	LightwoodFixedRollerPrivate *priv = roller->priv;
	ThornburyModel *model = lightwood_roller_get_model (LIGHTWOOD_ROLLER (roller));
	ClutterActor *child;
		gboolean needs_update;
	MxItemFactory *item_factory = lightwood_roller_get_factory (LIGHTWOOD_ROLLER (roller));
		child = g_hash_table_lookup (*valid_actors, (gpointer) **row);
		if (child != NULL)
			needs_update = FALSE;
		else if (*invalid_actors != NULL)
		{
			child = (ClutterActor *) (*invalid_actors)->data;
			*invalid_actors = g_slist_remove_link (*invalid_actors, *invalid_actors);
			needs_update = TRUE;
		}
		else
		{
			child = mx_item_factory_create (item_factory);
			clutter_actor_add_child (CLUTTER_ACTOR (roller),child);
			clutter_actor_show (child);
			needs_update = TRUE;
		}

		if (needs_update)
		{
			if((lightwood_roller_get_effective_rollover_enabled (LIGHTWOOD_ROLLER (roller)) == FALSE) && ((**row < 0) || (**row >= thornbury_model_get_n_rows (model))))
				;/* do nothing */
			else
			{


				if (**row < 0)
					**row += thornbury_model_get_n_rows (model);
				else if (**row >= thornbury_model_get_n_rows (model))
					**row -= thornbury_model_get_n_rows (model);


				update_actor (roller, child, **row);
				clutter_actor_show (child);

				if (LIGHTWOOD_IS_EXPANDABLE (child))
					lightwood_expandable_set_expanded (LIGHTWOOD_EXPANDABLE (child),
							**row == priv->expanded_row,
							FALSE);
			}
		}
	return child;
}
static void 
recreate_update_visible_children(LightwoodFixedRoller *roller,GSList **invalid_actors,GHashTable **valid_actors,gint *row,gint *first_row,gint needed_children)
{
	LightwoodFixedRollerPrivate *priv = roller->priv;
	ThornburyModel *model = lightwood_roller_get_model (LIGHTWOOD_ROLLER (roller));
	ClutterActor *child;
	gint i;

	ROLLER_DEBUG("%s",__FUNCTION__);

	for (i = 0; i < needed_children; i++)
	{

		*row = *first_row + i;
		if (lightwood_roller_get_effective_rollover_enabled (LIGHTWOOD_ROLLER (roller)) &&
				*row >= (gint) thornbury_model_get_n_rows (model))
			*row -= thornbury_model_get_n_rows (model);
		/* to avoid unnecessary execution of the loop if the value of the row is greater than
		   the maximum items */
		if((lightwood_roller_get_effective_rollover_enabled(LIGHTWOOD_ROLLER (roller)) == FALSE) &&
				(*row >= (gint) thornbury_model_get_n_rows (model)))
			break;
		child = _check_if_update_needed(roller,valid_actors,invalid_actors,&row);
		if((lightwood_roller_get_effective_rollover_enabled (LIGHTWOOD_ROLLER (roller)) == FALSE) && ((*row < 0) || (*row >= thornbury_model_get_n_rows (model))))
			;/* do not add child to list */
		else
			priv->visible_children = g_slist_append (priv->visible_children, child);
	}
	priv->first_visible_row = *first_row;
	if (priv->first_visible_row < 0)
		priv->first_visible_row += thornbury_model_get_n_rows (model);

}

/* ClutterActor implementation */

static gboolean
update_visible_children (LightwoodFixedRoller *roller)
{
	LightwoodFixedRollerPrivate *priv = roller->priv;
	gint needed_children;
	MxItemFactory *item_factory = lightwood_roller_get_factory (LIGHTWOOD_ROLLER (roller));
	ThornburyModel *model = lightwood_roller_get_model (LIGHTWOOD_ROLLER (roller));
	gint row, first_row;

	ROLLER_DEBUG("%s",__FUNCTION__);

	g_return_val_if_fail (item_factory != NULL, FALSE);
	g_return_val_if_fail (model != NULL, FALSE);

	priv->translation_y = 0.;
	ROLLER_DEBUG ("%s, %p: set translation_y = %f", G_STRFUNC, roller, 0.0);

	needed_children = get_number_of_needed_children (roller);
	first_row = get_row_from_visible_child_index (roller, 0, FALSE);

	/* avoid updating the roller items, if ::roll-over is FALSE and if rows is negative values */
	if(((lightwood_roller_get_effective_rollover_enabled(LIGHTWOOD_ROLLER(roller)) == FALSE )&& (first_row >= 0)) ||
			lightwood_roller_get_effective_rollover_enabled(LIGHTWOOD_ROLLER(roller)))
	{
		if (priv->first_visible_row != first_row ||
				g_slist_length (priv->visible_children) != needed_children)
		{
			GSList *invalid_actors = NULL; /* Actors pointing to rows that are out of bounds */
			GHashTable *valid_actors = NULL; /* Actors pointing to rows that are inside bounds */

			valid_actors = g_hash_table_new (NULL, NULL);

			/* Classify actors in invalid_actors and valid_actors */
			if (priv->first_visible_row == -1)
			{
				invalid_actors = priv->visible_children;
				priv->visible_children = NULL;
			}
			else
			{
				seperate_valid_invalid_child(roller,&invalid_actors,&valid_actors,&row,&first_row,needed_children);
			}

			/* Recreate visible_children based on invalid_actors and valid_actors */
			recreate_update_visible_children(roller,&invalid_actors,&valid_actors,&row,&first_row,needed_children);


			g_slist_free (invalid_actors);
			g_hash_table_unref (valid_actors);
		}
	}

	priv->children_update_needed = FALSE;

	return FALSE;
}

static void
actor_allocate (ClutterActor           *actor,
                const ClutterActorBox  *box,
                ClutterAllocationFlags  flags)
{
	LightwoodFixedRollerPrivate *priv = LIGHTWOOD_FIXED_ROLLER (actor)->priv;
	ThornburyModel *model = lightwood_roller_get_model (LIGHTWOOD_ROLLER (actor));
	gboolean animation_mode;
	/* Fix Bug #1
	   Height of the roller */
	ClutterActor* parent = clutter_actor_get_parent(actor);
	ClutterActorBox  parentBox;

	ROLLER_DEBUG("%s",__FUNCTION__);

	clutter_actor_get_allocation_box(parent,&parentBox);
	if (!clutter_actor_box_equal (&priv->allocation, &parentBox))//box))
	{
		/* Fix Bug #1
		   Height of the roller */
		priv->allocation = parentBox;//*box;
		priv->children_update_needed = TRUE;
		clutter_actor_queue_relayout (actor);
	}

	if (model != NULL && thornbury_model_get_n_rows (model) == 0)
	{
		/* Fix Bug #1
		   Height of the roller */
		CLUTTER_ACTOR_CLASS (lightwood_fixed_roller_parent_class)->allocate (actor, &parentBox,//box,
				flags);
		return;
	}

	/* If we are in animation mode, there's no need to update children */
	g_object_get (actor, "animation-mode", &animation_mode, NULL);

	/* Do the real allocation after we have updated the children */
	if (priv->children_update_needed && !animation_mode)
	{
		/* Chain up to ClutterActor's implementation */
		gpointer actor_class = g_type_class_peek_parent (
				lightwood_fixed_roller_parent_class);
		/* Fix Bug #1
		   Height of the roller */
		CLUTTER_ACTOR_CLASS (actor_class)->allocate (actor, &parentBox/*box*/, flags);

		/* This will cause a reallocation */
		g_idle_add_full (G_PRIORITY_HIGH, (GSourceFunc)update_visible_children,
				actor, NULL);
	}
	else
	{
		priv->translation_y = 0.;
		ROLLER_DEBUG ("%s, %p: set translation_y = %f", G_STRFUNC,
		              actor, 0.0);
		priv->children_update_needed = TRUE;
		/* Fix Bug #1
		   Height of the roller */
		CLUTTER_ACTOR_CLASS (lightwood_fixed_roller_parent_class)->allocate (actor, &parentBox,//box,
				flags);

		clutter_actor_queue_relayout (actor);
	}
}

static void
actor_get_preferred_width (ClutterActor *actor,
                           gfloat        for_height,
                           gfloat       *min_width_p,
                           gfloat       *natural_width_p)
{
  LightwoodFixedRollerPrivate *priv = LIGHTWOOD_FIXED_ROLLER (actor)->priv;
  ClutterActor *child;
  ThornburyModel *model = lightwood_roller_get_model (LIGHTWOOD_ROLLER (actor));
  guint n_items = thornbury_model_get_n_rows (model);

	ROLLER_DEBUG("%s",__FUNCTION__);

  if (n_items == 0)
    {
      if (min_width_p != NULL)
        *min_width_p = 0;

      if (natural_width_p != NULL)
        *natural_width_p = 0;

      return;
    }

  /* Assume all children have the same width, otherwise we'd need to iterate */
  child = get_actor_for_sizing (LIGHTWOOD_FIXED_ROLLER (actor));
  clutter_actor_get_preferred_width (child, -1, min_width_p, natural_width_p);

  if (priv->flow_layout)
    *natural_width_p *= n_items;
}

static void 
_calc_height(ClutterActor *actor,ClutterActor *child,gfloat for_width,gfloat **min_height_p,gfloat **natural_height_p)
{
	LightwoodFixedRollerPrivate *priv = LIGHTWOOD_FIXED_ROLLER (actor)->priv;
	gfloat item_width = clutter_actor_get_width (child);

	if (*min_height_p != NULL)
		**min_height_p = **min_height_p / floorf (for_width / item_width);

	if (natural_height_p != NULL)
		**natural_height_p = **natural_height_p / floorf (for_width / item_width);
	/* for the short roller consider minimum height, rather avoid calculating the height according to the no of items */
	if(priv->height > **natural_height_p)
	{
		**natural_height_p = priv->height;
	}

}

static ClutterActor *
_get_child_height(ClutterActor *actor,gfloat *for_width,gfloat **min_height_p, gfloat **natural_height_p)
{
	LightwoodFixedRollerPrivate *priv = LIGHTWOOD_FIXED_ROLLER (actor)->priv;
	ThornburyModel *model = lightwood_roller_get_model (LIGHTWOOD_ROLLER (actor));
	guint n_items = thornbury_model_get_n_rows (model);
	gfloat min_height, natural_height;
	ClutterActor *child;

	if (n_items == 0)
	{
		if (*min_height_p != NULL)
			**min_height_p = 0;

		if (*natural_height_p != NULL)
			**natural_height_p = 0;

		return NULL;
	}

	child = get_actor_for_sizing (LIGHTWOOD_FIXED_ROLLER (actor));
	if (*for_width == -1)
	{
		if (priv->allocation.x2 - priv->allocation.x1 > 0)
			*for_width = priv->allocation.x2 - priv->allocation.x1;
		else
			clutter_actor_get_preferred_width (child, -1, NULL, for_width);
	}

	/* Assume all children have the same height, otherwise we'd need to iterate */
	clutter_actor_get_preferred_height (child, *for_width, &min_height,
			&natural_height);

	if (*min_height_p != NULL)
		**min_height_p = min_height * n_items;

	if (*natural_height_p != NULL)
		**natural_height_p = natural_height * n_items;
	return child;
}

static void
actor_get_preferred_height (ClutterActor *actor,
                            gfloat        for_width,
                            gfloat       *min_height_p,
                            gfloat       *natural_height_p)
{
	LightwoodFixedRollerPrivate *priv = LIGHTWOOD_FIXED_ROLLER (actor)->priv;
	ClutterActor *child;

	ROLLER_DEBUG("%s",__FUNCTION__);
	
	child = _get_child_height(actor,&for_width,&min_height_p,&natural_height_p);
	if(NULL == child)
		return;
	
	/* Fix Bug #1
	   Height of the roller */
	if(priv->height > *natural_height_p)
	{
		*natural_height_p = priv->height;
	}
	if (priv->flow_layout)
	{
		_calc_height(actor,child,for_width,&min_height_p,&natural_height_p);
	}
}

static gboolean
actor_get_paint_volume (ClutterActor       *actor,
                        ClutterPaintVolume *volume)
{
  LightwoodFixedRollerPrivate *priv = LIGHTWOOD_FIXED_ROLLER (actor)->priv;
  ClutterActor *child;
  const ClutterPaintVolume *child_volume;
  ThornburyModel *model = lightwood_roller_get_model (LIGHTWOOD_ROLLER (actor));
  guint n_items;

	ROLLER_DEBUG("%s",__FUNCTION__);

  if (!clutter_paint_volume_set_from_allocation (volume, actor))
    return FALSE;

  /* If no model has been set, or if the model is empty, use the paint volume
   * from the allocation. */
  if (model == NULL)
    return TRUE;

  n_items = thornbury_model_get_n_rows (model);

  if (n_items == 0)
    return TRUE;

  /* Assume all children have the same volume, otherwise we'd need to iterate.
   * If the @child_volume is not known, we have to be conservative and return
   * an unknown paint volume for the entire roller. That's fine, as it's an
   * upper bound on the paint volume of the roller. */
  child = get_actor_for_sizing (LIGHTWOOD_FIXED_ROLLER (actor));
  child_volume = clutter_actor_get_transformed_paint_volume (child, actor);

  if (child_volume != NULL)
    {
      gfloat width, height;
      ClutterVertex vertex;
      gint n_rows, n_columns;

      clutter_paint_volume_union (volume, child_volume);

      n_columns = get_n_columns (LIGHTWOOD_FIXED_ROLLER (actor), -1, -1);
      width = clutter_paint_volume_get_width (child_volume) * n_columns;
      clutter_paint_volume_set_width (volume, width);

      n_rows = ceilf ((float) n_items / n_columns);
      height = clutter_paint_volume_get_height (child_volume) * n_rows;
      clutter_paint_volume_set_height (volume, height);

      clutter_paint_volume_get_origin (volume, &vertex);
      vertex.y += priv->translation_y;
      clutter_paint_volume_set_origin (volume, &vertex);

      return TRUE;
    }
  else
    return FALSE;
}

static void
actor_apply_transform (ClutterActor *actor,
                       CoglMatrix   *matrix)
{
  LightwoodFixedRollerPrivate *priv = LIGHTWOOD_FIXED_ROLLER (actor)->priv;

  CLUTTER_ACTOR_CLASS (lightwood_fixed_roller_parent_class)->apply_transform (actor,
      matrix);

  cogl_matrix_translate (matrix, 0, (int) -priv->translation_y, 0);

  ROLLER_DEBUG ("%s, %p: multiplies matrix (0, %f, 0)",
                G_STRFUNC, matrix,
                (int) -priv->translation_y);
}

static void
push_clip (LightwoodFixedRoller *roller)
{
  LightwoodFixedRollerPrivate *priv = roller->priv;
  ClutterActorBox box;
  gfloat width, height;

  clutter_actor_get_allocation_box (CLUTTER_ACTOR (roller), &box);
  width = box.x2 - box.x1;
  height = lightwood_roller_get_content_height (LIGHTWOOD_ROLLER (roller));

  ROLLER_DEBUG ("%s, %p: w=%f, h=%f, translation_y=%f",
                G_STRFUNC, roller,
                width, height, priv->translation_y);

  /* Need to clip here instead of using clip_to_allocation in order to take into
     account the scroll translation */
  cogl_clip_push_rectangle (0, priv->translation_y,
                            width, height + priv->translation_y);
}

static void
actor_paint (ClutterActor *actor)
{
	ROLLER_DEBUG("%s",__FUNCTION__);
  push_clip (LIGHTWOOD_FIXED_ROLLER (actor));

  CLUTTER_ACTOR_CLASS (lightwood_fixed_roller_parent_class)->paint (actor);

  cogl_clip_pop ();
}

static void
actor_pick (ClutterActor *actor, const ClutterColor *color)
{
	ROLLER_DEBUG("%s",__FUNCTION__);
  push_clip (LIGHTWOOD_FIXED_ROLLER (actor));

  CLUTTER_ACTOR_CLASS (lightwood_fixed_roller_parent_class)->pick (actor, color);

  cogl_clip_pop ();
}

/* LightwoodRoller implementation */

static guint
roller_get_row_from_coord (LightwoodRoller *roller, gfloat x, gfloat y)
{
  LightwoodFixedRollerPrivate *priv = LIGHTWOOD_FIXED_ROLLER (roller)->priv;
  gfloat item_height;
  gint n_items, row = -1;//set the default value
  ThornburyModel *model = lightwood_roller_get_model (roller);

	ROLLER_DEBUG("%s",__FUNCTION__);

	n_items = thornbury_model_get_n_rows (model);
	/* to avoid crash if n_items is '0' */
	if(n_items > 0)
	{
		item_height = get_item_height (LIGHTWOOD_FIXED_ROLLER (roller));

		if (priv->expanded_row == -1 || y <= priv->expanded_row * item_height)
		{
			row = floorf (y / item_height);

			if (priv->flow_layout)
			{
				ClutterActor *child = get_actor_for_sizing (LIGHTWOOD_FIXED_ROLLER (roller));
				gfloat item_width = clutter_actor_get_width (child);
				gint col = get_n_columns (LIGHTWOOD_FIXED_ROLLER (roller), -1, -1);

				g_assert (x >= 0.0);
				row *= col;//get_n_columns (LIGHTWOOD_FIXED_ROLLER (roller), -1, -1);
				row += floorf (x / item_width);
			}
		}
		else
		{
			gfloat expanded_item_height;

      expanded_item_height = clutter_actor_get_height (priv->expanded_actor);

			y -= priv->expanded_row * item_height;
			if (y <= expanded_item_height)
				row = priv->expanded_row;
			else
			{
				y -= expanded_item_height;
				row = floorf (y / item_height) + priv->expanded_row + 1;
			}
		}

  row = row % n_items;

  /* Fix modulo for negative numbers */
  if (row < 0)
    row += n_items;

		g_assert (row >= 0);
		g_assert (row < n_items);
	}
	return row;
}

static GSList *
roller_get_visible_items (LightwoodRoller *roller,
                          gfloat     forced_roller_width,
                          gfloat     forced_item_width,
                          gfloat    *first_x,
                          gfloat    *first_y)
{
  LightwoodFixedRollerPrivate *priv = LIGHTWOOD_FIXED_ROLLER (roller)->priv;

  ROLLER_DEBUG ("%s, %p: forced_roller_width=%f, forced_item_width=%f",  
                G_STRFUNC, roller,
                forced_roller_width, forced_item_width);

  if (first_x != NULL)
    {
      ClutterActor *child = get_actor_for_sizing (LIGHTWOOD_FIXED_ROLLER (roller));
      gfloat item_width, roller_width;
      guint n_columns;
/* Rollover effect has to be disabled,for list roller */
      gint row = get_row_from_visible_child_index (LIGHTWOOD_FIXED_ROLLER (roller), 0, lightwood_roller_get_effective_rollover_enabled(LIGHTWOOD_ROLLER(roller)));

      if (forced_item_width < 0.0)
        item_width = clutter_actor_get_width (child);
      else
        item_width = forced_item_width;

      if (forced_roller_width < 0.0)
        roller_width = clutter_actor_get_width (CLUTTER_ACTOR (roller));
      else
        roller_width = forced_roller_width;

      item_width = MIN (item_width, roller_width);

      n_columns = floorf (roller_width / item_width);
/* to avoid the crash when n_columns is zero */
	if(n_columns > 0)
      *first_x = item_width * (row % n_columns);
	else
	*first_x = item_width * (row % 1);

      ROLLER_DEBUG ("%s, first_x=%f", G_STRFUNC, *first_x);
    }

  if (first_y != NULL)
    {
      *first_y = get_coord_for_first_visible_child (LIGHTWOOD_FIXED_ROLLER (roller));
      ROLLER_DEBUG ("%s, first_y=%f",G_STRFUNC, *first_y);
    }

  return g_slist_copy (priv->visible_children);
}

static ClutterActor *
roller_get_item_from_row (LightwoodRoller *roller,
                          guint      row,
                          gfloat     forced_roller_width,
                          gfloat     forced_item_width,
                          gfloat     forced_item_height,
                          gfloat    *y)
{
  LightwoodFixedRollerPrivate *priv = LIGHTWOOD_FIXED_ROLLER (roller)->priv;
  ThornburyModel *model = lightwood_roller_get_model (LIGHTWOOD_ROLLER (roller));
  ClutterActor *child;
  gint first_visible_row, n_items, visible_row;

	ROLLER_DEBUG("%s",__FUNCTION__);

/* Rollover effect has to be disabled,for list roller */
  first_visible_row = get_row_from_visible_child_index (LIGHTWOOD_FIXED_ROLLER (roller), 0, lightwood_roller_get_effective_rollover_enabled(LIGHTWOOD_ROLLER(roller)));

  if (y != NULL)
    {
      gint first_row_in_line, n_columns;
      gfloat item_height;

      n_columns = get_n_columns (LIGHTWOOD_FIXED_ROLLER (roller), forced_roller_width, forced_item_width);

      if (forced_item_height == -1)
        item_height = get_item_height (LIGHTWOOD_FIXED_ROLLER (roller));
      else
        item_height = forced_item_height;

      first_row_in_line = row;
      first_row_in_line -= row % n_columns;

      *y = item_height * first_row_in_line;
      *y /= n_columns;
    }

  visible_row = row - first_visible_row;
  if (visible_row < 0)
    {
      n_items = thornbury_model_get_n_rows (model);
      visible_row += n_items;
    }

  if (visible_row < g_slist_length (priv->visible_children))
    {
      child = CLUTTER_ACTOR (g_slist_nth_data (priv->visible_children,
                                               visible_row));
      g_object_ref (child);
    }
  else
    child = NULL;

  return child;
}

static gint
roller_get_row_from_item (LightwoodRoller    *roller,
                          ClutterActor *child)
{
  LightwoodFixedRollerPrivate *priv = LIGHTWOOD_FIXED_ROLLER (roller)->priv;
  ThornburyModel *model = lightwood_roller_get_model (roller);
  gint index, row, n_rows;

	ROLLER_DEBUG("%s",__FUNCTION__);

  index = g_slist_index (priv->visible_children, child);

  g_debug ("%s: index: %i, n_visible_children: %u", G_STRFUNC, index,
           g_slist_length (priv->visible_children));

  if (index == -1)
    return -1;

  row = priv->first_visible_row + index;
  n_rows = thornbury_model_get_n_rows (model);

  g_debug ("%s: row: %i, n_rows: %i", G_STRFUNC, row, n_rows);

  if (row > n_rows)
    row -= n_rows;

  return row;
}

static gfloat
roller_get_item_height (LightwoodRoller *roller, ClutterActor *child)
{
  LightwoodFixedRollerPrivate *priv = LIGHTWOOD_FIXED_ROLLER (roller)->priv;
  gint index, row;
  gfloat item_height;

	ROLLER_DEBUG("%s",__FUNCTION__);

  if (child == NULL)
    return get_item_height (LIGHTWOOD_FIXED_ROLLER (roller));

  index = g_slist_index (priv->visible_children, child);
  row = get_row_from_visible_child_index (LIGHTWOOD_FIXED_ROLLER (roller), index, TRUE);

  if (row == priv->expanded_row)
    clutter_actor_get_preferred_height (priv->expanded_actor, -1, NULL,
                                        &item_height);
  else if (row == priv->previously_expanded_row)
    clutter_actor_get_preferred_height (priv->previously_expanded_actor, -1,
                                        NULL, &item_height);
  else
    item_height = get_item_height (LIGHTWOOD_FIXED_ROLLER (roller));

  return item_height;
}

static gfloat
roller_get_item_width (LightwoodRoller *roller, ClutterActor *actor)
{
  LightwoodFixedRollerPrivate *priv = LIGHTWOOD_FIXED_ROLLER (roller)->priv;
  gfloat item_width;

	ROLLER_DEBUG("%s",__FUNCTION__);

  if (priv->flow_layout)
    {
      ClutterActor *child = get_actor_for_sizing (LIGHTWOOD_FIXED_ROLLER (roller));
      clutter_actor_get_preferred_width (child, -1, NULL, &item_width);
    }
  else
    item_width = priv->allocation.x2 - priv->allocation.x1;

  return item_width;
}

static gfloat
roller_get_total_height (LightwoodRoller *roller,
                         gfloat     forced_roller_width,
                         gfloat     forced_item_width,
                         gfloat     forced_item_height)
{
  LightwoodFixedRollerPrivate *priv = LIGHTWOOD_FIXED_ROLLER (roller)->priv;
  ClutterActor *child;
  ThornburyModel *model = lightwood_roller_get_model (roller);
  guint n_items = thornbury_model_get_n_rows (model);
  gfloat item_width, item_height, roller_width, total_height;

	ROLLER_DEBUG("%s",__FUNCTION__);

  if (n_items == 0)
    return 0.0;

  child = get_actor_for_sizing (LIGHTWOOD_FIXED_ROLLER (roller));

  if (forced_item_width == -1)
    clutter_actor_get_preferred_width (child, -1, NULL, &item_width);
  else
    item_width = forced_item_width;

  if (forced_item_height == -1)
    clutter_actor_get_preferred_height (child, item_width, NULL, &item_height);
  else
    item_height = forced_item_height;

  total_height = item_height * n_items;

  if (forced_roller_width == -1)
    roller_width = priv->allocation.x2 - priv->allocation.x1;
  else
    roller_width = forced_roller_width;

  if (priv->flow_layout)
    total_height /= floorf (roller_width / item_width);
/* Animation Issue */
	if(priv->height > total_height)
		total_height = priv->height;
  return total_height;
}

static void
adjustment_value_notify_cb (MxAdjustment    *adjustment,
                            GParamSpec      *pspec,
                            LightwoodFixedRoller  *roller)
{
	LightwoodFixedRollerPrivate *priv = LIGHTWOOD_FIXED_ROLLER (roller)->priv;
	gdouble start = mx_adjustment_get_value (adjustment);
	ClutterActor *first_child;
	gfloat first_y;
	/* Fix Bug #1
	   Height of the roller */

	ROLLER_DEBUG("%s",__FUNCTION__);

	if(priv->visible_children)
	{
		gfloat content_height;

		/* Update the translation first. */
		priv->translation_y += start - priv->last_adjustment;
		ROLLER_DEBUG ("%s, %p: set translation_y = %f, start = %f, "
		              "last_adjustment = %f", G_STRFUNC, roller,
		              priv->translation_y, start,
		              priv->last_adjustment);

		/* Work out whether the current set of visible_children have been translated
		* out of view to the extent that more children need to be made visible. */
		first_child = CLUTTER_ACTOR (priv->visible_children->data);
		first_y = clutter_actor_get_y (first_child);
		content_height = lightwood_roller_get_content_height (LIGHTWOOD_ROLLER (roller));

		/* Fix Bug #1
		   Height of the roller */
		if (first_y - priv->translation_y >= 0.0 &&
		    lightwood_roller_get_scrolling_required (LIGHTWOOD_ROLLER (roller)))
		{
			update_visible_children (LIGHTWOOD_FIXED_ROLLER (roller));
			clutter_actor_queue_relayout (CLUTTER_ACTOR (roller));
		}
		else
		{
			ClutterActor *last_child;
			gfloat last_y;

			last_child = CLUTTER_ACTOR (g_slist_last (priv->visible_children)->data);
			last_y = clutter_actor_get_y (last_child);
			last_y += clutter_actor_get_height (last_child);

			if (last_y - priv->translation_y < content_height &&
			    lightwood_roller_get_scrolling_required (LIGHTWOOD_ROLLER (roller)))
			{
				update_visible_children (LIGHTWOOD_FIXED_ROLLER (roller));
				clutter_actor_queue_relayout (CLUTTER_ACTOR (roller));
			}
		}

		priv->last_adjustment = start;
	}
}
static 
void _roller_notify_item_type_cb (LightwoodFixedRoller *roller)
{
	LightwoodFixedRollerPrivate *priv = roller->priv;
	if (priv->unexpanded_actor != NULL)
        {
                ClutterActor *child = get_actor_for_sizing (LIGHTWOOD_FIXED_ROLLER (roller));
                gboolean width_set, height_set;
                gfloat width, height;

                g_object_get (child, "natural-width-set", &width_set,
                                "natural-height-set", &height_set,
                                "width", &width,
                                "height", &height,
                                NULL);

                //clutter_actor_unparent (priv->unexpanded_actor);
                g_object_unref (priv->unexpanded_actor);
                priv->unexpanded_actor = NULL;

                child = get_actor_for_sizing (LIGHTWOOD_FIXED_ROLLER (roller));
                if (width_set)
                        clutter_actor_set_width (child, width);

                if (height_set)
                        clutter_actor_set_height (child, height);
        }

        priv->expanded_row = -1;
        priv->previously_expanded_row = -1;

        update_visible_children (LIGHTWOOD_FIXED_ROLLER (roller));

        clutter_actor_queue_relayout (CLUTTER_ACTOR (roller));
}
static void
notify_item_type_cb (MxItemFactory  *factory,
		GParamSpec     *pspec,
		LightwoodFixedRoller *roller)
{
	LightwoodFixedRollerPrivate *priv = roller->priv;

	ROLLER_DEBUG("%s",__FUNCTION__);

	while (priv->visible_children != NULL)
	{
		clutter_actor_remove_child(CLUTTER_ACTOR (roller),
				priv->visible_children->data);
		priv->visible_children = g_slist_remove (priv->visible_children, priv->visible_children->data);

	}
	//clutter_container_remove_actor (CLUTTER_CONTAINER (roller),
	//priv->visible_children->data);

	priv->first_visible_row = -1;
	if (priv->expanded_actor != NULL)
	{
		//clutter_actor_unparent (priv->expanded_actor);
		clutter_actor_remove_child(CLUTTER_ACTOR (roller),
				priv->visible_children->data);
		g_object_unref (priv->expanded_actor);
		priv->expanded_actor = NULL;
	}

	if (priv->previously_expanded_actor != NULL)
	{
		//clutter_actor_unparent (priv->previously_expanded_actor);
		clutter_actor_remove_child(CLUTTER_ACTOR(roller),priv->previously_expanded_actor);
		g_object_unref (priv->previously_expanded_actor);
		priv->previously_expanded_actor = NULL;
	}
	_roller_notify_item_type_cb(roller);	
	
	if (priv->unexpanded_actor != NULL)
	{
		ClutterActor *child = get_actor_for_sizing (LIGHTWOOD_FIXED_ROLLER (roller));
		gboolean width_set, height_set;
		gfloat width, height;

		g_object_get (child, "natural-width-set", &width_set,
				"natural-height-set", &height_set,
				"width", &width,
				"height", &height,
				NULL);

		//clutter_actor_unparent (priv->unexpanded_actor);
		g_object_unref (priv->unexpanded_actor);
		priv->unexpanded_actor = NULL;

		child = get_actor_for_sizing (LIGHTWOOD_FIXED_ROLLER (roller));
		if (width_set)
			clutter_actor_set_width (child, width);

		if (height_set)
			clutter_actor_set_height (child, height);
	}

	priv->expanded_row = -1;
	priv->previously_expanded_row = -1;

	update_visible_children (LIGHTWOOD_FIXED_ROLLER (roller));

	clutter_actor_queue_relayout (CLUTTER_ACTOR (roller));
}

static void
item_factory_notify_cb (LightwoodFixedRoller *roller,
                        GParamSpec     *pspec,
                        gpointer        user_data)
{
  MxItemFactory *item_factory;

	ROLLER_DEBUG("%s",__FUNCTION__);

  g_object_get (roller, "factory", &item_factory, NULL);
  g_signal_connect (item_factory,
                    "notify::item-type",
                    G_CALLBACK (notify_item_type_cb),
                    roller);
}

static void
model_changed_cb (ThornburyModel     *model,
                  ThornburyModelIter *iter,
                  LightwoodFixedRoller   *roller)
{
  LightwoodFixedRollerPrivate *priv = roller->priv;

	ROLLER_DEBUG("%s",__FUNCTION__);

  priv->first_visible_row = -1;
/* to update the roller when changing the model consistently */
  priv->children_update_needed = TRUE;
	/* This will cause a reallocation */
                //g_idle_add_full (G_PRIORITY_HIGH, (GSourceFunc)update_visible_children, roller, NULL);
  clutter_actor_queue_relayout (CLUTTER_ACTOR (roller));
}

static void
sort_changed_cb (ThornburyModel     *model,
                  LightwoodFixedRoller   *roller)
{
  LightwoodFixedRollerPrivate *priv = roller->priv;

	ROLLER_DEBUG("%s",__FUNCTION__);

  priv->first_visible_row = -1;
  clutter_actor_queue_relayout (CLUTTER_ACTOR (roller));
}


static void
model_notify_cb (LightwoodFixedRoller *roller,
                 GParamSpec     *pspec,
                 gpointer        user_data)
{
  ThornburyModel *model;

	ROLLER_DEBUG("%s",__FUNCTION__);

  g_object_get (roller, "model", &model, NULL);

  g_signal_connect (model,
                    "row-added",
                    G_CALLBACK (model_changed_cb),
                    roller);

  g_signal_connect (model,
                    "row-changed",
                    G_CALLBACK (model_changed_cb),
                    roller);

  g_signal_connect (model,
                    "row-removed",
                    G_CALLBACK (model_changed_cb),
                    roller);
  g_signal_connect (model,
                    "sort-changed",
                    G_CALLBACK (sort_changed_cb),
                    roller);

}

static void
animation_mode_notify_cb (LightwoodFixedRoller *roller,
                        GParamSpec     *pspec,
                        gpointer        user_data)
{
	ROLLER_DEBUG("%s",__FUNCTION__);
  update_visible_children (roller);
  clutter_actor_queue_relayout (CLUTTER_ACTOR (roller));
}

static void
lightwood_fixed_roller_class_init (LightwoodFixedRollerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  ClutterActorClass *actor_class = CLUTTER_ACTOR_CLASS (klass);
  LightwoodRollerClass *roller_class = LIGHTWOOD_ROLLER_CLASS (klass);
  GParamSpec *pspec;

  ROLLER_DEBUG("%s",__FUNCTION__);

  g_type_class_add_private (klass, sizeof (LightwoodFixedRollerPrivate));

  object_class->get_property = get_property;
  object_class->set_property = set_property;

  actor_class->allocate = actor_allocate;
  actor_class->get_preferred_width = actor_get_preferred_width;
  actor_class->get_preferred_height = actor_get_preferred_height;
  actor_class->get_paint_volume = actor_get_paint_volume;
  actor_class->paint = actor_paint;
  actor_class->pick = actor_pick;
  actor_class->apply_transform = actor_apply_transform;

  roller_class->get_row_from_coord = roller_get_row_from_coord;
  roller_class->get_visible_items = roller_get_visible_items;
  roller_class->get_item_from_row = roller_get_item_from_row;
  roller_class->get_row_from_item = roller_get_row_from_item;
  roller_class->get_item_height = roller_get_item_height;
  roller_class->get_item_width = roller_get_item_width;
  roller_class->get_total_height = roller_get_total_height;
/* Fix Bug #1
    Height of the roller */
pspec = g_param_spec_float ("height",
                              "Height",
                              "Roller Height",
                              -G_MAXFLOAT,
                              G_MAXFLOAT,
                              0.0,
                              G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_HEIGHT, pspec);

  pspec = g_param_spec_boolean ("flow-layout",
                                "Flow layout",
                                "Whether more than one item can be placed in the same row",
                                FALSE,
                                G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_FLOW_LAYOUT, pspec);

  pspec = g_param_spec_float ("child-height",
                              "Child height",
                              "Height of each item",
                              -G_MAXFLOAT,
                              G_MAXFLOAT,
                              0.0,
                              G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_CHILD_HEIGHT, pspec);

  pspec = g_param_spec_float ("child-width",
                              "Child width",
                              "Width of each item",
                              -G_MAXFLOAT,
                              G_MAXFLOAT,
                              0.0,
                              G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_CHILD_WIDTH, pspec);

pspec = g_param_spec_boolean ("refresh",
                        "refresh",
                        "Refreshes the fixed roller",
                        FALSE,
                        G_PARAM_READWRITE);
        g_object_class_install_property (object_class, PROP_REFRESH, pspec);
  /**
  	 * LightwoodFixedRoller::item-expanded:
  	 * @roller: the #LightwoodFixedRoller emitting this signal
  	 * @expanded: #TRUE if @roller is now expanded, #FALSE otherwise
  	 *
  	 * Emitted when an item gets expanded, only if the item doesn't block the
  	 * release event.
  	 */
  	signals[ITEM_EXPANDED] =
  		g_signal_new ("item-expanded",
  				G_TYPE_FROM_CLASS (klass),
  				G_SIGNAL_RUN_LAST,
  				G_STRUCT_OFFSET (LightwoodFixedRollerClass, item_expanded),
  				NULL, NULL,
  				g_cclosure_marshal_VOID__BOOLEAN,
  				G_TYPE_NONE, 1, G_TYPE_BOOLEAN);
}

static void
lightwood_fixed_roller_init (LightwoodFixedRoller *self)
{
  MxAdjustment *vadjust;

	ROLLER_DEBUG("%s",__FUNCTION__);

  self->priv = FIXED_ROLLER_PRIVATE (self);
  self->priv->children_update_needed = TRUE;
  self->priv->first_visible_row = -1;
  self->priv->previously_expanded_row = -1;
  self->priv->expanded_row = -1;
  self->priv->min_visible_actors = MIN_VISIBLE_ACTORS;

  mx_scrollable_get_adjustments (MX_SCROLLABLE (self), NULL, &vadjust);
  g_signal_connect (vadjust, "notify::value",
                    G_CALLBACK (adjustment_value_notify_cb),
                    self);

  g_signal_connect (self, "notify::factory",
                    G_CALLBACK (item_factory_notify_cb),
                    NULL);

  g_signal_connect (self, "notify::model",
                    G_CALLBACK (model_notify_cb),
                    NULL);

  g_signal_connect (self, "notify::animation-mode",
                    G_CALLBACK (animation_mode_notify_cb),
                    NULL);
}

/**
 * lightwood_fixed_roller_new:
 *
 * Create a new #LightwoodFixedRoller
 *
 * Returns: a newly allocated #LightwoodFixedRoller
 */
ClutterActor *
lightwood_fixed_roller_new (void)
{
  return g_object_new (LIGHTWOOD_TYPE_FIXED_ROLLER, NULL);
}

static void
_expand_inside_if(LightwoodFixedRoller *roller, ClutterActor *actor)
{
	LightwoodFixedRollerPrivate *priv = roller->priv;
	guint index;
	ClutterActor *visible_actor;

	index = get_visible_child_index_from_row (roller, priv->previously_expanded_row);
	visible_actor = g_slist_nth_data (priv->visible_children, index);

	if (priv->previously_expanded_actor != NULL)
		lightwood_expandable_set_expanded (LIGHTWOOD_EXPANDABLE (priv->previously_expanded_actor), FALSE, TRUE);

	if (visible_actor != NULL)
		lightwood_expandable_set_expanded (LIGHTWOOD_EXPANDABLE (visible_actor), FALSE, TRUE);
}

static void 
_expand_break(LightwoodFixedRoller *roller, ClutterActor *actor)
{
	/* The previously expanded actor matters because of unexpanding animations */
	LightwoodFixedRollerPrivate *priv = roller->priv;
	if (priv->previously_expanded_actor != NULL)
	{
		clutter_actor_remove_child (CLUTTER_ACTOR (roller), priv->previously_expanded_actor);
		g_object_unref (priv->previously_expanded_actor);
	}

	priv->previously_expanded_actor = priv->expanded_actor;
	priv->expanded_actor = NULL;

	if (priv->previously_expanded_actor != NULL)
		g_object_ref (priv->previously_expanded_actor);

	if (priv->previously_expanded_row != -1)
	{
		_expand_inside_if(roller,actor);
	}
}
/**
 * lightwood_fixed_roller_set_expanded_item:
 * @roller: A #LightwoodFixedRoller
 * @actor: The #ClutterActor that is to be set as expanded or %NULL if all rows
 * should have the same height.
 *
 * Sets which item should be allocated based on its requested size, instead of
 * having the same fixed height as the rest of the entries.
 *
 * Only one item can be expanded at any given point, so setting one item unsets
 * the previous one, if any.
 */
void
lightwood_fixed_roller_set_expanded_item (LightwoodFixedRoller *roller, ClutterActor *actor)
{
	LightwoodFixedRollerPrivate *priv = roller->priv;
	MxItemFactory *item_factory = lightwood_roller_get_factory (LIGHTWOOD_ROLLER (roller));
	GSList *iter;
	guint i;

	ROLLER_DEBUG("%s",__FUNCTION__);

	g_return_if_fail (item_factory != NULL);
	g_return_if_fail (!priv->flow_layout);

	/* We create a new actor each time. Put these assertions in to guarantee
	 * we can simplify the refcounting below. */
	g_return_if_fail (actor == NULL || priv->previously_expanded_actor != actor);
	g_return_if_fail (actor == NULL || priv->expanded_actor != actor);

	priv->previously_expanded_row = priv->expanded_row;

	if (actor == NULL)
		priv->expanded_row = -1;
	else
	{
		for (iter = priv->visible_children, i = 0; iter; iter = iter->next, i++)
			if (iter->data == actor)
				break;
		g_return_if_fail (iter != NULL);
		priv->expanded_row = get_row_from_visible_child_index (roller, i, lightwood_roller_get_effective_rollover_enabled(LIGHTWOOD_ROLLER(roller)));
	}

	_expand_break(roller,actor);	
	
	/* Create the actor used for size calculations */
	if (priv->expanded_row == -1) {
		priv->expanded_actor = NULL;
		g_signal_emit (G_OBJECT(roller), signals[ITEM_EXPANDED], 0, FALSE);
	}
	else
	{
		priv->expanded_actor = mx_item_factory_create (item_factory);

		//clutter_actor_push_internal (CLUTTER_ACTOR (roller));
		//clutter_actor_set_parent (priv->expanded_actor, CLUTTER_ACTOR (roller));
		clutter_actor_add_child (CLUTTER_ACTOR (roller),priv->expanded_actor);
		//clutter_actor_pop_internal (CLUTTER_ACTOR (roller));

		clutter_actor_set_reactive (priv->expanded_actor, FALSE);
		clutter_actor_hide (priv->expanded_actor);
		update_actor (roller, priv->expanded_actor, priv->expanded_row);

		lightwood_expandable_set_expanded (LIGHTWOOD_EXPANDABLE (priv->expanded_actor), TRUE, TRUE);
		lightwood_expandable_set_expanded (LIGHTWOOD_EXPANDABLE (actor), TRUE, TRUE);
		g_signal_emit (G_OBJECT(roller), signals[ITEM_EXPANDED], 0, TRUE);
	}

	priv->children_update_needed = TRUE;
	clutter_actor_queue_relayout (CLUTTER_ACTOR (roller));
}
/**
 * lightwood_fixed_roller_set_height:
 * @roller: A #LightwoodFixedRoller.
 * @height: Height of the Roller.
 */
void
lightwood_fixed_roller_set_height(LightwoodFixedRoller *roller,gfloat height)
{
        LightwoodFixedRollerPrivate *priv = roller->priv;

        ROLLER_DEBUG("%s",__FUNCTION__);
        priv->height = height;

        return;
}
/**
 * lightwood_fixed_roller_set_flow_layout:
 * @roller: A #LightwoodFixedRoller.
 * @enabled: Whether more than one item can be placed in the same row.
 * 
 * When flow-layout is set, roller will place the items one after 
 * another horizontally provided width of the roller is greter than the 
 * sum of the total items width to be placed.
 * 
 */
void
lightwood_fixed_roller_set_flow_layout (LightwoodFixedRoller *roller, gboolean enabled)
{
  LightwoodFixedRollerPrivate *priv = roller->priv;

	ROLLER_DEBUG("%s",__FUNCTION__);

  if (priv->flow_layout != enabled)
    {
      priv->flow_layout = enabled;
      clutter_actor_queue_relayout (CLUTTER_ACTOR (roller));
    }
}

/**
 * lightwood_fixed_roller_set_min_visible_actors:
 * @roller: A #LightwoodFixedRoller.
 * @min_visible_actors: minimum number of actors to keep around
 *
 * Sets a minimum number of actors that need to be kept around regardless of how
 * many are needed to fill the screen. Useful for transitions and to reduce
 * the number of needed relayouts while scrolling.
 */
void
lightwood_fixed_roller_set_min_visible_actors (LightwoodFixedRoller *roller,
                                         gint            min_visible_actors)
{
  LightwoodFixedRollerPrivate *priv = roller->priv;

	ROLLER_DEBUG("%s",__FUNCTION__);

  if (priv->min_visible_actors != min_visible_actors)
    {
      priv->min_visible_actors = min_visible_actors;
      priv->children_update_needed = TRUE;
      clutter_actor_queue_relayout (CLUTTER_ACTOR (roller));
    }
}
/** 
 * lightwood_fixed_roller_refresh:
 * @roller: A #LightwoodRoller
 * 
 * Some times UI is not refreshed on adding/removing an item through model.
 * Fixing in code will make roller unreliable. Hence if developer notices refreshing issue only then 
 * it is recommended to call.
 *
 **/
void
lightwood_fixed_roller_refresh(LightwoodFixedRoller *roller)
{
  /* This will cause a reallocation */
  LightwoodFixedRollerPrivate *priv = roller->priv;
  priv->children_update_needed = TRUE;
  clutter_actor_queue_relayout (CLUTTER_ACTOR (roller));
  lightwood_roller_set_focused_row(LIGHTWOOD_ROLLER(roller),0,TRUE);
}
