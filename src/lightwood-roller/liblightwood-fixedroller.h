/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


#ifndef _LIGHTWOOD_FIXED_ROLLER_H
#define _LIGHTWOOD_FIXED_ROLLER_H

#include <stdlib.h>

#include "libthornbury-model.h"
#include "liblightwood-roller.h"



G_BEGIN_DECLS

#define LIGHTWOOD_TYPE_FIXED_ROLLER lightwood_fixed_roller_get_type()

#define LIGHTWOOD_FIXED_ROLLER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  LIGHTWOOD_TYPE_FIXED_ROLLER, LightwoodFixedRoller))

#define LIGHTWOOD_FIXED_ROLLER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  LIGHTWOOD_TYPE_FIXED_ROLLER, LightwoodFixedRollerClass))

#define LIGHTWOOD_IS_FIXED_ROLLER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  LIGHTWOOD_TYPE_FIXED_ROLLER))

#define LIGHTWOOD_IS_FIXED_ROLLER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  LIGHTWOOD_TYPE_FIXED_ROLLER))

#define LIGHTWOOD_FIXED_ROLLER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  LIGHTWOOD_TYPE_FIXED_ROLLER, LightwoodFixedRollerClass))

typedef struct _LightwoodFixedRoller LightwoodFixedRoller;
typedef struct _LightwoodFixedRollerClass LightwoodFixedRollerClass;
typedef struct _LightwoodFixedRollerPrivate LightwoodFixedRollerPrivate;

struct _LightwoodFixedRoller
{
  /*< private >*/
  LightwoodRoller parent;

  LightwoodFixedRollerPrivate *priv;
};

struct _LightwoodFixedRollerClass
{
  /*< private >*/
  LightwoodRollerClass parent_class;
  void (* item_expanded) (LightwoodFixedRoller *roller,  gboolean isExpanded);
};

GType lightwood_fixed_roller_get_type (void) G_GNUC_CONST;

ClutterActor *lightwood_fixed_roller_new (void);

void lightwood_fixed_roller_set_expanded_item (LightwoodFixedRoller *roller,
                                         ClutterActor *actor);

void lightwood_fixed_roller_set_flow_layout (LightwoodFixedRoller *roller,
                                       gboolean enabled);

void lightwood_fixed_roller_set_min_visible_actors (LightwoodFixedRoller *roller,
                                              gint            min_visible_actors);
void lightwood_fixed_roller_refresh(LightwoodFixedRoller *roller);

G_END_DECLS

#endif /* _LIGHTWOOD_FIXED_ROLLER_H */
