/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION:liblightwood-glowshader
 * @Title:LightwoodGlowShader
 * @short_description: Apply Glow to textures,text actors.
 *
 * #LightwoodGlowShader will give the glow effect to the text or Icon.The glow effect 
 * is applied using GLSL. It is advisable to apply a glow effect if required. And 
 * to make it more efficient it is better to instatiate in the class_init function. 
 * In Lightwood #LightwoodRoller Items are using this effect.
 *
 * 
 * ## Sample C Code
 * |[<!-- language="C" -->
 * obj_class_init(***)
 * {
 * //some code 
 * obj->glow_effect_1 = g_object_ref_sink (lightwood_glow_shader_new ());
 * }
 * // if you want it for every object then only instantiate in obj_init()else instantiate only in class_init()
 * obj_init(***)
 * {
 * //some code 
 * obj->glow_effect_1 = g_object_ref_sink (lightwood_glow_shader_new ());
 * }
 * 
 * // add glow effect wherever required like this
 * clutter_actor_add_effect_with_name (obj->icon, "glow", item->glow_effect_1);
 * // if you want to remove glow
 * clutter_actor_remove_effect_by_name (obj->icon, "glow");
 *
 * // Finally, on disposing of your object:
 * g_clear_object (&obj->glow_effect_1);
 * ]|
 *
 */


#include "liblightwood-glowshader.h"

G_DEFINE_TYPE (LightwoodGlowShader, lightwood_glow_shader, CLUTTER_TYPE_SHADER_EFFECT)

static gchar *shader_source =
"uniform sampler2D tex;\n"
"uniform float texstep;\n"
"\n"
"void main () {\n"
"    float offset[2];\n"
"    float weight[2];\n"
"    vec4 color = texture2D(tex, cogl_tex_coord_in[0].st);\n"
"    float local_offset;\n"
"\n"
/* Cannot use array constructors in GLSL < 1.20 and Cogl doesn't allow us to set
   the version */
"    offset[0] = 20.0;\n"
"    offset[1] = 30.0;\n"
"    weight[0] = 0.1;\n"
"    weight[1] = 0.05;\n"
"\n"
"    for (int i = 0; i < 2; i++) {\n"
"        local_offset = texstep * offset[i];\n"
"	color += texture2D(tex, cogl_tex_coord_in[0].xy + vec2(local_offset, local_offset)) * weight[i] * 8.0;\n"
"    }\n"
"\n"
"    gl_FragColor = color;\n"
"}";

static gboolean
lightwood_glow_shader_pre_paint (ClutterEffect *effect)
{
  ClutterShaderEffect *shader = CLUTTER_SHADER_EFFECT (effect);
  ClutterActor *actor = clutter_actor_meta_get_actor (CLUTTER_ACTOR_META (effect));
  float tex_width, tex_height, tex_step;

  if (!clutter_actor_meta_get_enabled (CLUTTER_ACTOR_META (effect)))
    return FALSE;

  clutter_shader_effect_set_shader_source (shader, shader_source);

  tex_width = clutter_actor_get_width (actor);
  tex_height = clutter_actor_get_height (actor);
  tex_step = 1 / (tex_width * tex_height);

  clutter_shader_effect_set_uniform (shader, "tex", G_TYPE_INT, 1, 0);
  clutter_shader_effect_set_uniform (shader, "texstep", G_TYPE_FLOAT, 1, tex_step);

  return CLUTTER_EFFECT_CLASS (lightwood_glow_shader_parent_class)->pre_paint (effect);
}

static void
lightwood_glow_shader_class_init (LightwoodGlowShaderClass *klass)
{
  ClutterEffectClass *shader_class = CLUTTER_EFFECT_CLASS (klass);

  shader_class->pre_paint = lightwood_glow_shader_pre_paint;
}

static void
lightwood_glow_shader_init (LightwoodGlowShader *self)
{
}

/**
 * lightwood_glow_shader_new:
 *
 * Create a #LightwoodGlowShader. Note that, as a #ClutterEffect, the
 * returned object has a floating reference.
 *
 * Returns: (transfer full): a new #LightwoodGlowShader
 */
ClutterEffect *
lightwood_glow_shader_new (void)
{
  return g_object_new (LIGHTWOOD_TYPE_GLOW_SHADER, NULL);
}
