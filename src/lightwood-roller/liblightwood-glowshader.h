/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef LIGHTWOOD_GLOW_SHADER_H
#define LIGHTWOOD_GLOW_SHADER_H

#include <glib-object.h>
#include <clutter/clutter.h>

G_BEGIN_DECLS

#define LIGHTWOOD_TYPE_GLOW_SHADER lightwood_glow_shader_get_type ()
G_DECLARE_DERIVABLE_TYPE (LightwoodGlowShader, lightwood_glow_shader, LIGHTWOOD, GLOW_SHADER, ClutterShaderEffect)

struct _LightwoodGlowShaderClass
{
  ClutterShaderEffectClass parent_class;
};

ClutterEffect *lightwood_glow_shader_new (void);

G_END_DECLS

#endif /* LIGHTWOOD_GLOW_SHADER_H */
