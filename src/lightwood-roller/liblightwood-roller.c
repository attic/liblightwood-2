/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION:liblightwood-roller
 * @Title:LightwoodRoller
 * @short_description: Abstract class providing the base for roller-like
 * widgets such as #LightwoodFixedRoller and #LightwoodVariableRoller.
 *
 * Actors are created using #LightwoodRoller:itemfactory,
 * with the data supplied by a #LightwoodRoller:model.Property #LightwoodRoller:width and 
 * #LightwoodRoller:hieght are must for creation of the widget.
 *
 * The contained actors can be made roll over to the start by setting the
 * #LightwoodRoller:roll-over property.To give the cylindrical effect by 
 * #LightwoodRoller:cylindrical-distortion using #LightwoodCylinderDeformEffect.
 */
/*******************************************************************************
 * Fixes
 * Description					Date 			Author

 *1) Roll-over if number of items are		|28-aug-2012		|Ramya Ajiri
     less than visible items
 *2) Height of the Roller			|29-oct-2012		|Ramya Ajiri
 *3) Roller Animation Issue 			|06-dec-2012		|Ramya Ajiri
 *4) Roller Focus Issue when items are less 	|12-dec-2012		|Ramya Ajiri
 *5) Added "focus-to-centre" property		|18-dec-2012		|Ramya Ajiri
     which will arrange the first item of the
     roller either on mid/top
 *6) List Roller support added			|21-dec-2012		|Ramya Ajiri
 *7) Changes to support thumbroller		|3-Jan-2012		|Ramya Ajiri
     if items are less
 *8) Passing button-release event 		|17-jan-2012		|Ramya Ajiri
 *   to item type
 *9) Changes updated for variable 		|21-jan-2012    	|Ramya Ajiri
 *   roller
 *10) Set defalut focus to first item 		|2/-feb-2013		|Ramya Ajiri
 *11) Removed deprecated API's and 		|25-feb-2012		|Ramya Ajiri
	 replaced with new API's
 *12) g_debug remove in focus_row()		|28-feb-2013		|Dheeraj Kotagiri
 *13) Connected to ThornburyModel::sort-changed	|7-Mar-2013		|Abhiruchi
	signal
 *14) property "language" is added 		|14-Mar-2013		|Ramya Ajiri
 *15) changes in focus-row() to fix rollover 	|20-Mar-2013		|Sachin
	issue
 *16) overshoot::kinetic scrollview set to 0.4	|19-Mar-2013		|Ramya
	if it is short roller for making
	deceleration rate high.
 *17) Emit item-activated signal when
	focus-row() returns TRUE in short roller|27-Mar-2013		|Ramya
 *18) Setting width and height property of 	|03-Mar-2013		|Abhiruchi
	roller in roller resize to improve
	animation.
*19) In focus_row_mid() function changed the
        update_focused 2 argument to FALSE      |23-May-2013            |Ramya
* 20) if the roller width and the total items   |18-Jun-2013            |Ramya
	width are equal then consider it as short
	roller.
*21) item-activated for less items resolved 	|29-Jun-2103			|Ramya
*22) cc resolved 				|27-Aug-2013		|Ramya
*23) if roll-over is FALSE and if short roller	|23-Oct-2013		|Ramya Ajiri
     then on adding and removing items sets 
     adjustment to initial pos.
*24) ->Two properties added in roller:visible-items
     and first-visible-item to support ui to
     fill roller visible area first.		|30-Apr-2014		|Abhiruchi
     ->Signal scroll-finished added with 
     above mentioned properties data		|30-Apr-2014		|Abhiruchi	 
*25) work around fix given for roller		|07-Aug-2014			|Abhiruchi
     resize to resize all thumb items
     without any animation(Lightwood detail to cover)					
********************************************************************************/

#include "liblightwood-roller.h"
#include "roller-private.h"
#include "liblightwood-blureffect.h"
#include "liblightwood-cylinderdeform.h"

#include <math.h>
#include <mx/mx.h>

static void focusable_iface_init (MxFocusableIface *iface);
static void scrollable_iface_init (MxScrollableIface *iface);
static gboolean
focus_row (LightwoodRoller *roller, gint row_to_focus, gboolean animate,
           gboolean force_scroll,
           gfloat forced_roller_width,
           gfloat forced_item_width,
           gfloat forced_item_height);
/* brings the first row to focus when roller items are less*/
static gboolean
focus_row_mid (LightwoodRoller *roller, gint row, gboolean focus_mid, gboolean animate,
           gboolean force_scroll,
           gfloat forced_roller_width,
           gfloat forced_item_width,
           gfloat forced_item_height);
static gint get_glow_row(LightwoodRoller *roller);

G_DEFINE_ABSTRACT_TYPE_WITH_CODE (LightwoodRoller, lightwood_roller, CLUTTER_TYPE_ACTOR,
    G_IMPLEMENT_INTERFACE (MX_TYPE_FOCUSABLE,
                           focusable_iface_init)
    G_IMPLEMENT_INTERFACE (MX_TYPE_SCROLLABLE,
                           scrollable_iface_init));

#define BLUR_EFFECT_NAME "blur"
#define CYLINDER_DISTORT_EFFECT_NAME "cylinder-distort"

#define ROLLER_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), LIGHTWOOD_TYPE_ROLLER, LightwoodRollerPrivate))

struct _LightwoodRollerPrivate
{
  MxAdjustment *pHadjustment;
  MxAdjustment *pVadjustment;

  gboolean rollover_enabled;

  MxItemFactory *item_factory;
  GSList *attributes;
  ThornburyModel *model;

  gboolean motion_blur_enabled;
  ClutterEffect *blur_effect;
  gfloat previous_value;

  gboolean cylinder_distort_enabled;

  gboolean animation_mode;

  gint64 press_timestamp;

  ClutterActorBox allocation;

  guint clamp_duration;
  gulong clamp_mode;
  gboolean centre_focus;
  gint iSetRowToFocus;
  gboolean glow;
	gint iGlowRow;
  gboolean bLongPressEnable;
  gboolean bFlag;
};

enum {
  PROP_0,
  PROP_HADJUST,
  PROP_VADJUST,
  PROP_ROLL_OVER,
  PROP_FOCUSED_ROW,
  PROP_MODEL,
  PROP_FACTORY,
  PROP_MOTION_BLUR,
  PROP_CYLINDER_DISTORT,
  PROP_ANIMATION_MODE,
  PROP_CLAMP_MODE,
  PROP_CLAMP_DURATION,
  PROP_CENTRE_FOCUS,
  PROP_GLOW,
  PROP_GLOW_ROW,
  PROP_FIRST_VISIBLE_ITEM,
  PROP_VISIBLE_ITEMS,
};

enum
{
  LOCKING_FINISHED,
  SCROLL_FINISHED,
  ITEM_ACTIVATED,
  SCROLL_STARTED,
  SCROLLING,

  LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0, };


static void
update_focused (LightwoodRoller *roller, gboolean scrolling)
{
  LightwoodRollerPrivate *priv = roller->priv;
  gfloat content_height, middle;
  GSList *items, *iter;
  ClutterActorBox *allocations;
  gint i;
  guint len;

  ROLLER_DEBUG ("%s: scrolling: %s", G_STRFUNC, scrolling ? "yes" : "no");

  content_height = lightwood_roller_get_content_height (roller);
  middle = content_height / 2;

  items = lightwood_roller_get_actors (roller);
  allocations = lightwood_roller_get_allocations (roller, -1, -1, -1, TRUE, &len);

  g_assert (g_slist_length (items) == len);

  /* Unfocus for those items that share a shader at the class level, and focus
   * the new middle. */
  for (iter = items, i = 0; iter; iter = iter->next, i++)
    {
      ClutterActor *child_actor = (ClutterActor *) iter->data;
      gboolean is_focused;

      if (g_object_class_find_property (G_OBJECT_GET_CLASS (child_actor), "focused") == NULL)
        continue;

      is_focused = (allocations[i].y1 <= middle && allocations[i].y2 > middle &&
          !scrolling && !priv->animation_mode);

      g_object_set (child_actor, "focused", is_focused, NULL);
    }

  g_slice_free1 (sizeof(ClutterActorBox) * g_slist_length (items),
                 allocations);
  g_slist_free (items);
}

/* MxFocusable implementation */

static MxFocusable *
move_focus (MxFocusable      *focusable,
            MxFocusDirection  direction,
            MxFocusable      *from)
{
  return NULL;
}

static MxFocusable *
accept_focus (MxFocusable *focusable, MxFocusHint hint)
{
  return NULL;
}

static void
focusable_iface_init (MxFocusableIface *iface)
{
  iface->move_focus = move_focus;
  iface->accept_focus = accept_focus;
}
/* MxScrollable implementation */

static void
adjustment_value_notify_cb (MxAdjustment    *adjustment,
                            GParamSpec      *pspec,
                            LightwoodRoller       *roller)
{
  LightwoodRollerPrivate *priv = roller->priv;

  ROLLER_DEBUG ("%s, %p: adjustment = %p, value = %f", G_STRFUNC, roller,
                adjustment, mx_adjustment_get_value (adjustment));

  if (priv->motion_blur_enabled)
  {
    gdouble start;
    guint blur_steps;

    if (priv->blur_effect == NULL)
    {
      priv->blur_effect = g_object_ref_sink (lightwood_blur_effect_new ());
    }

    start = mx_adjustment_get_value (adjustment);
    blur_steps = CLAMP (ABS (start - priv->previous_value) / 2, 0, 20);
    priv->previous_value = start;

    lightwood_blur_effect_set_steps (LIGHTWOOD_BLUR_EFFECT (priv->blur_effect),
	blur_steps);

    if (clutter_actor_get_effect (CLUTTER_ACTOR (roller),
	  BLUR_EFFECT_NAME) == NULL)
    {
      clutter_actor_add_effect_with_name (CLUTTER_ACTOR (roller),
	  BLUR_EFFECT_NAME,
	  priv->blur_effect);
    }
  }

  clutter_actor_queue_redraw (CLUTTER_ACTOR (roller));
}
static void
_set_vertical_adjustment (MxScrollable *scrollable,
                          MxAdjustment *adjustment)
{
	LightwoodRollerPrivate *priv = LIGHTWOOD_ROLLER (scrollable)->priv;
	if (priv->pVadjustment != adjustment)
	{
		ROLLER_DEBUG ("%s, %p: setting vadjustment to %p, value = %f",
		              G_STRFUNC, scrollable, adjustment,
		              mx_adjustment_get_value (adjustment));

		if (priv->pVadjustment != NULL)
		{
			g_signal_handlers_disconnect_by_func (priv->pVadjustment,
					adjustment_value_notify_cb,
					scrollable);
			g_object_unref (priv->pVadjustment);
		}

		if (adjustment != NULL)
		{
			g_object_ref (adjustment);
			g_signal_connect_after (adjustment, "notify::value",
					G_CALLBACK (adjustment_value_notify_cb),
					scrollable);
		}

		priv->pVadjustment = adjustment;
		g_object_notify (G_OBJECT (scrollable), "vertical-adjustment");
	}
}
static void
_set_horizontal_adjustment (MxScrollable *scrollable,
                            MxAdjustment *adjustment)
{
	LightwoodRollerPrivate *priv = LIGHTWOOD_ROLLER (scrollable)->priv;
	if (priv->pHadjustment != adjustment)
	{
		if (priv->pHadjustment != NULL)
		{
			g_signal_handlers_disconnect_by_func (priv->pHadjustment,
					adjustment_value_notify_cb,
					scrollable);
			g_object_unref (priv->pHadjustment);
		}

		if (adjustment != NULL)
		{
			g_object_ref (adjustment);
			g_signal_connect_after (adjustment, "notify::value",
					G_CALLBACK (adjustment_value_notify_cb),
					scrollable);
		}

		priv->pHadjustment = adjustment;
		g_object_notify (G_OBJECT (scrollable), "horizontal-adjustment");
	}
}

static void
scrollable_set_adjustments (MxScrollable *scrollable,
                            MxAdjustment *pHadjustment,
                            MxAdjustment *pVadjustment)
{
	ROLLER_DEBUG("%s",__FUNCTION__);
	_set_vertical_adjustment (scrollable, pVadjustment);
	_set_horizontal_adjustment (scrollable, pHadjustment);
}

static void
scrollable_get_adjustments (MxScrollable  *scrollable,
                            MxAdjustment **pHadjustment,
                            MxAdjustment **pVadjustment)
{
  LightwoodRollerPrivate *priv = (LIGHTWOOD_ROLLER (scrollable))->priv;
	ROLLER_DEBUG("%s",__FUNCTION__);

  if (pHadjustment != NULL)
    *pHadjustment = priv->pHadjustment;

  if (pVadjustment != NULL)
    *pVadjustment = priv->pVadjustment;
}

static void
scrollable_iface_init (MxScrollableIface *iface)
{
  iface->set_adjustments = scrollable_set_adjustments;
  iface->get_adjustments = scrollable_get_adjustments;
}

/* ClutterActor implementation */

/**
 * lightwood_roller_get_scrolling_required:
 * @roller: A #LightwoodRoller
 *
 * Gets whether scrolling is required to access any of the items in the roller
 * in its current state. That is, whether the list items take up more height
 * than is available to the list.
 *
 * This is intended to be used by subclass implementations.
 *
 * Returns: %TRUE if scrolling is required, else %FALSE.
 */
gboolean
lightwood_roller_get_scrolling_required (LightwoodRoller *roller)
{
  gfloat preferred_height;

  clutter_actor_get_preferred_height (CLUTTER_ACTOR (roller), -1, NULL,
                                      &preferred_height);

  return preferred_height > lightwood_roller_get_content_height (roller);
}

/**
 * lightwood_roller_get_effective_rollover_enabled:
 * @roller: A #LightwoodRoller
 *
 * Gets whether rollover needs to be taken into account for the roller in its
 * current state. That is, whether the list items take up more height than is
 * available to the list, and hence need to be scrolled; and whether
 * #LightwoodRoller:roll-over is %TRUE.
 *
 * This is intended to be used by subclass implementations.
 *
 * Returns: %TRUE if roll-over is enabled, else %FALSE.
 */
gboolean
lightwood_roller_get_effective_rollover_enabled (LightwoodRoller *roller)
{
  LightwoodRollerPrivate *priv = roller->priv;

  return (priv->rollover_enabled &&
          lightwood_roller_get_scrolling_required (roller));
}

/**
 * lightwood_roller_get_allocations:
 * @roller: A #LightwoodRoller
 * @forced_roller_width: roller width
 * @forced_item_height: item height
 * @forced_item_width: item width
 * @force_calculation: %TRUE/%FALSE
 * @len: (out): the number of #ClutterActorBox returned
 *
 * It will get all the childs allocation value inside the 
 * roller container.
 *
 * Returns: (transfer full) (array length=len): allocation box for all visible children.
 */
ClutterActorBox *
lightwood_roller_get_allocations (LightwoodRoller *roller,
                            gfloat forced_roller_width,
                            gfloat forced_item_height,
                            gfloat forced_item_width,
                            gboolean force_calculation,
                            guint *len)
{
  LightwoodRollerPrivate *priv = roller->priv;
  LightwoodRollerClass *roller_class = LIGHTWOOD_ROLLER_GET_CLASS (roller);
  gfloat last_x, last_y, item_height, roller_width;
  GSList *visible_children, *iter;
  ClutterActorBox *allocations;
  guint i;

	ROLLER_DEBUG("%s",__FUNCTION__);

  g_return_val_if_fail (len != NULL, NULL);

  if (forced_roller_width < 0.0)
    roller_width = priv->allocation.x2 - priv->allocation.x1;
  else
    roller_width = forced_roller_width;

  visible_children = roller_class->get_visible_items (roller,
                                                      forced_roller_width,
                                                      forced_item_width,
                                                      &last_x,
                                                      &last_y);

  *len = g_slist_length (visible_children);

  ROLLER_DEBUG ("%s, %p: force_calculation = %s, len = %u, last_y = %f, "
                "animation_mode = %s", G_STRFUNC, roller,
                force_calculation ? "yes" : "no", *len, last_y,
                priv->animation_mode ? "yes" : "no");

  allocations = g_slice_alloc0 (sizeof (ClutterActorBox) * *len);

  item_height = 0.0;
  for (iter = visible_children, i = 0; iter; iter = iter->next, i++)
    {
      ClutterActor *child_actor = (ClutterActor *) iter->data;
      gfloat last_item_height, item_width;

      last_item_height = item_height;

      if (priv->animation_mode && !force_calculation)
        {
          if (forced_item_height < 0.0)
            item_height = clutter_actor_get_height (child_actor);
          else
            item_height = forced_item_height;

          if (forced_item_width < 0.0)
            item_width = clutter_actor_get_width (child_actor);
          else
            item_width = forced_item_width;
        }
      else
        {
          if (forced_item_height < 0.0)
            item_height = roller_class->get_item_height (roller, child_actor);
          else
            item_height = forced_item_height;

          if (forced_item_width < 0.0)
            item_width = roller_class->get_item_width (roller, child_actor);
          else
            item_width = forced_item_width;
        }

      if (last_x + item_width > roller_width)
        {
          last_x = 0.0;
          last_y += last_item_height;
        }

      if (priv->animation_mode && !force_calculation)
        {
          allocations[i].x1 = clutter_actor_get_x (child_actor);
          allocations[i].y1 = clutter_actor_get_y (child_actor);
        }
      else
        {
          allocations[i].x1 = last_x;
          allocations[i].y1 = last_y;
        }

      allocations[i].x2 = allocations[i].x1 + item_width;
      allocations[i].y2 = allocations[i].y1 + item_height;

      ROLLER_DEBUG ("%s, %p: allocation for child %p: x1 = %f, x2 = %f, "
                    "y1 = %f, y2 = %f, item_height = %f, last_item_height = %f",
                    G_STRFUNC, roller, child_actor, allocations[i].x1,
                    allocations[i].x2, allocations[i].y1, allocations[i].y2,
                    item_height, last_item_height);

#if 0
      g_debug ("allocating actor %p '%s' at %f",
               child_actor,
               clutter_actor_get_name (child_actor),
               allocations[i].y1);
#endif

      last_x = last_x + item_width;
    }
  g_slist_free (visible_children);

  return allocations;
}

static void
_roller_set_prop (ClutterActor *actor,
                  MxAdjustment *vadjust,
                  ClutterActor *parent)
{
  LightwoodRoller *self = LIGHTWOOD_ROLLER (actor);
  LightwoodRollerPrivate *priv = self->priv;
  gboolean clamp_value, elastic;

  if (priv->rollover_enabled &&
      lightwood_roller_get_effective_rollover_enabled (self))
    {
      /* If roll-over is true and if it is a long roller, disable elastic effect*/
      clamp_value = FALSE;
      elastic = FALSE;
    }
  else if (priv->rollover_enabled &&
           !lightwood_roller_get_scrolling_required (self))
    {
      /* If roll-over is true and it's a short roller, clamp between the
       * boundaries. */
      clamp_value = TRUE;
      elastic = FALSE;
    }
  else
    {
      /* If it is a List roller, set elastic effect */
      if (MX_IS_KINETIC_SCROLL_VIEW (parent))
        g_object_set (parent, "overshoot", 0.4, NULL);

      clamp_value = TRUE;
      elastic = TRUE;
    }

  g_object_set (G_OBJECT (vadjust),
                "clamp-value", clamp_value,
                "elastic", elastic,
                NULL);
}

static void
roller_set_vadjustment_prop (const ClutterActorBox *box,
                             ClutterActor          *actor,
                             MxAdjustment          *vadjust,
                             ClutterActor          *parent)
{
  LightwoodRoller *self = LIGHTWOOD_ROLLER (actor);
  LightwoodRollerClass *roller_class = LIGHTWOOD_ROLLER_GET_CLASS (self);
  LightwoodRollerPrivate *priv = self->priv;
  gfloat total_height, content_height;
  gfloat item_height = 0.0;
  gdouble page_size, step_increment, page_increment, lower, upper, overshoot;

  clutter_actor_get_preferred_height (actor, box->x2 - box->x1, NULL,
      &total_height);

  item_height = roller_class->get_item_height (self, NULL);
  content_height = lightwood_roller_get_content_height (self);

  /* When clamp value is true, to restric the bounds */
  if (priv->rollover_enabled &&
      !lightwood_roller_get_scrolling_required (self))
    {
      overshoot = 0.4;
      page_size = content_height;
      step_increment = item_height;
      page_increment = content_height;
      lower = content_height * -2.0;
      upper = total_height * 3.0;
    }
  else
    {
      overshoot = 1.0;
      page_size = content_height;
      step_increment = item_height;
      page_increment = content_height;
      lower = 0.0;
      upper = total_height;
    }

  if (MX_IS_KINETIC_SCROLL_VIEW (parent))
    g_object_set (parent, "overshoot", overshoot, NULL);

  g_object_set (G_OBJECT (vadjust),
      "page-size", page_size,
      "step-increment", step_increment,
      "page-increment", page_increment,
      "lower", lower,
      "upper", upper,
      NULL);

  _roller_set_prop (actor, vadjust, parent);
}

static
void _allocate_visible_children(LightwoodRoller *roller,ClutterAllocationFlags  flags)
{
  GSList *visible_children, *iter;
  ClutterActorBox *allocations;
  gint i;
  guint len;
  LightwoodRollerClass *roller_class = LIGHTWOOD_ROLLER_GET_CLASS (roller);
  visible_children = roller_class->get_visible_items (LIGHTWOOD_ROLLER (roller), -1,
      -1, NULL, NULL);
  allocations = lightwood_roller_get_allocations (LIGHTWOOD_ROLLER (roller),
      -1,  /* roller width */
      -1,  /* item height */
      -1,  /* item width */
      FALSE, /* force calculation */
      &len);

  for (iter = visible_children, i = 0; iter; iter = iter->next, i++)
  {
    ClutterActor *child_actor = (ClutterActor *) iter->data;
    ROLLER_DEBUG ("%s, %p: allocating child %p y1 = %f, y2 = %f", G_STRFUNC,
                  roller, child_actor, allocations[i].y1, allocations[i].y2);
    clutter_actor_allocate (child_actor, &allocations[i], flags);
  }
  g_slice_free1 (sizeof(ClutterActorBox) * g_slist_length (visible_children),
      allocations);
  g_slist_free (visible_children);
}

static void
roller_set_hadjustment_prop (MxAdjustment *hadjust,
                             gdouble       width)
{
  ROLLER_DEBUG ("%s: width: %f", G_STRFUNC, width);

  g_object_set (G_OBJECT (hadjust),
                "lower", 0.0,
                "upper", width,
                "page-size", width,
                "step-increment", width,
                "page-increment", width,
                NULL);
}

static void
actor_allocate (ClutterActor           *actor,
                const ClutterActorBox  *box,
                ClutterAllocationFlags  flags)
{
  LightwoodRollerPrivate *priv = LIGHTWOOD_ROLLER (actor)->priv;
  ClutterActor *parent = clutter_actor_get_parent (actor);
  ThornburyModel *model = lightwood_roller_get_model (LIGHTWOOD_ROLLER (actor));
  MxAdjustment *hadjust, *vadjust;
  MxKineticScrollViewState state;

  ROLLER_DEBUG("%s",__FUNCTION__);

  priv->allocation = *box;

  CLUTTER_ACTOR_CLASS (lightwood_roller_parent_class)->allocate (actor, box, flags);

  if (thornbury_model_get_n_rows (model) == 0)
    return;

  if (MX_IS_KINETIC_SCROLL_VIEW (parent))
  {
    g_object_get (parent, "state", &state, NULL);
    update_focused (LIGHTWOOD_ROLLER (actor),
	state != MX_KINETIC_SCROLL_VIEW_STATE_IDLE);
  }

  /*Allocate visible_children */
  _allocate_visible_children(LIGHTWOOD_ROLLER (actor),flags);

  g_object_get (G_OBJECT (actor),
      "horizontal-adjustment", &hadjust,
      "vertical-adjustment", &vadjust,
      NULL);

  /* set h-adhustment property */
  if (hadjust != NULL)
    roller_set_hadjustment_prop (hadjust, box->x2 - box->x1);

  if (vadjust != NULL)
    roller_set_vadjustment_prop (box, actor, vadjust, parent);

  g_clear_object (&vadjust);
  g_clear_object (&hadjust);
}

static void
actor_paint (ClutterActor *actor)
{
  GSList *visible_children, *l;

	ROLLER_DEBUG("%s",__FUNCTION__);

  visible_children = LIGHTWOOD_ROLLER_GET_CLASS (actor)->get_visible_items (
      LIGHTWOOD_ROLLER (actor), -1, -1, NULL, NULL);

  for (l = visible_children; l; l = g_slist_next (l))
    {
      const ClutterPaintVolume *volume;
      ClutterActor *child_actor = CLUTTER_ACTOR (l->data);

      volume = clutter_actor_get_transformed_paint_volume (child_actor, actor);
      if (volume != NULL)
        {
          ClutterVertex origin;

          clutter_paint_volume_get_origin (volume, &origin);
          g_debug ("%s: painting child %p with transformed paint volume %f×%f "
                   "at (%f, %f, %f)", G_STRFUNC, child_actor,
                   clutter_paint_volume_get_width (volume),
                   clutter_paint_volume_get_height (volume),
                   origin.x, origin.y, origin.z);
        }

      clutter_actor_paint (child_actor);
    }

  g_slist_free (visible_children);
}

static void
actor_pick (ClutterActor *actor, const ClutterColor *color)
{
  ROLLER_DEBUG ("%s", __FUNCTION__);

  /* Chain up to get an outline painted for the roller itself. */
  CLUTTER_ACTOR_CLASS (lightwood_roller_parent_class)->pick (actor, color);

  /* Draw the visible children. */
  actor_paint (actor);
}

static void
interpolation_completed_cb (MxAdjustment* vadjust, LightwoodRoller *roller)
{
  GSList *visible_children = NULL;

	ROLLER_DEBUG("%s",__FUNCTION__);
  clutter_actor_remove_effect_by_name (CLUTTER_ACTOR (roller), BLUR_EFFECT_NAME);

  update_focused (roller, FALSE);

  g_signal_emit (G_OBJECT (roller), signals[LOCKING_FINISHED], 0);

  /* get visible children in the roller */	
  visible_children = LIGHTWOOD_ROLLER_GET_CLASS (roller)->get_visible_items ( LIGHTWOOD_ROLLER (roller), -1, -1, NULL, NULL);
  
  /* emit scroll finished with total visible children numbers and the first visible child count */	
  if(visible_children)	
  {	
  	g_signal_emit (G_OBJECT (roller), signals[SCROLL_FINISHED], 0, LIGHTWOOD_ROLLER_GET_CLASS (roller)->get_row_from_item (LIGHTWOOD_ROLLER (roller), 
							g_slist_nth_data(visible_children, 0) ), g_slist_length(visible_children) );	
	g_slist_free (visible_children);
  }
  else
  	g_signal_emit (G_OBJECT (roller), signals[SCROLL_FINISHED], 0, 0, 0);		
}

static gboolean
focus_row (LightwoodRoller *roller, gint row_to_focus, gboolean animate,
           gboolean force_scroll,
           gfloat forced_roller_width,
           gfloat forced_item_width,
           gfloat forced_item_height)
{
  LightwoodRollerPrivate *priv = roller->priv;
  LightwoodRollerClass *roller_class = LIGHTWOOD_ROLLER_GET_CLASS (roller);
  MxAdjustment *vadjust;
  ClutterActor *child;
  gfloat item_height, new_value, new_value_up, new_value_down, content_height,
	 child_y1, total_height;
  gdouble start;
  GSList *items;
  ClutterActorBox *allocations;
  ClutterActorBox child_box = { 0.0, };
  gulong clamp_mode;
  guint len;

  ROLLER_DEBUG ("%s: row_to_focus: %d, animate: %s, force_scroll: %s, "
                "forced_roller_width: %f, forced_item_width: %f, "
                "forced_item_height: %f", G_STRFUNC, row_to_focus,
                animate ? "yes" : "no", force_scroll ? "yes" : "no",
                forced_roller_width, forced_item_width, forced_item_height);

  child = roller_class->get_item_from_row (roller,
      row_to_focus,
      forced_roller_width,
      forced_item_width,
      forced_item_height,
      &child_y1);

  /* Move to next position, choose the closest direction */
  content_height = lightwood_roller_get_content_height (roller);

  ROLLER_DEBUG ("%s: child: %p, child_y1: %f, content_height: %f", G_STRFUNC,
                child, child_y1, content_height);

  /* Check if this row is already in the middle */
  if (!force_scroll && child != NULL)
  {
    /* Get allocation for this child */
    items = lightwood_roller_get_actors (roller);
    allocations = lightwood_roller_get_allocations (roller, -1, -1, -1, TRUE, &len);
    child_box = allocations[g_slist_index (items, child)];

    ROLLER_DEBUG ("%s: child_box.y1: %f, child_box.y2: %f", G_STRFUNC,
                  child_box.y1, child_box.y2);

    g_slice_free1 (sizeof(ClutterActorBox) * g_slist_length (items),
	allocations);
    g_slist_free (items);

    if (child_box.y1 < content_height / 2 &&
	child_box.y2 > content_height / 2)
    {
      g_object_unref (child);
      return FALSE;
    }
  }

  /* For the fixed roller, child may be NULL, but it doesn't matter */
  if (forced_item_height == -1)
    item_height = roller_class->get_item_height (roller, child);
  else
    item_height = forced_item_height;

  if (child != NULL)
    g_object_unref (child);

  mx_scrollable_get_adjustments (MX_SCROLLABLE (roller), NULL, &vadjust);
  if (vadjust != NULL)
    start = mx_adjustment_get_value (vadjust);
  else
    start = 0.0;

  new_value_down = child_y1 - content_height / 2 + item_height / 2;

  total_height = roller_class->get_total_height (roller,
      forced_roller_width,
      forced_item_width,
      forced_item_height);
  new_value_up = new_value_down - total_height;

  ROLLER_DEBUG ("%s: item_height: %f, start: %f, new_value_down: %f, "
                "total_height: %f, new_value_up: %f",
                G_STRFUNC, item_height, start, new_value_down, total_height,
                new_value_up);

  /* check for short roller */
  if (priv->rollover_enabled &&
      !lightwood_roller_get_scrolling_required (LIGHTWOOD_ROLLER (roller)))
  {
	gfloat y2;
          if((child_box.y1 < 0) &&
                (child_box.y2 < 0 ))
          {
                  y2 = ABS (child_box.y1) + item_height;

                          if(ABS (child_box.y1) < content_height / 2 &&
                                          y2 > content_height / 2)
                          {
                                  g_debug ("returnin false in short roller");
                                  return FALSE;
                          }
          }

    if (ABS (start - new_value_up) <= ABS (start - new_value_down))
      new_value = new_value_up;
    else
      new_value = new_value_down;
  }
  else
  {
      if (ABS (start - new_value_up) <= ABS (start - new_value_down))
        new_value = new_value_up;
      else
        new_value = new_value_down;
  }

  /* Due to the deprecation of clutter_alpha_register_func(), custom
   * animation modes no longer work here. Hard-code something
   * unoffensive until libmx catches up to use
   * clutter_timeline_set_progress_func(). */
  if (priv->clamp_mode >= CLUTTER_ANIMATION_LAST)
   clamp_mode = CLUTTER_EASE_OUT_ELASTIC;
  else
    clamp_mode = priv->clamp_mode;

  ROLLER_DEBUG ("%s: new_value: %f, clamp_mode: %u",
                G_STRFUNC, new_value, clamp_mode);

  if (animate)
  {
    g_signal_emit (G_OBJECT (roller), signals[SCROLL_STARTED], 0);

    mx_adjustment_interpolate (vadjust,
	new_value,
	priv->clamp_duration,
	clamp_mode);
  }
  else
    mx_adjustment_set_value (vadjust, new_value);

  update_focused (roller, TRUE);

  ROLLER_DEBUG ("%s, %p: set focused_row = %i, new_value = %f", G_STRFUNC,
                roller, row_to_focus, new_value);

  return TRUE;
}
static gboolean
focus_row_mid (LightwoodRoller *roller, gint row, gboolean focus_mid,gboolean animate,
           gboolean force_scroll,
           gfloat forced_roller_width,
           gfloat forced_item_width,
           gfloat forced_item_height)
{
  LightwoodRollerPrivate *priv = roller->priv;
  LightwoodRollerClass *roller_class = LIGHTWOOD_ROLLER_GET_CLASS (roller);
  MxAdjustment *vadjust;
  ClutterActor *child = NULL;
  gfloat item_height,new_value_down, content_height,
	 child_y1;
  GSList *items;
  ClutterActorBox *allocations;
  ClutterActorBox child_box;
  guint len;
  gfloat fChildWidth;

  ROLLER_DEBUG ("%s: row: %d, focus_mid: %s, animate: %s, force_scroll: %s, "
                "forced_roller_width: %f, forced_item_width: %f, "
                "forced_item_height: %f", G_STRFUNC, row,
                focus_mid ? "yes" : "no", animate ? "yes" : "no",
                force_scroll ? "yes" : "no", forced_roller_width,
                forced_item_width, forced_item_height);

  child = roller_class->get_item_from_row (roller,
      0,
      forced_roller_width,
      forced_item_width,
      forced_item_height,
      &child_y1);

  /* Move to next position, choose the closest direction */
  content_height = lightwood_roller_get_content_height (roller);

  ROLLER_DEBUG ("%s: child: %p, child_y1: %f, content_height: %f", G_STRFUNC,
                child, child_y1, content_height);

  /* Check if this row is already in the middle */
  if (!force_scroll && child != NULL)
  {
    /* Get allocation for this child */
    items = lightwood_roller_get_actors (roller);
    allocations = lightwood_roller_get_allocations (roller, -1, -1, -1, TRUE, &len);
    child_box = allocations[g_slist_index (items, child)];

    ROLLER_DEBUG ("%s: child_box.y1: %f, child_box.y2: %f", G_STRFUNC,
                  child_box.y1, child_box.y2);

    g_slice_free1 (sizeof(ClutterActorBox) * g_slist_length (items),
	allocations);
    g_slist_free (items);

    if (child_box.y1 < content_height / 2 &&
	child_box.y2 > content_height / 2)
    {
      g_object_unref (child);
      return FALSE;
    }
  }

  /* For the fixed roller, child may be NULL, but it doesn't matter */
  if (forced_item_height == -1)
    item_height = roller_class->get_item_height (roller, child);
  else
    item_height = forced_item_height;

  if (child != NULL)
    g_object_unref (child);

  if(focus_mid)
  {
    new_value_down = - content_height / 2 + item_height / 2;
  }
  else
  {
    new_value_down = 0.0;
  }

  /* for Other than first item,find the new value to bring to focus */
  fChildWidth = LIGHTWOOD_ROLLER_GET_CLASS (roller)->get_item_width(LIGHTWOOD_ROLLER(roller),NULL);

  ROLLER_DEBUG ("%s: item_height: %f, new_value_down: %f, fChildWidth: %f",
                G_STRFUNC, item_height, new_value_down, fChildWidth);

  if(focus_mid && row > 0)
  {
    row = row /floor((priv->allocation.x2 - priv->allocation.x1) / fChildWidth);
    new_value_down += item_height * row ;
  }

  ROLLER_DEBUG ("%s: row: %d, new_value_down: %f",
                G_STRFUNC, row, new_value_down);

  mx_scrollable_get_adjustments (MX_SCROLLABLE (roller), NULL, &vadjust);
  if (animate)
  {
    g_signal_emit (G_OBJECT (roller), signals[SCROLL_STARTED], 0);

    mx_adjustment_interpolate (vadjust,
	new_value_down,
	priv->clamp_duration,
	priv->clamp_mode);
  }
  else
    mx_adjustment_set_value (vadjust, new_value_down);
  update_focused (roller, FALSE);

  ROLLER_DEBUG ("%s, %p: set focused_row = %i, new_value_down = %f", G_STRFUNC,
                roller, row, new_value_down);

  return TRUE;
}

static gint
get_focused_row (LightwoodRoller *roller)
{
  LightwoodRollerClass *roller_class = LIGHTWOOD_ROLLER_GET_CLASS (roller);
  MxAdjustment *adjustment;
  gfloat content_height;
  gfloat middle;
  gdouble value;
  gint row;

  ROLLER_DEBUG("%s",__FUNCTION__);

  content_height = lightwood_roller_get_content_height (roller);

  mx_scrollable_get_adjustments (MX_SCROLLABLE (roller), NULL, &adjustment);
  value = mx_adjustment_get_value (adjustment);
  middle = value + content_height / 2;

  row = roller_class->get_row_from_coord (roller, 0, middle);

  ROLLER_DEBUG ("%s, %p: got focused_row = %i from middle = %f, value = %f, "
                "content_height = %f", G_STRFUNC, roller, row, middle, value,
                content_height);

  return row;
}

/* GObject implementation */

static void
get_property (GObject    *object,
              guint       property_id,
              GValue     *value,
              GParamSpec *pspec)
{
  LightwoodRoller *roller = LIGHTWOOD_ROLLER (object);

  switch (property_id)
    {
    case PROP_HADJUST:
      g_value_set_object (value, lightwood_roller_get_horizontal_adjustment (roller));
      break;

    case PROP_VADJUST:
      g_value_set_object (value, lightwood_roller_get_vertical_adjustment (roller));
      break;

    case PROP_ROLL_OVER:
      g_value_set_boolean (value, lightwood_roller_get_roll_over (roller));
      break;

    case PROP_FOCUSED_ROW:
      g_value_set_int (value, get_focused_row (roller));
      break;

    case PROP_GLOW_ROW:
      g_value_set_int (value, get_glow_row (roller));
      break;

    case PROP_CENTRE_FOCUS:
      g_value_set_boolean (value, lightwood_roller_get_focus_to_centre (roller));
      break;


    case PROP_MODEL:
      g_value_set_object (value, roller->priv->model);
      break;

    case PROP_FACTORY:
      g_value_set_object (value, roller->priv->item_factory);
      break;

    case PROP_MOTION_BLUR:
      g_value_set_boolean (value, lightwood_roller_get_motion_blur (roller));
      break;

    case PROP_CYLINDER_DISTORT:
      g_value_set_boolean (value, lightwood_roller_get_cylinder_distort (roller));
      break;

    case PROP_ANIMATION_MODE:
      g_value_set_boolean (value, lightwood_roller_get_animation_mode (roller));
      break;

    case PROP_CLAMP_MODE:
      g_value_set_ulong (value, lightwood_roller_get_clamp_mode (roller));
      break;

    case PROP_CLAMP_DURATION:
      g_value_set_uint (value, lightwood_roller_get_clamp_duration (roller));
      break;

    case PROP_FIRST_VISIBLE_ITEM:
      g_value_set_int (value, lightwood_roller_get_first_visible_item (roller));
      break;
    case PROP_VISIBLE_ITEMS:
      g_value_set_uint (value, lightwood_roller_get_visible_items (roller));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
set_property (GObject      *object,
              guint         property_id,
              const GValue *value,
              GParamSpec   *pspec)
{
  LightwoodRoller *roller = LIGHTWOOD_ROLLER (object);

  switch (property_id)
  {
    case PROP_HADJUST:
      lightwood_roller_set_horizontal_adjustment (roller, g_value_get_object (value));
      break;

    case PROP_VADJUST:
      lightwood_roller_set_vertical_adjustment (roller, g_value_get_object (value));
      break;

    case PROP_ROLL_OVER:
      lightwood_roller_set_roll_over (LIGHTWOOD_ROLLER (object),
	  g_value_get_boolean (value));
      break;

    case PROP_MODEL:
      lightwood_roller_set_model (LIGHTWOOD_ROLLER (object), g_value_get_object (value));
      break;

    case PROP_FACTORY:
      lightwood_roller_set_factory (LIGHTWOOD_ROLLER (object), g_value_get_object (value));
      break;

    case PROP_FOCUSED_ROW:
      {
	focus_row (LIGHTWOOD_ROLLER (object), g_value_get_int (value), TRUE, TRUE, -1, -1, -1);
      }
      break;

    case PROP_GLOW_ROW:
      lightwood_roller_set_glow_row (LIGHTWOOD_ROLLER (object), g_value_get_int (value));
      break;

    case PROP_CENTRE_FOCUS:
      {
	lightwood_roller_set_focus_to_centre (LIGHTWOOD_ROLLER (object),
	    g_value_get_boolean (value));
	break;
      }

    case PROP_MOTION_BLUR:
      lightwood_roller_set_motion_blur (LIGHTWOOD_ROLLER (object),
	  g_value_get_boolean (value));
      break;

    case PROP_CYLINDER_DISTORT:
      lightwood_roller_set_cylinder_distort (LIGHTWOOD_ROLLER (object),
	  g_value_get_boolean (value));
      break;

    case PROP_ANIMATION_MODE:
      lightwood_roller_set_animation_mode (LIGHTWOOD_ROLLER (object),
	  g_value_get_boolean (value));
      break;

    case PROP_CLAMP_MODE:
      lightwood_roller_set_clamp_mode (roller, g_value_get_ulong (value));
      break;

    case PROP_CLAMP_DURATION:
      lightwood_roller_set_clamp_duration (roller, g_value_get_uint (value));
      break;

    case PROP_GLOW:
      lightwood_roller_set_glow (roller, g_value_get_boolean (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
  }
}

static void
dispose (GObject *object)
{
  LightwoodRollerPrivate *priv = LIGHTWOOD_ROLLER(object)->priv;

  if (priv->pHadjustment != NULL)
    {
      g_object_unref (priv->pHadjustment);
      priv->pHadjustment = NULL;
    }

  if (priv->pVadjustment != NULL)
    {
      g_object_unref (priv->pVadjustment);
      priv->pVadjustment = NULL;
    }

  if (priv->item_factory != NULL)
    {
      g_object_unref (priv->item_factory);
      priv->item_factory = NULL;
    }

  if (priv->attributes != NULL)
    {
      g_slist_free_full (priv->attributes, g_free);
      priv->attributes = NULL;
    }

  if (priv->model != NULL)
    {
      g_object_unref (priv->model);
      priv->model = NULL;
    }

  if (priv->blur_effect != NULL)
    {
      g_object_unref (priv->blur_effect);
      priv->blur_effect = NULL;
    }

  G_OBJECT_CLASS (lightwood_roller_parent_class)->dispose (object);
}

static void
finalize (GObject *object)
{
  G_OBJECT_CLASS (lightwood_roller_parent_class)->finalize (object);
}

static gboolean
actor_captured_event (ClutterActor *actor,
                      ClutterEvent *event)
{
	LightwoodRollerPrivate *priv = LIGHTWOOD_ROLLER (actor)->priv;
	ClutterActor *stage, *child_actor;
	gint row;
	gboolean value;
	gfloat event_x = 0.0, event_y = 0.0;

	/* Grab the X and Y coordinate from the event. */
	switch (event->type)
	{
		case CLUTTER_BUTTON_PRESS:
		case CLUTTER_BUTTON_RELEASE: {
			ClutterButtonEvent *button_event = (ClutterButtonEvent *) event;

			event_x = button_event->x;
			event_y = button_event->y;

			break;
		}
		case CLUTTER_TOUCH_BEGIN:
		case CLUTTER_TOUCH_END: {
			ClutterTouchEvent *touch_event = (ClutterTouchEvent *) event;

			event_x = touch_event->x;
			event_y = touch_event->y;

			break;
		}
	}

	/* Handle the events. */
	switch (event->type)
	{
		case CLUTTER_BUTTON_PRESS:
		case CLUTTER_TOUCH_BEGIN:
			{
				g_debug ("%s: button-press/touch-begin: "
				         "bLongPressEnable: %d, "
				         "press_timestamp: %" G_GINT64_FORMAT ", "
				         "bFlag: %d, "
				         "x: %f, y: %f, "
				         "rollover_enabled: %d",
				         G_STRFUNC, priv->bLongPressEnable,
				         priv->press_timestamp,
				         priv->bFlag, event_x, event_y,
				         priv->rollover_enabled);

				if(!priv->bLongPressEnable)
				{
					priv->press_timestamp = g_get_monotonic_time ();
				}
				priv->bFlag = TRUE;
			}
			/* Ignore event, and if we are still moving, MxKineticScrollView will stop */
			return FALSE;

		case CLUTTER_BUTTON_RELEASE:
		case CLUTTER_TOUCH_END:
			g_debug ("%s: button-release/touch-end: "
			              "bLongPressEnable: %d, "
			              "press_timestamp: %" G_GINT64_FORMAT ", "
			              "bFlag: %d, "
			              "x: %f, y: %f, "
			              "rollover_enabled: %d",
			              G_STRFUNC, priv->bLongPressEnable,
			              priv->press_timestamp,
			              priv->bFlag, event_x, event_y,
			              priv->rollover_enabled);

			if(!priv->bLongPressEnable)
			{
				/* Consider we were panning, so don't focus or activate items */
				if (g_get_monotonic_time () - priv->press_timestamp > 250000)
					return TRUE;

				priv->press_timestamp = 0;
			}
			// making sure press and release event ocuurs in the widget.
			if(!priv->bFlag)
			{
				return TRUE;
			}

			priv->bFlag = FALSE;

			stage = clutter_actor_get_stage (actor);
			child_actor = clutter_stage_get_actor_at_pos (CLUTTER_STAGE (stage),
					CLUTTER_PICK_REACTIVE,
					event_x,
					event_y);

			row = LIGHTWOOD_ROLLER_GET_CLASS (actor)->get_row_from_item (LIGHTWOOD_ROLLER (actor),
					child_actor);

			g_debug ("%s: child_actor: %p, row: %i",
			              G_STRFUNC, child_actor, row);

			if (row < 0)
				return FALSE;
			/* If rollover is disabled allow item selection without briging the item to mid of roller */
			if(priv->rollover_enabled == FALSE)
			{
				return FALSE;
			}

			/* Roller Animation Issue */
			value = focus_row (LIGHTWOOD_ROLLER (actor), row, TRUE, FALSE, -1, -1, -1);
			g_debug ("%s: value: %d", G_STRFUNC, value);

			return value;

		case CLUTTER_TOUCH_CANCEL:
			priv->bFlag = FALSE;

    case CLUTTER_NOTHING:
    case CLUTTER_KEY_PRESS:
    case CLUTTER_KEY_RELEASE:
    case CLUTTER_MOTION:
    case CLUTTER_ENTER:
    case CLUTTER_LEAVE:
    case CLUTTER_SCROLL:
    case CLUTTER_STAGE_STATE:
    case CLUTTER_DESTROY_NOTIFY:
    case CLUTTER_CLIENT_MESSAGE:
    case CLUTTER_DELETE:
    case CLUTTER_TOUCH_UPDATE:
    case CLUTTER_EVENT_LAST:
    default:
      return FALSE;
	}
}

static void
notify_state_cb (MxKineticScrollView *scroll, GParamSpec *pspec,
                 LightwoodRoller *roller)
{
	MxKineticScrollViewState state;

	ROLLER_DEBUG("%s",__FUNCTION__);

  g_object_get (scroll, "state", &state, NULL);
  update_focused (roller, state != MX_KINETIC_SCROLL_VIEW_STATE_IDLE);
  if(state == MX_KINETIC_SCROLL_VIEW_STATE_PANNING)
	{
	
    g_signal_emit (G_OBJECT (roller), signals[SCROLL_STARTED], 0);//emit the signal on scroll started
				roller->priv->bFlag = FALSE;
	}
  if(state == MX_KINETIC_SCROLL_VIEW_STATE_SCROLLING)
	{
    g_signal_emit (G_OBJECT (roller), signals[SCROLLING], 0);//emit the signal on SCROLLING
	}
}

static void
actor_parent_set (ClutterActor *actor, ClutterActor *old_parent)
{
  ClutterActor *parent = clutter_actor_get_parent (actor);

	ROLLER_DEBUG("%s",__FUNCTION__);

  if (old_parent != NULL && MX_IS_KINETIC_SCROLL_VIEW (old_parent))
    g_signal_handlers_disconnect_by_func (old_parent, notify_state_cb, actor);

  if (parent != NULL && MX_IS_KINETIC_SCROLL_VIEW (parent))
    g_signal_connect (parent, "notify::state",
                      G_CALLBACK (notify_state_cb), actor);
}


static gint
get_glow_row(LightwoodRoller *roller)
{
	GSList *items, *iter;
	gint i;
	gboolean glow_row = -1;

	items = lightwood_roller_get_actors (roller);
	/* First unfocus for those items that share a shader at the class level */
        for (iter = items, i = 0; iter; iter = iter->next, i++)
        {
                ClutterActor *child_actor = (ClutterActor *) iter->data;

                if (g_object_class_find_property (G_OBJECT_GET_CLASS (child_actor), "highlight") == NULL)
                        continue;

                g_object_get (child_actor, "highlight", &glow_row, NULL);
		if(glow_row == TRUE)
			return glow_row;
        }
	return -1;
}



static void
update_glow (LightwoodRoller *roller, gint row)
{
	GSList *items, *iter;
	ClutterActorBox *allocations;
	gint i;
	guint len;
	ClutterActor *child_actor;
	ClutterActor *glow_actor = NULL;

	ROLLER_DEBUG("%s",__FUNCTION__);

	items = lightwood_roller_get_actors (roller);
	allocations = lightwood_roller_get_allocations (roller, -1, -1, -1, TRUE, &len);
	if (row >= 0)
		glow_actor = child_actor = lightwood_roller_get_actor_for_row (roller, row);

	/* First unfocus for those items that share a shader at the class level */
	for (iter = items, i = 0; iter; iter = iter->next, i++)
	{
		child_actor = (ClutterActor *) iter->data;

		if (g_object_class_find_property (G_OBJECT_GET_CLASS (child_actor), "highlight") == NULL)
			continue;

		g_object_set (child_actor,
		              "highlight", (child_actor == glow_actor),
		              NULL);
	}

	g_slice_free1 (sizeof(ClutterActorBox) * g_slist_length (items),
			allocations);
	g_slist_free (items);
}
/**
 * lightwood_roller_set_glow_row:
 * @roller: A #LightwoodRoller
 * @row: A row to be highLighted.
 * 
 * property :highlight has to exposed from the roller item object. 
 * This function is used only when :roll-over is %FALSE. 
 * when it is a list roller if we want to give the glow effect, 
 * then roller item object can have :highlight. property defined.
 * 
 */


void
lightwood_roller_set_glow_row (LightwoodRoller *roller,
                               gint row)
{
	update_glow(roller,row);
	return;
}

static gboolean
event_cb (ClutterActor       *actor,
                         ClutterButtonEvent *event,
                         gpointer            user_data)
{
  switch(event->type)
  {
    case CLUTTER_BUTTON_RELEASE:
    case CLUTTER_TOUCH_END:
      {
	ClutterActor *stage, *child_actor;
	LightwoodRollerPrivate *priv = LIGHTWOOD_ROLLER (actor)->priv;
	stage = clutter_actor_get_stage (actor);
	child_actor = clutter_stage_get_actor_at_pos (CLUTTER_STAGE (stage),
	    CLUTTER_PICK_REACTIVE,
	    event->x, event->y);

	if (child_actor != NULL)
	{
	  gint row = LIGHTWOOD_ROLLER_GET_CLASS (actor)->get_row_from_item (LIGHTWOOD_ROLLER (actor),
	      child_actor);
	  /* when row returned is 0 or the max value in the model,
	     then it is always first row in the roller */
	  if(row == 0 || row == thornbury_model_get_n_rows(priv->model))
	    row = 0;
	  /* row = -1 should not be given to applications */
	  if(row != -1)
	  {
		if(priv->glow == TRUE)
		update_glow(LIGHTWOOD_ROLLER (actor),row);
		g_debug ("ROLLER:ITEM_ACTIVATED");
	    g_signal_emit (actor, signals[ITEM_ACTIVATED], 0, row);
	  }
	  return TRUE;
	}
      }

    case CLUTTER_NOTHING:
    case CLUTTER_KEY_PRESS:
    case CLUTTER_KEY_RELEASE:
    case CLUTTER_MOTION:
    case CLUTTER_ENTER:
    case CLUTTER_LEAVE:
    case CLUTTER_BUTTON_PRESS:
    case CLUTTER_SCROLL:
    case CLUTTER_STAGE_STATE:
    case CLUTTER_DESTROY_NOTIFY:
    case CLUTTER_CLIENT_MESSAGE:
    case CLUTTER_DELETE:
    case CLUTTER_TOUCH_BEGIN:
    case CLUTTER_TOUCH_UPDATE:
    case CLUTTER_TOUCH_CANCEL:
    case CLUTTER_EVENT_LAST:
    default:
      return FALSE;
  }

}

static void
lightwood_roller_class_init (LightwoodRollerClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	ClutterActorClass *actor_class = CLUTTER_ACTOR_CLASS (klass);
	GParamSpec *pspec;

	g_type_class_add_private (klass, sizeof (LightwoodRollerPrivate));

	object_class->get_property = get_property;
	object_class->set_property = set_property;
	object_class->dispose = dispose;
	object_class->finalize = finalize;

	actor_class->paint = actor_paint;
	actor_class->pick = actor_pick;
	actor_class->allocate = actor_allocate;
	actor_class->captured_event = actor_captured_event;
	actor_class->parent_set = actor_parent_set;

	pspec = g_param_spec_boolean ("roll-over",
			"Roll over",
			"Roll over to the start when the end is reached",
			FALSE,
			G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_ROLL_OVER, pspec);

	pspec = g_param_spec_int ("focused-row",
			"Focused row",
			"Row currently focused",
			-1,
			G_MAXINT,
			-1,
			G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_FOCUSED_ROW, pspec);

	pspec = g_param_spec_int ("first-visible-item",
                        "First Visible Item",
                        "Row currently visible",
                        -1,
                        G_MAXINT,
                        -1,
                        G_PARAM_READABLE);
        g_object_class_install_property (object_class, PROP_FIRST_VISIBLE_ITEM, pspec);


	pspec = g_param_spec_uint ("visible-items",
                        "Visible Items",
                        "Items Currently Visible",
                        0,
                        G_MAXUINT,
                        0,
                        G_PARAM_READABLE);
        g_object_class_install_property (object_class, PROP_VISIBLE_ITEMS, pspec);

	pspec = g_param_spec_int ("glow-row",
                        "glow row",
                        "Row currently highlighted",
                        -1,
                        G_MAXINT,
                        -1,
                        G_PARAM_READWRITE);
        g_object_class_install_property (object_class, PROP_GLOW_ROW, pspec);

	pspec = g_param_spec_object ("model",
			"Model",
			"Model to use to construct the items",
			G_TYPE_OBJECT, /* has to be changed with T808 */
			G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_MODEL, pspec);

	pspec = g_param_spec_object ("factory",
			"Item factory",
			"MxItemFactory to use for creating the children",
			G_TYPE_OBJECT, /* has to implement MxItemFactoryIface */
			G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_FACTORY, pspec);

	pspec = g_param_spec_boolean ("motion-blur",
			"Motion blur",
			"Enable motion blur while scrolling",
			FALSE,
			G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_MOTION_BLUR, pspec);

	pspec = g_param_spec_boolean ("cylinder-distort",
			"Cylinder distort",
			"Enable cylinder distort effect while scrolling",
			FALSE,
			G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_CYLINDER_DISTORT,
			pspec);

	pspec = g_param_spec_boolean ("animation-mode",
			"Animation mode",
			"Allocates children based on their fixed size "
			"and position, assures children actors won't "
			"be repointed to other rows",
			FALSE,
			G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_ANIMATION_MODE, pspec);

	pspec = g_param_spec_uint ("clamp-duration",
			"Clamp duration",
			"Duration of the adjustment clamp animation.",
			0, G_MAXUINT, 250,
			G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_CLAMP_DURATION, pspec);

	pspec = g_param_spec_ulong ("clamp-mode",
			"Clamp mode",
			"Animation mode to use for the clamp animation.",
			0, G_MAXULONG, CLUTTER_EASE_OUT_QUAD,
			G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_CLAMP_MODE, pspec);

	pspec = g_param_spec_boolean ("focus-to-centre",
			"Focus to centre",
			"First item will be in the middle of the roller",
			FALSE,
			G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_CENTRE_FOCUS, pspec);

	pspec = g_param_spec_boolean ("glow",
                        "glow",
                        "Glow effect is enabled",
                        FALSE,
                        G_PARAM_READWRITE);

	g_object_class_install_property (object_class, PROP_GLOW, pspec);

	g_object_class_override_property (object_class,
			PROP_HADJUST,
			"horizontal-adjustment");

	g_object_class_override_property (object_class,
			PROP_VADJUST,
			"vertical-adjustment");

	/**
	 * LightwoodRoller::locking-finished:
 	 * @roller:roller object
	 *
	 * The ::locking-finished Emitted when the locking animation finishes
	 */
	signals[LOCKING_FINISHED] =
		g_signal_new ("locking-finished",
				G_TYPE_FROM_CLASS (klass),
				G_SIGNAL_RUN_LAST,
				G_STRUCT_OFFSET (LightwoodRollerClass, locking_finished),
				NULL, NULL,
				g_cclosure_marshal_VOID__VOID,
				G_TYPE_NONE, 0);
	
	/**
         * LightwoodRoller::scroll-finished:
         * @roller: the #LightwoodRoller emitting this signal
         * @first: the index of the first visible child, if any, or 0
         * @nb_visible: the number of visible children
         *
         * Emitted when the locking animation finishes
         */
        signals[SCROLL_FINISHED] = 
		 g_signal_new ("scroll-finished",
                                G_TYPE_FROM_CLASS (klass),
                                G_SIGNAL_RUN_LAST,
                                G_STRUCT_OFFSET (LightwoodRollerClass, scroll_finished),
                                NULL, NULL,
                                g_cclosure_marshal_generic,
                                G_TYPE_NONE, 2, G_TYPE_UINT, G_TYPE_UINT);

	/**
	 * LightwoodRoller::item-activated:
	 * @roller: the #LightwoodRoller emitting this signal
	 * @row: the id of the activated row
	 *
	 * Emitted when an item gets activated, only if the item doesn't block the
	 * release event.
	 */
	signals[ITEM_ACTIVATED] =
		g_signal_new ("item-activated",
				G_TYPE_FROM_CLASS (klass),
				G_SIGNAL_RUN_LAST,
				G_STRUCT_OFFSET (LightwoodRollerClass, item_activated),
				NULL, NULL,
				g_cclosure_marshal_VOID__UINT,
				G_TYPE_NONE, 1, G_TYPE_UINT);
	/**
	 * LightwoodRoller::scroll-started:
	 *
	 * Emitted when an drag threshold value exceeds 
	 */

	signals[SCROLL_STARTED] =
		g_signal_new ("scroll-started",
				G_TYPE_FROM_CLASS (klass),
				G_SIGNAL_RUN_LAST,
				G_STRUCT_OFFSET (LightwoodRollerClass, scroll_started),
				NULL, NULL,
				g_cclosure_marshal_VOID__VOID,
				G_TYPE_NONE, 0);
	/**
	 * LightwoodRoller::scrolling:
	 *
	 * Emitted while scrolling.
	 */



	signals[SCROLLING] =
		g_signal_new ("scrolling",
				G_TYPE_FROM_CLASS (klass),
				G_SIGNAL_RUN_LAST,
				G_STRUCT_OFFSET (LightwoodRollerClass, scrolling),
				NULL, NULL,
				g_cclosure_marshal_VOID__VOID,
				G_TYPE_NONE, 0);
}

static void
lightwood_roller_init (LightwoodRoller *self)
{
  MxAdjustment *hadjust, *vadjust;

  self->priv = ROLLER_PRIVATE (self);
  self->priv->rollover_enabled = FALSE;
  self->priv->model = NULL;
  self->priv->clamp_duration = 1250;
  self->priv->clamp_mode = CLUTTER_EASE_OUT_ELASTIC;
  self->priv->centre_focus = TRUE;
	self->priv->bLongPressEnable = TRUE;
	self->priv->glow = TRUE;
  self->priv->bFlag = FALSE;

  clutter_actor_set_reactive (CLUTTER_ACTOR (self), TRUE);

  hadjust = g_object_new (MX_TYPE_ADJUSTMENT, NULL);
  vadjust = g_object_new (MX_TYPE_ADJUSTMENT, NULL);
  scrollable_set_adjustments (MX_SCROLLABLE (self), hadjust, vadjust);
  g_object_unref (hadjust);
  g_object_unref (vadjust);

  g_signal_connect (vadjust,
      "interpolation-completed",
      G_CALLBACK (interpolation_completed_cb),
      self);

  g_signal_connect (G_OBJECT (self), "button-release-event",
      G_CALLBACK (event_cb), NULL);
  g_signal_connect (G_OBJECT (self), "touch-event",
      G_CALLBACK (event_cb), NULL);
}

/* Private LightwoodRoller implementation */
/**
 * lightwood_roller_get_attributes:
 * @roller: A #LightwoodRoller
 *
 * 
 * Returns: List of attributes for each item.
 */

GSList *lightwood_roller_get_attributes (LightwoodRoller *roller)
{
  return roller->priv->attributes;
}
/**
 * lightwood_roller_get_factory:
 * @roller: A #LightwoodRoller
 *
 * #MxItemFactory Instance used by roller.
 *
 * Returns: (transfer none) (nullable): ItemFactory.
 */
MxItemFactory *
lightwood_roller_get_factory (LightwoodRoller *roller)
{
  return roller->priv->item_factory;
}
/**
 * lightwood_roller_get_model:
 * @roller: A #LightwoodRoller
 *
 * Returns a Model. 
 *
 * Returns: (transfer none) (nullable): a #ThornburyModel
 */
ThornburyModel *
lightwood_roller_get_model (LightwoodRoller *roller)
{
  return roller->priv->model;
}
/**
 * lightwood_roller_get_normalized_adjustment_position:
 * @roller: A #LightwoodRoller
 *
 * 
 * Returns:adjsutment position. 
 */

gdouble
lightwood_roller_get_normalized_adjustment_position (LightwoodRoller *roller)
{
  LightwoodRollerPrivate *priv = roller->priv;
  gdouble start;
  gfloat total_height;
  MxAdjustment *vadjust;

	ROLLER_DEBUG("%s",__FUNCTION__);

  mx_scrollable_get_adjustments (MX_SCROLLABLE (roller), NULL, &vadjust);
  if (vadjust != NULL)
    start = mx_adjustment_get_value (vadjust);
  else
    start = 0.0;

  ROLLER_DEBUG ("%s, %p: start = %f, vadjust = %p", G_STRFUNC, roller, start,
                vadjust);

  if (start < 0.0)
    start = floor (start);
  else
    start = ceil (start);
/* Fix for bug # 1 */
  if (lightwood_roller_get_effective_rollover_enabled (roller))
    {
      clutter_actor_get_preferred_height (CLUTTER_ACTOR (roller),
        priv->allocation.x2 - priv->allocation.x1,
        NULL,
        &total_height);

      ROLLER_DEBUG ("%s, %p: total_height = %f, start = %f", G_STRFUNC, roller,
                    total_height, start);

      while (start < 0)
        start += total_height;
      while (start > total_height)
        start -= total_height;
    }

  return start;
}

/* LightwoodRoller implementation */

/**
 * lightwood_roller_set_factory:
 * @roller: A #LightwoodRoller
 * @factory: A #MxItemFactory
 *
 * Sets @factory to be the factory used for creating new items
 */
void
lightwood_roller_set_factory (LightwoodRoller     *roller,
                        MxItemFactory *factory)
{
  LightwoodRollerPrivate *priv;

	ROLLER_DEBUG("%s",__FUNCTION__);

  g_return_if_fail (LIGHTWOOD_IS_ROLLER (roller));
  g_return_if_fail (factory == NULL || MX_IS_ITEM_FACTORY (factory));

  priv = roller->priv;

  if (priv->item_factory == factory)
    return;

  if (priv->item_factory != NULL)
    {
      g_object_unref (priv->item_factory);
      priv->item_factory = NULL;
    }

  if (factory != NULL)
    priv->item_factory = g_object_ref (factory);

  clutter_actor_queue_relayout (CLUTTER_ACTOR (roller));

  g_object_notify (G_OBJECT (roller), "factory");
}

/**
 * lightwood_roller_add_attribute:
 * @roller: A #LightwoodRoller
 * @property: Name of the attribute
 * @column: Column number
 *
 * Maps a property of the item actors to a column of the current #ThornburyModel.
 *
 */
void
lightwood_roller_add_attribute (LightwoodRoller   *roller,
                          const gchar *property,
                          gint         column)
{
  LightwoodRollerPrivate *priv = roller->priv;
  AttributeData *attribute;

	ROLLER_DEBUG("%s",__FUNCTION__);

  g_return_if_fail (property != NULL);
  g_return_if_fail (column >= 0);

  attribute = g_new (AttributeData, 1);
  attribute->name = g_strdup (property);
  attribute->column = column;

  priv->attributes = g_slist_prepend (priv->attributes, attribute);

  clutter_actor_queue_relayout (CLUTTER_ACTOR (roller));
}

static void
update_position (LightwoodRoller *roller, ThornburyModelIter *iter, gboolean added)
{
	LightwoodRollerClass *roller_class = LIGHTWOOD_ROLLER_GET_CLASS (roller);
	LightwoodRollerPrivate *priv = roller->priv;
	MxAdjustment *adjustment;
	ClutterActor *child;
	gint new_row = thornbury_model_iter_get_row (iter);
	gdouble value;

	ROLLER_DEBUG("%s",__FUNCTION__);

	if (new_row > get_focused_row (roller))
		return;

	child = LIGHTWOOD_ROLLER_GET_CLASS (roller)->get_item_from_row (roller,
			new_row,
			-1,
			-1,
			-1,
			NULL);
	mx_scrollable_get_adjustments (MX_SCROLLABLE (roller), NULL, &adjustment);
	value = mx_adjustment_get_value (adjustment);
	if (added)
	{
		value += roller_class->get_item_height (roller, child);
	}

	else
		value -= roller_class->get_item_height (roller, child);
	if(!priv->rollover_enabled)
	{
		{
			gfloat total;
			clutter_actor_get_preferred_height(CLUTTER_ACTOR(roller),-1,&total,NULL);
			if( total < clutter_actor_get_height(CLUTTER_ACTOR(roller)))
			{
				//mx_adjustment_set_value(adjustment,0);	
				value = 0;
			}
		}

		//if( value <= clutter_actor_get_height(roller))
	}
	mx_adjustment_set_value (adjustment, value);

	ROLLER_DEBUG ("%s, %p: value = %f", G_STRFUNC, roller, value);

	if (child != NULL)
		g_object_unref (child);
}
static void
row_added_cb (ThornburyModel     *model,
              ThornburyModelIter *iter,
              LightwoodRoller        *roller)
{
	ROLLER_DEBUG("%s",__FUNCTION__);
	update_position (roller, iter, TRUE);
	clutter_actor_queue_relayout (CLUTTER_ACTOR (roller));
}

static void
row_changed_cb (ThornburyModel     *model,
                ThornburyModelIter *iter,
                LightwoodRoller        *roller)
{
	ROLLER_DEBUG("%s",__FUNCTION__);
	clutter_actor_queue_relayout (CLUTTER_ACTOR (roller));
}

static void
row_removed_cb (ThornburyModel     *model,
                ThornburyModelIter *iter,
                LightwoodRoller        *roller)
{
	ROLLER_DEBUG("%s",__FUNCTION__);
  	update_position (roller, iter, FALSE);
  clutter_actor_queue_relayout (CLUTTER_ACTOR (roller));
}

static void sort_changed_cb(ThornburyModel *model, LightwoodRoller    *roller)
{
	ROLLER_DEBUG("%s",__FUNCTION__);
  clutter_actor_queue_relayout (CLUTTER_ACTOR (roller));
}

/**
 * lightwood_roller_set_model:
 * @roller: A #LightwoodRoller
 * @model: A #ThornburyModel
 *
 * Set the model to #LightwoodRoller
 */
void
lightwood_roller_set_model (LightwoodRoller    *roller,
                      ThornburyModel *model)
{
  LightwoodRollerPrivate *priv = roller->priv;

	ROLLER_DEBUG("%s",__FUNCTION__);

  if (priv->model != NULL)
    {
      g_signal_handlers_disconnect_by_func (priv->model,
                                            G_CALLBACK (row_added_cb),
                                            roller);
      g_signal_handlers_disconnect_by_func (priv->model,
                                            G_CALLBACK (row_changed_cb),
                                            roller);
      g_signal_handlers_disconnect_by_func (priv->model,
                                            G_CALLBACK (row_removed_cb),
                                            roller);
	g_signal_handlers_disconnect_by_func (priv->model,
                                            G_CALLBACK (sort_changed_cb),
						roller);
      g_object_unref (priv->model);

      priv->model = NULL;
    }

  if (model != NULL)
    {
      g_return_if_fail (G_IS_OBJECT (model));

      priv->model = g_object_ref (model);

      g_signal_connect (priv->model,
                        "row-added",
                        G_CALLBACK (row_added_cb),
                        roller);

      g_signal_connect (priv->model,
                        "row-changed",
                        G_CALLBACK (row_changed_cb),
                        roller);

    g_signal_connect (priv->model,
	"row-removed",
	G_CALLBACK (row_removed_cb),
	roller);

    g_signal_connect (priv->model,
	"sort-changed",
	G_CALLBACK (sort_changed_cb),
	roller);
  }

  clutter_actor_queue_relayout (CLUTTER_ACTOR (roller));

  g_object_notify (G_OBJECT (roller), "model");
}

/**
 * lightwood_roller_get_row_at:
 * @roller: A #LightwoodRoller
 * @position: A position in the roller such as given by #MxAdjustment:value
 *
 * The row of the #ThornburyModel that lays at the given position
 * Returns: y position
 */
gint
lightwood_roller_get_row_at (LightwoodRoller *roller,
                       gfloat     position)
{
	ROLLER_DEBUG("%s",__FUNCTION__);
  return LIGHTWOOD_ROLLER_GET_CLASS (roller)->get_row_from_coord (roller, -1,
                                                            position);
}
/**
 * lightwood_roller_set_focus_to_centre:
 * @roller: A #LightwoodRoller
 * @centre_focus_enable: roller focus to centre or at the top.
 * 
 * this will be sensible anly when :roll-over is true.
 * 
 */
void
lightwood_roller_set_focus_to_centre(LightwoodRoller *roller,
                          gboolean centre_focus_enable)
{
	roller->priv->centre_focus = centre_focus_enable;
	ROLLER_DEBUG("%s",__FUNCTION__);
	g_object_notify (G_OBJECT (roller), "focus-to-centre");
}

/**
 * lightwood_roller_set_roll_over:
 * @roller: A #LightwoodRoller
 * @roll_over_enabled: Whether the list should roll over to the start when it
 * reaches the end.
 * 
 * Sets the state of roll-over  
 */
void
lightwood_roller_set_roll_over (LightwoodRoller *roller,
                          gboolean   roll_over_enabled)
{
	ROLLER_DEBUG("%s",__FUNCTION__);
  roller->priv->rollover_enabled = roll_over_enabled;
  clutter_actor_queue_relayout (CLUTTER_ACTOR (roller));
  g_object_notify (G_OBJECT (roller), "roll-over");
}

/**
 * lightwood_roller_set_motion_blur:
 * @roller: A #LightwoodRoller
 * @motion_blur_enabled: Whether a vertical motion effect should be applied
 * while scrolling.
 * 
 * This is to provide blur effect on motion of the roller items.
 */
void
lightwood_roller_set_motion_blur (LightwoodRoller *roller,
                            gboolean   motion_blur_enabled)
{
	ROLLER_DEBUG("%s",__FUNCTION__);
  roller->priv->motion_blur_enabled = motion_blur_enabled;
  g_object_notify (G_OBJECT (roller), "motion-blur");
}

/**
 * lightwood_roller_set_cylinder_distort:
 * @roller: A #LightwoodRoller
 * @cylinder_distort_enabled: Whether a cylinder distort effect should be
 * applied.
 */
void
lightwood_roller_set_cylinder_distort (LightwoodRoller *roller,
                                 gboolean   cylinder_distort_enabled)
{
	ROLLER_DEBUG("%s",__FUNCTION__);
  if (roller->priv->cylinder_distort_enabled == cylinder_distort_enabled)
    return;

  roller->priv->cylinder_distort_enabled = cylinder_distort_enabled;

  if (roller->priv->cylinder_distort_enabled)
    {
      ClutterEffect *cylinder_effect = lightwood_cylinder_deform_effect_new ();
      clutter_actor_add_effect_with_name (CLUTTER_ACTOR (roller),
                                          CYLINDER_DISTORT_EFFECT_NAME,
                                          cylinder_effect);
    }
  else
    clutter_actor_remove_effect_by_name (CLUTTER_ACTOR (roller),
                                         CYLINDER_DISTORT_EFFECT_NAME);

  g_object_notify (G_OBJECT (roller), "cylinder-distort");
}

/**
 * lightwood_roller_set_animation_mode:
 * @roller: A #LightwoodRoller
 * @animation_mode_enabled: Whether to enable animation mode
 *
 * When animation mode is enabled, the roller will allocate its children at
 * fixed positions.
 */
void
lightwood_roller_set_animation_mode (LightwoodRoller *roller,
                               gboolean animation_mode_enabled)
{
  gint i;
  guint len;
  GSList *items, *iter;
  ClutterActorBox *allocations;

	ROLLER_DEBUG("%s",__FUNCTION__);

  if (roller->priv->animation_mode == animation_mode_enabled)
    return;

  roller->priv->animation_mode = animation_mode_enabled;

  /* Give subclasses time to update the actors before we set the initial positions */
  g_object_notify (G_OBJECT (roller), "animation-mode");

  if (roller->priv->animation_mode)
    {
      items = lightwood_roller_get_actors (roller);
      allocations = lightwood_roller_get_allocations (roller, -1, -1, -1, TRUE, &len);
      for (iter = items, i = 0; iter; iter = iter->next, i++)
        {
          ClutterActor *child_actor = (ClutterActor *) iter->data;

          clutter_actor_set_position (child_actor,
                                      allocations[i].x1, allocations[i].y1);

          clutter_actor_set_size (child_actor,
                                  allocations[i].x2 - allocations[i].x1,
                                  allocations[i].y2 - allocations[i].y1);
        }
      g_slice_free1 (sizeof(ClutterActorBox) * g_slist_length (items),
                     allocations);
      g_slist_free (items);
    }
}

/**
 * lightwood_roller_get_actors:
 * @roller: A #LightwoodRoller
 *
 * When animation mode is enabled, the roller will allocate its children at
 * fixed positions.
 *
 * Returns: (transfer container) (element-type ClutterActor) (nullable): all actors presently used to represent the model.
 */
GSList *
lightwood_roller_get_actors (LightwoodRoller *roller)
{
	ROLLER_DEBUG("%s",__FUNCTION__);
  return LIGHTWOOD_ROLLER_GET_CLASS (roller)->get_visible_items (roller, -1, -1,
                                                           NULL, NULL);
}

/**
 * lightwood_roller_set_focused_row:
 * @roller: A #LightwoodRoller
 * @focused_row: Row to focus
 * @animate: Whether to move to the new position with an animation
 *
 * Scroll so @focused_row is placed in the middle.
 */
void
lightwood_roller_set_focused_row (LightwoodRoller *roller,
                                  gint focused_row,
                                  gboolean animate)
{
	ROLLER_DEBUG("%s",__FUNCTION__);
	//if(priv->rollover_enabled)
	{
		if (!lightwood_roller_get_scrolling_required (roller))
			/* if it is short roller focus given row*/
		{
			focus_row_mid (LIGHTWOOD_ROLLER (roller),focused_row,roller->priv->centre_focus , TRUE, TRUE, -1, -1, -1);
		}
		else
		{
			focus_row (roller, focused_row, animate, TRUE, -1, -1, -1);
		}
	}
}

/**
 * lightwood_roller_resize:
 * @roller: A #LightwoodRoller
 * @roller_width: Final width of the roller
 * @item_width: Final width of each item
 * @item_height: Final height of each item
 * @focused_row: Row that should be kept in the middle
 * @timeline: Timeline of the animation
 * @bAnimate: Animation enabled/disabled.
 *
 * Resize roller and its children
 */
void
lightwood_roller_resize (LightwoodRoller *roller, gfloat roller_width, gfloat item_width,
                   gfloat item_height, gint focused_row, ClutterTimeline *timeline, gboolean bAnimate)
{
  GSList *items, *iter;
  gint i;
  guint len;
  ClutterActor *child;
  ClutterActorBox *allocations;
  gfloat offset_y;
  gint focused_index;

  ROLLER_DEBUG ("%s, %p", G_STRFUNC, roller);

  items = lightwood_roller_get_actors (roller);
  allocations = lightwood_roller_get_allocations (roller, roller_width, item_height,
                                            item_width, TRUE, &len);

  if (focused_row != -1)
    {
      child = LIGHTWOOD_ROLLER_GET_CLASS (roller)->get_item_from_row (roller,
                                                                focused_row,
                                                                -1,
                                                                -1,
                                                                -1,
                                                                NULL);
      focused_index = g_slist_index (items, child);

      focus_row (roller, focused_row, FALSE, TRUE, roller_width, item_width,
                 item_height);
    }
  else
    focused_index = -1;

  if (focused_index >= 0)
    {
      gfloat texture_height;

      texture_height = lightwood_roller_get_content_height (roller);

      offset_y = allocations[focused_index].y1 - (texture_height / 2 - item_height / 2);
    }
  else
    offset_y = 0;

  for (iter = items, i = 0; iter; iter = iter->next, i++)
    {
      ClutterActor *child_actor = (ClutterActor *) iter->data;

#if 0
      g_debug ("%s %f -> %f, %f -> %f",
               clutter_actor_get_name (child_actor),
               clutter_actor_get_x (child_actor), allocations[i].x1,
               clutter_actor_get_y (child_actor), allocations[i].y1 - offset_y);
#endif
		if(bAnimate)
		{
			clutter_actor_save_easing_state (child_actor);
			clutter_actor_set_easing_duration (child_actor,clutter_timeline_get_duration(timeline));
			clutter_actor_set_easing_mode (child_actor, CLUTTER_EASE_IN_QUART);
			clutter_actor_set_position (child_actor, allocations[i].x1, allocations[i].y1 - offset_y);
			//clutter_actor_set_size (child_actor, allocations[i].x2 - allocations[i].x1, allocations[i].y2 - allocations[i].y1);
			/* this will call overidden properties hieght and width of itemtype */
			clutter_actor_set_width(child_actor,allocations[i].x2 - allocations[i].x1);
			clutter_actor_set_height(child_actor,allocations[i].y2 - allocations[i].y1);
			clutter_actor_restore_easing_state (child_actor);
			clutter_timeline_start(timeline);
		}
		else
		{
				/* if animation not required (timeline duration is very short),
				 * work around fix added for detail to cover transition in LIGHTWOOD without animation 
				 * because all thumbs were not getting resized */
				//clutter_actor_set_position (child_actor, allocations[i].x1, allocations[i].y1 - offset_y);
                g_object_set(child_actor, "x", allocations[i].x1, "y", allocations[i].y1 - offset_y, "width", allocations[i].x2 - allocations[i].x1, "height", allocations[i].y2 - allocations[i].y1, NULL);
		}
    }
	/* if animation not required (timeline duration is very short),
	 * work around fix added for detail to cover transition in LIGHTWOOD without animation */
	if(! bAnimate)
		clutter_timeline_start(timeline);

  g_slice_free1 (sizeof(ClutterActorBox) * g_slist_length (items),
                 allocations);
  g_slist_free (items);
}

/**
 * lightwood_roller_get_actor_for_row:
 * @roller: A #LightwoodRoller
 * @row: Row of the actor we are interested in
 *
 * The actor being used to represent a row, or %NULL if the row doesn't
 * have one, for example because it isn't visible.
 *
 * Returns: (transfer full) (nullable): a #ClutterActor
 */
ClutterActor *
lightwood_roller_get_actor_for_row (LightwoodRoller *roller, guint row)
{
	ROLLER_DEBUG("%s",__FUNCTION__);
  return LIGHTWOOD_ROLLER_GET_CLASS (roller)->get_item_from_row (roller, row, -1, -1, -1, NULL);
}

/**
 * lightwood_roller_get_horizontal_adjustment:
 * @self: a #LightwoodRoller
 *
 * Return the #MxScrollable:horizontal-adjustment property
 *
 * Returns: (transfer none): the value of #LightwoodRoller:horizontal-adjustment property
 */
MxAdjustment *
lightwood_roller_get_horizontal_adjustment (LightwoodRoller *self)
{
  MxAdjustment *adjustment;

  g_return_val_if_fail (LIGHTWOOD_IS_ROLLER (self), NULL);

  scrollable_get_adjustments (MX_SCROLLABLE (self), &adjustment, NULL);
  return adjustment;
}

/**
 * lightwood_roller_set_horizontal_adjustment:
 * @self: a #LightwoodRoller
 * @adjustment: a #MxAdjustment
 *
 * Update the #MxScrollable:horizontal-adjustment property
 */
void
lightwood_roller_set_horizontal_adjustment (LightwoodRoller *self,
                                            MxAdjustment *adjustment)
{
  g_return_if_fail (LIGHTWOOD_IS_ROLLER (self));

  scrollable_set_adjustments (MX_SCROLLABLE (self), adjustment, self->priv->pVadjustment);

  g_object_notify (G_OBJECT (self), "horizontal-adjustment");
}

/**
 * lightwood_roller_get_vertical_adjustment:
 * @self: a #LightwoodRoller
 *
 * Return the #MxScrollable:vertical-adjustment property
 *
 * Returns: (transfer none): the value of #LightwoodRoller:vertical-adjustment property
 */
MxAdjustment *
lightwood_roller_get_vertical_adjustment (LightwoodRoller *self)
{
  MxAdjustment *adjustment;

  g_return_val_if_fail (LIGHTWOOD_IS_ROLLER (self), NULL);

  scrollable_get_adjustments (MX_SCROLLABLE (self), NULL, &adjustment);
  return adjustment;
}

/**
 * lightwood_roller_set_vertical_adjustment:
 * @self: a #LightwoodRoller
 * @adjustment: a #MxAdjustment
 *
 * Update the #MxScrollable:vertical-adjustment property
 */
void
lightwood_roller_set_vertical_adjustment (LightwoodRoller *self,
                                          MxAdjustment *adjustment)
{
  g_return_if_fail (LIGHTWOOD_IS_ROLLER (self));

  scrollable_set_adjustments (MX_SCROLLABLE (self), self->priv->pHadjustment, adjustment);

  g_object_notify (G_OBJECT (self), "vertical-adjustment");
}

/**
 * lightwood_roller_get_roll_over:
 * @self: a #LightwoodRoller
 *
 * Return the #LightwoodRoller:roll-over property
 *
 * Returns: the value of #LightwoodRoller:roll-over property
 */
gboolean
lightwood_roller_get_roll_over (LightwoodRoller *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_ROLLER (self), FALSE);

  return self->priv->rollover_enabled;
}

/**
 * lightwood_roller_get_focused_row:
 * @self: a #LightwoodRoller
 *
 * Return the #LightwoodRoller:focused-row property
 *
 * Returns: the value of #LightwoodRoller:focused-row property
 */
gint
lightwood_roller_get_focused_row (LightwoodRoller *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_ROLLER (self), 0);

  return get_focused_row (self);
}

/**
 * lightwood_roller_get_motion_blur:
 * @self: a #LightwoodRoller
 *
 * Return the #LightwoodRoller:motion-blur property
 *
 * Returns: the value of #LightwoodRoller:motion-blur property
 */
gboolean
lightwood_roller_get_motion_blur (LightwoodRoller *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_ROLLER (self), FALSE);

  return self->priv->motion_blur_enabled;
}

/**
 * lightwood_roller_get_cylinder_distort:
 * @self: a #LightwoodRoller
 *
 * Return the #LightwoodRoller:cylinder-distort property
 *
 * Returns: the value of #LightwoodRoller:cylinder-distort property
 */
gboolean
lightwood_roller_get_cylinder_distort (LightwoodRoller *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_ROLLER (self), FALSE);

  return self->priv->cylinder_distort_enabled;
}

/**
 * lightwood_roller_get_animation_mode:
 * @self: a #LightwoodRoller
 *
 * Return the #LightwoodRoller:animation-mode property
 *
 * Returns: the value of #LightwoodRoller:animation-mode property
 */
gboolean
lightwood_roller_get_animation_mode (LightwoodRoller *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_ROLLER (self), FALSE);

  return self->priv->animation_mode;
}

/**
 * lightwood_roller_get_clamp_mode:
 * @self: a #LightwoodRoller
 *
 * Return the #LightwoodRoller:clamp-mode property
 *
 * Returns: the value of #LightwoodRoller:clamp-mode property
 */
gulong
lightwood_roller_get_clamp_mode (LightwoodRoller *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_ROLLER (self), CLUTTER_EASE_OUT_QUAD);

  return self->priv->clamp_mode;
}

/**
 * lightwood_roller_set_clamp_mode:
 * @self: a #LightwoodRoller
 * @mode: the new value
 *
 * Update the #LightwoodRoller:clamp-mode property
 */
void
lightwood_roller_set_clamp_mode (LightwoodRoller *self,
                                 gulong mode)
{
  g_return_if_fail (LIGHTWOOD_IS_ROLLER (self));

  if (mode == self->priv->clamp_mode)
    return;

  self->priv->clamp_mode = mode;
  g_object_notify (G_OBJECT (self), "clamp-mode");
}

/**
 * lightwood_roller_get_clamp_duration:
 * @self: a #LightwoodRoller
 *
 * Return the #LightwoodRoller:clamp-duration property
 *
 * Returns: the value of #LightwoodRoller:clamp-duration property
 */
guint
lightwood_roller_get_clamp_duration (LightwoodRoller *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_ROLLER (self), 0);

  return self->priv->clamp_duration;
}

/**
 * lightwood_roller_set_clamp_duration:
 * @self: a #LightwoodRoller
 * @duration: the new value
 *
 * Update the #LightwoodRoller:clamp-duration property
 */
void
lightwood_roller_set_clamp_duration (LightwoodRoller *self,
                                     guint duration)
{
  g_return_if_fail (LIGHTWOOD_IS_ROLLER (self));

  if (duration == self->priv->clamp_duration)
    return;

  self->priv->clamp_duration = duration;
  g_object_notify (G_OBJECT (self), "clamp-duration");
}

/**
 * lightwood_roller_get_focus_to_centre:
 * @self: a #LightwoodRoller
 *
 * Return the #LightwoodRoller:focus-to-centre property
 *
 * Returns: the value of #LightwoodRoller:focus-to-centre property
 */
gboolean
lightwood_roller_get_focus_to_centre (LightwoodRoller *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_ROLLER (self), FALSE);

  return self->priv->centre_focus;
}

/**
 * lightwood_roller_get_glow:
 * @self: a #LightwoodRoller
 *
 * Return the #LightwoodRoller:glow property
 *
 * Returns: the value of #LightwoodRoller:glow property
 */
gboolean
lightwood_roller_get_glow (LightwoodRoller *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_ROLLER (self), FALSE);

  return self->priv->glow;
}

/**
 * lightwood_roller_set_glow:
 * @self: a #LightwoodRoller
 * @glow: %TRUE to enable the glow effect
 *
 * Update the #LightwoodRoller:glow property
 */
void
lightwood_roller_set_glow (LightwoodRoller *self,
                           gboolean glow)
{
  g_return_if_fail (LIGHTWOOD_IS_ROLLER (self));

  if (self->priv->glow == glow)
    return;

  self->priv->glow = glow;
  g_object_notify (G_OBJECT (self), "glow");
}

/**
 * lightwood_roller_get_glow_row:
 * @self: a #LightwoodRoller
 *
 * Return the #LightwoodRoller:glow-row property
 *
 * Returns: the value of #LightwoodRoller:glow-row property
 */
gint
lightwood_roller_get_glow_row (LightwoodRoller *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_ROLLER (self), -1);

  return get_glow_row (self);
}

/**
 * lightwood_roller_get_first_visible_item:
 * @self: a #LightwoodRoller
 *
 * Return the #LightwoodRoller:first-visible-item property
 *
 * Returns: the value of #LightwoodRoller:first-visible-item property
 */
gint
lightwood_roller_get_first_visible_item (LightwoodRoller *self)
{
  gint result;
  GSList *visible_children;

  g_return_val_if_fail (LIGHTWOOD_IS_ROLLER (self), -1);

  visible_children = LIGHTWOOD_ROLLER_GET_CLASS (self)->get_visible_items ( LIGHTWOOD_ROLLER (self), -1, -1, NULL, NULL);
  if (visible_children == NULL)
    return -1;

  result = LIGHTWOOD_ROLLER_GET_CLASS (self)->get_row_from_item (LIGHTWOOD_ROLLER (self), g_slist_nth_data (visible_children, 0));

  g_slist_free (visible_children);
  return result;
}

/**
 * lightwood_roller_get_visible_items:
 * @self: a #LightwoodRoller
 *
 * Return the #LightwoodRoller:visible-items property
 *
 * Returns: the value of #LightwoodRoller:visible-items property
 */
guint
lightwood_roller_get_visible_items (LightwoodRoller *self)
{
  gint result;
  GSList *visible_children;

  g_return_val_if_fail (LIGHTWOOD_IS_ROLLER (self), 0);

  visible_children = LIGHTWOOD_ROLLER_GET_CLASS (self)->get_visible_items ( LIGHTWOOD_ROLLER (self), -1, -1, NULL, NULL);
  if (visible_children == NULL)
    return 0;

  result = g_slist_length (visible_children);

  g_slist_free (visible_children);
  return result;
}

/**
 * lightwood_roller_get_content_height:
 * @self: a #LightwoodRoller
 *
 * Get the vertical space the roller has in which to render content. If
 * #LightwoodRoller:cylinder-distort is %FALSE, this is equal to the roller’s
 * allocated height. Otherwise, it’s equal to half the circumference of the
 * distortion cylinder with diameter equal to the allocated height, meaning the
 * roller can render more content.
 *
 * This function is intended to be used by subclass implementations.
 *
 * Returns: the content height in pixels
 * Since: 0.3.0
 */
gfloat
lightwood_roller_get_content_height (LightwoodRoller *self)
{
  gfloat height;

  g_return_val_if_fail (LIGHTWOOD_IS_ROLLER (self), 0.0);

  height = self->priv->allocation.y2 - self->priv->allocation.y1;

  if (self->priv->cylinder_distort_enabled)
    height *= M_PI / 2;

  return height;
}
