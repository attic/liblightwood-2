/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _ROLLER_H
#define _ROLLER_H

#include <glib-object.h>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-prototypes"
#include <mx/mx.h>
#pragma GCC diagnostic pop
#include "libthornbury-model.h"

G_BEGIN_DECLS

#define LIGHTWOOD_TYPE_ROLLER lightwood_roller_get_type()

#define LIGHTWOOD_ROLLER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  LIGHTWOOD_TYPE_ROLLER, LightwoodRoller))

#define LIGHTWOOD_ROLLER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  LIGHTWOOD_TYPE_ROLLER, LightwoodRollerClass))

#define LIGHTWOOD_IS_ROLLER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  LIGHTWOOD_TYPE_ROLLER))

#define LIGHTWOOD_IS_ROLLER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  LIGHTWOOD_TYPE_ROLLER))

#define LIGHTWOOD_ROLLER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  LIGHTWOOD_TYPE_ROLLER, LightwoodRollerClass))

typedef struct _LightwoodRoller LightwoodRoller;
typedef struct _LightwoodRollerClass LightwoodRollerClass;
typedef struct _LightwoodRollerPrivate LightwoodRollerPrivate;

struct _LightwoodRoller
{
  /*< private >*/
  ClutterActor parent;

  LightwoodRollerPrivate *priv;
};

/**
 * LightwoodRollerClass:
 * @locking_finished: signal class handler for #LightwoodRoller::locking-finished
 * @item_activated: signal class handler for #LightwoodRoller::item-activated
 * @scroll_started: signal class handler for #LightwoodRoller::scroll-started
 * @scrolling: signal class handler for #LightwoodRoller::scrolling
 * @scroll_finished: signal class handler for #LightwoodRoller::scroll-finished
 *
 * Base class for rollers.
 */
struct _LightwoodRollerClass
{
  /*< private >*/
  ClutterActorClass parent_class;

  GSList *(* get_visible_items) (LightwoodRoller *roller,
                                 gfloat     forced_roller_width,
                                 gfloat     forced_item_width,
                                 gfloat    *first_x,
                                 gfloat    *first_y);

  guint (* get_row_from_coord) (LightwoodRoller *roller,
                                gfloat     x,
                                gfloat     y);

  ClutterActor *(* get_item_from_row) (LightwoodRoller *roller,
                                       guint      row,
                                       gfloat     forced_roller_width,
                                       gfloat     forced_item_width,
                                       gfloat     forced_item_height,
                                       gfloat    *y);

  gint (* get_row_from_item) (LightwoodRoller    *roller,
                              ClutterActor *child);

  gfloat (* get_item_height) (LightwoodRoller *roller, ClutterActor *actor);

  gfloat (* get_item_width) (LightwoodRoller *roller, ClutterActor *actor);

  gfloat (* get_total_height) (LightwoodRoller *roller,
                               gfloat     forced_roller_width,
                               gfloat     forced_item_width,
                               gfloat     forced_item_height);

  /*< public >*/
  void (* locking_finished) (LightwoodRoller *roller);
  void (* item_activated) (LightwoodRoller *roller, ClutterActor *item);
  void (* scroll_started) (LightwoodRoller *roller);
  void (* scrolling) (LightwoodRoller *roller);
  void (* scroll_finished) (LightwoodRoller *roller, guint first_visible_row, guint total_visible_items);
};

GType lightwood_roller_get_type (void) G_GNUC_CONST;

void lightwood_roller_add_attribute (LightwoodRoller   *roller,
                               const gchar *property,
                               gint         column);

gint lightwood_roller_get_row_at (LightwoodRoller *roller, gfloat position);

GSList *lightwood_roller_get_actors (LightwoodRoller *roller);

ClutterActorBox *lightwood_roller_get_allocations (LightwoodRoller *roller,
                                             gfloat forced_roller_width,
                                             gfloat forced_item_height,
                                             gfloat forced_item_width,
                                             gboolean force_calculation,
                                             guint *len);

void lightwood_roller_resize (LightwoodRoller *roller,
                        gfloat roller_width,
                        gfloat item_width,
                        gfloat item_height,
                        gint focused_row,
                        ClutterTimeline *timeline,
						gboolean bAnimate);

gboolean lightwood_roller_get_scrolling_required (LightwoodRoller *roller);
gboolean lightwood_roller_get_effective_rollover_enabled (LightwoodRoller *roller);

ClutterActor *lightwood_roller_get_actor_for_row (LightwoodRoller *roller, guint row);

/* Properties */

MxAdjustment *lightwood_roller_get_horizontal_adjustment (LightwoodRoller *self);
void lightwood_roller_set_horizontal_adjustment (LightwoodRoller *self,
                                                 MxAdjustment *adjustment);
MxAdjustment *lightwood_roller_get_vertical_adjustment (LightwoodRoller *self);
void lightwood_roller_set_vertical_adjustment (LightwoodRoller *self,
                                               MxAdjustment *adjustment);

gboolean lightwood_roller_get_roll_over (LightwoodRoller *self);
void lightwood_roller_set_roll_over (LightwoodRoller *roller, gboolean roll_over_enabled);

gint lightwood_roller_get_focused_row (LightwoodRoller *self);
void lightwood_roller_set_focused_row (LightwoodRoller *roller, gint focused_row,
                                 gboolean animate);

ThornburyModel *lightwood_roller_get_model (LightwoodRoller *roller);
void lightwood_roller_set_model (LightwoodRoller *roller, ThornburyModel *model);

MxItemFactory *lightwood_roller_get_factory (LightwoodRoller *roller);
void lightwood_roller_set_factory (LightwoodRoller *roller, MxItemFactory *factory);

gboolean lightwood_roller_get_motion_blur (LightwoodRoller *self);
void lightwood_roller_set_motion_blur (LightwoodRoller *roller,
                                       gboolean motion_blur_enabled);

gboolean lightwood_roller_get_cylinder_distort (LightwoodRoller *self);
void lightwood_roller_set_cylinder_distort (LightwoodRoller *roller,
                                            gboolean cylinder_distort_enabled);

gboolean lightwood_roller_get_animation_mode (LightwoodRoller *self);
void lightwood_roller_set_animation_mode (LightwoodRoller *roller,
                                          gboolean animation_mode_enabled);

gulong lightwood_roller_get_clamp_mode (LightwoodRoller *self);
void lightwood_roller_set_clamp_mode (LightwoodRoller *self,
                                      gulong mode);

guint lightwood_roller_get_clamp_duration (LightwoodRoller *self);
void lightwood_roller_set_clamp_duration (LightwoodRoller *self,
                                          guint duration);

gboolean lightwood_roller_get_focus_to_centre (LightwoodRoller *self);
void lightwood_roller_set_focus_to_centre (LightwoodRoller *roller,
                                           gboolean centre_focus_enable);

gboolean lightwood_roller_get_glow (LightwoodRoller *self);
void lightwood_roller_set_glow (LightwoodRoller *self,
                                gboolean glow);

gint lightwood_roller_get_glow_row (LightwoodRoller *self);
void lightwood_roller_set_glow_row (LightwoodRoller *roller,
                                    gint row);

gint lightwood_roller_get_first_visible_item (LightwoodRoller *self);

guint lightwood_roller_get_visible_items (LightwoodRoller *self);

gfloat lightwood_roller_get_content_height (LightwoodRoller *self);

G_END_DECLS

#endif /* _ROLLER_H */
