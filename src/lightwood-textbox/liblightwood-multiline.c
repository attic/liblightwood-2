/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* multi_line.c
 *
 * Author: Dheeraj Kotagiri
 *
 * multi_line.c */

/**
 * SECTION:liblightwood-multiline
 * @Title: LightwoodMultilineEntry
 * @short_description:Display more than one line in the entry box
 * @title: LightwoodMultilineEntry
 * @include: lightwood_multiline_entry.h
 * Multiline entry is used to display more than one line
 * in the entry box within the same field
 * 
 * ## Sample C Code
 * |[<!-- language="C" -->
 *
 *  void some_function()
 * {
 * multiLineEntry = (LightwoodMultilineEntry *)lightwood_multiline_entry_new();
 * clutter_actor_add_child(pTextGroup,CLUTTER_ACTOR(multiLineEntry));
 * clutter_actor_hide(CLUTTER_ACTOR(multiLineEntry));
 * lightwood_multiline_entry_set_color(multiLineEntry, &messageColor);
 *
 * g_signal_connect(lightwood_multiline_get_active_text_actor(multiLineEntry), "cursor-event",		 G_CALLBACK (cursor_event_cb),  pLightwoodTextBox);
 * g_signal_connect(lightwood_multiline_get_active_text_actor(multiLineEntry), "key-focus-in", 		 G_CALLBACK (key_focus_cb),     pLightwoodTextBox);
 * g_signal_connect(lightwood_multiline_get_active_text_actor(multiLineEntry), "key-focus-out", 	 	 G_CALLBACK (key_focus_cb),     pLightwoodTextBox);
 * g_signal_connect(lightwood_multiline_get_active_text_actor(multiLineEntry), "delete-text",    	 G_CALLBACK (text_deleted_cb),  pLightwoodTextBox);
 * g_signal_connect(lightwood_multiline_get_active_text_actor(multiLineEntry), "key-press-event",	 G_CALLBACK (text_entered_cb),  pLightwoodTextBox);
 * g_signal_connect(lightwood_multiline_get_active_text_actor(multiLineEntry), "button-release-event",G_CALLBACK (text_pressed_cb),  pLightwoodTextBox);
 * g_signal_connect(lightwood_multiline_get_active_text_actor(multiLineEntry), "touch-event",         G_CALLBACK (captured_event_cb),pLightwoodTextBox);
 * g_signal_connect(multiLineEntry, "line-count-changed" , G_CALLBACK (linecount_changed),pLightwoodTextBox);
 * }
 *
 * ]|
 */

#include <clutter/clutter.h>
#include <clutter/clutter-enum-types.h>
#include <clutter/clutter-keysyms.h>
#include <string.h>
#include "liblightwood-multiline.h"

#define LIGHTWOOD_MULTILINE_ENTRY_PRINT(...)  //g_print(__VA_ARGS__)

#define LIGHTWOOD_DEFAULT_FONT_NAME	LIGHTWOOD_DEFAULT_FONT (24)
#define LIGHTWOOD_DEFAULT_FONT(size) "DejaVuSansCondensed " #size"px"

/* The signals emitted by the media element */
/**
 * LightwoodMultilineEntrySignals:
 * @SIG_LIGHTWOOD_MULTILINE_ENTRY_FIRST_SIGNAL: Represents 0
 * @SIG_LIGHTWOOD_MULTILINE_ENTRY_LINE_COUNT_CHANGED: signal for line count change
 * @SIG_LIGHTWOOD_MULTILINE_ENTRY_SIGNAL: signal for entry press
 *
 * The signals for liblightwood multiline-entry
 *
 */
enum LightwoodMultilineEntrySignals
{
	SIG_LIGHTWOOD_MULTILINE_ENTRY_FIRST_SIGNAL,
	SIG_LIGHTWOOD_MULTILINE_ENTRY_LINE_COUNT_CHANGED,
	SIG_LIGHTWOOD_MULTILINE_ENTRY_SIGNAL
};

static guint32 lightwood_multiline_entry_signals[SIG_LIGHTWOOD_MULTILINE_ENTRY_SIGNAL] = {0,};
static void lightwood_multiline_entry_insert_unichar (LightwoodMultilineEntry *entry, gunichar c);
static void lightwood_multiline_shift_lines_up( LightwoodMultilineEntry *entry );
static void lightwood_multiline_shift_lines_down( LightwoodMultilineEntry *entry );
static gboolean button_released_cb(ClutterText *text, ClutterEvent *event, gpointer userData);
static gboolean cursor_position_changed(ClutterText *text, ClutterGeometry *geometry, gpointer userData);
static gboolean text_changed_clb(ClutterText *text, gpointer userData);

/* private structure for the liblightwood text box */
typedef struct
{
	gint width;
	gint nlines;
	gint line_count ;
	gboolean	editable;
	PangoLayout *layout;
	ClutterText *entry;
	ClutterText *display;
	gchar	*textSelected;
	gint bound;
	gint cursorPosition;
} LightwoodMultilineEntryPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (LightwoodMultilineEntry, lightwood_multiline_entry, CLUTTER_TYPE_ACTOR);

#define GET_PRIVATE(o) (lightwood_multiline_entry_get_instance_private ((LightwoodMultilineEntry *) o))

/********************************************************
 * Function : captured_event_cb
 * Description: callback function,invoke at touch event
 * Parameters: ClutterText,ClutterEvent,userData
 * Return value: gboolean
 ********************************************************/
static gboolean captured_event_cb (ClutterText *text, ClutterEvent *event, gpointer userData)
{
	switch (event->type)
	{
	case CLUTTER_BUTTON_PRESS:
	case CLUTTER_TOUCH_BEGIN:
	{
		return FALSE;
		break;
	}
	case CLUTTER_BUTTON_RELEASE:
	case CLUTTER_TOUCH_END:
	{
		return button_released_cb (text, event,userData);
		break;
	}
	case CLUTTER_NOTHING:
	case CLUTTER_KEY_PRESS:
	case CLUTTER_KEY_RELEASE:
	case CLUTTER_MOTION:
	case CLUTTER_ENTER:
	case CLUTTER_LEAVE:
	case CLUTTER_SCROLL:
	case CLUTTER_STAGE_STATE:
	case CLUTTER_DESTROY_NOTIFY:
	case CLUTTER_CLIENT_MESSAGE:
	case CLUTTER_DELETE:
	case CLUTTER_TOUCH_UPDATE:
	case CLUTTER_TOUCH_CANCEL:
	case CLUTTER_EVENT_LAST:
	default:
	{
		return FALSE;
		break;
	}
	}
}

/********************************************************
 * Function : button_released_cb
 * Description: callback function,invoke at button release
 * Parameters: ClutterText,ClutterEvent,userData
 * Return value: gboolean
 ********************************************************/
static gboolean button_released_cb(ClutterText *text, ClutterEvent *event, gpointer userData)
{
	LightwoodMultilineEntry* entry = userData;
	LightwoodMultilineEntryPrivate *priv = GET_PRIVATE (entry);

	LIGHTWOOD_MULTILINE_ENTRY_PRINT("%s\n", __FUNCTION__);

	if(! LIGHTWOOD_IS_MULTILINE_ENTRY (userData))
		return TRUE;

	priv->textSelected = NULL;
	priv->bound = -1;

	priv->textSelected = clutter_text_get_selection(CLUTTER_TEXT(text));
	if(priv->textSelected && g_strcmp0(priv->textSelected, "") != 0)
	{
		priv->bound = clutter_text_get_selection_bound(CLUTTER_TEXT(text));
	}
	return FALSE ;
}

/********************************************************
 * Function : text_changed_clb
 * Description: callback function,invoke at text change
 * Parameters: ClutterText,userData
 * Return value: gboolean
 ********************************************************/
static gboolean text_changed_clb(ClutterText *text, gpointer userData)
{
	LightwoodMultilineEntry* self = LIGHTWOOD_MULTILINE_ENTRY(userData);
	LightwoodMultilineEntryPrivate *priv = GET_PRIVATE (self);

	LIGHTWOOD_MULTILINE_ENTRY_PRINT("%s\n", __FUNCTION__);

	priv->layout = clutter_text_get_layout (CLUTTER_TEXT (priv->entry));
	if(priv->layout)
	{
		gint line_count = pango_layout_get_line_count (priv->layout);
		// TODO : fire only on line count change ! save previou entry text line count for this purpose
		g_signal_emit(self, lightwood_multiline_entry_signals[SIG_LIGHTWOOD_MULTILINE_ENTRY_LINE_COUNT_CHANGED], 0 ,line_count, NULL);
	}
	return TRUE;
}

/********************************************************
 * Function : cursor_position_changed
 * Description: callback function,invoke at cursor position change
 * Parameters: ClutterText,ClutterGeometry,userData
 * Return value: gboolean
 ********************************************************/
static gboolean cursor_position_changed(ClutterText *text, ClutterGeometry *geometry, gpointer userData)
{
	LightwoodMultilineEntry* entry = userData;
	LightwoodMultilineEntryPrivate *priv = GET_PRIVATE (entry);

	LIGHTWOOD_MULTILINE_ENTRY_PRINT("%s\n", __FUNCTION__);

	if(! LIGHTWOOD_IS_MULTILINE_ENTRY (userData))
		return TRUE;

	priv->cursorPosition = clutter_text_get_cursor_position(text);
	clutter_text_set_cursor_position(priv->entry, clutter_text_get_cursor_position(text));
	return TRUE;
}

/********************************************************
 * Function : lightwood_multiline_default_entry
 * Description: function,invoke at insert text
 * Parameters: LightwoodMultilineEntry,keyval,text
 * Return value: void
 ********************************************************/
void
lightwood_multiline_default_entry (LightwoodMultilineEntry *entry,
                                   gint keyval,
                                   const gchar *text)
{
	LightwoodMultilineEntryPrivate *priv = GET_PRIVATE (entry);
	gint new_line_count;

	if(! LIGHTWOOD_IS_MULTILINE_ENTRY (entry))
		return ;

	if(priv->textSelected && g_strcmp0(priv->textSelected, "") != 0)
	{
		gint position;

		lightwood_multiline_entry_delete_char (entry);
		/* update the cursor position */
		if(priv->bound > priv->cursorPosition && priv->cursorPosition != 0)
			position = priv->bound - priv->cursorPosition;
		else if(priv ->bound == 0)
			position = priv->cursorPosition - 1;
		else if(priv->cursorPosition == 0)
			position = priv->bound - 1;
		else
			position = priv->cursorPosition - priv->bound;

		clutter_text_set_cursor_position(priv->entry, position);
	}

	clutter_text_insert_text( priv->entry ,text, clutter_text_get_cursor_position(priv->display) );
	priv->layout = clutter_text_get_layout(CLUTTER_TEXT(priv->entry));
	new_line_count = pango_layout_get_line_count( priv->layout );

	if( new_line_count > (priv->line_count + priv->nlines ) )
	{
		lightwood_multiline_shift_lines_up(entry);
		priv->line_count ++;
	}
	else
	{
		clutter_text_insert_text( priv->display ,text, clutter_text_get_cursor_position(priv->display) );
	}
}

/**
 * lightwood_multiline_entry_handle_key_event:
 * @entry: The created multiline entry actor.
 * @key: the clutter key event on the entry
 *
 * Handles key events for the multiline entry.
 */
void lightwood_multiline_entry_handle_key_event (LightwoodMultilineEntry *entry,  ClutterKeyEvent *kev_event)
{
	LightwoodMultilineEntryPrivate *priv = GET_PRIVATE (entry);
	gint keyval = kev_event->keyval;

	LIGHTWOOD_MULTILINE_ENTRY_PRINT("%s \n",__FUNCTION__);

	g_return_if_fail (LIGHTWOOD_IS_MULTILINE_ENTRY (entry));

	switch (keyval)
	{
	case CLUTTER_Escape:
	case CLUTTER_Shift_L:
	case CLUTTER_Shift_R:
	case CLUTTER_Left:
	case CLUTTER_KP_Left:
	case CLUTTER_Right:
	case CLUTTER_KP_Right:
	case CLUTTER_End:
	case CLUTTER_KP_End:
	case CLUTTER_Begin:
	case CLUTTER_Home:
	case CLUTTER_KP_Home:
		break;

	case CLUTTER_BackSpace:
		lightwood_multiline_entry_delete_char (entry);
		break;

	case CLUTTER_Delete:
	case CLUTTER_KP_Delete:
		lightwood_multiline_entry_delete_char (entry);
		break;

	case CLUTTER_Up:
	case CLUTTER_KP_Up:
		lightwood_multiline_show_prev_lines(entry);
		break;

	case CLUTTER_Down:
	case CLUTTER_KP_Down:
		lightwood_multiline_show_next_lines(entry);
		break;

	case CLUTTER_Return:
		//lightwood_multiline_entry_insert_unichar(entry, 10);
		break;

	default:
	{
		/* if text selection is there and any key has been pressed,
		 * delete selected text and then insert the char */
		if(priv->textSelected && g_strcmp0(priv->textSelected, "") != 0)
		{
			gint position;

			lightwood_multiline_entry_delete_char (entry);
			/* update the cursor position */
			if(priv->bound > priv->cursorPosition && priv->cursorPosition != 0)
				position = priv->bound - priv->cursorPosition;
			else if(priv ->bound == 0)
				position = priv->cursorPosition - 1;
			else if(priv->cursorPosition == 0)
				position = priv->bound - 1;
			else
				position = priv->cursorPosition - priv->bound;

			clutter_text_set_cursor_position(priv->entry, position);
		}
		/* insert the char */
		lightwood_multiline_entry_insert_unichar (entry, clutter_keysym_to_unicode (keyval));
		break;

	}
	}
}
/**
 * lightwood_multiline_entry_insert_unichar:
 * @entry: a #LightwoodMultilineEntry
 * @c: a Unicode character
 *
 * Insert a Unicode character at the cursor position,
 *
 */
static void lightwood_multiline_entry_insert_unichar (LightwoodMultilineEntry *entry,gunichar c)
{
	LightwoodMultilineEntryPrivate *priv = GET_PRIVATE (entry);
	gint new_line_count;

	LIGHTWOOD_MULTILINE_ENTRY_PRINT("\n %s \n",__FUNCTION__);

	g_return_if_fail (LIGHTWOOD_IS_MULTILINE_ENTRY (entry));
	g_return_if_fail (g_unichar_validate (c));

	if (c == 0)
		return;

	LIGHTWOOD_MULTILINE_ENTRY_PRINT(" entered char = %c \n",c);

	clutter_text_insert_unichar( priv->entry ,c );
	priv->layout = clutter_text_get_layout(priv->entry);
	new_line_count = pango_layout_get_line_count( priv->layout );
	LIGHTWOOD_MULTILINE_ENTRY_PRINT("\n!!!!!!!!!!!!!!!new_line_count=%d,line_count=%d nlines=%d \n",new_line_count,priv->line_count,priv->nlines);

	if( new_line_count > (priv->line_count + priv->nlines ) )
	{
		lightwood_multiline_shift_lines_up(entry);
		priv->line_count ++;
	}
}

/********************************************************
 * Function : lightwood_multiline_entry_delete_selected_text
 * Description: function,invoke at delete selected text
 * Parameters: LightwoodMultilineEntry,strlength
 * Return value: void
 ********************************************************/
void lightwood_multiline_entry_delete_selected_text(LightwoodMultilineEntry *entry,guint strlength)
{
	LightwoodMultilineEntryPrivate *priv = GET_PRIVATE (entry);
	gint pos = priv->cursorPosition;

	if(pos == -1)
		clutter_text_delete_text (priv->entry, priv->bound, strlength);
	if(priv->bound == -1)
		clutter_text_delete_text (priv->entry, pos, strlength);
	else
	{
		if(priv->bound < pos)
			clutter_text_delete_text (priv->entry, priv->bound, pos);
		else
			clutter_text_delete_text (priv->entry, pos, priv->bound);
	}
	priv->textSelected = NULL;
}

/********************************************************
 * Function : lightwood_multiline_entry_delete_normal_text
 * Description: function,invoke at delete text
 * Parameters: LightwoodMultilineEntry,strlength
 * Return value: void
 ********************************************************/
void lightwood_multiline_entry_delete_normal_text(LightwoodMultilineEntry *entry,guint strlength)
{
	LightwoodMultilineEntryPrivate *priv = GET_PRIVATE (entry);
	gint pos = priv->cursorPosition;

	if(pos == -1)
		clutter_text_delete_text (priv->entry, strlength - 1 , strlength );
	else
		clutter_text_delete_text (priv->entry, pos - 1 , pos );
}

/**
 * lightwood_multiline_entry_delete_chars:
 * @entry: a #LightwoodMultilineEntry
 *
 * One Character is removed from at cursor position.
 *
 */
void lightwood_multiline_entry_delete_char (LightwoodMultilineEntry *entry )
{
	LightwoodMultilineEntryPrivate *priv = GET_PRIVATE (entry);
	const gchar * string = clutter_text_get_text(priv->entry);
	guint strlength =0;

	LIGHTWOOD_MULTILINE_ENTRY_PRINT("\n %s \n",__FUNCTION__);

	if(string != NULL)
	{
		strlength = strlen(string );
	}
	if( strlength > 0 )
	{
		gint new_line_count;

		if(priv->textSelected && g_strcmp0(priv->textSelected, "") != 0)
		{
			lightwood_multiline_entry_delete_selected_text(entry,strlength);
		}
		else
		{
			lightwood_multiline_entry_delete_normal_text(entry,strlength);
		}
		priv->layout = clutter_text_get_layout(CLUTTER_TEXT(priv->entry));
		new_line_count = pango_layout_get_line_count( priv->layout );

		if( (priv->line_count > 0 ) && (new_line_count < (priv->line_count + priv->nlines  )) )
		{
			priv->line_count--;
			lightwood_multiline_shift_lines_down(entry);
		}
	}
}


/**
 * lightwood_multiline_display_entry_delete_char:
 * @entry: a #LightwoodMultilineEntry
 *
 * One Character is removed from at cursor position.
 *
 */
void
lightwood_multiline_display_entry_delete_char (LightwoodMultilineEntry *entry)
{
	LightwoodMultilineEntryPrivate *priv = GET_PRIVATE (entry);
	const gchar * string = clutter_text_get_text(priv->entry);
	guint strlength =0;

	LIGHTWOOD_MULTILINE_ENTRY_PRINT("\n %s \n",__FUNCTION__);

	if( (string != NULL) && (strlen(string) > 0) )
	{
		gint new_line_count;

		strlength = strlen(string);
		if(priv->textSelected && g_strcmp0(priv->textSelected, "") != 0)
		{
			lightwood_multiline_entry_delete_selected_text(entry,strlength);
		}
		else
		{
			lightwood_multiline_entry_delete_normal_text(entry,strlength);
		}
		priv->layout = clutter_text_get_layout(CLUTTER_TEXT(priv->entry));
		new_line_count = pango_layout_get_line_count( priv->layout );

		if( (priv->line_count > 0 ) && (new_line_count < (priv->line_count + priv->nlines  )) )
		{
			priv->line_count--;
			lightwood_multiline_shift_lines_down(entry);
		}
		else
		{
			string = clutter_text_get_text(priv->display);
			strlength = 0;

			if(NULL != string )
			{
				gint pos = clutter_text_get_cursor_position(priv->display);

				strlength = strlen(string );
				if(pos == -1)
					clutter_text_delete_text (priv->display, strlength - 1 , strlength );
				else
					clutter_text_delete_text (priv->display, pos - 1 , pos );
			}
		}
	}
}

/********************************************************
 * Function : lightwood_multiline_shift_lines_up
 * Description: function to shift the lines up
 * Parameters: LightwoodMultilineEntry
 * Return value: void
 ********************************************************/
static void lightwood_multiline_shift_lines_up( LightwoodMultilineEntry *entry )
{
	LightwoodMultilineEntryPrivate *priv = GET_PRIVATE (entry);

	LIGHTWOOD_MULTILINE_ENTRY_PRINT("\n %s \n",__FUNCTION__);
	LIGHTWOOD_MULTILINE_ENTRY_PRINT("\n layout = %p linecount = %d nlines = %d \n",priv->layout,priv->line_count,priv->nlines);

	if( pango_layout_get_line_count( priv->layout ) > (priv->line_count + priv->nlines ) )
	{
		PangoLayoutLine * line = pango_layout_get_line( priv->layout, priv->line_count + 1 );

		LIGHTWOOD_MULTILINE_ENTRY_PRINT("Number of lines in greater than current line count\n");
		LIGHTWOOD_MULTILINE_ENTRY_PRINT("Text of new line = %s\n", pango_layout_get_text(priv->layout) + line->start_index);
		if(line != NULL)
		{
			clutter_text_set_text( priv->display, pango_layout_get_text(priv->layout) + line->start_index );
		}
	}
}

/********************************************************
 * Function : lightwood_multiline_shift_lines_down
 * Description: function to shift the lines down
 * Parameters: LightwoodMultilineEntry
 * Return value: void
 ********************************************************/
static void lightwood_multiline_shift_lines_down( LightwoodMultilineEntry *entry )
{
	LightwoodMultilineEntryPrivate *priv = GET_PRIVATE (entry);
	PangoLayoutLine * line = pango_layout_get_line( priv->layout, priv->line_count );

	if(line != NULL)
	{
		clutter_text_set_text( priv->display, pango_layout_get_text(priv->layout) + line->start_index );
	}
}

/**
 * lightwood_multiline_show_next_lines:
 * @entry: a #LightwoodMultilineEntry
 * Returns : returns TRUE if next lines are shown
 *
 * Show next lines within a mutiline entry box field
 */
gboolean lightwood_multiline_show_next_lines( LightwoodMultilineEntry *entry )
{
	LightwoodMultilineEntryPrivate *priv = GET_PRIVATE (entry);
	gint total_line_count;

	priv->layout = clutter_text_get_layout(CLUTTER_TEXT(priv->entry));
	total_line_count = pango_layout_get_line_count( priv->layout );

	LIGHTWOOD_MULTILINE_ENTRY_PRINT("\n %s \n",__FUNCTION__);

	if( (priv->line_count + priv->nlines ) < (total_line_count) )
	{
		PangoLayoutLine *line_start;

		priv->line_count++;
		LIGHTWOOD_MULTILINE_ENTRY_PRINT("Number of lines in greater than current line count\n");

		line_start = pango_layout_get_line( priv->layout, priv->line_count );

		if(line_start != NULL)
		{
			gchar * entrytext = (char*) pango_layout_get_text( priv->layout );

			PangoLayoutLine * line_end = pango_layout_get_line( priv->layout, priv->line_count + priv->nlines );

			if( line_end != NULL )
			{
				gchar temp = entrytext[line_end->start_index];
				entrytext[line_end->start_index] = '\0';

				LIGHTWOOD_MULTILINE_ENTRY_PRINT(" entrytext = %s \n",entrytext);

				clutter_text_set_text( priv->display, entrytext + line_start->start_index );
				entrytext[line_end->start_index] = temp;
				return TRUE;
			}
			else
				clutter_text_set_text( priv->display, entrytext + line_start->start_index );
			return TRUE;
		}
	}
	return FALSE;
}

/**
 * lightwood_multiline_index_to_line_x:
 * @entry: a #LightwoodMultilineEntry
 * @char_pos: position of the character
 * @line: (out) (optional): location to store resulting line index.
 * @x_pos: (out) (optional): location to store resulting position within line
 *
 * Converts from char_pos within the entry to line and X position
 */
void lightwood_multiline_index_to_line_x( LightwoodMultilineEntry *entry, guint char_pos, gint* line, gint* x_pos )
{
	LightwoodMultilineEntryPrivate *priv = GET_PRIVATE (entry);
	PangoLayout * layout = clutter_text_get_layout(CLUTTER_TEXT(priv->entry));

	pango_layout_index_to_line_x(layout, char_pos, FALSE, line, x_pos);
}

/**
 * lightwood_multiline_show_prev_lines:
 * @entry: a #LightwoodMultilineEntry
 * Returns : returns TRUE if previous lines are shown
 *
 * Show previous lines within a mutiline entry box field
 */
gboolean lightwood_multiline_show_prev_lines( LightwoodMultilineEntry *entry )
{
	LightwoodMultilineEntryPrivate *priv = GET_PRIVATE (entry);
	priv->layout = clutter_text_get_layout(CLUTTER_TEXT(priv->entry));

	LIGHTWOOD_MULTILINE_ENTRY_PRINT("\n %s \n",__FUNCTION__);
	if( priv->line_count > 0 )
	{
		PangoLayoutLine *line_start;

		priv->line_count--;
		line_start = pango_layout_get_line( priv->layout, priv->line_count );

		if(line_start != NULL)
		{
			gchar * entrytext = (char*) pango_layout_get_text( priv->layout );
			PangoLayoutLine * line_end = pango_layout_get_line( priv->layout, priv->line_count + priv->nlines );
			if( line_end != NULL )
			{
				gchar temp = entrytext[line_end->start_index];
				entrytext[line_end->start_index] = '\0';
				clutter_text_set_text( priv->display, entrytext + line_start->start_index );
				entrytext[line_end->start_index] = temp;
				return TRUE;
			}
		}
	}
	return FALSE;
}

/**
 * lightwood_multiline_entry_set_font_name:
 * @entry: a #LightwoodMultilineEntry
 * @font_name: a font name and size, or %NULL for the default font
 *
 * Sets font as the font used by entry.
 */
void lightwood_multiline_entry_set_font_name (LightwoodMultilineEntry *entry,const gchar  *font)
{
	LightwoodMultilineEntryPrivate *priv = GET_PRIVATE (entry);

	g_return_if_fail (LIGHTWOOD_IS_MULTILINE_ENTRY (entry));
	if (!font || font[0] == '\0')
		font = LIGHTWOOD_DEFAULT_FONT_NAME;

	clutter_text_set_font_name(priv->entry,font);
	clutter_text_set_font_name(priv->display, font);
}

/**
 * lightwood_multiline_entry_set_color:
 * @entry: a #LightwoodMultilineEntry
 * @color: a font color
 *
 * Sets font as the font used by entry.
 */
void lightwood_multiline_entry_set_color (LightwoodMultilineEntry *entry,const ClutterColor *color)
{
	LightwoodMultilineEntryPrivate *priv = GET_PRIVATE (entry);

	g_return_if_fail (LIGHTWOOD_IS_MULTILINE_ENTRY (entry));
	g_return_if_fail (color != NULL);

	clutter_text_set_color(priv->entry,color);
	clutter_text_set_color(priv->display, color);
}

/**
 * lightwood_multiline_entry_set_cursor_visible:
 * @entry: a #LightwoodMultilineEntry
 * @bVisible: bVisible
 *
 *  function to set cursor to focus
 */
void lightwood_multiline_entry_set_cursor_visible(LightwoodMultilineEntry *entry, gboolean bVisible )
{
	LightwoodMultilineEntryPrivate *priv = GET_PRIVATE (entry);

	g_return_if_fail (LIGHTWOOD_IS_MULTILINE_ENTRY (entry));
	clutter_text_set_cursor_visible(priv->display, bVisible);
}

/**
 * lightwood_multiline_get_active_text_actor:
 * @entry: a #LightwoodMultilineEntry
 *
 * function to get active entry
 *
 * Returns: (transfer none): text actor.
 */
ClutterActor *
lightwood_multiline_get_active_text_actor (LightwoodMultilineEntry *entry)
{
	LightwoodMultilineEntryPrivate *priv = GET_PRIVATE (entry);

	g_return_val_if_fail (LIGHTWOOD_IS_MULTILINE_ENTRY (entry), NULL);

	return (ClutterActor*)priv->display;
}

/**
 * lightwood_multiline_entry_set_properties:
 * @entry: a #LightwoodMultilineEntry
 * @NLinesToDisplay:NLinesToDisplay
 * @bEditable: TRUE/FALSE
 *
 *  function to set properties for textbox
 */
void lightwood_multiline_entry_set_properties(LightwoodMultilineEntry *entry, gint NLinesToDisplay, gboolean bEditable)
{
	LightwoodMultilineEntryPrivate *priv = GET_PRIVATE (entry);

	g_return_if_fail (LIGHTWOOD_IS_MULTILINE_ENTRY (entry));

	priv->nlines = NLinesToDisplay;
	priv->editable = bEditable;

	clutter_text_set_cursor_visible(priv->display , bEditable);
	clutter_text_set_editable(priv->entry , bEditable);
}

/**
 * lightwood_multiline_entry_set_width:
 * @entry: a #LightwoodMultilineEntry
 * @Width: value to set width of textbox
 *
 *  function to set width of textbox
 */
void lightwood_multiline_entry_set_width(LightwoodMultilineEntry *entry, gint Width)
{
	LightwoodMultilineEntryPrivate *priv = GET_PRIVATE (entry);

	g_return_if_fail (LIGHTWOOD_IS_MULTILINE_ENTRY (entry));

	clutter_actor_set_width((ClutterActor*)priv->display ,Width);
	clutter_actor_set_width((ClutterActor*)priv->entry , Width);
}

/**
 * lightwood_multiline_entry_clear_text:
 * @entry: a #LightwoodMultilineEntry
 *
 *  Clears the text present in the multiline entry.
 */
void lightwood_multiline_entry_clear_text( LightwoodMultilineEntry *entry)
{
	LightwoodMultilineEntryPrivate *priv = GET_PRIVATE (entry);

	g_return_if_fail (LIGHTWOOD_IS_MULTILINE_ENTRY (entry));

	clutter_text_delete_text(priv->entry, 0, strlen(clutter_text_get_text(CLUTTER_TEXT(priv->entry))));
	clutter_text_delete_text(priv->display, 0, strlen(clutter_text_get_text(CLUTTER_TEXT(priv->display))));
	priv->line_count = 0;

}

/**
 * lightwood_multiline_entry_set_height:
 * @entry: a #LightwoodMultilineEntry
 * @height: value to set height of textbox
 *
 *  function to set height of textbox
 */
void lightwood_multiline_entry_set_height( LightwoodMultilineEntry *entry, gint height)
{
	LightwoodMultilineEntryPrivate *priv = GET_PRIVATE (entry);

	g_return_if_fail (LIGHTWOOD_IS_MULTILINE_ENTRY (entry));

	clutter_actor_set_height((ClutterActor*)priv->display , height);
}

/**
 * lightwood_multiline_entry_get_text:
 * @entry: a #LightwoodMultilineEntry
 *
 *  function to get entered text
 *
 * Returns: (nullable): constant string pointer.
 */
const gchar *
lightwood_multiline_entry_get_text (LightwoodMultilineEntry *entry)
{
	LightwoodMultilineEntryPrivate *priv = GET_PRIVATE (entry);

	g_return_val_if_fail (LIGHTWOOD_IS_MULTILINE_ENTRY (entry), NULL);
	return clutter_text_get_text (priv->entry);
}

/**
 * lightwood_multiline_entry_grab_key_focus:
 * @entry: a #LightwoodMultilineEntry
 *
 *  function to set focus
 */
void lightwood_multiline_entry_grab_key_focus(LightwoodMultilineEntry *entry)
{
	LightwoodMultilineEntryPrivate *priv = GET_PRIVATE (entry);

	if( !LIGHTWOOD_IS_MULTILINE_ENTRY( entry ) )
	{
		g_warning( "invalid instance\n" );
		return;
	}
	g_return_if_fail (LIGHTWOOD_IS_MULTILINE_ENTRY (entry));
	clutter_actor_grab_key_focus(CLUTTER_ACTOR(priv->display));
	clutter_stage_set_key_focus(CLUTTER_STAGE(clutter_stage_new()), CLUTTER_ACTOR(priv->display));
}

/********************************************************
 * Function : lightwood_multiline_entry_dispose
 * Description: Dispose the liblightwood mutli-line text box object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void lightwood_multiline_entry_dispose (GObject *object)
{
	G_OBJECT_CLASS (lightwood_multiline_entry_parent_class)->dispose (object);
}

/********************************************************
 * Function : lightwood_multiline_entry_finalize
 * Description: Finalize theliblightwood mutli-line text box object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void lightwood_multiline_entry_finalize (GObject *object)
{
	G_OBJECT_CLASS (lightwood_multiline_entry_parent_class)->finalize (object);
}

/********************************************************
 * Function   : lightwood_multiline_entry_class_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters : The object's class reference
 * Return value: void
 ********************************************************/
static void lightwood_multiline_entry_class_init (LightwoodMultilineEntryClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
	gobject_class->finalize     = lightwood_multiline_entry_finalize;
	gobject_class->dispose      = lightwood_multiline_entry_dispose;

	/**
	 * LightwoodMultilineEntry::line-count-changed:
	 * @LightwoodMultilineEntry: The object which received the signal
	 * @count: the new live count
	 *
	 * ::line-count-changed is emitted when entry goes to next line
	 */

	lightwood_multiline_entry_signals[SIG_LIGHTWOOD_MULTILINE_ENTRY_LINE_COUNT_CHANGED] = g_signal_new ("line-count-changed",
			G_TYPE_FROM_CLASS (gobject_class),
			G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
			G_STRUCT_OFFSET (LightwoodMultilineEntryClass, line_count_changed),
			NULL, NULL,
			g_cclosure_marshal_VOID__INT,
			G_TYPE_NONE, 1,
			G_TYPE_INT);
}

/*********************************************************************
 * Function   : lightwood_multiline_entry_init
 * Description: Instance initialisation function for the object type.
 *              Called automatically on every call to g_object_new
 * Parameters : The object's reference
 * Return value: void
 *********************************************************************/
static void lightwood_multiline_entry_init (LightwoodMultilineEntry *self)
{
	LightwoodMultilineEntryPrivate *priv = GET_PRIVATE (self);
	ClutterColor light_blue = {0x20, 0x3E, 0x60, 0x80};

	priv->entry = (ClutterText*)clutter_text_new();
	priv->layout = clutter_text_get_layout( priv->entry );
	clutter_actor_hide((ClutterActor*)priv->entry);

	priv->display = (ClutterText*)clutter_text_new();

	clutter_text_set_line_wrap( priv->entry, TRUE);
	clutter_text_set_line_wrap_mode( priv->entry, PANGO_WRAP_WORD_CHAR );
	clutter_text_set_editable(priv->entry , FALSE);
	clutter_text_set_activatable(priv->entry , TRUE);

	clutter_text_set_line_wrap( priv->display, TRUE);
	clutter_text_set_line_wrap_mode( priv->display , PANGO_WRAP_WORD_CHAR );
	clutter_text_set_editable(priv->display , TRUE);
	clutter_text_set_activatable(priv->display , TRUE);

	clutter_text_set_selectable(priv->display , TRUE);
	clutter_actor_set_reactive(CLUTTER_ACTOR(priv->display), TRUE);
	clutter_text_set_selection_color (CLUTTER_TEXT (priv->display), &light_blue);

	clutter_text_set_cursor_visible(priv->display , FALSE);
	clutter_text_set_cursor_position( priv->display , 0);
	clutter_actor_set_position(CLUTTER_ACTOR(priv->entry),2,4);
	clutter_actor_set_position(CLUTTER_ACTOR(priv->display),2,4);

	g_signal_connect(priv->display,  "cursor-event", G_CALLBACK(cursor_position_changed), self);
	g_signal_connect (priv->display, "button-release-event", G_CALLBACK (button_released_cb),self );
	g_signal_connect (priv->display, "touch-event", G_CALLBACK (captured_event_cb), NULL);
	g_signal_connect(priv->display,  "text-changed", G_CALLBACK(text_changed_clb), self);

	clutter_actor_add_child (CLUTTER_ACTOR (self), (ClutterActor*) priv->display);
	priv->nlines          = 0;
	priv->line_count      = 0;
	priv->editable        = FALSE;
	priv->textSelected = NULL;
}

/**
 * lightwood_multiline_entry_new:
 *
 * Creates a new, empty #LightwoodMultilineEntry.
 *
 * Returns: (transfer full): the newly created #LightwoodMultilineEntry
 */
ClutterActor *
lightwood_multiline_entry_new (void)
{
	return g_object_new (LIGHTWOOD_TYPE_MULTILINE_ENTRY, NULL);
}
