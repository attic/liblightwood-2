/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION:liblightwood-textbox
 * @Title:LightwoodTextBox
 * @Short_Description: Text box widget.
 * @See_Also: #ClutterActor
 *
 * #LightwoodTextBox is a widget where input data can be taken from user.
 * In Lightwood the idea to use this in spellers, where input data is given from keyboard.
 * If there are any specific requirement for the variants of liblightwood, then it can be extended.
 * If we want to use a password text box then sinply set the property password-enable.
 * It is also providing multiline text box using #LightwoodMultilineTextBox.
 * 
 *
 * ## Freeing the widget
 *      Call g_object_unref() to free the widget.
 *
 * ## Sample C Code
 * |[<!-- language="C" -->
 * ClutterActor *create_text_box(void)
 * {
 *	ClutterActor *pTextBox = g_object_new(LIGHTWOOD_TYPE_TEXT_BOX);
 *	if(pTextBox)
 *	{
 *		g_object_set(G_OBJECT(pTextBox),"width",WIDTH,"height",HEIGHT,"text","sample",NULL);
 *	}
 *	clutter_actor_show(pTextBox);
 * 	return pTextBox;
 * }
 * ]|
 */
 
#include "liblightwood-textbox.h"
#include "liblightwood-multiline.h"


/*keyval for backspace*/
#define CLUTTER_BKSP 8592

/* debug prints */
#define LIGHTWOOD_TEXTBOX_PRINT(...)  //g_print(__VA_ARGS__)

enum _LightwoodTextBoxProperty
{
    LIGHTWOOD_TEXTBOX_PROP_ENUM_FIRST = 0,
        LIGHTWOOD_TEXTBOX_PROP_ENUM_MULTI_LINE,
        LIGHTWOOD_TEXTBOX_PROP_ENUM_MAX_LINES,
        LIGHTWOOD_TEXTBOX_PROP_ENUM_DISPLAYED_LINE,
        LIGHTWOOD_TEXTBOX_PROP_ENUM_FONT_TYPE,
        LIGHTWOOD_TEXTBOX_PROP_ENUM_TEXT,
        LIGHTWOOD_TEXTBOX_PROP_ENUM_FOCUS_CURSOR,
        LIGHTWOOD_TEXTBOX_PROP_ENUM_PASSWORD,
        LIGHTWOOD_TEXTBOX_PROP_ENUM_EDITABLE,
        LIGHTWOOD_TEXTBOX_PROP_ENUM_TEXT_COLOR,
        LIGHTWOOD_TEXTBOX_PROP_ENUM_BACKGROUND_COLOR,
        LIGHTWOOD_TEXTBOX_PROP_ENUM_TEXT_X,
        LIGHTWOOD_TEXTBOX_PROP_ENUM_TEXT_Y,
    LIGHTWOOD_TEXTBOX_PROP_ENUM_LAST
};

/* signals of liblightwood textbox */
/**
 * LightwoodTextBoxSignals:
 * @LIGHTWOOD_SIG_TEXT_BOX_ENTRY_PRESS: signal for entry press
 * @LIGHTWOOD_SIG_TEXT_BOX_KEY_PRESS_EVENT: signal for key press
 *
 * The signals for liblightwood textbox
 *
 */
/* speller signals */
enum _LightwoodTextBoxSignals
{
    LIGHTWOOD_SIG_TEXT_BOX_FIRST ,
    LIGHTWOOD_SIG_TEXT_BOX_ENTRY_PRESS,
    LIGHTWOOD_SIG_TEXT_BOX_KEY_PRESS_EVENT,
    LIGHTWOOD_SIG_TEXT_BOX_LAST
};

static guint32 uin_text_box_signals[LIGHTWOOD_SIG_TEXT_BOX_LAST] = {0,};

/* liblightwood text box editor tags*/
typedef struct _LightwoodEditorTag
{
    gint start_index;
    gint end_index;
    gchar *text;
}LightwoodEditorTag;

/* private structure for the liblightwood text box */
typedef struct
{
	ClutterActor *pEntryBox;
	ClutterActor *pTextEntry;
	ClutterActor *pTextGroup;

	gboolean entry_password;
	gboolean multi_line;
	gboolean cursor_focus;
	gboolean editable;
	gchar *font_type;
	gchar *text_color;
	gchar *bg_color;
	gchar *text;
	gint max_lines;
	guint displayed_lines;
    gchar *textSelected;
    gint bound;
    gint cursorPosition;

	LightwoodMultilineEntry *multiLineEntry;
    gint wstart;
    GList *tags;
    LightwoodEditorTag *chosen_tag;
} LightwoodTextBoxPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (LightwoodTextBox, lightwood_text_box, CLUTTER_TYPE_ACTOR)

#define TEXT_BOX_PRIVATE(o) lightwood_text_box_get_instance_private ((LightwoodTextBox* ) o)

/********************************************************
 * Function : linecount_changed
 * Description:callback function, invoked at linecount change
 * Parameters: LightwoodMultilineEntry*,line count,userdata
 * Return value: void
 ********************************************************/
static void linecount_changed (LightwoodMultilineEntry *multiline, gint line_count,gpointer userData)
{
    LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE( userData );

    LIGHTWOOD_TEXTBOX_PRINT(" %s\n", __FUNCTION__);
    g_return_if_fail(LIGHTWOOD_IS_TEXT_BOX(userData));

    LIGHTWOOD_TEXTBOX_PRINT("%s %d  max lines = %d line_count = %d\n",__FUNCTION__,__LINE__,priv->max_lines,line_count);

    g_object_set(LIGHTWOOD_TEXT_BOX(userData), "displayed-lines", line_count, NULL);

	if( priv->max_lines > line_count)
	{
        LIGHTWOOD_TEXTBOX_PRINT(" entered into specified max line code \n");
        clutter_actor_set_position(CLUTTER_ACTOR(priv->pEntryBox), 0.0,0.0 );
        clutter_actor_set_height(CLUTTER_ACTOR(priv->pEntryBox), 64 + (34 * (line_count-1)) );
	}
}

/********************************************************
 * Function : compare_tags_with_attr
 * Description:editor compare function
 * Parameters: LightwoodEditorTag*,PangoAttribute
 * Return value: gint
 ********************************************************/
static gint compare_tags_with_attr(LightwoodEditorTag *tag,PangoAttribute *attr)
{
    LIGHTWOOD_TEXTBOX_PRINT(" %s\n", __FUNCTION__);
    if((attr->start_index == tag->start_index) && (attr->end_index == tag->end_index))
        return 0;

    return -1;
}

/********************************************************
 * Function : filter_attributes
 * Description:filters attributes for editor
 * Parameters: PangoAttribute*,userdata
 * Return value: gboolean
 ********************************************************/
static gboolean filter_attributes(PangoAttribute *attribute,gpointer data)
{
    LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (data);
    GList *tag_item = g_list_find_custom(priv->tags,attribute,(GCompareFunc)compare_tags_with_attr);

    LIGHTWOOD_TEXTBOX_PRINT(" %s\n", __FUNCTION__);

    if(tag_item &&  tag_item->data)
    {
        LightwoodEditorTag *tag = tag_item->data;
        //1. check the new position of this tag in main text
        gchar *complete_text = (gchar *)clutter_text_get_text(CLUTTER_TEXT(lightwood_multiline_get_active_text_actor(priv->multiLineEntry)));
        gchar *new_tag = g_strrstr_len(complete_text,-1,tag->text);
        if(new_tag)
        {
            // update new boundaries for tag
            gint start_index = g_utf8_pointer_to_offset(complete_text,new_tag);
            tag->start_index = start_index;
            tag->end_index = start_index + g_utf8_strlen(tag->text,-1) -1;
            attribute->start_index = start_index;
            attribute->end_index = tag->end_index;
            return TRUE;
        }
        priv->tags = g_list_remove(priv->tags,tag);
        g_free(tag);
    }
    return FALSE;
}

/********************************************************
 * Function : refresh_attributes
 * Description:refresh attributes for editor
 * Parameters: text box object reference
 * Return value: void
 ********************************************************/
static void refresh_attributes(LightwoodTextBox *self)
{
    LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (self);
    PangoAttrList *old_attrs = clutter_text_get_attributes(CLUTTER_TEXT(lightwood_multiline_get_active_text_actor(priv->multiLineEntry)));

    LIGHTWOOD_TEXTBOX_PRINT(" %s\n", __FUNCTION__);

    if(old_attrs)
    {
        PangoAttrList *new_attrs = pango_attr_list_filter(old_attrs,filter_attributes,self);
        PangoAttribute *attr_color = pango_attr_underline_color_new (0xFFFF,0,0);
        if(new_attrs)
            pango_attr_list_insert (new_attrs, attr_color);

        clutter_text_set_attributes(CLUTTER_TEXT(lightwood_multiline_get_active_text_actor(priv->multiLineEntry)),new_attrs);
    }
}

/********************************************************
 * Function : text_entered_cb
 * Description:callback function, invoke when text entered to textbox
 * Parameters: ClutterActor,ClutterKeyEvent,userdata
 * Return value: gboolean
 ********************************************************/
static gboolean text_entered_cb (ClutterActor *actor, ClutterKeyEvent *event, gpointer userData)
{
    LightwoodTextBox *self = LIGHTWOOD_TEXT_BOX(userData);
    LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (self);

    LIGHTWOOD_TEXTBOX_PRINT(" %s\n", __FUNCTION__);
    lightwood_multiline_entry_handle_key_event(priv->multiLineEntry,event);
    return FALSE;
}

/********************************************************
 * Function : delete_unichar
 * Description: function to delete a unichar from textbox
 * Parameters: ClutterActor,userdata
 * Return value: void
 ********************************************************/
static void delete_unichar(ClutterActor *actor,gpointer userData)
{
    LightwoodTextBox *self = LIGHTWOOD_TEXT_BOX(userData);
    LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (self);

    const gchar * string = clutter_text_get_text(CLUTTER_TEXT(actor));
    guint strlength =0;
    gint pos = clutter_text_get_cursor_position (CLUTTER_TEXT(actor));
    if(string != NULL)
    {
      strlength = strlen(string );
    }
    if( strlength > 0 )
    {
        if(priv->textSelected && g_strcmp0(priv->textSelected, "") != 0)
        {
            pos = priv->cursorPosition;//clutter_text_get_cursor_position (priv->entry);

            if(pos == -1)
                clutter_text_delete_text (CLUTTER_TEXT(actor), priv->bound, strlength);
            if(priv->bound == -1)
                clutter_text_delete_text (CLUTTER_TEXT(actor), pos, strlength);
            else
            {
                if(priv->bound < pos)
                    clutter_text_delete_text (CLUTTER_TEXT(actor), priv->bound, pos);
                else
                    clutter_text_delete_text (CLUTTER_TEXT(actor), pos, priv->bound);
            }
            priv->textSelected = NULL;
        }
        else
        {
            pos = priv->cursorPosition;
            if(pos == -1)
                clutter_text_delete_text (CLUTTER_TEXT(actor), strlength - 1 , strlength );
            else
                clutter_text_delete_text (CLUTTER_TEXT(actor), pos - 1 , pos );
        }
    }
}

/********************************************************
 * Function : key_release_cb
 * Description: callback function, invoke at key release
 * Parameters: ClutterActor,ClutterKeyEvent,userdata
 * Return value: gboolean
 ********************************************************/
static gboolean key_release_cb (ClutterActor *actor, ClutterKeyEvent *event, gpointer userData)
{
    LightwoodTextBox *self = LIGHTWOOD_TEXT_BOX(userData);
    LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (self);
    gint keyval = event->keyval;

    LIGHTWOOD_TEXTBOX_PRINT( " \n %s \n", __FUNCTION__ );

    switch (keyval)
    {
          case CLUTTER_Escape:
          case CLUTTER_Shift_L:
          case CLUTTER_Shift_R:
          case CLUTTER_Left:
          case CLUTTER_KP_Left:
          case CLUTTER_Right:
          case CLUTTER_KP_Right:
          case CLUTTER_End:
          case CLUTTER_KP_End:
          case CLUTTER_Begin:
          case CLUTTER_Home:
          case CLUTTER_KP_Home:
          case CLUTTER_Return:
          case CLUTTER_Up:
          case CLUTTER_Down:
          case CLUTTER_Delete:
          case CLUTTER_KP_Delete:
            break;

          case CLUTTER_BackSpace:
          {
                delete_unichar(actor,userData);
          }
          break;
          default:
            /* if text selection is there and any key has been pressed,
             * delete selected text and then insert the char */

            if(priv->textSelected && g_strcmp0(priv->textSelected, "") != 0)
            {
                gint position;

                delete_unichar(actor,userData);
                /* update the cursor position */
                if(priv->bound > priv->cursorPosition && priv->cursorPosition != 0)
                    position = priv->bound - priv->cursorPosition;
                else if(priv ->bound == 0)
                    position = priv->cursorPosition - 1;
                else if(priv->cursorPosition == 0)
                    position = priv->bound - 1;
                else
                    position = priv->cursorPosition - priv->bound;

                clutter_text_set_cursor_position(CLUTTER_TEXT(actor), position);
            }
            break;
    }
    g_signal_emit(CLUTTER_ACTOR(userData), uin_text_box_signals[LIGHTWOOD_SIG_TEXT_BOX_KEY_PRESS_EVENT], 0, keyval);
    return FALSE;
}

/********************************************************
 * Function : recalcuate_word_start_position
 * Description: function invoke when text entered into textbox to cal
 *               the starting word position
 * Parameters:text box object reference
 * Return value: void
 ********************************************************/
static void recalcuate_word_start_position(LightwoodTextBox *self)
{
    LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (self);
    ClutterActor *actor = lightwood_multiline_get_active_text_actor(priv->multiLineEntry);

    LIGHTWOOD_TEXTBOX_PRINT(" %s\n", __FUNCTION__);
    if(clutter_actor_has_key_focus (actor))
    {
        gint cursor_pos = g_utf8_strlen (clutter_text_get_text(CLUTTER_TEXT(actor)),clutter_text_get_cursor_position(CLUTTER_TEXT(actor)));
        gint pos;

        priv->wstart = 0;
        for(pos = cursor_pos-1; pos > 0 ; pos--)
        {
            gunichar prev = g_utf8_get_char(g_utf8_offset_to_pointer(clutter_text_get_text(CLUTTER_TEXT(actor)),pos));
            if((prev == ' ') || (prev == '.'))
            {
                priv->wstart = pos + 1;
                break;
            }
        }
    }
}

/********************************************************
 * Function : key_focus_cb
 * Description: callback function, invoke at key focus
 * Parameters: ClutterActor,userdata
 * Return value: void
 ********************************************************/
static void key_focus_cb (ClutterActor *actor, gpointer userdata)
{
    LightwoodTextBox *self = LIGHTWOOD_TEXT_BOX(userdata);
    LIGHTWOOD_TEXTBOX_PRINT(" %s\n", __FUNCTION__);

    recalcuate_word_start_position(self);
}

/********************************************************
 * Function : cursor_event_cb
 * Description: callback function ,invoke at cursor focus
 * Parameters: ClutterText,ClutterGeometry,userdata
 * Return value: void
 ********************************************************/
static void cursor_event_cb (ClutterText *actor,ClutterGeometry *geometry,gpointer userdata)
{
    LightwoodTextBox *self = LIGHTWOOD_TEXT_BOX(userdata);
    LIGHTWOOD_TEXTBOX_PRINT(" %s\n", __FUNCTION__);

    recalcuate_word_start_position(self);
}

/********************************************************
 * Function : compare_tag
 * Description: function to compare tags
 * Parameters: LightwoodEditorTag,ClutterText
 * Return value: gint
 ********************************************************/
static gint compare_tag(LightwoodEditorTag *tag,ClutterText *actor)
{
    gint cursor_pos = clutter_text_get_cursor_position(actor);
    LIGHTWOOD_TEXTBOX_PRINT(" %s\n", __FUNCTION__);

    if((cursor_pos > tag->start_index) && (cursor_pos < tag->end_index))
        return 0;

    return -1;
}

/********************************************************
 * Function : text_pressed_cb
 * Description: callback function ,invoke at text press
 * Parameters: ClutterActor,ClutterEvent,userdata
 * Return value: gboolean
 ********************************************************/
static gboolean text_pressed_cb (ClutterActor *actor,ClutterEvent *event,gpointer user_data)
{
    LightwoodTextBox *self = LIGHTWOOD_TEXT_BOX(user_data);
    LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (self);
    GList *list_item = g_list_find_custom(priv->tags,CLUTTER_TEXT(actor),(GCompareFunc)compare_tag);

    LIGHTWOOD_TEXTBOX_PRINT(" %s\n", __FUNCTION__);

    if(list_item)
    {
        priv->chosen_tag = list_item->data;
    }
    else
    {
        recalcuate_word_start_position(self);
    }
    //g_signal_emit(CLUTTER_ACTOR(user_data), uin_text_box_signals[LIGHTWOOD_SIG_TEXT_BOX_ENTRY_PRESS], 0, NULL);
    return FALSE;
}

/********************************************************
 * Function : text_deleted_cb
 * Description: callback function ,invoke at text delete
 * Parameters: ClutterText,start_position,end_position,userdata
 * Return value: void
 ********************************************************/
static void text_deleted_cb(ClutterText *self,gint start_pos,gint end_pos,gpointer user_data)
{
    refresh_attributes(LIGHTWOOD_TEXT_BOX(user_data));
}

/**
 * lightwood_text_box_get_multi_line:
 * @self: a #LightwoodTextBox
 *
 * Return the #LightwoodTextBox:multi-line property
 *
 * Returns: the value of #LightwoodTextBox:multi-line property
 */
gboolean
lightwood_text_box_get_multi_line (LightwoodTextBox *self)
{
  LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (self);

  LIGHTWOOD_TEXTBOX_PRINT( " \n %s \n", __FUNCTION__ );
  g_return_val_if_fail (LIGHTWOOD_IS_TEXT_BOX (self), FALSE);

  return priv->multi_line;
}

/**
 * lightwood_text_box_get_focus_cursor:
 * @self: a #LightwoodTextBox
 *
 * Return the #LightwoodTextBox:focus-cursor property
 *
 * Returns: the value of #LightwoodTextBox:focus-cursor property
 */
gboolean
lightwood_text_box_get_focus_cursor (LightwoodTextBox *self)
{
  LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (self);

  LIGHTWOOD_TEXTBOX_PRINT( " \n %s \n", __FUNCTION__ );
  g_return_val_if_fail (LIGHTWOOD_IS_TEXT_BOX (self), FALSE);

  return priv->cursor_focus;
}

/**
 * lightwood_text_box_get_password_enable:
 * @self: a #LightwoodTextBox
 *
 * Return the #LightwoodTextBox:password-enable property
 *
 * Returns: the value of #LightwoodTextBox:password-enable property
 */
gboolean
lightwood_text_box_get_password_enable (LightwoodTextBox *self)
{
  LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (self);

  g_return_val_if_fail (LIGHTWOOD_IS_TEXT_BOX (self), FALSE);
  LIGHTWOOD_TEXTBOX_PRINT( " \n %s \n", __FUNCTION__ );

  return priv->entry_password;
}

/**
 * lightwood_text_box_get_text_editable:
 * @self: a #LightwoodTextBox
 *
 * Return the #LightwoodTextBox:text-editable property
 *
 * Returns: the value of #LightwoodTextBox:text-editable property
 */
gboolean
lightwood_text_box_get_text_editable (LightwoodTextBox *self)
{
  LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (self);

  g_return_val_if_fail (LIGHTWOOD_IS_TEXT_BOX (self), FALSE);
  LIGHTWOOD_TEXTBOX_PRINT( " \n %s \n", __FUNCTION__ );

  return priv->editable;
}

/**
 * lightwood_text_box_get_font_type:
 * @self: a #LightwoodTextBox
 *
 * Return the #LightwoodTextBox:font-type property
 *
 * Returns: (nullable): the value of #LightwoodTextBox:font-type property
 */
const gchar *
lightwood_text_box_get_font_type (LightwoodTextBox *self)
{
  LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (self);

  g_return_val_if_fail (LIGHTWOOD_IS_TEXT_BOX (self), NULL);
  LIGHTWOOD_TEXTBOX_PRINT( " \n %s \n", __FUNCTION__ );

  return priv->font_type;
}

/**
 * lightwood_text_box_get_text_color:
 * @self: a #LightwoodTextBox
 *
 * Return the #LightwoodTextBox:text-color property
 *
 * Returns: (nullable): the value of #LightwoodTextBox:text-color property
 */
const gchar *
lightwood_text_box_get_text_color (LightwoodTextBox *self)
{
  LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (self);

  g_return_val_if_fail (LIGHTWOOD_IS_TEXT_BOX (self), NULL);
  LIGHTWOOD_TEXTBOX_PRINT( " \n %s \n", __FUNCTION__ );

  return priv->text_color;
}

/**
 * lightwood_text_box_get_background_color:
 * @self: a #LightwoodTextBox
 *
 * Return the #LightwoodTextBox:background-color property
 *
 * Returns: (nullable): the value of #LightwoodTextBox:background-color property
 */
const gchar *
lightwood_text_box_get_background_color (LightwoodTextBox *self)
{
  LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (self);

  g_return_val_if_fail (LIGHTWOOD_IS_TEXT_BOX (self), NULL);
  LIGHTWOOD_TEXTBOX_PRINT( " \n %s \n", __FUNCTION__ );

  return priv->bg_color;
}

/**
 * lightwood_text_box_get_text:
 * @self: a #LightwoodTextBox
 *
 * Return the #LightwoodTextBox:text property
 *
 * Returns: (nullable): the value of #LightwoodTextBox:text property
 */
const gchar *
lightwood_text_box_get_text (LightwoodTextBox *self)
{
  LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (self);
  const gchar *text;

  g_return_val_if_fail (LIGHTWOOD_IS_TEXT_BOX (self), NULL);
  LIGHTWOOD_TEXTBOX_PRINT( " \n %s \n", __FUNCTION__ );

  if (priv->multi_line)
    text = clutter_text_get_text (CLUTTER_TEXT (lightwood_multiline_get_active_text_actor (priv->multiLineEntry)));
  else
    text = clutter_text_get_text (CLUTTER_TEXT (priv->pTextEntry));

  return text;
}

/**
 * lightwood_text_box_get_max_lines:
 * @self: a #LightwoodTextBox
 *
 * Return the #LightwoodTextBox:max-lines property
 *
 * Returns: the value of #LightwoodTextBox:max-lines property
 */
gint
lightwood_text_box_get_max_lines (LightwoodTextBox *self)
{
  LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (self);

  LIGHTWOOD_TEXTBOX_PRINT( " \n %s \n", __FUNCTION__ );
  g_return_val_if_fail (LIGHTWOOD_IS_TEXT_BOX (self), -1);

  return priv->max_lines;
}

/**
 * lightwood_text_box_get_displayed_lines:
 * @self: a #LightwoodTextBox
 *
 * Return the #LightwoodTextBox:displayed-lines property
 *
 * Returns: the value of #LightwoodTextBox:displayed-lines property
 */
guint
lightwood_text_box_get_displayed_lines (LightwoodTextBox *self)
{
  LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (self);

  LIGHTWOOD_TEXTBOX_PRINT( " \n %s \n", __FUNCTION__ );
  g_return_val_if_fail (LIGHTWOOD_IS_TEXT_BOX (self), 0);

  return priv->displayed_lines;
}

#define WIDTH_OFFSET 4

static void
width_notify_cb (GObject *object,
                 GParamSpec *spec,
                 LightwoodTextBox *self)
{
  LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (self);
  gfloat width = clutter_actor_get_width (CLUTTER_ACTOR (self));

  LIGHTWOOD_TEXTBOX_PRINT( " \n %s \n", __FUNCTION__ );

  clutter_actor_set_width (priv->pEntryBox, width);
  clutter_actor_set_width (priv->pTextEntry, width - WIDTH_OFFSET);
  lightwood_multiline_entry_set_width (priv->multiLineEntry, width - WIDTH_OFFSET);
}

static void
height_notify_cb (GObject *object,
                  GParamSpec *spec,
                  LightwoodTextBox *self)
{
  LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (self);
  gfloat height = clutter_actor_get_height (CLUTTER_ACTOR (self));

  LIGHTWOOD_TEXTBOX_PRINT( " \n %s \n", __FUNCTION__ );

  clutter_actor_set_height (priv->pEntryBox, height);
}

/**
 * lightwood_text_box_set_text_x:
 * @self: a #LightwoodTextBox
 * @xpos: the new value
 *
 * Update the #LightwoodTextBox:text-x property
 */
void
lightwood_text_box_set_text_x (LightwoodTextBox *self,
                               gfloat xpos)
{
  LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (self);
  LIGHTWOOD_TEXTBOX_PRINT( " \n %s \n", __FUNCTION__ );

  g_return_if_fail (LIGHTWOOD_IS_TEXT_BOX (self));

  clutter_actor_set_x (CLUTTER_ACTOR (priv->pTextEntry), xpos);
}

/**
 * lightwood_text_box_set_text_y:
 * @self: a #LightwoodTextBox
 * @ypos: the new value
 *
 * Update the #LightwoodTextBox:text-y property
 */
void
lightwood_text_box_set_text_y (LightwoodTextBox *self,
                               gfloat ypos)
{
  LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (self);
  LIGHTWOOD_TEXTBOX_PRINT( " \n %s \n", __FUNCTION__ );

  g_return_if_fail (LIGHTWOOD_IS_TEXT_BOX (self));

  clutter_actor_set_y (CLUTTER_ACTOR (priv->pTextEntry), ypos);
}

/**
 * lightwood_text_box_get_text_x:
 * @self: a #LightwoodTextBox
 *
 * Return the #LightwoodTextBox:text-x property
 *
 * Returns: the value of #LightwoodTextBox:text-x property
 */
gfloat
lightwood_text_box_get_text_x (LightwoodTextBox *self)
{
  LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (self);
  LIGHTWOOD_TEXTBOX_PRINT( " \n %s \n", __FUNCTION__ );

  g_return_val_if_fail (LIGHTWOOD_IS_TEXT_BOX (self), 0.0);

  return clutter_actor_get_x (CLUTTER_ACTOR (priv->pTextEntry));
}

/**
 * lightwood_text_box_get_text_y:
 * @self: a #LightwoodTextBox
 *
 * Return the #LightwoodTextBox:text-y property
 *
 * Returns: the value of #LightwoodTextBox:text-y property
 */
gfloat
lightwood_text_box_get_text_y (LightwoodTextBox *self)
{
  LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (self);
  LIGHTWOOD_TEXTBOX_PRINT( " \n %s \n", __FUNCTION__ );

  g_return_val_if_fail (LIGHTWOOD_IS_TEXT_BOX (self), 0.0);

  return clutter_actor_get_y (CLUTTER_ACTOR (priv->pTextEntry));
}

/**
 * lightwood_text_box_select_text:
 * @self: a #LightwoodTextBox
 *
 * Select the text displayed in @self.
 */
void
lightwood_text_box_select_text (LightwoodTextBox *self)
{
  LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (self);
  guint textlen = 0;
  const gchar *text = NULL;

  LIGHTWOOD_TEXTBOX_PRINT( "\n %s\n", __FUNCTION__ );

  text = lightwood_text_box_get_text (self);
  textlen = strlen (text);

  clutter_text_set_selection (CLUTTER_TEXT (priv->pTextEntry), 0, textlen);

  priv->textSelected = clutter_text_get_selection (CLUTTER_TEXT (priv->pTextEntry));
  if (priv->textSelected && g_strcmp0 (priv->textSelected, "") != 0)
    {
      priv->bound = clutter_text_get_selection_bound (CLUTTER_TEXT (priv->pTextEntry));
    }
}

/**
 * lightwood_text_box_clear_text:
 * @self: a #LightwoodTextBox
 *
 * Clear the text displayed in @self.
 */
void
lightwood_text_box_clear_text (LightwoodTextBox *self)
{
  LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (self);

  LIGHTWOOD_TEXTBOX_PRINT( " \n %s \n", __FUNCTION__ );
  g_return_if_fail (LIGHTWOOD_IS_TEXT_BOX (self));

  if (priv->multi_line)
    lightwood_multiline_entry_clear_text(priv->multiLineEntry);
  else
    g_object_set (priv->pTextEntry, "text", "", NULL);
}

/**
 * lightwood_text_box_set_multi_line:
 * @self: a #LightwoodTextBox
 * @multi_line: the new value
 *
 * Update the #LightwoodTextBox:multi-line property
 */
void
lightwood_text_box_set_multi_line (LightwoodTextBox *self,
                                   gboolean multi_line)
{
  LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE( self );

  LIGHTWOOD_TEXTBOX_PRINT( " \n %s \n", __FUNCTION__ );
  g_return_if_fail (LIGHTWOOD_IS_TEXT_BOX (self));

  if (priv->multi_line == multi_line)
    return;

  if (multi_line)
    {
      LIGHTWOOD_TEXTBOX_PRINT(" \n Multiline entry mode is set \n");
      priv->multi_line = TRUE;
      clutter_actor_hide(priv->pTextEntry);
      clutter_actor_show(CLUTTER_ACTOR(priv->multiLineEntry));
      lightwood_multiline_entry_grab_key_focus(priv->multiLineEntry);
      lightwood_multiline_entry_set_cursor_visible(priv->multiLineEntry,TRUE);
      clutter_actor_set_clip_to_allocation(CLUTTER_ACTOR(priv->pEntryBox),TRUE);
    }
    else
    {
      LIGHTWOOD_TEXTBOX_PRINT(" \n Singleline entry mode is set \n");
      priv->multi_line = FALSE;
      clutter_actor_hide(CLUTTER_ACTOR(priv->multiLineEntry));
      clutter_actor_show(priv->pTextEntry);
      clutter_text_set_activatable (CLUTTER_TEXT(priv->pTextEntry), TRUE);
      clutter_text_set_selection (CLUTTER_TEXT ( priv->pTextEntry),0,0);
      //clutter_text_set_cursor_visible(CLUTTER_TEXT( priv->pTextEntry), TRUE);
      //clutter_actor_grab_key_focus(CLUTTER_ACTOR( priv->pTextEntry));
      //clutter_stage_set_key_focus(CLUTTER_STAGE(clutter_stage_new()), (ClutterActor *)priv->pTextEntry);
    }

    g_object_notify (G_OBJECT (self), "multi-line");
}

/**
 * lightwood_text_box_set_focus_cursor:
 * @self: a #LightwoodTextBox
 * @focus: the new value
 *
 * Update the #LightwoodTextBox:focus-cursor property
 */
void
lightwood_text_box_set_focus_cursor (LightwoodTextBox *self,
                                     gboolean focus)
{
  LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (self);

  LIGHTWOOD_TEXTBOX_PRINT( " \n %s \n", __FUNCTION__ );
  g_return_if_fail (LIGHTWOOD_IS_TEXT_BOX (self));

  if (priv->cursor_focus == focus)
    return;

  if (focus)
    {
      if (priv->multi_line)
        {
          clutter_stage_set_key_focus (CLUTTER_STAGE (clutter_stage_new ()), (ClutterActor *) priv->multiLineEntry);
          clutter_actor_grab_key_focus ((ClutterActor *) priv->multiLineEntry);
        }
      else
        {
          clutter_stage_set_key_focus (CLUTTER_STAGE (clutter_stage_new ()), (ClutterActor *) priv->pTextEntry);
          clutter_actor_grab_key_focus ((ClutterActor *) priv->pTextEntry);
        }
    }

  priv->cursor_focus = focus;
  g_object_notify (G_OBJECT (self), "focus-cursor");
}

/**
 * lightwood_text_box_set_password_enable:
 * @self: a #LightwoodTextBox
 * @enable: the new value
 *
 * Update the #LightwoodTextBox:password-enable property
 */
void
lightwood_text_box_set_password_enable (LightwoodTextBox *self,
                                        gboolean enable)
{
  LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (self);

  g_return_if_fail (LIGHTWOOD_IS_TEXT_BOX (self));
  LIGHTWOOD_TEXTBOX_PRINT( " \n %s \n", __FUNCTION__ );

  if (priv->entry_password == enable)
    return;

  if (enable)
    {
      if (priv->multi_line)
        clutter_text_set_password_char (CLUTTER_TEXT(priv->pTextEntry), '*');
      else
        clutter_text_set_password_char (CLUTTER_TEXT(priv->pTextEntry), '*');
    }

  priv->entry_password = enable;
  g_object_notify (G_OBJECT (self), "password-enable");
}

static void
reactive_notify_cb (GObject *object,
                    GParamSpec *spec,
                    LightwoodTextBox *self)
{
  LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (self);
  gboolean reactive = clutter_actor_get_reactive (CLUTTER_ACTOR (self));

  LIGHTWOOD_TEXTBOX_PRINT( " \n %s \n", __FUNCTION__ );

  clutter_actor_set_reactive (priv->pEntryBox, reactive);
}

/**
 * lightwood_text_box_set_text_editable:
 * @self: a #LightwoodTextBox
 * @editable: the new value
 *
 * Update the #LightwoodTextBox:text-editable property
 */
void
lightwood_text_box_set_text_editable (LightwoodTextBox *self,
                                      gboolean editable)
{
  LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (self);

  LIGHTWOOD_TEXTBOX_PRINT( " \n %s \n", __FUNCTION__ );
  g_return_if_fail (LIGHTWOOD_IS_TEXT_BOX (self));

  if (priv->editable == editable)
    return;

  priv->editable = editable;

  if (priv->multi_line)
    g_object_set (priv->multiLineEntry, "editable", priv->editable, NULL);
  else
    g_object_set (priv->pTextEntry, "editable", priv->editable, NULL);

  g_object_notify (G_OBJECT (self), "text-editable");
}

/**
 * lightwood_text_box_shift_up:
 * @self: a #LightwoodTextBox
 *
 * Shift up text displayed in @self.
 */
void
lightwood_text_box_shift_up (LightwoodTextBox *self)
{
  LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (self);

  LIGHTWOOD_TEXTBOX_PRINT( " \n %s \n", __FUNCTION__ );

  lightwood_multiline_show_prev_lines (priv->multiLineEntry);
}

/**
 * lightwood_text_box_shift_down:
 * @self: a #LightwoodTextBox
 *
 * Shift down text displayed in @self.
 */
void
lightwood_text_box_shift_down (LightwoodTextBox *self)
{
  LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (self);

  LIGHTWOOD_TEXTBOX_PRINT( " \n %s \n", __FUNCTION__ );

  lightwood_multiline_show_next_lines (priv->multiLineEntry);
}

/**
 * lightwood_text_box_set_font_type:
 * @self: a #LightwoodTextBox
 * @font_type: the new value
 *
 * Update the #LightwoodTextBox:font-type property
 */
void
lightwood_text_box_set_font_type (LightwoodTextBox *self,
                                  const gchar *font_type)
{
  LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (self);

  g_return_if_fail (LIGHTWOOD_IS_TEXT_BOX (self));
  g_return_if_fail (font_type != NULL);
  LIGHTWOOD_TEXTBOX_PRINT( " \n %s \n", __FUNCTION__ );

  if (g_strcmp0 (priv->font_type, font_type) == 0)
    return;

  g_free (priv->font_type);
  priv->font_type = g_strdup (font_type);
  clutter_text_set_font_name (CLUTTER_TEXT (priv->pTextEntry), priv->font_type);
  lightwood_multiline_entry_set_font_name (priv->multiLineEntry, priv->font_type);

  g_object_notify (G_OBJECT (self), "font-type");
}


/**
 * lightwood_text_box_set_background_color:
 * @self: a #LightwoodTextBox
 * @background_color: the new value
 *
 * Update the #LightwoodTextBox:background-color property
 */
void
lightwood_text_box_set_background_color (LightwoodTextBox *self,
                                         const gchar *background_color)
{
  LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (self);
  ClutterColor setColor;

  g_return_if_fail (LIGHTWOOD_IS_TEXT_BOX (self));
  g_return_if_fail (background_color != NULL);
  g_return_if_fail (clutter_color_from_string (&setColor, background_color));
  LIGHTWOOD_TEXTBOX_PRINT( " \n %s \n", __FUNCTION__ );

  if (g_strcmp0 (priv->bg_color, background_color) == 0)
    return;

  g_free (priv->bg_color);
  priv->bg_color = g_strdup (background_color);

  clutter_actor_set_background_color (priv->pEntryBox, &setColor);
}

/**
 * lightwood_text_box_set_text_color:
 * @self: a #LightwoodTextBox
 * @text_color: the new value
 *
 * Update the #LightwoodTextBox:text-color property
 */
void
lightwood_text_box_set_text_color (LightwoodTextBox *self,
                                   const gchar *text_color)
{
  LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (self);
  ClutterColor setColor;

  LIGHTWOOD_TEXTBOX_PRINT( " \n %s \n", __FUNCTION__ );
  g_return_if_fail (LIGHTWOOD_IS_TEXT_BOX (self));
  g_return_if_fail (text_color != NULL);
  g_return_if_fail (clutter_color_from_string (&setColor, text_color));

  if (g_strcmp0 (priv->text_color, text_color) == 0)
    return;

  g_free (priv->text_color);
  priv->text_color = g_strdup (text_color);

  if (priv->multi_line)
    lightwood_multiline_entry_set_color (priv->multiLineEntry, &setColor);
  else
    clutter_text_set_color (CLUTTER_TEXT (priv->pTextEntry), &setColor);

  g_object_notify (G_OBJECT (self), "text-color");
}

/**
 * lightwood_text_box_set_text:
 * @self: a #LightwoodTextBox
 * @text: the new value
 *
 * Update the #LightwoodTextBox:text property
 */
void
lightwood_text_box_set_text (LightwoodTextBox *self,
                             const gchar *text)
{
  LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (self);
  gint keyval;

  LIGHTWOOD_TEXTBOX_PRINT( " \n %s \n", __FUNCTION__ );
  g_return_if_fail (LIGHTWOOD_IS_TEXT_BOX (self));
  g_return_if_fail (text != NULL);

  keyval = (gint) g_utf8_get_char (text);

    switch (keyval)
    {
      case CLUTTER_BKSP:
      case CLUTTER_BackSpace:
      {
      	if(priv->multi_line == TRUE)
      	{
		lightwood_multiline_display_entry_delete_char (priv->multiLineEntry);
      	}
      	else
      	{

            const char * string = clutter_text_get_text(CLUTTER_TEXT(priv->pTextEntry));
            int len = 0;
            gint pos = clutter_text_get_cursor_position (CLUTTER_TEXT(priv->pTextEntry));

            if(string != NULL)
            {
                gunichar *gucs = g_utf8_to_ucs4_fast(string, -1, NULL);
                for (len = 0; gucs[len] != 0; len++);
            }
            if(pos == -1)
            {
                clutter_text_delete_text (CLUTTER_TEXT(priv->pTextEntry), len - 1 , len );
            }
            else
            {
                clutter_text_delete_text (CLUTTER_TEXT(priv->pTextEntry), pos - 1 , pos );
            }

      	}
      }
      break;
      default:
      {
      	if(priv->multi_line == TRUE)
      	{
      		lightwood_multiline_default_entry(priv->multiLineEntry,keyval,g_strdup(text));
      	}
      	else
      	{
            int pos;

            if(priv->textSelected && g_strcmp0(priv->textSelected, "") != 0)
            {
                clutter_text_delete_selection(CLUTTER_TEXT(priv->pTextEntry));
            }
            pos = clutter_text_get_cursor_position (CLUTTER_TEXT(priv->pTextEntry));
            clutter_text_insert_text( CLUTTER_TEXT(priv->pTextEntry) ,text,pos);
      	}
      }
      break;
    }
}

#define MAX_LINE_COUNT 15

/**
 * lightwood_text_box_set_max_lines:
 * @self: a #LightwoodTextBox
 * @max_lines: the new value
 *
 * Update the #LightwoodTextBox:max-lines property
 */
void
lightwood_text_box_set_max_lines (LightwoodTextBox *self,
                                  gint max_lines)
{
  LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (self);

  LIGHTWOOD_TEXTBOX_PRINT( " \n %s \n", __FUNCTION__ );
  g_return_if_fail (LIGHTWOOD_IS_TEXT_BOX (self));

  if (priv->max_lines == max_lines)
    return;

  priv->max_lines = max_lines;
  if (max_lines == -1)
    priv->max_lines = MAX_LINE_COUNT;

  lightwood_multiline_entry_set_properties (priv->multiLineEntry, priv->max_lines, TRUE);
  g_object_notify (G_OBJECT (self), "max-lines");
}

/**
 * lightwood_text_box_set_displayed_lines:
 * @self: a #LightwoodTextBox
 * @displayed_lines: the new value
 *
 * Update the #LightwoodTextBox:displayed-lines property
 */
void
lightwood_text_box_set_displayed_lines (LightwoodTextBox *self,
                                        guint displayed_lines)
{
  LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (self);

  LIGHTWOOD_TEXTBOX_PRINT( " \n %s \n", __FUNCTION__ );
  g_return_if_fail (LIGHTWOOD_IS_TEXT_BOX (self));

  if (priv->displayed_lines == displayed_lines)
    return;

  priv->displayed_lines = displayed_lines;

  g_object_notify (G_OBJECT (self), "displayed-lines");
}

/********************************************************
 * Function : lightwood_text_box_get_property
 * Description: Get a property value
 * Parameters: The object reference, property Id,
 *              return location for where the property
 *              value is to be returned and
 *              the param spec of the object
 * Return value: void
 ********************************************************/
static void lightwood_text_box_get_property( GObject *pObject, guint inProperty_id, GValue *pValue, GParamSpec *pPspec )
{
	LightwoodTextBox *pLightwoodTextBox = LIGHTWOOD_TEXT_BOX(pObject);

   	LIGHTWOOD_TEXTBOX_PRINT( " \n %s \n", __FUNCTION__ );

	switch( inProperty_id )
	{
        case LIGHTWOOD_TEXTBOX_PROP_ENUM_MULTI_LINE:
                g_value_set_boolean (pValue, lightwood_text_box_get_multi_line (pLightwoodTextBox));
                break;

        case LIGHTWOOD_TEXTBOX_PROP_ENUM_FOCUS_CURSOR:
                g_value_set_boolean (pValue, lightwood_text_box_get_focus_cursor (pLightwoodTextBox));
                break;

        case LIGHTWOOD_TEXTBOX_PROP_ENUM_PASSWORD:
                g_value_set_boolean (pValue, lightwood_text_box_get_password_enable (pLightwoodTextBox));
                break;

        case LIGHTWOOD_TEXTBOX_PROP_ENUM_MAX_LINES:
                g_value_set_int (pValue, lightwood_text_box_get_max_lines (pLightwoodTextBox));
                break;

       case LIGHTWOOD_TEXTBOX_PROP_ENUM_DISPLAYED_LINE:
                g_value_set_uint (pValue, lightwood_text_box_get_displayed_lines (pLightwoodTextBox));
                break;

        case LIGHTWOOD_TEXTBOX_PROP_ENUM_EDITABLE:
                g_value_set_boolean (pValue, lightwood_text_box_get_text_editable (pLightwoodTextBox));
                break;

        case LIGHTWOOD_TEXTBOX_PROP_ENUM_FONT_TYPE:
                g_value_set_string (pValue, lightwood_text_box_get_font_type (pLightwoodTextBox));
                break;

        case LIGHTWOOD_TEXTBOX_PROP_ENUM_TEXT_COLOR:
                g_value_set_string (pValue, lightwood_text_box_get_text_color (pLightwoodTextBox));
                break;

        case LIGHTWOOD_TEXTBOX_PROP_ENUM_TEXT:
                g_value_set_string (pValue, lightwood_text_box_get_text (pLightwoodTextBox));
                break;

        case LIGHTWOOD_TEXTBOX_PROP_ENUM_BACKGROUND_COLOR:
                g_value_set_string (pValue, lightwood_text_box_get_background_color (pLightwoodTextBox));
                break;

        case LIGHTWOOD_TEXTBOX_PROP_ENUM_TEXT_X:
                g_value_set_float (pValue, lightwood_text_box_get_text_x (pLightwoodTextBox));
                break;

        case LIGHTWOOD_TEXTBOX_PROP_ENUM_TEXT_Y:
                g_value_set_float (pValue, lightwood_text_box_get_text_y (pLightwoodTextBox));
                break;

		default:
                G_OBJECT_WARN_INVALID_PROPERTY_ID( pObject, inProperty_id, pPspec );
                break;
	}
}


/********************************************************
 * Function : lightwood_text_box_set_property
 * Description: Get a property value
 * Parameters: The object reference, property Id,
 *              return location for where the property
 *              value is to be returned and
 *              the param spec of the object
 * Return value: void
 ********************************************************/
static void lightwood_text_box_set_property( GObject *pObject, guint inProperty_id, const GValue *pValue, GParamSpec *pPspec )
{
	LightwoodTextBox *pLightwoodTextBox = LIGHTWOOD_TEXT_BOX(pObject);

   	LIGHTWOOD_TEXTBOX_PRINT( " \n %s \n", __FUNCTION__ );

	switch( inProperty_id )
	{
        case LIGHTWOOD_TEXTBOX_PROP_ENUM_MULTI_LINE:
                lightwood_text_box_set_multi_line (pLightwoodTextBox, g_value_get_boolean (pValue));
                break;

        case LIGHTWOOD_TEXTBOX_PROP_ENUM_FOCUS_CURSOR:
                lightwood_text_box_set_focus_cursor (pLightwoodTextBox, g_value_get_boolean (pValue));
                break;

        case LIGHTWOOD_TEXTBOX_PROP_ENUM_PASSWORD:
                lightwood_text_box_set_password_enable (pLightwoodTextBox, g_value_get_boolean (pValue));
                break;

        case LIGHTWOOD_TEXTBOX_PROP_ENUM_MAX_LINES:
                lightwood_text_box_set_max_lines (pLightwoodTextBox, g_value_get_int (pValue));
                break;

        case LIGHTWOOD_TEXTBOX_PROP_ENUM_DISPLAYED_LINE:
                lightwood_text_box_set_displayed_lines (pLightwoodTextBox, g_value_get_uint (pValue));
                break;

        case LIGHTWOOD_TEXTBOX_PROP_ENUM_EDITABLE:
                lightwood_text_box_set_text_editable (pLightwoodTextBox, g_value_get_boolean (pValue));
                break;

        case LIGHTWOOD_TEXTBOX_PROP_ENUM_FONT_TYPE:
                lightwood_text_box_set_font_type (pLightwoodTextBox, g_value_get_string (pValue));
                break;

        case LIGHTWOOD_TEXTBOX_PROP_ENUM_TEXT_COLOR:
                lightwood_text_box_set_text_color (pLightwoodTextBox, g_value_get_string (pValue));
                break;
        case LIGHTWOOD_TEXTBOX_PROP_ENUM_BACKGROUND_COLOR:
                lightwood_text_box_set_background_color (pLightwoodTextBox, g_value_get_string (pValue));
                break;
        case LIGHTWOOD_TEXTBOX_PROP_ENUM_TEXT:
                lightwood_text_box_set_text (pLightwoodTextBox, g_value_get_string (pValue));
                break;
        case LIGHTWOOD_TEXTBOX_PROP_ENUM_TEXT_X:
                lightwood_text_box_set_text_x (pLightwoodTextBox, g_value_get_float (pValue));
                break;
        case LIGHTWOOD_TEXTBOX_PROP_ENUM_TEXT_Y:
                lightwood_text_box_set_text_y (pLightwoodTextBox, g_value_get_float (pValue));
                break;

        default:
                G_OBJECT_WARN_INVALID_PROPERTY_ID( pObject, inProperty_id, pPspec );
                break;
	}
}
/*********************************************************************
 * Function   : lightwood_text_box_init
 * Description: Instance initialisation function for the object type.
 *              Called automatically on every call to g_object_new
 * Parameters : The object's reference
 * Return value: void
 *********************************************************************/
static void lightwood_text_box_init( LightwoodTextBox *pSelf )
{
    LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (pSelf);

    LIGHTWOOD_TEXTBOX_PRINT(" \n %s \n", __FUNCTION__);

    priv->pEntryBox = NULL;
    priv->multi_line = FALSE ;
    priv->font_type = NULL;
    priv->text = NULL;
    priv->max_lines = 0;
    priv->displayed_lines = 0;
    priv->editable = TRUE;

    g_signal_connect (pSelf, "notify::width", G_CALLBACK (width_notify_cb), pSelf);
    g_signal_connect (pSelf, "notify::height", G_CALLBACK (height_notify_cb), pSelf);
    g_signal_connect (pSelf, "notify::reactive", G_CALLBACK (reactive_notify_cb), pSelf);
}

/********************************************************
 * Function : lightwood_text_box_dispose
 * Description: Dispose the liblightwood text box object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void lightwood_text_box_dispose( GObject *pObject )
{
    LIGHTWOOD_TEXTBOX_PRINT(" \n %s \n", __FUNCTION__);
  	G_OBJECT_CLASS(lightwood_text_box_parent_class)->dispose( pObject );
}

/********************************************************
 * Function : lightwood_text_box_finalize
 * Description: Finalize the liblightwood text box object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void lightwood_text_box_finalize( GObject *pObject )
{
    LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (pObject);
    LIGHTWOOD_TEXTBOX_PRINT(" \n %s \n", __FUNCTION__);

    g_free (priv->font_type);
    g_free (priv->text_color);
    g_free (priv->bg_color);

    G_OBJECT_CLASS(lightwood_text_box_parent_class)->finalize( pObject );
}

/********************************************************
 * Function : button_released_event_cb
 * Description: callback function,invoke at button release
 * Parameters: ClutterText,ClutterEvent,userData
 * Return value: gboolean
 ********************************************************/
static gboolean button_released_event_cb(ClutterText *text, ClutterEvent *event, gpointer userData)
{
    LightwoodTextBox *entry = userData;
    LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (entry);
    LIGHTWOOD_TEXTBOX_PRINT(" \n %s \n", __FUNCTION__);

	priv->textSelected = NULL;
	priv->bound = -1;

	priv->textSelected = clutter_text_get_selection(CLUTTER_TEXT(text));
	if(priv->textSelected && g_strcmp0(priv->textSelected, "") != 0)
	{
		priv->bound = clutter_text_get_selection_bound(CLUTTER_TEXT(text));
	}
	g_signal_emit(CLUTTER_ACTOR(userData), uin_text_box_signals[LIGHTWOOD_SIG_TEXT_BOX_ENTRY_PRESS], 0, NULL);
	return FALSE ;
}

/********************************************************
 * Function : button_press_event_cb
 * Description: callback function,invoke at button press
 * Parameters: ClutterActor,ClutterEvent,userData
 * Return value: gboolean
 ********************************************************/
static gboolean button_press_event_cb(ClutterActor *pActor, ClutterEvent *event, gpointer userData)
{
	LIGHTWOOD_TEXTBOX_PRINT( " \n %s \n", __FUNCTION__ );
	return FALSE ;
}

/********************************************************
 * Function : single_line_captured_event_cb
 * Description: callback function,invoke at touch event
 * Parameters: ClutterActor,ClutterEvent,userData
 * Return value: gboolean
 ********************************************************/
static gboolean single_line_captured_event_cb(ClutterActor *actor, ClutterEvent *event, gpointer userData)
{
	LIGHTWOOD_TEXTBOX_PRINT( " \n  %s  %d \n", __FUNCTION__,event->type );
  switch (event->type)
  {
    case CLUTTER_BUTTON_PRESS:
    case CLUTTER_TOUCH_BEGIN:
    {
      return button_press_event_cb(actor, event,userData);;
      break;
    }
    case CLUTTER_BUTTON_RELEASE:
    case CLUTTER_TOUCH_END:
    {
      return button_released_event_cb (CLUTTER_TEXT(actor), event,userData);
      break;
    }
    case CLUTTER_NOTHING:
    case CLUTTER_KEY_PRESS:
    case CLUTTER_KEY_RELEASE:
    case CLUTTER_MOTION:
    case CLUTTER_ENTER:
    case CLUTTER_LEAVE:
    case CLUTTER_SCROLL:
    case CLUTTER_STAGE_STATE:
    case CLUTTER_DESTROY_NOTIFY:
    case CLUTTER_CLIENT_MESSAGE:
    case CLUTTER_DELETE:
    case CLUTTER_TOUCH_UPDATE:
    case CLUTTER_TOUCH_CANCEL:
    case CLUTTER_EVENT_LAST:
    default:
    {
      return FALSE;
      break;
    }
  }
}

/********************************************************
 * Function : captured_event_cb
 * Description: callback function,invoke at touch event
 * Parameters: ClutterActor,ClutterEvent,userData
 * Return value: gboolean
 ********************************************************/
static gboolean captured_event_cb (ClutterActor *actor, ClutterEvent *event, gpointer userData)
{
  switch (event->type)
  {
    case CLUTTER_BUTTON_PRESS:
    case CLUTTER_TOUCH_BEGIN:
    {
      return FALSE;
      break;
    }
    case CLUTTER_BUTTON_RELEASE:
    case CLUTTER_TOUCH_END:
    {
      return text_pressed_cb (actor, event,userData);
      break;
    }
    case CLUTTER_NOTHING:
    case CLUTTER_KEY_PRESS:
    case CLUTTER_KEY_RELEASE:
    case CLUTTER_MOTION:
    case CLUTTER_ENTER:
    case CLUTTER_LEAVE:
    case CLUTTER_SCROLL:
    case CLUTTER_STAGE_STATE:
    case CLUTTER_DESTROY_NOTIFY:
    case CLUTTER_CLIENT_MESSAGE:
    case CLUTTER_DELETE:
    case CLUTTER_TOUCH_UPDATE:
    case CLUTTER_TOUCH_CANCEL:
    case CLUTTER_EVENT_LAST:
    default:
    {
      return FALSE;
      break;
    }
  }
}

/********************************************************
 * Function : lightwood_text_box_constructed
 * Description: Get construct by g_object
 * Parameters: object
 * Return value: void
 ********************************************************/
static void lightwood_text_box_constructed( GObject *pObject )
{
    LightwoodTextBox *pLightwoodTextBox = LIGHTWOOD_TEXT_BOX(pObject);
    LightwoodTextBoxPrivate *priv = TEXT_BOX_PRIVATE (pLightwoodTextBox);
    /* The black background */
    ClutterColor backgroundcolor = {0x00,0x00,0x00,0x33};
    ClutterColor light_blue      = {0x20,0x3E,0x60,0x80};
    ClutterColor messageColor    = {0x98,0xA9,0xB2,0xFF};

    LIGHTWOOD_TEXTBOX_PRINT( " \n %s \n", __FUNCTION__ );

    priv->pTextGroup = clutter_actor_new();
    clutter_actor_add_child( CLUTTER_ACTOR( pLightwoodTextBox ), priv->pTextGroup);

    //priv->pEntryBox = clutter_rectangle_new_with_color(&backgroundcolor);
    priv->pEntryBox = clutter_actor_new();
    clutter_actor_set_background_color ( priv->pEntryBox,&backgroundcolor);
    clutter_actor_add_child( CLUTTER_ACTOR( priv->pTextGroup ), priv->pEntryBox);

    priv->pTextEntry = clutter_text_new();
    clutter_actor_add_child(CLUTTER_ACTOR(priv->pTextGroup),priv->pTextEntry);
    g_object_set( priv->pTextEntry,"editable",TRUE,"single-line-mode",TRUE,"reactive",TRUE,"selection-color",&light_blue,NULL);
    clutter_text_set_color(CLUTTER_TEXT(priv->pTextEntry), &messageColor);
    clutter_actor_set_position(CLUTTER_ACTOR(priv->pTextEntry),2,16);
    clutter_text_set_activatable (CLUTTER_TEXT(priv->pTextEntry), TRUE);
    clutter_text_set_selection (CLUTTER_TEXT ( priv->pTextEntry),0,0);
    g_signal_connect(priv->pTextEntry, "key-release-event", G_CALLBACK (key_release_cb), pLightwoodTextBox);

    g_signal_connect (priv->pTextEntry, "button-press-event", G_CALLBACK(button_press_event_cb),pLightwoodTextBox);
    g_signal_connect (priv->pTextEntry, "button-release-event", G_CALLBACK(button_released_event_cb),pLightwoodTextBox);
    g_signal_connect (priv->pTextEntry, "touch-event", G_CALLBACK(single_line_captured_event_cb),pLightwoodTextBox);

    //clutter_text_set_cursor_visible(CLUTTER_TEXT(priv->pTextEntry), TRUE);
    //clutter_actor_grab_key_focus(CLUTTER_ACTOR(priv->pTextEntry));

    priv->multiLineEntry = (LightwoodMultilineEntry *)lightwood_multiline_entry_new();
    clutter_actor_add_child(priv->pTextGroup,CLUTTER_ACTOR(priv->multiLineEntry));
    clutter_actor_hide(CLUTTER_ACTOR(priv->multiLineEntry));
    lightwood_multiline_entry_set_color(priv->multiLineEntry, &messageColor);

    g_signal_connect(lightwood_multiline_get_active_text_actor(priv->multiLineEntry), "cursor-event",		 G_CALLBACK (cursor_event_cb),  pLightwoodTextBox);
    g_signal_connect(lightwood_multiline_get_active_text_actor(priv->multiLineEntry), "key-focus-in", 		 G_CALLBACK (key_focus_cb),     pLightwoodTextBox);
    g_signal_connect(lightwood_multiline_get_active_text_actor(priv->multiLineEntry), "key-focus-out", 	 	 G_CALLBACK (key_focus_cb),     pLightwoodTextBox);
    g_signal_connect(lightwood_multiline_get_active_text_actor(priv->multiLineEntry), "delete-text",    	 G_CALLBACK (text_deleted_cb),  pLightwoodTextBox);
    g_signal_connect(lightwood_multiline_get_active_text_actor(priv->multiLineEntry), "key-press-event",	 G_CALLBACK (text_entered_cb),  pLightwoodTextBox);
    g_signal_connect(lightwood_multiline_get_active_text_actor(priv->multiLineEntry), "button-release-event",G_CALLBACK (text_pressed_cb),  pLightwoodTextBox);
    g_signal_connect(lightwood_multiline_get_active_text_actor(priv->multiLineEntry), "touch-event",         G_CALLBACK (captured_event_cb),pLightwoodTextBox);
    g_signal_connect(priv->multiLineEntry, "line-count-changed" , G_CALLBACK (linecount_changed),pLightwoodTextBox);
}


/********************************************************
 * Function   : lightwood_text_box_class_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters : The object's class reference
 * Return value: void
 ********************************************************/
static void lightwood_text_box_class_init( LightwoodTextBoxClass *pKlass )
{
    GObjectClass *pObjectClass = G_OBJECT_CLASS( pKlass );
    GParamSpec *pPspec = NULL;

    LIGHTWOOD_TEXTBOX_PRINT(" \n %s \n", __FUNCTION__);

    pObjectClass->constructed  = lightwood_text_box_constructed;
    pObjectClass->get_property = lightwood_text_box_get_property;
    pObjectClass->set_property = lightwood_text_box_set_property;
    pObjectClass->dispose      = lightwood_text_box_dispose;
    pObjectClass->finalize     = lightwood_text_box_finalize;

    //ClutterActorClass *actor_class = CLUTTER_ACTOR_CLASS (pKlass);
    //actor_class->key_release_event = lightwood_entry_button_release_event;

	/**
     * LightwoodTextBox:multi-line:
     *
     * to set multi-line functionality to textbox enable/disable
     */
    pPspec = g_param_spec_boolean( "multi-line",
                                   "multi-line",
                                   "multi-line for the textbox",
                                    FALSE,
                                    G_PARAM_READWRITE );
    g_object_class_install_property( pObjectClass, LIGHTWOOD_TEXTBOX_PROP_ENUM_MULTI_LINE, pPspec );

	/**
     * LightwoodTextBox:focus-cursor:
     *
     * to enable/disable focus-cursor for active entry
     */
    pPspec = g_param_spec_boolean( "focus-cursor",
                                   "focus-cursor",
                                   "focus-cursor for the textbox",
                                    FALSE,
                                    G_PARAM_READWRITE );
    g_object_class_install_property( pObjectClass, LIGHTWOOD_TEXTBOX_PROP_ENUM_FOCUS_CURSOR, pPspec );


	/**
     * LightwoodTextBox:max-lines:
     *
     * to set max-lines for entry
     */
    pPspec = g_param_spec_int( "max-lines",
                                 "max-lines",
                                 "max-lines in the textbox",
                                  -1, G_MAXINT, -1,
                                  G_PARAM_READWRITE );
    g_object_class_install_property( pObjectClass, LIGHTWOOD_TEXTBOX_PROP_ENUM_MAX_LINES, pPspec );

	/**
     * LightwoodTextBox:displayed-lines:
     *
     * to set displayed-lines/visible line in entry
     */
    pPspec = g_param_spec_uint ("displayed-lines",
                                "Displayed lines",
                                "count of displayed lines",
                                 0, G_MAXUINT, 0,
                                 G_PARAM_READWRITE );
   g_object_class_install_property (pObjectClass, LIGHTWOOD_TEXTBOX_PROP_ENUM_DISPLAYED_LINE, pPspec);

	/**
     * LightwoodTextBox:text-editable:
     *
     * to set text-editable TRUE/FALSE
     */
    pPspec = g_param_spec_boolean( "text-editable",
                                   "text-editable",
                                   "text-editable for the textbox",
                                    TRUE,
                                    G_PARAM_READWRITE);
    g_object_class_install_property( pObjectClass, LIGHTWOOD_TEXTBOX_PROP_ENUM_EDITABLE, pPspec );

	/**
     * LightwoodTextBox:password-enable:
     *
     * to set password-enable TRUE/FALSE
     */
    pPspec = g_param_spec_boolean( "password-enable",
                                   "password-enable",
                                   "password-enable for the textbox",
                                    FALSE,
                                    G_PARAM_READWRITE);
    g_object_class_install_property( pObjectClass, LIGHTWOOD_TEXTBOX_PROP_ENUM_PASSWORD, pPspec );

	/**
     * LightwoodTextBox:font-type:
     *
     * to set font-type to specified
     */
    pPspec = g_param_spec_string( "font-type",
                                  "font-type",
                                  "font-type",
                                   NULL,
                                   G_PARAM_READWRITE );
    g_object_class_install_property( pObjectClass, LIGHTWOOD_TEXTBOX_PROP_ENUM_FONT_TYPE, pPspec );


	/**
     * LightwoodTextBox:text:
     *
     * to set text to specified
     */
    pPspec = g_param_spec_string( "text",
                                  "text",
				"text to be displayed on text box",
                                   NULL,
                                   G_PARAM_READWRITE );
    g_object_class_install_property( pObjectClass, LIGHTWOOD_TEXTBOX_PROP_ENUM_TEXT, pPspec );

	/**
     * LightwoodTextBox:text-color:
     *
     * to set text-color to specified
     */
    pPspec = g_param_spec_string( "text-color",
                                  "text-color",
                                  "text color",
                                   NULL,
                                   G_PARAM_READWRITE );
	g_object_class_install_property( pObjectClass, LIGHTWOOD_TEXTBOX_PROP_ENUM_TEXT_COLOR, pPspec );

	 pPspec = g_param_spec_string( "background-color",
	                               "background-color",
	                               "background-color for text box",
	                                   NULL,
	                                   G_PARAM_READWRITE );
		g_object_class_install_property( pObjectClass, LIGHTWOOD_TEXTBOX_PROP_ENUM_BACKGROUND_COLOR, pPspec );


		pPspec = g_param_spec_float( "text-x",
			                          "text-x",
			                          "cursor x position ",
			                          0.0, G_MAXFLOAT, 0.0,
			                          G_PARAM_READWRITE );
	   g_object_class_install_property( pObjectClass, LIGHTWOOD_TEXTBOX_PROP_ENUM_TEXT_X, pPspec );

	   pPspec = g_param_spec_float( "text-y",
							         "text-y",
							         "Cursor y position ",
							         0.0, G_MAXFLOAT, 0.0,
							          G_PARAM_READWRITE );
	   g_object_class_install_property( pObjectClass, LIGHTWOOD_TEXTBOX_PROP_ENUM_TEXT_Y, pPspec );

     /**
     * LightwoodTextBox::entry-press:
     * @LightwoodTextBox: The object which received the signal
     *
     * ::entry-press is emitted when entry is pressed
     */
    uin_text_box_signals[LIGHTWOOD_SIG_TEXT_BOX_ENTRY_PRESS] = g_signal_new ("entry-press",
            G_TYPE_FROM_CLASS (pObjectClass),
            G_SIGNAL_NO_RECURSE |  G_SIGNAL_RUN_LAST,
            0,
            NULL, NULL,
            g_cclosure_marshal_VOID__VOID,
            G_TYPE_NONE, 0,NULL);

     /**
     * LightwoodTextBox::keypress-event:
     * @LightwoodTextBox: The object which received the signal
     * @key: the ID of the key pressed
     *
     * ::keypress-event is emitted when key is pressed
     */
    uin_text_box_signals[LIGHTWOOD_SIG_TEXT_BOX_KEY_PRESS_EVENT] = g_signal_new ("keypress-event",
            G_TYPE_FROM_CLASS (pObjectClass),
            G_SIGNAL_NO_RECURSE |  G_SIGNAL_RUN_LAST,
            0,
            NULL, NULL,
            g_cclosure_marshal_VOID__INT,
            G_TYPE_NONE, 1,G_TYPE_INT);
}
