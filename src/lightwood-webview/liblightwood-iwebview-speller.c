/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION:liblightwood-iwebview-speller
 * @Title:LightwoodIWebviewSpeller
 * @short_description: Web View speller interface
 * @see_also: #liblightwoodmodel #clutteractor
 *
 * #LightwoodIWebviewSpeller is a interface class which will provide 
 * show,hide,is_shown virtual methods, which needs to implemented 
 * from the variant.
 *
 * ## freeing the widget
 *      call g_object_unref() to free the widget.
 *
 * ## sample c code
 * |[<!-- language="c" -->
 * //write a lightwood_webview_speller 
 static void
lightwood_webview_speller_iface_init (LightwoodWebViewSpellerInterface *iface)
{
    iface->show = _lightwood_webview_speller_show;
    iface->hide = _lightwood_webview_speller_hide;
    iface->is_shown = _lightwood_webview_speller_is_shown;
}
 ]|
 *
 */
#include "liblightwood-iwebview-speller.h"

enum
{
  LIGHTWOOD_WEBVIEW_SPELLER_VISIBLE,
  LIGHTWOOD_WEBVIEW_SPELLER_DATA_ENETERD,
  LIGHTWOOD_WEBVIEW_SPELLER_LAST_SIGNAL
};

static guint signals[LIGHTWOOD_WEBVIEW_SPELLER_LAST_SIGNAL] = { 0, };

G_DEFINE_INTERFACE(LightwoodWebViewSpeller, lightwood_webview_speller, 0)

static void lightwood_webview_speller_default_init(LightwoodWebViewSpellerInterface* iface)
{
    iface->show = NULL;
    iface->hide = NULL;
    iface->is_shown = NULL;

    signals[LIGHTWOOD_WEBVIEW_SPELLER_VISIBLE] =
        g_signal_new ("webview_speller_visible",
                      G_TYPE_FROM_INTERFACE (iface),
                      G_SIGNAL_RUN_LAST,
                      0,
                      NULL, NULL,
                      g_cclosure_marshal_VOID__BOOLEAN,
                      G_TYPE_NONE, 1, G_TYPE_BOOLEAN);
    signals[LIGHTWOOD_WEBVIEW_SPELLER_DATA_ENETERD] =
        g_signal_new ("webview_speller_data_entered",
                      G_TYPE_FROM_INTERFACE (iface),
                      G_SIGNAL_RUN_LAST,
                      0,
                      NULL, NULL,
                      NULL,
                      G_TYPE_STRING, 2, G_TYPE_STRING,G_TYPE_STRING);
}

/**
 * lightwood_webview_speller_show:
 * @self :The  reference to #LightwoodWebViewSpeller
 * @prompt : the promt
 * @default_text : default text to display.
 *
 * This Function will invoke show speller interface.
 */

void lightwood_webview_speller_show (LightwoodWebViewSpeller* self,const gchar* prompt,const gchar* default_text)
{
	LIGHTWOOD_WEBVIEW_SPELLER_GET_IFACE (self)->show (self, prompt, default_text);
}
/**
 * lightwood_webview_speller_hide:
 * @self :The  reference to #LightwoodWebViewSpeller
 *
 * This Function will  invoke hide speller interface
 * Returns: %TRUE/%FALSE
 */


gboolean lightwood_webview_speller_hide        (LightwoodWebViewSpeller* self)
{
	return LIGHTWOOD_WEBVIEW_SPELLER_GET_IFACE (self)->hide (self);
}
/**
 * lightwood_webview_speller_is_shown:
 * @self :The  reference to #LightwoodWebViewSpeller
 *
 * This Function will give status of speller
 * Returns: if %TRUEi shown/%FALSE hidden
 */


gboolean lightwood_webview_speller_is_shown    (LightwoodWebViewSpeller* self)
{
	return LIGHTWOOD_WEBVIEW_SPELLER_GET_IFACE (self)->is_shown (self);
}


