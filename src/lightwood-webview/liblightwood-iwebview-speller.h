/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* liblightwood-iwebview-speller.h */

#ifndef _liblightwoodwebviewspeller_h
#define _liblightwoodwebviewspeller_h

#include <glib-object.h>
#include <clutter/clutter.h>

G_BEGIN_DECLS

#define LIGHTWOOD_TYPE_WEBVIEW_SPELLER lightwood_webview_speller_get_type ()
G_DECLARE_INTERFACE (LightwoodWebViewSpeller, lightwood_webview_speller, LIGHTWOOD, WEBVIEW_SPELLER, GObject)

struct _LightwoodWebViewSpellerInterface {
    GTypeInterface parent;

    void (*show) (LightwoodWebViewSpeller* self,const gchar* prompt,const gchar* default_text);
    gboolean (*hide)        (LightwoodWebViewSpeller* self);
    gboolean (*is_shown)    (LightwoodWebViewSpeller* self);
    void (*attach) (LightwoodWebViewSpeller* self,ClutterActor *actor);

    /* Padding for future expansion */
    void (*_lightwood_reserved0) (void);
    void (*_lightwood_reserved1) (void);

};

void lightwood_webview_speller_show        (LightwoodWebViewSpeller* self,const gchar* prompt,const gchar* default_text);
gboolean lightwood_webview_speller_hide        (LightwoodWebViewSpeller* self);
gboolean lightwood_webview_speller_is_shown    (LightwoodWebViewSpeller* self);

G_END_DECLS

#endif
