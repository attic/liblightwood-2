/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* liblightwood-webview_assistant.h */

#ifndef _LIGHTWOOD_WEBVIEW_ASSISTANT_H
#define _LIGHTWOOD_WEBVIEW_ASSISTANT_H

#include <glib-object.h>
#include <clutter/clutter.h>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-prototypes"
#include <webkit/webkitiwebview.h>
#pragma GCC diagnostic pop
#include <webkit/mx-web-view.h>
#include <webkitdom/webkitdom.h>
#include <webkit/webkitirendertheme.h>

#include "liblightwood-iwebview-speller.h"
/**
 * SECTION:lightwood_webview_assistant
 * @Title:LightwoodWebviewAssistant
 * @short_description: Assistant to create webview widget.
 */
 
typedef struct _LightwoodWebviewAssistant LightwoodWebviewAssistant;
/**
 * LightwoodWebviewAssistant:
 * @popupmenu_factory: pointer to popup_menu creation
 * @alert_handler_factory:pointer to alert handler creation
 * @speller_factory:  Ponter to speller creation
 * @m_renderTheme:  Pointer to theme creation
 * @m_soupAuth_handler:  Pointer to soup authentication handler creation
 *
 * The structure will have all the widgets references for creation of the webview widget.
 */

struct _LightwoodWebviewAssistant
{
 /* pointer to popup_menu creation,which includes customized roller population.*/
	WebKitIPopupMenu* (*popupmenu_factory) (gfloat width,gfloat height);
 /* pointer to alert handler creation,which includes alert popups */
	GObject* (*alert_handler_factory) (ClutterActor *parent, const gchar* app_name, ClutterActor *web_view);
 /* pointer to speller creation */
	LightwoodWebViewSpeller* (*speller_factory) (gboolean is_prompt,gboolean is_authenticate);
 /* pointer to theme for the widget */
	WebKitIRenderTheme* m_renderTheme;
 /* pointer to soup authentication handler */
	SoupSessionFeature* m_soupAuth_handler;

};




#endif /* _LIGHTWOOD_WEBVIEW_ASSISTANT_H */
