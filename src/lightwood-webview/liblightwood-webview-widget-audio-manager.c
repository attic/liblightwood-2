/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* liblightwood-web-view-widget-audio-manager.c */

#include "liblightwood-webview-widget-audio-manager.h"
#include "liblightwood-webview.h"
#include "liblightwood-webview-widget.h"

G_DEFINE_TYPE (LightwoodWebViewWidgetAudioManager, lightwood_webview_widget_audio_manager, G_TYPE_OBJECT)

enum
{
  PROP_0,

  PROP_AUDIO_STATUS,

  PROP_LAST,
};
GParamSpec* properties[PROP_LAST];

enum
{
  AUDIO_REQUESTED,
  AUDIO_STATUS_CHANGE,
  LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0, };


#define WEBVIEW_WIDGET_AUDIO_MANAGER_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), LIGHTWOOD_TYPE_WEBVIEW_WIDGET_AUDIO_MANAGER, LightwoodWebViewWidgetAudioManagerPrivate))

struct _LightwoodWebViewWidgetAudioManagerPrivate
{
	WebKitDOMHTMLMediaElement* m_mediaElement;
	gboolean m_is_audio_progress;
/*	gboolean m_is_audio_requested;
	gboolean m_audio_resource_given;*/

	LightwoodWebViewAudioRequest m_audio_request;
	LightwoodWebViewAudioStatus  m_audio_status;
};

static void
_lightwood_webview_widget_audio_manager_set_audio_progress (LightwoodWebViewWidgetAudioManager *self,LightwoodWebViewAudioStatus is_playing);
static void
_lightwood_webview_widget_audio_manager_refresh_audio_status (LightwoodWebViewWidgetAudioManager *self);

static void
lightwood_webview_widget_audio_manager_get_property (GObject    *object,
                                                guint       property_id,
                                                GValue     *value,
                                                GParamSpec *pspec)
{
	LightwoodWebViewWidgetAudioManager *self = LIGHTWOOD_WEBVIEW_WIDGET_AUDIO_MANAGER(object);
  switch (property_id)
    {

		case PROP_AUDIO_STATUS:
		{
			g_value_set_int(value,self->priv->m_audio_status);
		}
		  break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
lightwood_webview_widget_audio_manager_set_property (GObject      *object,
                                                guint         property_id,
                                                const GValue *value,
                                                GParamSpec   *pspec)
{
	LightwoodWebViewWidgetAudioManager *self = LIGHTWOOD_WEBVIEW_WIDGET_AUDIO_MANAGER(object);
  switch (property_id)
    {

		case PROP_AUDIO_STATUS:
		{
			_lightwood_webview_widget_audio_manager_set_audio_progress(self,g_value_get_int(value));
		}
		  break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
lightwood_webview_widget_audio_manager_dispose (GObject *object)
{
	LightwoodWebViewWidgetAudioManager *self = LIGHTWOOD_WEBVIEW_WIDGET_AUDIO_MANAGER(object);
	if(self->priv->m_mediaElement && self->priv->m_audio_status != LIGHTWOOD_WEBVIEW_AUDIO_STATUS_ENDED)
	{
		//_lightwood_webview_widget_audio_manager_set_audio_progress(self,FALSE);
		lightwood_webview_widget_audio_manager_stop_audio(self);
	}
	self->priv->m_mediaElement = NULL;

  G_OBJECT_CLASS (lightwood_webview_widget_audio_manager_parent_class)->dispose (object);
}

static void
lightwood_webview_widget_audio_manager_class_init (LightwoodWebViewWidgetAudioManagerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  g_type_class_add_private (klass, sizeof (LightwoodWebViewWidgetAudioManagerPrivate));

  object_class->get_property = lightwood_webview_widget_audio_manager_get_property;
  object_class->set_property = lightwood_webview_widget_audio_manager_set_property;
  object_class->dispose = lightwood_webview_widget_audio_manager_dispose;


	/**
	 * LightwoodWebViewWidgetAudioManager:audio_status:
	 *
	 * whether the form assistance should be enabled.
	 * if set to FALSE the virtual keyboard will not be shown for focus on text entry elements.
	 **/
  properties[PROP_AUDIO_STATUS] = g_param_spec_int( "audio_status",
								"audio_progress",
								"audio_progress",
								0,
								LIGHTWOOD_WEBVIEW_AUDIO_STATUS_PAUSED,
								LIGHTWOOD_WEBVIEW_AUDIO_STATUS_ENDED,
								G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_AUDIO_STATUS, properties[PROP_AUDIO_STATUS]);


  signals[AUDIO_STATUS_CHANGE] =
     g_signal_new ("audio_mgr_status_change",
                   G_TYPE_FROM_CLASS (klass),
                   G_SIGNAL_RUN_LAST,
                   0,
                   NULL, NULL,
                   g_cclosure_marshal_VOID__VOID,
                   G_TYPE_NONE,0);

/*  signals[AUDIO_REQUESTED] =
    g_signal_new ("audio_requested",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL,
                  g_cclosure_marshal_VOID__VOID,
                  G_TYPE_NONE, 0);*/


}

static void
lightwood_webview_widget_audio_manager_init (LightwoodWebViewWidgetAudioManager *self)
{
  self->priv = WEBVIEW_WIDGET_AUDIO_MANAGER_PRIVATE (self);
  self->priv->m_mediaElement = NULL;
  self->priv->m_audio_status =LIGHTWOOD_WEBVIEW_AUDIO_STATUS_ENDED;
  self->priv->m_audio_request =LIGHTWOOD_WEBVIEW_AUDIO_REQUEST_NONE;
}

LightwoodWebViewWidgetAudioManager *
lightwood_webview_widget_audio_manager_new (void)
{
  return g_object_new (LIGHTWOOD_TYPE_WEBVIEW_WIDGET_AUDIO_MANAGER, NULL);
}



static void
_lightwood_webview_widget_audio_manager_set_audio_progress(LightwoodWebViewWidgetAudioManager *self,LightwoodWebViewAudioStatus state)
{
	LightwoodWebViewWidgetAudioManagerPrivate* priv = WEBVIEW_WIDGET_AUDIO_MANAGER_PRIVATE(self);
	if(priv->m_audio_status != state)
	{
		priv->m_audio_status = state;
		g_print("\n !!!!!!!!!received media state change notification %d......",priv->m_audio_status);
		//g_object_notify_by_pspec(G_OBJECT(self),properties[PROP_AUDIO_PROGRESS]);
		//g_object_notify_by_pspec(G_OBJECT(self),properties[PROP_AUDIO_STATUS]);
		g_signal_emit_by_name(G_OBJECT(self),"audio_mgr_status_change");
	}
}

/*static gboolean lightwood_webview_widget_audio_manager_pause_audio_cb (LightwoodWebViewWidgetAudioManager *self)
{
	g_print("\n pause audio..");
	LightwoodWebViewWidgetAudioManagerPrivate* priv = WEBVIEW_WIDGET_AUDIO_MANAGER_PRIVATE(self);
	if(priv->m_mediaElement)
		webkit_dom_html_media_element_pause(priv->m_mediaElement);
	return FALSE;
}*/

static void
_lightwood_webview_widget_audio_manager_audio_state_change_cb(WebKitDOMHTMLMediaElement* node,WebKitDOMEvent* event,LightwoodWebViewWidgetAudioManager *self)
{
//	LightwoodWebViewWidgetAudioManagerPrivate* priv = WEBVIEW_WIDGET_AUDIO_MANAGER_PRIVATE(self);
//	gboolean is_playing = !(webkit_dom_html_media_element_get_paused(node) || webkit_dom_html_media_element_get_ended(node));
/*
	if(is_playing)
	{
		//its a play back request
		if(!priv->m_audio_resource_given)
		{
			g_timeout_add(0,lightwood_webview_widget_audio_manager_pause_audio_cb,self);
			if(!priv->m_is_audio_requested)
				g_signal_emit(self,signals[AUDIO_REQUESTED],0);
			priv->m_is_audio_requested = TRUE;
		}
		else //play back state change
		{

		}

	}

	if(priv->m_audio_resource_given)
*/

	_lightwood_webview_widget_audio_manager_refresh_audio_status(self);
	self->priv->m_audio_request =LIGHTWOOD_WEBVIEW_AUDIO_REQUEST_NONE;

}

gboolean lightwood_webview_widget_audio_manager_initialize (LightwoodWebViewWidgetAudioManager *self,WebKitWebPage* webPage)
{

	LightwoodWebViewWidgetAudioManagerPrivate* priv = WEBVIEW_WIDGET_AUDIO_MANAGER_PRIVATE(self);

	g_print("\n Webpage Audio Management ENabed!!!!!");
	WebKitDOMHTMLMediaElement* old_element = priv->m_mediaElement;
	priv->m_mediaElement = NULL;
	priv->m_audio_request =LIGHTWOOD_WEBVIEW_AUDIO_REQUEST_NONE;
	gboolean media_element_found = FALSE;
	WebKitDOMDocument* doc = webkit_web_page_get_dom_document(webPage);
	WebKitDOMNode* root_element = (WebKitDOMNode*)webkit_dom_document_get_document_element(doc);
	WebKitDOMXPathNSResolver* ns_resolver = webkit_dom_document_create_ns_resolver(doc,root_element);
	WebKitDOMXPathResult* xpath_result = webkit_dom_document_evaluate(doc,"/html/body//audio",root_element,ns_resolver,0,NULL,NULL);
	if(xpath_result != NULL)
	{
		priv->m_mediaElement = (WebKitDOMHTMLMediaElement*)webkit_dom_xpath_result_iterate_next(xpath_result,NULL);
		media_element_found = ((priv->m_mediaElement != NULL) && WEBKIT_DOM_IS_HTML_MEDIA_ELEMENT(priv->m_mediaElement));
	}
	if(!media_element_found)
	{
		xpath_result = webkit_dom_document_evaluate(doc,"/html/body//video",root_element,ns_resolver,0,NULL,NULL);
		if(xpath_result != NULL)
		{
			priv->m_mediaElement = (WebKitDOMHTMLMediaElement*)webkit_dom_xpath_result_iterate_next(xpath_result,NULL);
			media_element_found  = ((priv->m_mediaElement != NULL) && WEBKIT_DOM_IS_HTML_MEDIA_ELEMENT(priv->m_mediaElement));
		}
	}

	if(media_element_found)
	{
		if( priv->m_mediaElement != old_element)
		{
			if(old_element)
				_lightwood_webview_widget_audio_manager_set_audio_progress(self,LIGHTWOOD_WEBVIEW_AUDIO_STATUS_ENDED);

			webkit_dom_event_target_add_event_listener(WEBKIT_DOM_EVENT_TARGET(priv->m_mediaElement),"pause",G_CALLBACK(_lightwood_webview_widget_audio_manager_audio_state_change_cb),FALSE,self);
			webkit_dom_event_target_add_event_listener(WEBKIT_DOM_EVENT_TARGET(priv->m_mediaElement),"play",G_CALLBACK(_lightwood_webview_widget_audio_manager_audio_state_change_cb),FALSE,self);
			webkit_dom_event_target_add_event_listener(WEBKIT_DOM_EVENT_TARGET(priv->m_mediaElement),"ended",G_CALLBACK(_lightwood_webview_widget_audio_manager_audio_state_change_cb),FALSE,self);

		}
		if(!(webkit_dom_html_media_element_get_paused(priv->m_mediaElement) || webkit_dom_html_media_element_get_ended(priv->m_mediaElement)))
			_lightwood_webview_widget_audio_manager_refresh_audio_status(self);
	}
	else if(old_element)
	{
		_lightwood_webview_widget_audio_manager_set_audio_progress(self,LIGHTWOOD_WEBVIEW_AUDIO_STATUS_ENDED);
		priv->m_mediaElement = NULL;
	}

	return media_element_found;
}

void lightwood_webview_widget_audio_manager_play_audio (LightwoodWebViewWidgetAudioManager *self)
{
	LightwoodWebViewWidgetAudioManagerPrivate* priv = WEBVIEW_WIDGET_AUDIO_MANAGER_PRIVATE(self);
	g_print("\n resuming audio....");

	if(priv->m_mediaElement && (priv->m_audio_status != LIGHTWOOD_WEBVIEW_AUDIO_STATUS_PLAYING))
	{
		priv->m_audio_request =LIGHTWOOD_WEBVIEW_AUDIO_REQUEST_PLAY;
		webkit_dom_html_media_element_play(priv->m_mediaElement);
	}

}

void lightwood_webview_widget_audio_manager_pause_audio (LightwoodWebViewWidgetAudioManager *self)
{
	LightwoodWebViewWidgetAudioManagerPrivate* priv = WEBVIEW_WIDGET_AUDIO_MANAGER_PRIVATE(self);
	_lightwood_webview_widget_audio_manager_refresh_audio_status(self);
	if(priv->m_mediaElement && (priv->m_audio_status == LIGHTWOOD_WEBVIEW_AUDIO_STATUS_PLAYING))
	{
		priv->m_audio_request =LIGHTWOOD_WEBVIEW_AUDIO_REQUEST_PAUSE;
		webkit_dom_html_media_element_pause(priv->m_mediaElement);
	}

}

void lightwood_webview_widget_audio_manager_stop_audio (LightwoodWebViewWidgetAudioManager *self)
{
	LightwoodWebViewWidgetAudioManagerPrivate* priv = WEBVIEW_WIDGET_AUDIO_MANAGER_PRIVATE(self);
	g_print("\n stop audio api called....");
	_lightwood_webview_widget_audio_manager_refresh_audio_status(self);
	if(priv->m_mediaElement && (priv->m_audio_status != LIGHTWOOD_WEBVIEW_AUDIO_STATUS_ENDED))
	{
		priv->m_audio_request = LIGHTWOOD_WEBVIEW_AUDIO_REQUEST_STOP;
		if(priv->m_audio_status == LIGHTWOOD_WEBVIEW_AUDIO_STATUS_PAUSED)
			_lightwood_webview_widget_audio_manager_set_audio_progress(self,LIGHTWOOD_WEBVIEW_AUDIO_STATUS_ENDED);
		else
		{
			g_print("\n stopping audio....");
			webkit_dom_html_media_element_pause(priv->m_mediaElement);
		}
	}

}

static void
_lightwood_webview_widget_audio_manager_refresh_audio_status (LightwoodWebViewWidgetAudioManager *self)
{
	WebKitDOMHTMLMediaElement* node = self->priv->m_mediaElement;
	if(!node) return;

	g_print("\n _lightwood_webview_widget_audio_manager_refresh_audio_status....");
	if(!(webkit_dom_html_media_element_get_paused(node) || webkit_dom_html_media_element_get_ended(node)))
	{
		g_print("--playing\n");
		_lightwood_webview_widget_audio_manager_set_audio_progress(self,LIGHTWOOD_WEBVIEW_AUDIO_STATUS_PLAYING);
	}
	else
	{
		if(webkit_dom_html_media_element_get_ended(node))
		{
			g_print("--ended\n");
			_lightwood_webview_widget_audio_manager_set_audio_progress(self,LIGHTWOOD_WEBVIEW_AUDIO_STATUS_ENDED);
		}
		else
		{

			if(self->priv->m_audio_request == LIGHTWOOD_WEBVIEW_AUDIO_REQUEST_PAUSE)
			{
				g_print("--paused\n");
				_lightwood_webview_widget_audio_manager_set_audio_progress(self,LIGHTWOOD_WEBVIEW_AUDIO_STATUS_PAUSED);
			}

			else
			{
				g_print("--ended\n");
				_lightwood_webview_widget_audio_manager_set_audio_progress(self,LIGHTWOOD_WEBVIEW_AUDIO_STATUS_ENDED);
			}
		}
	}
}
