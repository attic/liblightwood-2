/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* liblightwood-web-view-widget-audio-manager.h */

#ifndef _LIGHTWOOD_WEBVIEW_WIDGET_AUDIO_MANAGER_H
#define _LIGHTWOOD_WEBVIEW_WIDGET_AUDIO_MANAGER_H

#include <glib-object.h>
#include <webkit/webkitwebpage.h>

G_BEGIN_DECLS

#define LIGHTWOOD_TYPE_WEBVIEW_WIDGET_AUDIO_MANAGER lightwood_webview_widget_audio_manager_get_type()

#define LIGHTWOOD_WEBVIEW_WIDGET_AUDIO_MANAGER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  LIGHTWOOD_TYPE_WEBVIEW_WIDGET_AUDIO_MANAGER, LightwoodWebViewWidgetAudioManager))

#define LIGHTWOOD_WEBVIEW_WIDGET_AUDIO_MANAGER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  LIGHTWOOD_TYPE_WEBVIEW_WIDGET_AUDIO_MANAGER, LightwoodWebViewWidgetAudioManagerClass))

#define LIGHTWOOD_IS_WEBVIEW_WIDGET_AUDIO_MANAGER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  LIGHTWOOD_TYPE_WEBVIEW_WIDGET_AUDIO_MANAGER))

#define LIGHTWOOD_IS_WEBVIEW_WIDGET_AUDIO_MANAGER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  LIGHTWOOD_TYPE_WEBVIEW_WIDGET_AUDIO_MANAGER))

#define LIGHTWOOD_WEBVIEW_WIDGET_AUDIO_MANAGER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  LIGHTWOOD_TYPE_WEBVIEW_WIDGET_AUDIO_MANAGER, LightwoodWebViewWidgetAudioManagerClass))

typedef struct _LightwoodWebViewWidgetAudioManager LightwoodWebViewWidgetAudioManager;
typedef struct _LightwoodWebViewWidgetAudioManagerClass LightwoodWebViewWidgetAudioManagerClass;
typedef struct _LightwoodWebViewWidgetAudioManagerPrivate LightwoodWebViewWidgetAudioManagerPrivate;

struct _LightwoodWebViewWidgetAudioManager
{
  GObject parent;

  LightwoodWebViewWidgetAudioManagerPrivate *priv;
};

struct _LightwoodWebViewWidgetAudioManagerClass
{
  GObjectClass parent_class;
};


GType lightwood_webview_widget_audio_manager_get_type (void) G_GNUC_CONST;

LightwoodWebViewWidgetAudioManager *lightwood_webview_widget_audio_manager_new (void);
gboolean lightwood_webview_widget_audio_manager_initialize (LightwoodWebViewWidgetAudioManager *self,WebKitWebPage* webPage);
void lightwood_webview_widget_audio_manager_pause_audio (LightwoodWebViewWidgetAudioManager *self);
void lightwood_webview_widget_audio_manager_play_audio (LightwoodWebViewWidgetAudioManager *self);
void lightwood_webview_widget_audio_manager_stop_audio (LightwoodWebViewWidgetAudioManager *self);

G_END_DECLS

#endif /* _LIGHTWOOD_WEBVIEW_WIDGET_AUDIO_MANAGER_H */
