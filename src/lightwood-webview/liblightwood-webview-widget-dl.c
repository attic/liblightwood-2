/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION: lightwood_webview_widget_dl
 * @short_description: a convenience library for dynamic loading of #LightwoodWebViewWidget instance.
 * @title: liblightwood WebViewWidgetDL
 * @stability: stable
 * @include:lightwood_webview_widget_dl.h
 *
 *if the application directly links to webviewwidget the startup time may be little high due to webkit loading-linking.
 *So the app can use the apis here to create the webviewwidget and hence load webkit when needed at runtime.
 *
 */

#include <liblightwood-webview-widget-dl.h>
#include <gmodule.h>

static gpointer _lightwood_webview_widget_dl_find_func(const gchar* name)
{
	static GModule* webview_widget = NULL;    
	if(!webview_widget)
	{
		gchar *libPath = g_module_build_path(PKGLIBDIR, "liblightwoodwebviewwidget");
		if(libPath != NULL)
			webview_widget = g_module_open(libPath, G_MODULE_BIND_LOCAL);
	}
    if(webview_widget == NULL)
	{
		const gchar* error = g_module_error();
		g_warning("## Webview widget Module load error=%s\n", error);
		return NULL;
	}
	gpointer pfunc = NULL;
	g_module_symbol(webview_widget,name,&pfunc);
	if(!pfunc)
		g_warning("## Webview widget symbol not found, error=%s\n", g_module_error());
	return pfunc;
}


typedef ClutterActor* (*lightwood_webview_widget_new_func)(const gchar* app_name);
ClutterActor* lightwood_webview_widget_dl_new (const gchar* app_name)
{
    if (! g_module_supported ())
	{
		g_warning("Webview widget ERROR: Module not supported\n");
		return NULL;
	}
	static lightwood_webview_widget_new_func pfunc= NULL;
	if(!pfunc)
		pfunc = _lightwood_webview_widget_dl_find_func("lightwood_webview_widget_new");

	return pfunc(app_name);
}

typedef ClutterActor* (*lightwood_webview_widget_get_webview_func)(ClutterActor* self);

/**
 * lightwood_webview_widget_dl_get_webview:
 * @self: a #LightwoodWebViewWidget
 *
 * FIXME
 *
 * Return: (transfer none): the web view
 */
ClutterActor* lightwood_webview_widget_dl_get_webview(ClutterActor* self)
{
	static lightwood_webview_widget_get_webview_func pfunc= NULL;
	if(!pfunc)
		pfunc = _lightwood_webview_widget_dl_find_func("lightwood_webview_widget_get_webview");

	return pfunc(self);
}

typedef void (*lightwood_webview_widget_load_url_func) (ClutterActor* self,const gchar* url);
void lightwood_webview_widget_dl_load_url (ClutterActor* self,const gchar* url)
{
	static lightwood_webview_widget_load_url_func pfunc= NULL;
	if(!pfunc)
		pfunc = _lightwood_webview_widget_dl_find_func("lightwood_webview_widget_load_url");

	pfunc(self,url);
}
