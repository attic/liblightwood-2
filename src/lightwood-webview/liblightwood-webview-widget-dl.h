/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* liblightwood-web-view-widget-dl.h */

#ifndef _LIGHTWOOD_WEBVIEW_WIDGET_DL_H
#define _LIGHTWOOD_WEBVIEW_WIDGET_DL_H

#include <glib-object.h>
#include <clutter/clutter.h>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-prototypes"
#include <mx/mx.h>
#pragma GCC diagnostic pop


G_BEGIN_DECLS

typedef enum {
    WEBKIT_LOAD_PROVISIONAL,
    WEBKIT_LOAD_COMMITTED,
    WEBKIT_LOAD_FINISHED,
    WEBKIT_LOAD_FIRST_VISUALLY_NON_EMPTY_LAYOUT,
    WEBKIT_LOAD_FAILED
} WebKitLoadStatus;

ClutterActor* lightwood_webview_widget_dl_new (const gchar* app_name);

ClutterActor* lightwood_webview_widget_dl_get_webview(ClutterActor* self);

void lightwood_webview_widget_dl_load_url (ClutterActor* self,const gchar* url);

G_END_DECLS

#endif /* _LIGHTWOOD_WEBVIEW_WIDGET_DL_H */
