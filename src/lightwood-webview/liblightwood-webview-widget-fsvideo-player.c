/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* liblightwood-web-view-widget-fsvideo-player.c */

#include "liblightwood-webview-widget-fsvideo-player.h"
//#include "liblightwood_webview_widget.h"
#include "liblightwood-webview.h"

static gboolean
_lightwood_webview_widget_fsvideo_player_media_buttonReleaseOnTexture(ClutterActor* texture, ClutterEvent* event, LightwoodWebViewWidgetFSVideoPlayer *self);
static gboolean
_lightwood_webview_widget_fsvideo_player_media_exited_fullscreen_cb(WebKitWebActor* webActor, ClutterActor* texture,LightwoodWebViewWidgetFSVideoPlayer *self);
static gboolean
_lightwood_webview_widget_fsvideo_player_media_fullscreen_cb(WebKitWebActor* webActor, WebKitDOMHTMLElement* element, ClutterActor* texture,LightwoodWebViewWidgetFSVideoPlayer *self);
extern void webkit_web_page_request_exit_fullscreen_media(WebKitWebPage*);


G_DEFINE_TYPE (LightwoodWebViewWidgetFSVideoPlayer, lightwood_webview_widget_fsvideo_player, G_TYPE_OBJECT)

#define WEBVIEW_WIDGET_FSVIDEO_PLAYER_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), LIGHTWOOD_TYPE_WEBVIEW_WIDGET_FSVIDEO_PLAYER, LightwoodWebViewWidgetFSVideoPlayerPrivate))

struct _LightwoodWebViewWidgetFSVideoPlayerPrivate
{
	ClutterActor *m_webview;
	ClutterActor *texture_parent;
	WebKitDOMHTMLElement* element;
};


static void
lightwood_webview_widget_fsvideo_player_dispose (GObject *object)
{
	LightwoodWebViewWidgetFSVideoPlayer *self=LIGHTWOOD_WEBVIEW_WIDGET_FSVIDEO_PLAYER(object);
	g_object_unref(self->priv->m_webview);
  G_OBJECT_CLASS (lightwood_webview_widget_fsvideo_player_parent_class)->dispose (object);
}

static void
lightwood_webview_widget_fsvideo_player_class_init (LightwoodWebViewWidgetFSVideoPlayerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  g_type_class_add_private (klass, sizeof (LightwoodWebViewWidgetFSVideoPlayerPrivate));

  object_class->dispose = lightwood_webview_widget_fsvideo_player_dispose;
}

static void
lightwood_webview_widget_fsvideo_player_init (LightwoodWebViewWidgetFSVideoPlayer *self)
{
  self->priv = WEBVIEW_WIDGET_FSVIDEO_PLAYER_PRIVATE (self);
}

LightwoodWebViewWidgetFSVideoPlayer *
lightwood_webview_widget_fsvideo_player_new (void)
{
  return g_object_new (LIGHTWOOD_TYPE_WEBVIEW_WIDGET_FSVIDEO_PLAYER, NULL);
}

void lightwood_webview_widget_fsvideo_player_initialize(LightwoodWebViewWidgetFSVideoPlayer *self,ClutterActor *webview)
{
	self->priv->m_webview=webview;
	 webkit_iweb_view_get_page(WEBKIT_IWEB_VIEW(self->priv->m_webview));
	g_object_connect (self->priv->m_webview,
	    				"signal::media-fullscreen",G_CALLBACK(_lightwood_webview_widget_fsvideo_player_media_fullscreen_cb), self,
						 "signal::media-exit-fullscreen",G_CALLBACK(_lightwood_webview_widget_fsvideo_player_media_exited_fullscreen_cb), self,
						  NULL);
}

static void
_lightwood_webview_widget_fsvideo_player_media_fs_exit_cb(
		ClutterActor *texture,LightwoodWebViewWidgetFSVideoPlayer *self)
{

	ClutterActor* parent_actor= clutter_actor_get_parent(texture);
	clutter_actor_remove_child(parent_actor, texture);
	//clutter_actor_add_child(self->priv->m_webview, texture);
	webkit_web_page_request_exit_fullscreen_media(webkit_iweb_view_get_page((WebKitIWebView*)self->priv->m_webview));
	webkit_dom_html_media_element_play(WEBKIT_DOM_HTML_MEDIA_ELEMENT(self->priv->element));
	/* disconnect so we don't get multiple notifications */
	g_signal_handlers_disconnect_by_func (texture,
			  	  	  	  	  	  	  	  _lightwood_webview_widget_fsvideo_player_media_fs_exit_cb,
	                                        self);
}

static gboolean
_lightwood_webview_widget_fsvideo_player_media_buttonReleaseOnTexture(ClutterActor* texture, ClutterEvent* event,LightwoodWebViewWidgetFSVideoPlayer *self)
{
	//ENTRY_TRACE
	webkit_dom_html_media_element_pause(WEBKIT_DOM_HTML_MEDIA_ELEMENT(self->priv->element));

	 glong element_width,element_height;
	 element_width = webkit_dom_element_get_offset_width(WEBKIT_DOM_ELEMENT(self->priv->element));
 	 element_height = webkit_dom_element_get_offset_height(WEBKIT_DOM_ELEMENT(self->priv->element));

	 glong offset_x,offset_y;
	 offset_x=(glong)g_object_get_data(G_OBJECT(texture),"element_x");
	 offset_y=(glong)g_object_get_data(G_OBJECT(texture),"element_y");
/*	g_signal_connect_after(
			clutter_actor_animate(texture, CLUTTER_LINEAR, 1000,
						  "width", (gfloat)element_width,
						  "height",(gfloat)element_height,
						  "x",(gfloat)offset_x,
						  "y",(gfloat)offset_y,
						  NULL),
		"completed",G_CALLBACK(_lightwood_webview_widget_fsvideo_player_media_fs_exit_cb),element);*/

	clutter_actor_save_easing_state (texture);
	clutter_actor_set_easing_mode(texture,CLUTTER_LINEAR);
	clutter_actor_set_easing_duration(texture,500);
	clutter_actor_set_size (texture, element_width,element_height);
	clutter_actor_set_position (texture, offset_x,offset_y);
	clutter_actor_restore_easing_state (texture);

	  g_signal_connect (texture,
	                    "transitions-completed",
	                    G_CALLBACK (_lightwood_webview_widget_fsvideo_player_media_fs_exit_cb),
	                    self);

	return TRUE;
}

static gboolean _lightwood_webview_widget_fsvideo_player_media_touch_events_cb(ClutterActor *actor,ClutterEvent *event,gpointer userdata)
{
	LightwoodWebViewWidgetFSVideoPlayer *self=(LightwoodWebViewWidgetFSVideoPlayer *)userdata;
	switch(event->type)
		{

		case CLUTTER_TOUCH_BEGIN:
			{
				_lightwood_webview_widget_fsvideo_player_media_buttonReleaseOnTexture(actor,event,self);
			break;
			}
		default:break;

		}

		return FALSE;
}




 static gboolean
 _lightwood_webview_widget_fsvideo_player_media_fullscreen_cb(WebKitWebActor* webActor, WebKitDOMHTMLElement* element, ClutterActor* texture,LightwoodWebViewWidgetFSVideoPlayer *self)
{
	//ENTRY_TRACE
	// LightwoodWebViewWidgetPrivate* priv = WEBVIEW_WIDGET_PRIVATE(self);
	// webkit_dom_html_media_element_pause(WEBKIT_DOM_HTML_MEDIA_ELEMENT(element));

	 int page_offset_x, page_offset_y;
	 glong abs_x, abs_y;
	 glong element_width,element_height;
     self->priv->element=element;
	WebKitWebPage* webPage = webkit_iweb_view_get_page(WEBKIT_IWEB_VIEW(webActor));
	WebKitWebFrame* webFrame = webkit_web_page_get_main_frame(webPage);
	element_width = webkit_dom_element_get_offset_width(WEBKIT_DOM_ELEMENT(element));
	element_height = webkit_dom_element_get_offset_height(WEBKIT_DOM_ELEMENT(element));
	webkit_web_frame_get_scroll_offset(webFrame, &page_offset_x, &page_offset_y);
	lightwood_webview_get_absolute_offset_for_element(WEBKIT_DOM_ELEMENT(element),1.0, &abs_x, &abs_y);
	self->priv->texture_parent=clutter_actor_get_parent(texture);
	clutter_actor_set_reactive(texture, TRUE);
	clutter_actor_remove_child(self->priv->texture_parent,texture);
	g_signal_connect(texture, "button-press-event",
			G_CALLBACK(_lightwood_webview_widget_fsvideo_player_media_buttonReleaseOnTexture), self);
	g_signal_connect(texture, "touch-event",
				G_CALLBACK(_lightwood_webview_widget_fsvideo_player_media_touch_events_cb), self);
	ClutterActor *parent=clutter_actor_get_parent(self->priv->m_webview);
	g_assert(CLUTTER_IS_ACTOR(self->priv->m_webview)&&(self->priv->m_webview));
		clutter_actor_add_child(parent, texture);
	//clutter_actor_set_opacity(texture,0xff);
	clutter_actor_set_position(texture,abs_x-page_offset_x,abs_y-page_offset_y);
	clutter_actor_set_size(texture,element_width,element_height);

	g_object_set_data(G_OBJECT(texture),"element",(gpointer)(element));
	g_object_set_data(G_OBJECT(texture),"element_x",(gpointer)(abs_x-page_offset_x));
	g_object_set_data(G_OBJECT(texture),"element_y",(gpointer)(abs_y-page_offset_y));

/*
	clutter_actor_animate(texture, CLUTTER_LINEAR, 1000,
						  "width", clutter_actor_get_width(CLUTTER_ACTOR(self->priv->m_webview)),
						  "height", clutter_actor_get_height(CLUTTER_ACTOR(self->priv->m_webview)),
						  "x",0.0,
						  "y",0.0,
						  NULL);
*/
	clutter_actor_save_easing_state (texture);
	clutter_actor_set_easing_mode(texture,CLUTTER_LINEAR);
	clutter_actor_set_easing_duration(texture,500);
	clutter_actor_set_size (texture,
	clutter_actor_get_width(CLUTTER_ACTOR(parent)),
	clutter_actor_get_height(CLUTTER_ACTOR(parent)));
	clutter_actor_set_position (texture, 0.0,0.0);
	clutter_actor_restore_easing_state (texture);

//webkit_dom_html_media_element_play(WEBKIT_DOM_HTML_MEDIA_ELEMENT(element));
	return TRUE;
}

 static gboolean
 _lightwood_webview_widget_fsvideo_player_media_exited_fullscreen_cb(WebKitWebActor* webActor, ClutterActor* texture,LightwoodWebViewWidgetFSVideoPlayer *self)
 {
 	//ENTRY_TRACE
	 g_print("\n _lightwood_webview_widget_fsvideo_player_media_exited_fullscreen_cb");

	// WebKitDOMHTMLElement* element = g_object_get_data(G_OBJECT(texture),"element");
 	 //clutter_actor_hide(texture);
 	 //clutter_container_remove_actor(CLUTTER_CONTAINER(self->priv->m_webview), texture);
 	 //webkit_dom_html_media_element_play(WEBKIT_DOM_HTML_MEDIA_ELEMENT(element));

 	 return FALSE;
 }
