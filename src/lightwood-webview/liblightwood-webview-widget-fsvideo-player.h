/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* liblightwood-web-view-widget-fsvideo-player.h */

#ifndef _LIGHTWOOD_WEBVIEW_WIDGET_FSVIDEO_PLAYER_H
#define _LIGHTWOOD_WEBVIEW_WIDGET_FSVIDEO_PLAYER_H

#include <glib-object.h>
#include <clutter/clutter.h>


G_BEGIN_DECLS

#define LIGHTWOOD_TYPE_WEBVIEW_WIDGET_FSVIDEO_PLAYER lightwood_webview_widget_fsvideo_player_get_type()

#define LIGHTWOOD_WEBVIEW_WIDGET_FSVIDEO_PLAYER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  LIGHTWOOD_TYPE_WEBVIEW_WIDGET_FSVIDEO_PLAYER, LightwoodWebViewWidgetFSVideoPlayer))

#define LIGHTWOOD_WEBVIEW_WIDGET_FSVIDEO_PLAYER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  LIGHTWOOD_TYPE_WEBVIEW_WIDGET_FSVIDEO_PLAYER, LightwoodWebViewWidgetFSVideoPlayerClass))

#define LIGHTWOOD_IS_WEBVIEW_WIDGET_FSVIDEO_PLAYER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  LIGHTWOOD_TYPE_WEBVIEW_WIDGET_FSVIDEO_PLAYER))

#define LIGHTWOOD_IS_WEBVIEW_WIDGET_FSVIDEO_PLAYER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  LIGHTWOOD_TYPE_WEBVIEW_WIDGET_FSVIDEO_PLAYER))

#define LIGHTWOOD_WEBVIEW_WIDGET_FSVIDEO_PLAYER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  LIGHTWOOD_TYPE_WEBVIEW_WIDGET_FSVIDEO_PLAYER, LightwoodWebViewWidgetFSVideoPlayerClass))

typedef struct _LightwoodWebViewWidgetFSVideoPlayer LightwoodWebViewWidgetFSVideoPlayer;
typedef struct _LightwoodWebViewWidgetFSVideoPlayerClass LightwoodWebViewWidgetFSVideoPlayerClass;
typedef struct _LightwoodWebViewWidgetFSVideoPlayerPrivate LightwoodWebViewWidgetFSVideoPlayerPrivate;

struct _LightwoodWebViewWidgetFSVideoPlayer
{
  GObject parent;

  LightwoodWebViewWidgetFSVideoPlayerPrivate *priv;

};

struct _LightwoodWebViewWidgetFSVideoPlayerClass
{
  GObjectClass parent_class;
};

GType lightwood_webview_widget_fsvideo_player_get_type (void) G_GNUC_CONST;

LightwoodWebViewWidgetFSVideoPlayer *lightwood_webview_widget_fsvideo_player_new (void);
void lightwood_webview_widget_fsvideo_player_initialize(LightwoodWebViewWidgetFSVideoPlayer *self,ClutterActor *webview);



G_END_DECLS

#endif /* _LIGHTWOOD_WEBVIEW_WIDGET_FSVIDEO_PLAYER_H */
