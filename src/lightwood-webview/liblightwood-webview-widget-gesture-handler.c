/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* core-webview-widget-gesture-handler.c */

#include "liblightwood-webview-widget-gesture-handler.h"
#include "liblightwood-webview-widget.h"
#include "liblightwood-webview.h"

#include <webkit/webkitenumtypes.h>
#include <webkit/webkitwebpolicydecision.h>
#include <webkit/webkitwebwindowfeatures.h>
#include <webkit/webkitnetworkresponse.h>
//#include <webkit/webkitmarshal.h>
#include <webkit/webkitgwebframe.h>
#include <webkit/webkitgeolocationpolicydecision.h>
#include <webkit/webkitwebinspector.h>

#include <math.h>
#include <stdlib.h>

#define LIGHTWOOD_WEBVIEW_DOUBLE_TAP_DELAY 		750 //msecs
#define LIGHTWOOD_WEBVIEW_DOUBLE_TAP_DISTANCE 	200*3 //pixels
#define LIGHTWOOD_WEBVIEW_DRAG_THRESHOLD 			25 //pixels
#define LIGHTWOOD_WEBVIEW_LINK_CLICK_THRESHOLD 	30, 30, 30, 30 //pixels area
#define LIGHTWOOD_WEBVIEW_LONG_PRESS_DELAY		1000 //msecs

G_DEFINE_TYPE (LightwoodWebViewWidgetGestureHandler, lightwood_webview_widget_gesture_handler, G_TYPE_OBJECT)

#define WEBVIEW_WIDGET_GESTURE_HANDLER_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), LIGHTWOOD_TYPE_WEBVIEW_WIDGET_GESTURE_HANDLER, LightwoodWebViewWidgetGestureHandlerPrivate))

typedef struct {
	LightwoodWebViewWidgetGestureHandler* self;
	ClutterActor* actor;
    ClutterButtonEvent* event;
    ClutterTouchEvent* touch_event;
    glong delayed_event_id;
} DelayedEventData;

typedef struct {
	ClutterActor* 		actor;
    ClutterButtonEvent* event;
    ClutterTouchEvent* touch_event;
    gboolean 			passthrough;
    DelayedEventData* 	delayed_event;
    gint touch_press_x;
    gint touch_press_y;
}TouchEventData;


struct _LightwoodWebViewWidgetGestureHandlerPrivate
{
	LightwoodWebViewWidget* m_webview_widget;
	LightwoodWebView* m_webview;

	guint 				m_touchpoints;
	TouchEventData  	m_pressEvent;
	ClutterButtonEvent*	m_prevButtonEvent;
	ClutterTouchEvent* m_prevTouchEvent;
	gulong 				m_longpress_event_id;
	glong 				m_delayed_event_id;

	gboolean 			m_is_element_scrollable;
	gboolean 			m_is_perform_scroll;
	gboolean 			m_is_element_clickable;
	gfloat 				m_click_x_adj;
	gfloat 				m_click_y_adj;
	WebKitDOMElement* 	m_clickable_or_scrollableElement;
	gboolean 			m_scrollableElementDoubletap;
	gboolean 		m_page_in_scroll_mode;
	gboolean		m_double_tap_enabled;
	gboolean         m_is_second_tap;
	guint            m_doubletap_timer;
	gulong 			m_capture_signal;
    gulong          m_touch_signal;
    gboolean      m_new_view_enable;
};


enum
{
	PROP_0,
   PROP_NEW_VIEW_ENABLE
};

static WebKitDOMElement*
_lightwood_webview_widget_gesture_handler_test_for_clickable_or_scrollable_element(LightwoodWebViewWidgetGestureHandler *self,
			ClutterEvent* event,gboolean *is_element_scrollable,gboolean *is_perform_scroll,gboolean *is_element_clickable,gfloat *x_adj,gfloat *y_adj);

static WebKitDOMElement*
_lightwood_webview_widget_gesture_handler_test_for_touchable_or_scrollable_element(LightwoodWebViewWidgetGestureHandler *self,
                        ClutterEvent* event,gboolean *is_element_scrollable,gboolean *is_perform_scroll,gboolean *is_element_clickable,gfloat *x_adj,gfloat *y_adj);




static gboolean
_lightwood_webview_widget_webview_captured_event_cb(ClutterActor* web_view,ClutterEvent*event,LightwoodWebViewWidgetGestureHandler *self);
static gboolean
_lightwood_webview_widget_gesture_handler_long_press_event_cb(gpointer data);
static gboolean
_lightwood_webview_widget_gesture_handler_handle_button_press_event(LightwoodWebViewWidgetGestureHandler *self, ClutterEvent* event);

static gboolean
_lightwood_webview_widget_gesture_handler_handle_touch_press_event(LightwoodWebViewWidgetGestureHandler *self, ClutterEvent* event);

static gboolean
_lightwood_webview_widget_gesture_handler_handle_button_release_event(LightwoodWebViewWidgetGestureHandler *self, ClutterEvent* event);

static gboolean
_lightwood_webview_widget_gesture_handler_handle_touch_release_event(LightwoodWebViewWidgetGestureHandler *self, ClutterEvent* event);
static gboolean
_lightwood_webview_widget_gesture_handler_handle_motion_event(LightwoodWebViewWidgetGestureHandler *self, ClutterEvent* event);

static gboolean
_lightwood_webview_widget_gesture_handler_handle_touch_motion_event(LightwoodWebViewWidgetGestureHandler *self, ClutterEvent* event);

static gboolean
_lightwood_webview_widget_gesture_handler_delayed_button_click_event(gpointer user_data);

static gboolean
_lightwood_webview_widget_gesture_handler_delayed_touch_press_event(gpointer user_data);


static void
lightwood_webview_widget_gesture_handler_get_property (GObject    *object,
                                                   guint       property_id,
                                                   GValue     *value,
                                                   GParamSpec *pspec)
{
	LightwoodWebViewWidgetGestureHandlerPrivate* priv = WEBVIEW_WIDGET_GESTURE_HANDLER_PRIVATE(object);
  switch (property_id)
    {
  case PROP_NEW_VIEW_ENABLE:
	  g_value_set_boolean(value,priv->m_new_view_enable);
	  break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
lightwood_webview_widget_gesture_handler_set_property (GObject      *object,
                                                   guint         property_id,
                                                   const GValue *value,
                                                   GParamSpec   *pspec)
{
	LightwoodWebViewWidgetGestureHandlerPrivate* priv = WEBVIEW_WIDGET_GESTURE_HANDLER_PRIVATE(object);
  switch (property_id)
    {
  case PROP_NEW_VIEW_ENABLE:
	  priv->m_new_view_enable=g_value_get_boolean(value);
  	  break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
lightwood_webview_widget_gesture_handler_dispose (GObject *object)
{
  G_OBJECT_CLASS (lightwood_webview_widget_gesture_handler_parent_class)->dispose (object);
}

static void
lightwood_webview_widget_gesture_handler_class_init (LightwoodWebViewWidgetGestureHandlerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  g_type_class_add_private (klass, sizeof (LightwoodWebViewWidgetGestureHandlerPrivate));

  object_class->get_property = lightwood_webview_widget_gesture_handler_get_property;
  object_class->set_property = lightwood_webview_widget_gesture_handler_set_property;
  object_class->dispose = lightwood_webview_widget_gesture_handler_dispose;

  GParamSpec *pspec;

   pspec = g_param_spec_boolean( "new-view-enable",
 								"new-view-enable",
 								"new-view-enable",
 								true,
 								G_PARAM_CONSTRUCT | G_PARAM_READWRITE);
   g_object_class_install_property (object_class, PROP_NEW_VIEW_ENABLE, pspec);

}

static void
lightwood_webview_widget_gesture_handler_init (LightwoodWebViewWidgetGestureHandler *self)
{
  self->priv = WEBVIEW_WIDGET_GESTURE_HANDLER_PRIVATE (self);
  self->priv->m_double_tap_enabled = TRUE;
  self->priv->m_page_in_scroll_mode = FALSE;
  self->priv->m_new_view_enable=FALSE;
	ClutterSettings* settings = clutter_settings_get_default();
	g_object_set(settings,
					"double-click-distance",LIGHTWOOD_WEBVIEW_DOUBLE_TAP_DISTANCE,
					"double-click-time",LIGHTWOOD_WEBVIEW_DOUBLE_TAP_DELAY,
					NULL);
	MxSettings *mx_settings = mx_settings_get_default ();
	g_object_set (G_OBJECT (mx_settings),
	                       "drag-threshold", LIGHTWOOD_WEBVIEW_DRAG_THRESHOLD,
	                       NULL);
	self->priv->m_touchpoints=0;
}


static gboolean _lightwood_webview_widget_webview_touch_event_cb(ClutterActor* web_view,ClutterEvent*event,LightwoodWebViewWidgetGestureHandler *self)
{
	LightwoodWebViewWidgetGestureHandlerPrivate* priv = WEBVIEW_WIDGET_GESTURE_HANDLER_PRIVATE(self);
	static gboolean event_handled = CLUTTER_EVENT_PROPAGATE;
	switch(event->type)
	{
        case CLUTTER_TOUCH_BEGIN:

                   priv->m_click_x_adj = priv->m_click_y_adj =0;
                                priv->m_clickable_or_scrollableElement =
                                        _lightwood_webview_widget_gesture_handler_test_for_touchable_or_scrollable_element(self,event,

&(priv->m_is_element_scrollable),&(priv->m_is_perform_scroll),&(priv->m_is_element_clickable),

&priv->m_click_x_adj,&priv->m_click_y_adj);

                                event_handled =  _lightwood_webview_widget_gesture_handler_handle_touch_press_event(self,event);

                              break;
  	
  case CLUTTER_TOUCH_UPDATE:

                                 {
                                        event_handled =  _lightwood_webview_widget_gesture_handler_handle_touch_motion_event(self,event);
                                }
                             break;


case CLUTTER_TOUCH_END:
	{
	event_handled =  _lightwood_webview_widget_gesture_handler_handle_touch_release_event(self,event);
	}
	break;



				default:
					break;
	}
	return event_handled;
}


static void
_lightwood_webview_widget_gesture_handler_initialize (LightwoodWebViewWidgetGestureHandler *self, LightwoodWebViewWidget *webview_widget)
{
	LightwoodWebViewWidgetGestureHandlerPrivate* priv = WEBVIEW_WIDGET_GESTURE_HANDLER_PRIVATE(self);
	priv->m_webview_widget = webview_widget;
	priv->m_webview =LIGHTWOOD_WEBVIEW(lightwood_webview_widget_get_webview(webview_widget));
	priv->m_is_second_tap=FALSE;
	priv->m_capture_signal = g_signal_connect (CLUTTER_ACTOR(webview_widget),
			 	 	 	 	 	 	 	 	 	 "captured-event",
			 	 	 	 	 	 	 	 	 	G_CALLBACK (_lightwood_webview_widget_webview_captured_event_cb),
			 	 	 	 	 	 	 	 	 	self);
  	

priv->m_touch_signal=g_signal_connect (CLUTTER_ACTOR(webview_widget),
					 	 	 	 	 	 	 	 	 	 "touch-event",
					 	 	 	 	 	 	 	 	 	G_CALLBACK (_lightwood_webview_widget_webview_touch_event_cb),
					 	 	 	 	 	 	 	 	 	self);

}

LightwoodWebViewWidgetGestureHandler *
lightwood_webview_widget_gesture_handler_new (LightwoodWebViewWidget * webview_widget)
{
	LightwoodWebViewWidgetGestureHandler *self = g_object_new (LIGHTWOOD_TYPE_WEBVIEW_WIDGET_GESTURE_HANDLER, NULL);
	_lightwood_webview_widget_gesture_handler_initialize(self,webview_widget);
	return self;
}

void lightwood_webview_widget_gesture_handler_enable_gesture(LightwoodWebViewWidgetGestureHandler* self,gboolean is_enabled)
{
	LightwoodWebViewWidgetGestureHandlerPrivate* priv = WEBVIEW_WIDGET_GESTURE_HANDLER_PRIVATE(self);
	if(is_enabled){
		g_signal_handler_unblock (CLUTTER_ACTOR(priv->m_webview_widget),priv->m_capture_signal);
		g_signal_handler_unblock (CLUTTER_ACTOR(priv->m_webview_widget),priv->m_touch_signal);


	}
	else{
			g_signal_handler_block(CLUTTER_ACTOR(priv->m_webview_widget),priv->m_capture_signal);
			g_signal_handler_block (CLUTTER_ACTOR(priv->m_webview_widget),priv->m_touch_signal);
	}
}

static gboolean
_lightwood_webview_widget_webview_captured_event_cb(ClutterActor* web_view,ClutterEvent*event,LightwoodWebViewWidgetGestureHandler *self)
{
	LightwoodWebViewWidgetGestureHandlerPrivate* priv = WEBVIEW_WIDGET_GESTURE_HANDLER_PRIVATE(self);
	ClutterButtonEvent *bevent = (ClutterButtonEvent *)event;
	gboolean event_handled = CLUTTER_EVENT_PROPAGATE;
	gfloat x,y;

	 if (!(  ((event->type == CLUTTER_MOTION) ||
			 (event->type == CLUTTER_BUTTON_RELEASE) ||
			 (event->type == CLUTTER_BUTTON_PRESS)) &&
			 (bevent->flags != CLUTTER_EVENT_FLAG_SYNTHETIC) &&
			clutter_actor_transform_stage_point (CLUTTER_ACTOR (priv->m_webview),
												  bevent->x,
												  bevent->y,
												  &x, &y)))
	  {
		 
		 //bevent->flags = CLUTTER_EVENT_NONE;
		  return event_handled;
	  }

		if(!clutter_actor_get_reactive(CLUTTER_ACTOR(priv->m_webview_widget)))
		{
			// g_print("\n clutter_actor_get_reactive.....");
			return CLUTTER_EVENT_STOP;
		}

		ClutterSettings* settings = clutter_settings_get_default();
		g_object_set(settings,
						"double-click-distance",LIGHTWOOD_WEBVIEW_DOUBLE_TAP_DISTANCE,
						"double-click-time",LIGHTWOOD_WEBVIEW_DOUBLE_TAP_DELAY,
						NULL);
		MxSettings *mx_settings = mx_settings_get_default ();
		g_object_set (G_OBJECT (mx_settings),
		                       "drag-threshold", LIGHTWOOD_WEBVIEW_DRAG_THRESHOLD,
		                       NULL);

		switch(event->type)
		{
			case CLUTTER_BUTTON_PRESS:
			{
				if ( bevent->button == 3 )
				{
					// g_print("\n launching inspector");
					 WebKitWebPage* page = webkit_iweb_view_get_page(WEBKIT_IWEB_VIEW(priv->m_webview));
					 WebKitWebInspector* inspector = webkit_web_page_get_inspector(page);
					 webkit_web_inspector_inspect_coordinates(inspector, bevent->x, bevent->y);

					 return CLUTTER_EVENT_STOP;
				 }
				//*****return if scroll in progress*********

				//do a point ht test and check for scroll mode and the element type
				priv->m_click_x_adj = priv->m_click_y_adj =0;
				priv->m_clickable_or_scrollableElement =
					_lightwood_webview_widget_gesture_handler_test_for_clickable_or_scrollable_element(self,event,
										&(priv->m_is_element_scrollable),&(priv->m_is_perform_scroll),&(priv->m_is_element_clickable),
										&priv->m_click_x_adj,&priv->m_click_y_adj);

				event_handled =  _lightwood_webview_widget_gesture_handler_handle_button_press_event(self,event);
			}
			break;

			case CLUTTER_BUTTON_RELEASE:
			{
				event_handled =  _lightwood_webview_widget_gesture_handler_handle_button_release_event(self,event);
		}
			break;

			case CLUTTER_MOTION:
			{

				 event_handled = _lightwood_webview_widget_gesture_handler_handle_motion_event(self,event);
			}
			break;

			default:
				break;
		}

	  return event_handled;
}

/*
static guint motion_event_src=0;
static gboolean
_lightwood_webview_widget_gesture_handler_emit_motion_event_cb(ClutterMotionEvent* event)
{
	g_print("\n _lightwood_webview_widget_gesture_handler_emit_motion_event_cb");
	//clutter_actor_event(clutter_actor_get_parent(event->source),event,FALSE);
	return FALSE;
}

static gboolean
_lightwood_webview_widget_gesture_handler_motion_event_after_cb(ClutterActor* actor, ClutterEvent* event, gpointer data)
{
    LightwoodWebViewWidget* self  = (LightwoodWebViewWidget*)data;
    LightwoodWebViewWidgetPrivate* priv = WEBVIEW_WIDGET_PRIVATE(self);
    g_print("\n _lightwood_webview_widget_gesture_handler_motion_event_after_cb");
	//motion_event_src = g_idle_add(_lightwood_webview_widget_gesture_handler_emit_motion_event_cb,clutter_event_copy(event));
	//clutter_actor_event(CLUTTER_ACTOR(self)),event,FALSE);
	return FALSE;
}*/

static WebKitDOMElement*
_lightwood_webview_widget_gesture_handler_test_for_clickable_or_scrollable_element(LightwoodWebViewWidgetGestureHandler *self,
			ClutterEvent* event,gboolean *is_element_scrollable,gboolean *is_perform_scroll,gboolean *is_element_clickable,gfloat *x_adj,gfloat *y_adj)
{
	LightwoodWebViewWidgetGestureHandlerPrivate* priv = WEBVIEW_WIDGET_GESTURE_HANDLER_PRIVATE(self);

	gchar* role=NULL;gchar* class=NULL;gchar* id=NULL;
	*is_element_scrollable = FALSE;
	*is_element_clickable = FALSE;
	*is_perform_scroll = FALSE;

	WebKitDOMElement* scrollable_element = NULL;

	WebKitDOMElement* nearest_node = lightwood_webview_get_clickable_node(LIGHTWOOD_WEBVIEW(priv->m_webview),(ClutterButtonEvent*)event,LIGHTWOOD_WEBVIEW_LINK_CLICK_THRESHOLD,x_adj,y_adj);
	if(*x_adj != 0 || *y_adj != 0)
	{
		g_print("\n x_adj,y_adj=%f,%f;x,y=%f,%f",*x_adj,*y_adj,((ClutterButtonEvent*)event)->x,((ClutterButtonEvent*)event)->y);
	}


	if(!*is_element_clickable)
	{
		char* href=NULL;
		if(g_object_class_find_property(G_OBJECT_CLASS(WEBKIT_DOM_ELEMENT_GET_CLASS(nearest_node)),"href"))
					g_object_get(nearest_node, "href", &href, NULL);
		if(href != NULL){
			*is_element_clickable = TRUE;
			//g_print("\n hyperlink clicked...");
		}

	}
	if(*is_element_clickable){
		return nearest_node;
	}

	WebKitDOMElement* div = (WebKitDOMElement*)webkit_dom_node_get_parent_node((WebKitDOMNode*)nearest_node);
		while(WEBKIT_DOM_IS_HTML_DIV_ELEMENT(div))
		{
			id = webkit_dom_element_get_attribute(div,"id");
			class = webkit_dom_element_get_attribute(div,"class");
			role = webkit_dom_element_get_attribute(div,"role");
			//g_print("\n id=%s,class=%s,role=%s",id,class,role);
			if(role && g_strrstr(role,"menu"))
			{
				scrollable_element = div;
				//g_print("\n sending scroll event.....");
				*is_element_scrollable = TRUE;
				*is_perform_scroll = TRUE;
				break;
			}
			else if(id && g_strrstr(id,"map"))
			{
				scrollable_element = div;
				//g_print("\n sending motion event.....");
				*is_element_scrollable = TRUE;
				break;
			}
			div = (WebKitDOMElement*)webkit_dom_node_get_parent_node((WebKitDOMNode*)div);
		}
		if(scrollable_element)
			g_print("\n id=%s,class=%s,role=%s",id,class,role);

		else
		{
			*is_element_clickable = WEBKIT_DOM_IS_HTML_IMAGE_ELEMENT(nearest_node);
			if(*is_element_clickable)
				scrollable_element = nearest_node;
		}
	return scrollable_element;
}


static WebKitDOMElement*
_lightwood_webview_widget_gesture_handler_test_for_touchable_or_scrollable_element(LightwoodWebViewWidgetGestureHandler *self,
			ClutterEvent* event,gboolean *is_element_scrollable,gboolean *is_perform_scroll,gboolean *is_element_clickable,gfloat *x_adj,gfloat *y_adj)
{
	LightwoodWebViewWidgetGestureHandlerPrivate* priv = WEBVIEW_WIDGET_GESTURE_HANDLER_PRIVATE(self);

	gchar* role=NULL;gchar* class=NULL;gchar* id=NULL;
	*is_element_scrollable = FALSE;
	*is_element_clickable = FALSE;
	*is_perform_scroll = FALSE;

	WebKitDOMElement* scrollable_element = NULL;

	WebKitDOMElement* nearest_node = lightwood_webview_get_touchable_node(LIGHTWOOD_WEBVIEW(priv->m_webview),(ClutterTouchEvent*)event,LIGHTWOOD_WEBVIEW_LINK_CLICK_THRESHOLD,x_adj,y_adj);
	if(*x_adj != 0 || *y_adj != 0)
	{
		g_print("\n x_adj,y_adj=%f,%f;x,y=%f,%f",*x_adj,*y_adj,((ClutterTouchEvent*)event)->x,((ClutterTouchEvent*)event)->y);
	}


	if(!*is_element_clickable)
	{
		char* href=NULL;
		if(g_object_class_find_property(G_OBJECT_CLASS(WEBKIT_DOM_ELEMENT_GET_CLASS(nearest_node)),"href"))
					g_object_get(nearest_node, "href", &href, NULL);
		if(href != NULL){
			*is_element_clickable = TRUE;
			//g_print("\n hyperlink clicked...");
		}

	}
	if(*is_element_clickable){
		return nearest_node;
	}

	WebKitDOMElement* div = (WebKitDOMElement*)webkit_dom_node_get_parent_node((WebKitDOMNode*)nearest_node);
		while(WEBKIT_DOM_IS_HTML_DIV_ELEMENT(div))
		{
			id = webkit_dom_element_get_attribute(div,"id");
			class = webkit_dom_element_get_attribute(div,"class");
			role = webkit_dom_element_get_attribute(div,"role");
			//g_print("\n id=%s,class=%s,role=%s",id,class,role);
			if(role && g_strrstr(role,"menu"))
			{
				scrollable_element = div;
				//g_print("\n sending scroll event.....");
				*is_element_scrollable = TRUE;
				*is_perform_scroll = TRUE;
				break;
			}
			else if(id && g_strrstr(id,"map"))
			{
				scrollable_element = div;
				//g_print("\n sending motion event.....");
				*is_element_scrollable = TRUE;
				break;
			}
			div = (WebKitDOMElement*)webkit_dom_node_get_parent_node((WebKitDOMNode*)div);
		}
		if(scrollable_element)
			g_print("\n id=%s,class=%s,role=%s",id,class,role);

		else
		{
			*is_element_clickable = WEBKIT_DOM_IS_HTML_IMAGE_ELEMENT(nearest_node);
			if(*is_element_clickable)
				scrollable_element = nearest_node;
		}
	return scrollable_element;
}
typedef struct
{
	LightwoodWebViewWidgetGestureHandler* self;
	guint long_press_evt_id;
}LongPressEventData;

static gboolean
_lightwood_webview_widget_gesture_handler_long_press_event_cb(gpointer data)
{
	LongPressEventData* long_press_evt = (LongPressEventData*)data;
	LightwoodWebViewWidgetGestureHandler* self = long_press_evt->self;
	LightwoodWebViewWidgetGestureHandlerPrivate* priv = WEBVIEW_WIDGET_GESTURE_HANDLER_PRIVATE(self);


	if( priv->m_pressEvent.event &&
		priv->m_longpress_event_id &&
		(long_press_evt->long_press_evt_id == priv->m_longpress_event_id))
	{
		//	g_print("\n long press alert...");

			char* href = NULL;
			if(priv->m_clickable_or_scrollableElement)
				if(g_object_class_find_property(G_OBJECT_CLASS(WEBKIT_DOM_ELEMENT_GET_CLASS(priv->m_clickable_or_scrollableElement)),"href"))
					g_object_get(priv->m_clickable_or_scrollableElement, "href", &href, NULL);
			if(href)
			{
/*				WebKitDOMCSSStyleDeclaration* style = NULL;
				g_object_get(nearest_node, "style", &style, NULL);
				webkit_dom_css_style_declaration_set_property(style, "color", "brown", "", NULL);
				webkit_dom_css_style_declaration_set_property(style, "text-decoration", "blink", "", NULL);*/
				//g_print("\n long press alert on link...%s",href);
				g_signal_emit_by_name(priv->m_webview_widget,"new_view_requested",href);

			}
			else if(WEBKIT_DOM_IS_HTML_IMAGE_ELEMENT(priv->m_clickable_or_scrollableElement))
			{
				//g_print("\n long press alert on image...");
				g_signal_emit_by_name(priv->m_webview_widget,"save_image_requested",
						webkit_dom_html_image_element_get_src(WEBKIT_DOM_HTML_IMAGE_ELEMENT(priv->m_clickable_or_scrollableElement)));
			}
			else
			{
				priv->m_pressEvent.event->flags = CLUTTER_EVENT_FLAG_SYNTHETIC;
				priv->m_pressEvent.event->time = clutter_get_current_event_time();
				//clutter_actor_event(priv->m_pressEvent.actor, (ClutterEvent*)priv->m_pressEvent.event, TRUE);
				//clutter_event_put(priv->m_pressEvent.event);
			}


//			else
//			{
//				priv->m_ignore_press = FALSE;
//
//				gboolean result=FALSE;
//				g_signal_emit (self, signals[ENABLE_WEBPAGE_SCROLL], 0,TRUE,&result);
//				if(result)
//				{
//					mx_kinetic_scroll_view_set_use_captured(MX_KINETIC_SCROLL_VIEW(self),FALSE);
//					priv->m_page_in_scroll_mode = TRUE;
//				}
//				/*priv->m_pressEvent.event->flags = CLUTTER_EVENT_FLAG_SYNTHETIC;
//							    priv->m_pressEvent.event->time = clutter_get_current_event_time();
//							    clutter_actor_event(priv->m_pressEvent.actor, (ClutterEvent*)priv->m_pressEvent.event, FALSE);*/
//
//				//lightwood_webview_widget_gesture_handler_set_double_tap_alert(CLUTTER_ACTOR(self),!use_captured_event);
//
///*				_popup.enType = MMD_MSG_BOX_TYPE_CONFIRM_OPTION_ONE;
//				_popup.enMsgDisplay = MMD_MSG_SINGLE_LINE;
//				_popup.button1Text = g_strdup("OK");
//				_popup.pMessage =(gchar *)g_strdup("Enabled scroll in webpage");
//				_popup.pCallBk =NULL ;_popup.pBtn1CallBk =NULL; 	_popup.pUserData =NULL;
//				_popup.timeout = 50;
//				 mmdlib_message_box_create_and_show(0xff, _popup);*/
//			}

			if(priv->m_pressEvent.event)
		    			clutter_event_free((ClutterEvent*)priv->m_pressEvent.event);
			priv->m_pressEvent.event = NULL;
	}



	g_free(long_press_evt);
	long_press_evt = NULL;

	return FALSE;
}


static gboolean
_lightwood_webview_widget_gesture_handler_long_touch_press_event_cb(gpointer data)
{


	LongPressEventData* long_press_evt = (LongPressEventData*)data;
	LightwoodWebViewWidgetGestureHandler* self = long_press_evt->self;
	LightwoodWebViewWidgetGestureHandlerPrivate* priv = WEBVIEW_WIDGET_GESTURE_HANDLER_PRIVATE(self);
	if( priv->m_pressEvent.touch_event &&
		priv->m_longpress_event_id &&
		(long_press_evt->long_press_evt_id == priv->m_longpress_event_id))
	{
			char* href = NULL;
			if(priv->m_clickable_or_scrollableElement)
				if(g_object_class_find_property(G_OBJECT_CLASS(WEBKIT_DOM_ELEMENT_GET_CLASS(priv->m_clickable_or_scrollableElement)),"href"))
					g_object_get(priv->m_clickable_or_scrollableElement, "href", &href, NULL);
			if(href)
			{
/*				WebKitDOMCSSStyleDeclaration* style = NULL;
				g_object_get(nearest_node, "style", &style, NULL);
				webkit_dom_css_style_declaration_set_property(style, "color", "brown", "", NULL);
				webkit_dom_css_style_declaration_set_property(style, "text-decoration", "blink", "", NULL);*/
				//g_print("\n long press alert on link...%s",href);
				priv->m_new_view_enable=TRUE;
				g_signal_emit_by_name(priv->m_webview_widget,"new_view_requested",href);

			}
			else if(WEBKIT_DOM_IS_HTML_IMAGE_ELEMENT(priv->m_clickable_or_scrollableElement))
			{
				//g_print("\n long press alert on image...");
				g_signal_emit_by_name(priv->m_webview_widget,"save_image_requested",
						webkit_dom_html_image_element_get_src(WEBKIT_DOM_HTML_IMAGE_ELEMENT(priv->m_clickable_or_scrollableElement)));
			}
			else
			{
				priv->m_pressEvent.touch_event->flags = CLUTTER_EVENT_FLAG_SYNTHETIC;
				priv->m_pressEvent.touch_event->time = clutter_get_current_event_time();
				//clutter_actor_event(priv->m_pressEvent.actor, (ClutterEvent*)priv->m_pressEvent.event, TRUE);
				//clutter_event_put(priv->m_pressEvent.event);
			}


			if(priv->m_pressEvent.touch_event)
		    			clutter_event_free((ClutterEvent*)priv->m_pressEvent.touch_event);
			priv->m_pressEvent.touch_event = NULL;
	}



	g_free(long_press_evt);
	long_press_evt = NULL;

	return FALSE;
}








static gboolean
_lightwood_webview_widget_gesture_handler_handle_button_press_event(LightwoodWebViewWidgetGestureHandler * self, ClutterEvent* event)
{
    LightwoodWebViewWidgetGestureHandlerPrivate* priv = WEBVIEW_WIDGET_GESTURE_HANDLER_PRIVATE(self);
	ClutterButtonEvent* button_evt = (ClutterButtonEvent*)event;

	if(button_evt->flags == CLUTTER_EVENT_FLAG_SYNTHETIC)
	{
		priv->m_pressEvent.passthrough = TRUE;
		return CLUTTER_EVENT_PROPAGATE;
	}
	if(!priv->m_pressEvent.event || ((((ClutterButtonEvent*)event)->time - priv->m_pressEvent.event->time) > 3000))
		priv->m_touchpoints=0;

	priv->m_touchpoints++;

	//ignore 2 conecutive pressed without release
	if(priv->m_touchpoints > 1)
	{
		//g_print("\n ******two conecutive presses invalidating.......");
		 priv->m_pressEvent.passthrough = TRUE;
		 priv->m_longpress_event_id =0;
	     return CLUTTER_EVENT_PROPAGATE;
	}

	 if ( button_evt->click_count == 2 )
	 {
		 if( (priv->m_page_in_scroll_mode  ||
			 priv->m_is_element_scrollable) &&
					 priv->m_double_tap_enabled)

		 {
			// g_print("\n double tap press on scrollable element.....");

			 return CLUTTER_EVENT_STOP;
		 }
		 else
		 {
			// g_print("\n double tap press on page.....");
			 priv->m_pressEvent.passthrough = TRUE;
			 return CLUTTER_EVENT_PROPAGATE;
		 }

	 }
	 else
		 g_print("\n single tap press.....");

		//if(priv->m_long_press_enabled)
		{
			LongPressEventData* long_press_evt = g_new(LongPressEventData,1);
			long_press_evt->self = self;
			long_press_evt->long_press_evt_id = g_timeout_add(LIGHTWOOD_WEBVIEW_LONG_PRESS_DELAY,
												_lightwood_webview_widget_gesture_handler_long_press_event_cb,long_press_evt);
			priv->m_longpress_event_id = long_press_evt->long_press_evt_id;
		}

		priv->m_pressEvent.actor = CLUTTER_ACTOR(priv->m_webview);
		priv->m_pressEvent.passthrough = FALSE;
		priv->m_pressEvent.event = (ClutterButtonEvent*)clutter_event_copy((ClutterEvent*)button_evt);

		return CLUTTER_EVENT_PROPAGATE;
}


static gboolean
_lightwood_webview_widget_gesture_handler_handle_touch_press_event(LightwoodWebViewWidgetGestureHandler * self, ClutterEvent* event)
{


	LightwoodWebViewWidgetGestureHandlerPrivate* priv = WEBVIEW_WIDGET_GESTURE_HANDLER_PRIVATE(self);
	ClutterTouchEvent* button_evt = (ClutterTouchEvent*)event;
            
                       priv->m_pressEvent.touch_press_x=button_evt->x;
                      priv->m_pressEvent.touch_press_y=button_evt->y;
		if(button_evt->flags == CLUTTER_EVENT_FLAG_SYNTHETIC)
		{
			priv->m_pressEvent.passthrough = TRUE;
			return CLUTTER_EVENT_PROPAGATE;
		}
		if(!priv->m_pressEvent.touch_event || ((((ClutterTouchEvent*)event)->time - priv->m_pressEvent.touch_event->time) > 3000))
			priv->m_touchpoints=0;

		priv->m_touchpoints++;

		//ignore 2 conecutive pressed without release
		if(priv->m_touchpoints > 1)
		{
			//g_print("\n ******two conecutive presses invalidating.......");
			 priv->m_pressEvent.passthrough = TRUE;
			 priv->m_longpress_event_id =0;
		     return CLUTTER_EVENT_PROPAGATE;
		}

		 if ( priv->m_is_second_tap )
		 {
			 if( (priv->m_page_in_scroll_mode  ||
				 priv->m_is_element_scrollable) &&
						 priv->m_double_tap_enabled)

			 {
				// g_print("\n double tap press on scrollable element.....");

				 return CLUTTER_EVENT_STOP;
			 }
			 else
			 {
				 g_print("\n double TOUCH press on page.....");
				priv->m_pressEvent.touch_event = (ClutterTouchEvent*)clutter_event_copy((ClutterEvent*)button_evt);

                                 priv->m_pressEvent.passthrough = TRUE;
				 return CLUTTER_EVENT_PROPAGATE;
			 }

		 }
		 else
			 g_print("\n single TOUCH  press.....");

                          //g_print("\n TOUCH_EVENT IS ASSIGNED SOME VALUE \n");			
//if(priv->m_long_press_enabled)
			{
				LongPressEventData* long_press_evt = g_new(LongPressEventData,1);
				long_press_evt->self = self;
				long_press_evt->long_press_evt_id = g_timeout_add(LIGHTWOOD_WEBVIEW_LONG_PRESS_DELAY,
						_lightwood_webview_widget_gesture_handler_long_touch_press_event_cb,long_press_evt);
				priv->m_longpress_event_id = long_press_evt->long_press_evt_id;
			}

			priv->m_pressEvent.actor = CLUTTER_ACTOR(priv->m_webview);
			priv->m_pressEvent.passthrough = FALSE;
			priv->m_pressEvent.touch_event = (ClutterTouchEvent*)clutter_event_copy((ClutterEvent*)button_evt);
			return CLUTTER_EVENT_PROPAGATE;
}






static gboolean
_lightwood_webview_widget_gesture_handler_send_scroll_event_cb(ClutterScrollEvent* scroll_event)
{
	webkit_iweb_view_scroll_event(CLUTTER_ACTOR(scroll_event->source),scroll_event);
  	return FALSE;
}


static gboolean
_lightwood_webview_widget_gesture_handler_handle_button_release_event(LightwoodWebViewWidgetGestureHandler * self, ClutterEvent* event)
{
	LightwoodWebViewWidgetGestureHandlerPrivate* priv = WEBVIEW_WIDGET_GESTURE_HANDLER_PRIVATE(self);
    ClutterButtonEvent* cur_buttonEvent = (ClutterButtonEvent*)event;
    priv->m_new_view_enable=FALSE;
    //g_signal_handlers_disconnect_by_func(actor,_lightwood_webview_widget_gesture_handler_motion_event_cb,self);
    static float flPrevX = 0, flPrevY = 0;
//    g_signal_handler_unblock (CLUTTER_ACTOR(priv->m_webview_widget),priv->m_capture_signal);
    if(cur_buttonEvent->flags == CLUTTER_EVENT_FLAG_SYNTHETIC)
    return CLUTTER_EVENT_PROPAGATE;

	priv->m_longpress_event_id = 0;
	priv->m_delayed_event_id = 0;
	priv->m_touchpoints--;
	g_assert(priv->m_touchpoints >= 0);
	//ignore single tap if its  scrollable element
    if(!priv->m_pressEvent.passthrough && priv->m_is_element_scrollable && (cur_buttonEvent->click_count == 1))
    {
    	priv->m_prevButtonEvent=(ClutterButtonEvent*)clutter_event_copy(event);
    	if(priv->m_pressEvent.event)
	    	 clutter_event_free((ClutterEvent*)priv->m_pressEvent.event);
	     priv->m_pressEvent.event = NULL;

	 	return CLUTTER_EVENT_PROPAGATE;
    }

	//if double tap clear the cached prev button event and the scheduled delayed event to start fresh again
	//and signal double tap.

    if((priv->m_touchpoints == 0) && (cur_buttonEvent->click_count == 2))
    {
                gdouble distance=0.0;

  		distance = hypot( (flPrevX- cur_buttonEvent->x),
								      (flPrevY - cur_buttonEvent->y));

		gboolean event_handled = CLUTTER_EVENT_PROPAGATE;
		//gboolean result = CLUTTER_EVENT_STOP;
		//g_print("\n **double tap alert**");
		if(priv->m_is_element_scrollable && priv->m_clickable_or_scrollableElement &&
			g_strrstr(
				webkit_dom_element_get_attribute(priv->m_clickable_or_scrollableElement,"id"),"map")
		)
		{

			g_print("\n double tap on map with dist %f",distance);
			ClutterScrollEvent* scroll_event = (ClutterScrollEvent*)clutter_event_copy(event);
			scroll_event->x = ((ClutterButtonEvent*)priv->m_prevButtonEvent)->x;
			scroll_event->y = ((ClutterButtonEvent*)priv->m_prevButtonEvent)->y;
			scroll_event->type = CLUTTER_SCROLL;
			scroll_event->modifier_state=0;
			scroll_event->direction = distance > 50?CLUTTER_SCROLL_DOWN:CLUTTER_SCROLL_UP;
			//event_handled = !webkit_iweb_view_scroll_event(CLUTTER_ACTOR(priv->m_webview),scroll_event);
			g_idle_add((GSourceFunc)_lightwood_webview_widget_gesture_handler_send_scroll_event_cb,scroll_event);

			cur_buttonEvent->click_count=1;
			event_handled = CLUTTER_EVENT_PROPAGATE;
		}
		else
		{
			

lightwood_webview_widget_context_sensitive_zoom(self->priv->m_webview_widget, distance < 50.0, cur_buttonEvent->x, cur_buttonEvent->y);
			/*g_signal_emit_by_name((LightwoodWebViewWidget*)data,"double_tap_alert",event,&result);
			if(result)
			{
				cur_buttonEvent->click_count=1;
				event_handled = FALSE;
			}*/
		}

 	     if(priv->m_pressEvent.event)
 	    	 clutter_event_free((ClutterEvent*)priv->m_pressEvent.event);
 	     priv->m_pressEvent.event = NULL;
 	    if(priv->m_prevButtonEvent)
 	    	  clutter_event_free((ClutterEvent*)priv->m_prevButtonEvent);
 	      priv->m_prevButtonEvent = NULL;
 	     flPrevX = cur_buttonEvent->x;
 	     flPrevY = cur_buttonEvent->y;
 	     return event_handled;
    }


    flPrevX = cur_buttonEvent->x;
    flPrevY = cur_buttonEvent->y;
    //ignore if press event is invalidated say due to let in,lonpress or page scroll
    if(!priv->m_pressEvent.event)
    {
    	return CLUTTER_EVENT_STOP;
    }

	//ignore if press and release are at different coordinates(beyond drag_threshold)
/*	gdouble distance = sqrt( pow(priv->m_pressEvent.event->x - cur_buttonEvent->x,2) +
						  pow(priv->m_pressEvent.event->y - cur_buttonEvent->y,2)
					   );
*/
	gdouble distance = hypot( (priv->m_pressEvent.event->x - cur_buttonEvent->x),
						      (priv->m_pressEvent.event->y - cur_buttonEvent->y)
					        );


	if( !priv->m_pressEvent.passthrough && (distance > ((gfloat)LIGHTWOOD_WEBVIEW_DRAG_THRESHOLD)) ) //(gfloat)LIGHTWOOD_WEBVIEW_DRAG_THRESHOLD
	{
	    clutter_event_free((ClutterEvent*)priv->m_pressEvent.event);
		priv->m_pressEvent.event = NULL;
    	return CLUTTER_EVENT_STOP;
	}
	/*g_print("\n ***press event=%f,%f release event=%f,%f,dist=%f(threshold=25)",
			priv->m_pressEvent.event->x,priv->m_pressEvent.event->y,
			cur_buttonEvent->x,cur_buttonEvent->y,
			distance);*/
    clutter_event_free((ClutterEvent*)priv->m_pressEvent.event);
	priv->m_pressEvent.event = NULL;

	if(priv->m_pressEvent.passthrough )
	{
		//priv->m_prevButtonEvent=(ClutterButtonEvent*)clutter_event_copy(event);
		priv->m_pressEvent.passthrough = FALSE;

		//g_print("\n letting the release event...");

		return CLUTTER_EVENT_PROPAGATE;
	}

    if (cur_buttonEvent->click_count == 1)
    {
    	gboolean event_handled = FALSE;
		DelayedEventData* buttonEvent = g_new0(DelayedEventData,1);
		buttonEvent->actor = CLUTTER_ACTOR(priv->m_webview);
		buttonEvent->self = self;
		buttonEvent->event = (ClutterButtonEvent*)clutter_event_copy(event);
		priv->m_prevButtonEvent=(ClutterButtonEvent*)clutter_event_copy(event);
		priv->m_delayed_event_id = buttonEvent->delayed_event_id =
				g_timeout_add(priv->m_double_tap_enabled?LIGHTWOOD_WEBVIEW_DOUBLE_TAP_DELAY+5:255,_lightwood_webview_widget_gesture_handler_delayed_button_click_event, buttonEvent);

		event_handled = CLUTTER_EVENT_STOP;


    	return event_handled;
    }

   return CLUTTER_EVENT_PROPAGATE;
}


static gboolean b_webview_widget_handler_double_tap_timer_clear(gpointer pUserData)
{
	LightwoodWebViewWidgetGestureHandlerPrivate* priv = WEBVIEW_WIDGET_GESTURE_HANDLER_PRIVATE(pUserData);
	priv->m_is_second_tap = FALSE;
	priv->m_doubletap_timer = 0;

    return FALSE;
}


static gboolean
_lightwood_webview_widget_gesture_handler_handle_touch_release_event(LightwoodWebViewWidgetGestureHandler * self, ClutterEvent* event)
{

	LightwoodWebViewWidgetGestureHandlerPrivate* priv = WEBVIEW_WIDGET_GESTURE_HANDLER_PRIVATE(self);
    ClutterTouchEvent* cur_buttonEvent = (ClutterTouchEvent*)event;
    priv->m_new_view_enable=FALSE;
    static float flPrevX = 0, flPrevY = 0;
    //g_signal_handlers_disconnect_by_func(actor,_lightwood_webview_widget_gesture_handler_motion_event_cb,self);

    // g_print("\n _lightwood_webview_widget_gesture_handler_handle_touch_release_event  \n");

    if(cur_buttonEvent->flags == CLUTTER_EVENT_FLAG_SYNTHETIC)
    return CLUTTER_EVENT_PROPAGATE;

	priv->m_longpress_event_id = 0;
	priv->m_delayed_event_id = 0;
	priv->m_touchpoints--;
	g_assert(priv->m_touchpoints >= 0);
	//ignore single tap if its  scrollable element
    if(!priv->m_pressEvent.passthrough && priv->m_is_element_scrollable && (!priv->m_is_second_tap))
    {
    	priv->m_prevTouchEvent=(ClutterTouchEvent*)clutter_event_copy(event);
    	if(priv->m_pressEvent.touch_event)
	    	 clutter_event_free((ClutterEvent*)priv->m_pressEvent.touch_event);
	     priv->m_pressEvent.touch_event = NULL;

	 	return CLUTTER_EVENT_PROPAGATE;
    }

	//if double tap clear the cached prev button event and the scheduled delayed event to start fresh again
	//and signal double tap.
 if(NULL == priv->m_pressEvent.touch_event)
 {
      //   g_print("\n @@@@@@@@@@@@ TOUCH_EVENT IS NULL \n ");
 	if (priv->m_doubletap_timer != 0)
 	            g_source_remove(priv->m_doubletap_timer);
 	priv->m_doubletap_timer = 0;
        priv->m_is_second_tap=FALSE;
	 return CLUTTER_EVENT_PROPAGATE;
 }


	 if((priv->m_touchpoints == 0) && (priv->m_is_second_tap))
    {
    //g_print("\n double tap touch release \n");


                gdouble distance=0.0;
                if(cur_buttonEvent)

		{
  		distance = hypot( (flPrevX - cur_buttonEvent->x),
								      (flPrevY - cur_buttonEvent->y));
		}
		gboolean event_handled = CLUTTER_EVENT_PROPAGATE;
		//gboolean result = CLUTTER_EVENT_STOP;
		//g_print("\n **double tap alert**");
		if(priv->m_is_element_scrollable && priv->m_clickable_or_scrollableElement &&
			g_strrstr(
				webkit_dom_element_get_attribute(priv->m_clickable_or_scrollableElement,"id"),"map")
		)
		{

			//g_print("\n double tap on map with dist %f",distance);
			ClutterScrollEvent* scroll_event = (ClutterScrollEvent*)clutter_event_copy(event);
			scroll_event->x = flPrevX;//((ClutterTouchEvent*)priv->m_prevTouchEvent)->x;
			scroll_event->y = flPrevY;//((ClutterTouchEvent*)priv->m_prevTouchEvent)->y;
			scroll_event->type = CLUTTER_SCROLL;
			scroll_event->modifier_state=0;
			scroll_event->direction = distance > 50?CLUTTER_SCROLL_DOWN:CLUTTER_SCROLL_UP;
			//event_handled = !webkit_iweb_view_scroll_event(CLUTTER_ACTOR(priv->m_webview),scroll_event);
			g_idle_add((GSourceFunc)_lightwood_webview_widget_gesture_handler_send_scroll_event_cb,scroll_event);
			//cur_buttonEvent->click_count=1;
			event_handled = CLUTTER_EVENT_PROPAGATE;
		}
		else
		{
                         
			//  g_print("\n TOUCH  CONTEXT ZOOM distance %lf %lf %lf \n",distance,cur_buttonEvent->x,cur_buttonEvent->y);
				lightwood_webview_widget_context_sensitive_zoom (self->priv->m_webview_widget, distance < 50.0, cur_buttonEvent->x, cur_buttonEvent->y);
			/*g_signal_emit_by_name((LightwoodWebViewWidget*)data,"double_tap_alert",event,&result);
			if(result)
			{
				cur_buttonEvent->click_count=1;
				event_handled = FALSE;
			}*/
		}

 	     if(priv->m_pressEvent.touch_event)
 	    	 clutter_event_free((ClutterEvent*)priv->m_pressEvent.touch_event);
 	     priv->m_pressEvent.touch_event = NULL;
 	    if(priv->m_prevTouchEvent)
 	    	  clutter_event_free((ClutterEvent*)priv->m_prevTouchEvent);
 	      priv->m_prevTouchEvent = NULL;
             
 	     priv->m_is_second_tap=FALSE;
 	    flPrevX = cur_buttonEvent->x;
 	    flPrevY = cur_buttonEvent->y;
 	     return event_handled;
    }
    else
    {
   // g_print("\n not a double tap touch\n");
    	priv->m_is_second_tap=TRUE;
    	flPrevX = cur_buttonEvent->x;
        flPrevY = cur_buttonEvent->y;
    	if (priv->m_doubletap_timer != 0)
    	            g_source_remove(priv->m_doubletap_timer);
    	priv->m_doubletap_timer = 0;
    	priv->m_doubletap_timer = g_timeout_add(
    	                                         LIGHTWOOD_WEBVIEW_DOUBLE_TAP_DELAY,
    	                                         b_webview_widget_handler_double_tap_timer_clear,self);

    }
    //ignore if press event is invalidated say due to let in,lonpress or page scroll
    if(!priv->m_pressEvent.touch_event)
    {
    	return CLUTTER_EVENT_STOP;
    }

	//ignore if press and release are at different coordinates(beyond drag_threshold)
/*	gdouble distance = sqrt( pow(priv->m_pressEvent.event->x - cur_buttonEvent->x,2) +
						  pow(priv->m_pressEvent.event->y - cur_buttonEvent->y,2)
					   );
*/
	gdouble distance = hypot( (priv->m_pressEvent.touch_event->x - cur_buttonEvent->x),
						      (priv->m_pressEvent.touch_event->y - cur_buttonEvent->y)
					        );
	flPrevX = cur_buttonEvent->x;
	flPrevY = cur_buttonEvent->y;

	if( !priv->m_pressEvent.passthrough && (distance > ((gfloat)LIGHTWOOD_WEBVIEW_DRAG_THRESHOLD)) ) //(gfloat)LIGHTWOOD_WEBVIEW_DRAG_THRESHOLD
	{
	    clutter_event_free((ClutterEvent*)priv->m_pressEvent.touch_event);
		priv->m_pressEvent.touch_event = NULL;
    	return CLUTTER_EVENT_STOP;
	}
	/*g_print("\n ***press event=%f,%f release event=%f,%f,dist=%f(threshold=25)",
			priv->m_pressEvent.touch_event->x,priv->m_pressEvent.touch_event->y,
			cur_buttonEvent->x,cur_buttonEvent->y,
			distance);*/
    clutter_event_free((ClutterEvent*)priv->m_pressEvent.touch_event);
	priv->m_pressEvent.touch_event = NULL;

	if(priv->m_pressEvent.passthrough )
	{
		//priv->m_prevButtonEvent=(ClutterButtonEvent*)clutter_event_copy(event);
		priv->m_pressEvent.passthrough = FALSE;

		//g_print("\n letting the release event...");

		return CLUTTER_EVENT_PROPAGATE;
	}
if(!priv->m_is_second_tap)
    {
    	gboolean event_handled = FALSE;
		DelayedEventData* buttonEvent = g_new0(DelayedEventData,1);
		buttonEvent->actor = CLUTTER_ACTOR(priv->m_webview);
		buttonEvent->self = self;
		buttonEvent->touch_event = (ClutterTouchEvent*)clutter_event_copy(event);
		priv->m_prevTouchEvent=(ClutterTouchEvent*)clutter_event_copy(event);
		priv->m_delayed_event_id = buttonEvent->delayed_event_id =
				g_timeout_add(priv->m_double_tap_enabled?LIGHTWOOD_WEBVIEW_DOUBLE_TAP_DELAY+5:255,_lightwood_webview_widget_gesture_handler_delayed_touch_press_event, buttonEvent);

		event_handled = CLUTTER_EVENT_STOP;


    	return event_handled;
    }

   return CLUTTER_EVENT_PROPAGATE;
}





/*typedef enum
{
	INPAGE_GESTURE_NONE,
	INPAGE_GESTURE_SCROLL,
	INPAGE_GESTURE_MOTION
}eMoveGesture;

static eMoveGesture inpage_gesture=INPAGE_GESTURE_NONE;*/
static gfloat motion_y=0.0;
static gboolean
_lightwood_webview_widget_gesture_handler_handle_motion_event(LightwoodWebViewWidgetGestureHandler * self,ClutterEvent* event)
{
	LightwoodWebViewWidgetGestureHandlerPrivate* priv = WEBVIEW_WIDGET_GESTURE_HANDLER_PRIVATE(self);
    static gboolean evt_handled = CLUTTER_EVENT_PROPAGATE;

    priv->m_longpress_event_id = 0;

    //return FALSE;
    if(priv->m_pressEvent.event && (priv->m_touchpoints == 1))
    {
		if(priv->m_is_element_scrollable && priv->m_is_perform_scroll)
		{
			ClutterScrollEvent* scroll_event = (ClutterScrollEvent*)clutter_event_copy(event);
			scroll_event->type = CLUTTER_SCROLL;
			gfloat dist = scroll_event->y - motion_y;
			scroll_event->direction = dist > 0?CLUTTER_SCROLL_UP:CLUTTER_SCROLL_DOWN;
			if(abs(dist) > 27)
			{
				motion_y = scroll_event->y;
				scroll_event->x = priv->m_pressEvent.event->x;
				scroll_event->y = priv->m_pressEvent.event->y;
				//g_print("\n scroll:%f",dist);
				evt_handled = !webkit_iweb_view_scroll_event(CLUTTER_ACTOR(priv->m_webview),scroll_event);
				if(!evt_handled)
					evt_handled = !webkit_iweb_view_motion_event(CLUTTER_ACTOR(priv->m_webview),(ClutterMotionEvent*)event);
			}

			clutter_event_free((ClutterEvent*) priv->m_pressEvent.event);
			priv->m_pressEvent.event = NULL;
		}
	    else
	    {
	       priv->m_pressEvent.passthrough = TRUE;
	       priv->m_delayed_event_id = 0;
	    }

	}
    //g_signal_handler_block(CLUTTER_ACTOR(priv->m_webview_widget),priv->m_capture_signal);
	return evt_handled;
}


static gboolean
_lightwood_webview_widget_gesture_handler_handle_touch_motion_event(LightwoodWebViewWidgetGestureHandler *self, ClutterEvent* event)
{
	LightwoodWebViewWidgetGestureHandlerPrivate* priv = WEBVIEW_WIDGET_GESTURE_HANDLER_PRIVATE(self);
	    static gboolean evt_handled = CLUTTER_EVENT_PROPAGATE;

	    priv->m_longpress_event_id = 0;
ClutterTouchEvent *touch=(ClutterTouchEvent *)event;

gint x_gitter=abs(touch->x- priv->m_pressEvent.touch_press_x);
gint y_gitter=abs(touch->y-priv->m_pressEvent.touch_press_y);

	    //return FALSE;
	    if(priv->m_pressEvent.touch_event && (priv->m_touchpoints == 1)&&((x_gitter > LIGHTWOOD_WEBVIEW_DRAG_THRESHOLD)||(y_gitter > LIGHTWOOD_WEBVIEW_DRAG_THRESHOLD) ))
	    {
			if(priv->m_is_element_scrollable && priv->m_is_perform_scroll)
			{
				ClutterScrollEvent* scroll_event = (ClutterScrollEvent*)clutter_event_copy(event);
				scroll_event->type = CLUTTER_SCROLL;
				gfloat dist = scroll_event->y - motion_y;
				scroll_event->direction = dist > 0?CLUTTER_SCROLL_UP:CLUTTER_SCROLL_DOWN;
				if(abs(dist) > 27)
				{
					motion_y = scroll_event->y;
					scroll_event->x = priv->m_pressEvent.touch_event->x;
					scroll_event->y = priv->m_pressEvent.touch_event->y;
					//g_print("\n scroll:%f",dist);
					evt_handled = !webkit_iweb_view_scroll_event(CLUTTER_ACTOR(priv->m_webview),scroll_event);
					if(!evt_handled)
						evt_handled = !webkit_iweb_view_motion_event(CLUTTER_ACTOR(priv->m_webview),(ClutterMotionEvent*)event);
				}
                                clutter_event_free((ClutterEvent*) priv->m_pressEvent.touch_event);

				priv->m_pressEvent.touch_event = NULL;
			}
		    else
		    {
		       priv->m_pressEvent.passthrough = TRUE;
		       priv->m_delayed_event_id = 0;
		    }
                
                     clutter_event_free((ClutterEvent*) priv->m_pressEvent.touch_event);
                     priv->m_pressEvent.touch_event = NULL;
		}

		return evt_handled;
}



static gboolean
_lightwood_webview_widget_gesture_handler_delayed_button_click_event(gpointer user_data)
{
	DelayedEventData* delayed_event = (DelayedEventData*)user_data;
	LightwoodWebViewWidgetGestureHandlerPrivate* priv = WEBVIEW_WIDGET_GESTURE_HANDLER_PRIVATE(delayed_event->self);

	/*if( priv->m_prev_buttonEvent.event != delayed_event->event)
	{
	    g_free(delayed_event);
		 return FALSE;
	}*/
	if(priv->m_delayed_event_id == 0)
	{
	    clutter_event_free((ClutterEvent*)delayed_event->event);
	    g_free(delayed_event);
		return G_SOURCE_REMOVE;
	}
	delayed_event->delayed_event_id  = 0;

	/*gfloat x_adj=0.0;gfloat y_adj=0.0;
	WebKitDOMElement* clickable_node = lightwood_webview_get_clickable_node(LIGHTWOOD_WEBVIEW(priv->m_webview),delayed_event->event,LIGHTWOOD_WEBVIEW_LINK_CLICK_THRESHOLD,&x_adj,&y_adj);
	if(x_adj != 0 || y_adj != 0) g_print("\n x_adj,y_adj=%f,%f",x_adj,y_adj);
	delayed_event->event->x += x_adj;
	delayed_event->event->y += y_adj;*/

	gboolean event_handled = FALSE;
	g_print("\n **single tap alert x,y=%f,%f",delayed_event->event->x,delayed_event->event->y);
	g_signal_emit_by_name(priv->m_webview_widget,"single_tap_alert",delayed_event->event,&event_handled);
	if(event_handled) return G_SOURCE_REMOVE;

	//delayed_event->event->x += priv->m_click_x_adj;
	//delayed_event->event->y += priv->m_click_y_adj;
	delayed_event->event->type = CLUTTER_BUTTON_RELEASE;
	delayed_event->event->flags = CLUTTER_EVENT_FLAG_SYNTHETIC;
	delayed_event->event->time = clutter_get_current_event_time()+1;
	//delayed_event->event->source = priv->m_webview;
    //clutter_actor_event(delayed_event->actor, (ClutterEvent*)delayed_event->event, TRUE);
	clutter_event_put((ClutterEvent*)delayed_event->event);

/*	if(WEBKIT_DOM_IS_HTML_ANCHOR_ELEMENT(clickable_node))
	{
		 WebKitDOMCSSStyleDeclaration* style = NULL;
    	 g_object_get(clickable_node, "style", &style, NULL);
    	 webkit_dom_css_style_declaration_set_property(style, "color", "brown", "", NULL);
    	 webkit_dom_css_style_declaration_set_property(style, "text-decoration", "blink", "", NULL);
	}*/


    return G_SOURCE_REMOVE;
}

static gboolean
_lightwood_webview_widget_gesture_handler_delayed_touch_press_event(gpointer user_data)
{
	DelayedEventData* delayed_event = (DelayedEventData*)user_data;
	LightwoodWebViewWidgetGestureHandlerPrivate* priv = WEBVIEW_WIDGET_GESTURE_HANDLER_PRIVATE(delayed_event->self);

	/*if( priv->m_prev_buttonEvent.event != delayed_event->event)
	{
	    g_free(delayed_event);
		 return FALSE;
	}*/
	if(priv->m_delayed_event_id == 0)
	{
	    clutter_event_free((ClutterEvent*)delayed_event->touch_event);
	    g_free(delayed_event);
		return G_SOURCE_REMOVE;
	}
	delayed_event->delayed_event_id  = 0;

	/*gfloat x_adj=0.0;gfloat y_adj=0.0;
	WebKitDOMElement* clickable_node = lightwood_webview_get_clickable_node(LIGHTWOOD_WEBVIEW(priv->m_webview),delayed_event->event,LIGHTWOOD_WEBVIEW_LINK_CLICK_THRESHOLD,&x_adj,&y_adj);
	if(x_adj != 0 || y_adj != 0) g_print("\n x_adj,y_adj=%f,%f",x_adj,y_adj);
	delayed_event->event->x += x_adj;
	delayed_event->event->y += y_adj;*/

	gboolean event_handled = FALSE;
	g_print("\n **single tap alert x,y=%f,%f",delayed_event->touch_event->x,delayed_event->touch_event->y);
	g_signal_emit_by_name(priv->m_webview_widget,"single_tap_alert",delayed_event->touch_event,&event_handled);
	if(event_handled) return G_SOURCE_REMOVE;

	//delayed_event->event->x += priv->m_click_x_adj;
	//delayed_event->event->y += priv->m_click_y_adj;
	delayed_event->touch_event->type = CLUTTER_TOUCH_END;
	delayed_event->touch_event->flags = CLUTTER_EVENT_FLAG_SYNTHETIC;
	delayed_event->touch_event->time = clutter_get_current_event_time()+1;
	//delayed_event->event->source = priv->m_webview;
    //clutter_actor_event(delayed_event->actor, (ClutterEvent*)delayed_event->event, TRUE);
	clutter_event_put((ClutterEvent*)delayed_event->touch_event);

/*	if(WEBKIT_DOM_IS_HTML_ANCHOR_ELEMENT(clickable_node))
	{
		 WebKitDOMCSSStyleDeclaration* style = NULL;
    	 g_object_get(clickable_node, "style", &style, NULL);
    	 webkit_dom_css_style_declaration_set_property(style, "color", "brown", "", NULL);
    	 webkit_dom_css_style_declaration_set_property(style, "text-decoration", "blink", "", NULL);
	}*/


    return G_SOURCE_REMOVE;
}



