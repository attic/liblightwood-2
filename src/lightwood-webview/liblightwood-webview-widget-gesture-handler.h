/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* liblightwood-webview-widget-gesture-handler.h */

#ifndef _LIGHTWOOD_WEBVIEW_WIDGET_GESTURE_HANDLER_H
#define _LIGHTWOOD_WEBVIEW_WIDGET_GESTURE_HANDLER_H

#include <glib-object.h>
#include <clutter/clutter.h>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-prototypes"
#include <mx/mx.h>
#pragma GCC diagnostic pop

#include "liblightwood-webview-widget.h"

G_BEGIN_DECLS

#define LIGHTWOOD_TYPE_WEBVIEW_WIDGET_GESTURE_HANDLER lightwood_webview_widget_gesture_handler_get_type()

#define LIGHTWOOD_WEBVIEW_WIDGET_GESTURE_HANDLER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  LIGHTWOOD_TYPE_WEBVIEW_WIDGET_GESTURE_HANDLER, LightwoodWebViewWidgetGestureHandler))

#define LIGHTWOOD_WEBVIEW_WIDGET_GESTURE_HANDLER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  LIGHTWOOD_TYPE_WEBVIEW_WIDGET_GESTURE_HANDLER, LightwoodWebViewWidgetGestureHandlerClass))

#define LIGHTWOOD_IS_WEBVIEW_WIDGET_GESTURE_HANDLER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  LIGHTWOOD_TYPE_WEBVIEW_WIDGET_GESTURE_HANDLER))

#define LIGHTWOOD_IS_WEBVIEW_WIDGET_GESTURE_HANDLER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  LIGHTWOOD_TYPE_WEBVIEW_WIDGET_GESTURE_HANDLER))

#define LIGHTWOOD_WEBVIEW_WIDGET_GESTURE_HANDLER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  LIGHTWOOD_TYPE_WEBVIEW_WIDGET_GESTURE_HANDLER, LightwoodWebViewWidgetGestureHandlerClass))

typedef struct _LightwoodWebViewWidgetGestureHandler LightwoodWebViewWidgetGestureHandler;
typedef struct _LightwoodWebViewWidgetGestureHandlerClass LightwoodWebViewWidgetGestureHandlerClass;
typedef struct _LightwoodWebViewWidgetGestureHandlerPrivate LightwoodWebViewWidgetGestureHandlerPrivate;

struct _LightwoodWebViewWidgetGestureHandler
{
  GObject parent;

  LightwoodWebViewWidgetGestureHandlerPrivate *priv;
};

struct _LightwoodWebViewWidgetGestureHandlerClass
{
  GObjectClass parent_class;
};

GType lightwood_webview_widget_gesture_handler_get_type (void) G_GNUC_CONST;

LightwoodWebViewWidgetGestureHandler *lightwood_webview_widget_gesture_handler_new (LightwoodWebViewWidget *webview_widget);
void lightwood_webview_widget_gesture_handler_enable_gesture(LightwoodWebViewWidgetGestureHandler* self,gboolean is_enabled);
G_END_DECLS

#endif /* _LIGHTWOOD_WEBVIEW_WIDGET_GESTURE_HANDLER_H */
