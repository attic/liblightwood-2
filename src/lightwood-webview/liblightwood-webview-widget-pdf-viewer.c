/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* liblightwood-webview-widget-pdf-viewer.c */

#include "liblightwood-webview-widget-pdf-viewer.h"
#include <glib/poppler.h>
#include <cairo.h>
#include <stdlib.h>
#include <math.h>

G_DEFINE_TYPE (LightwoodWebViewWidgetPdfViewer, lightwood_webview_widget_pdf_viewer, MX_TYPE_KINETIC_SCROLL_VIEW)

#define WEBVIEW_WIDGET_PDF_VIEWER_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), LIGHTWOOD_TYPE_WEBVIEW_WIDGET_PDF_VIEWER, LightwoodWebViewWidgetPdfViewerPrivate))

struct _LightwoodWebViewWidgetPdfViewerPrivate
{
	ClutterCanvas* 		m_pdf_canvas;
	ClutterActor*  		m_pdf_actor;
	PopplerDocument*	m_pdf_document;

	guint 		   		m_idle_resize_id;
	gfloat				m_scale_factor;
};

static gboolean
_lightwood_webview_widget_pdf_viewer_render_pdf_cb(ClutterCanvas* texture,cairo_t* cr,gint childWidth,gint childHeight,LightwoodWebViewWidgetPdfViewer* self);
static void
lightwood_webview_widget_pdf_viewer_get_property (GObject    *object,
                                              guint       property_id,
                                              GValue     *value,
                                              GParamSpec *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
lightwood_webview_widget_pdf_viewer_set_property (GObject      *object,
                                              guint         property_id,
                                              const GValue *value,
                                              GParamSpec   *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
lightwood_webview_widget_pdf_viewer_dispose (GObject *object)
{
  G_OBJECT_CLASS (lightwood_webview_widget_pdf_viewer_parent_class)->dispose (object);
}

static void
lightwood_webview_widget_pdf_viewer_class_init (LightwoodWebViewWidgetPdfViewerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  g_type_class_add_private (klass, sizeof (LightwoodWebViewWidgetPdfViewerPrivate));

  object_class->get_property = lightwood_webview_widget_pdf_viewer_get_property;
  object_class->set_property = lightwood_webview_widget_pdf_viewer_set_property;
  object_class->dispose = lightwood_webview_widget_pdf_viewer_dispose;
}


/*static gboolean
idle_resize_cb (LightwoodWebViewWidgetPdfViewer* self)
{

  float width, height;
   match the canvas size to the actor's
  clutter_actor_get_size (self->priv->m_pdf_actor, &width, &height);
  clutter_canvas_set_size (CLUTTER_CANVAS (clutter_actor_get_content (self->priv->m_pdf_actor)),
                           ceilf (width),
                           ceilf (height));

   unset the guard
  self->priv->m_idle_resize_id = 0;

   remove the timeout
  return G_SOURCE_REMOVE;
}*/

/*static void
lightwood_webview_widget_pdf_viewer_on_pdf_actor_resize_cb(ClutterActor* actor,const ClutterActorBox *allocation,
													ClutterAllocationFlags  flags,LightwoodWebViewWidgetPdfViewer* self)
{
	   throttle multiple actor allocations to one canvas resize; we use a guard
	   * variable to avoid queueing multiple resize operations

	  if (self->priv->m_idle_resize_id == 0)
		  self->priv->m_idle_resize_id = clutter_threads_add_timeout (1000, (GSourceFunc)idle_resize_cb, self);
}*/

static void
lightwood_webview_widget_pdf_viewer_init (LightwoodWebViewWidgetPdfViewer *self)
{
	LightwoodWebViewWidgetPdfViewerPrivate* priv = self->priv = WEBVIEW_WIDGET_PDF_VIEWER_PRIVATE (self);
  ClutterActor* view_port = mx_viewport_new();
  //mx_bin_set_child(MX_BIN(self),view_port);
 // mx_bin_set_fill(MX_BIN(self),TRUE,TRUE);
  clutter_actor_add_child(CLUTTER_ACTOR(self),view_port);
 clutter_actor_show(CLUTTER_ACTOR(self));
  //clutter_actor_add_constraint(view_port,clutter_bind_constraint_new (self,CLUTTER_BIND_SIZE,0));
  priv->m_scale_factor = 1.0;
  priv->m_pdf_canvas = (ClutterCanvas*)clutter_canvas_new();
  priv->m_pdf_actor = clutter_actor_new();
  clutter_actor_set_content(priv->m_pdf_actor,CLUTTER_CONTENT(priv->m_pdf_canvas));
  //mx_bin_set_child(MX_BIN(view_port),priv->m_pdf_actor);
  //mx_bin_set_fill(MX_BIN(view_port),TRUE,TRUE);
  clutter_actor_add_child(view_port,CLUTTER_ACTOR(priv->m_pdf_actor));
 clutter_actor_show(view_port);
  mx_viewport_set_sync_adjustments(MX_VIEWPORT(view_port),TRUE);

	MxAdjustment *hadjustment, *vadjustment;
	mx_scrollable_get_adjustments(MX_SCROLLABLE(self) ,&hadjustment,&vadjustment);
	mx_adjustment_set_elastic(MX_ADJUSTMENT(hadjustment),TRUE);
	mx_adjustment_set_elastic(MX_ADJUSTMENT(vadjustment),TRUE);

  /* resize the canvas whenever the actor changes size */
 // g_signal_connect (priv->m_pdf_actor, "allocation-changed", G_CALLBACK (lightwood_webview_widget_pdf_viewer_on_pdf_actor_resize_cb), self);

}

LightwoodWebViewWidgetPdfViewer *
lightwood_webview_widget_pdf_viewer_new (void)
{
  return g_object_new (LIGHTWOOD_TYPE_WEBVIEW_WIDGET_PDF_VIEWER, NULL);
}
/*typedef struct
{
	PopplerPage* pdf_page;
	cairo_t* cr;

}PageData;
static gboolean _lightwood_webview_widget_pdf_viewer_render_page(PageData* pdf_data)
{
	double page_width,page_height;
	poppler_page_get_size (pdf_data->pdf_page, &page_width, &page_height);
	poppler_page_render (pdf_data->pdf_page, pdf_data->cr);
	cairo_translate(pdf_data->cr,0,page_height+100);
	g_object_unref (pdf_data->pdf_page);
	g_free(pdf_data);
	return G_SOURCE_REMOVE;
}*/

static gboolean
_lightwood_webview_widget_pdf_viewer_render_pdf_cb(ClutterCanvas* texture,cairo_t* cr,gint childWidth,gint childHeight,LightwoodWebViewWidgetPdfViewer* self)
{
	LightwoodWebViewWidgetPdfViewerPrivate* priv = WEBVIEW_WIDGET_PDF_VIEWER_PRIVATE (self);

	g_assert(priv->m_pdf_document != NULL);
	gint num_pages = poppler_document_get_n_pages(priv->m_pdf_document);
	gint i=0;

cairo_save(cr);
	cairo_scale(cr,priv->m_scale_factor,priv->m_scale_factor);
	double page_width,page_height;
	for(i=0;i<num_pages;i++)
	{
		PopplerPage* pdf_page = poppler_document_get_page(priv->m_pdf_document,i);
		poppler_page_get_size (pdf_page, &page_width, &page_height);
		poppler_page_render (pdf_page, cr);
		cairo_translate(cr,0.0,page_height);
		g_object_unref (pdf_page);
		/*PageData* pdf_data = g_new(PageData,1);
		pdf_data->pdf_page = poppler_document_get_page(priv->m_pdf_document,i);
		pdf_data->cr = cr;
		g_timeout_add(3000,(GSourceFunc)_lightwood_webview_widget_pdf_viewer_render_page,pdf_data);*/
	}
	//cairo_set_operator (cr, CAIRO_OPERATOR_DEST_OVER);
	//cairo_set_source_rgb (cr, 1, 1, 1);
	//cairo_paint (cr);
cairo_restore(cr);


	return TRUE;
}

void lightwood_webview_widget_pdf_viewer_show(LightwoodWebViewWidgetPdfViewer* self,const gfloat scale_factor,const gchar* pdf_uri)
{
	LightwoodWebViewWidgetPdfViewerPrivate* priv = WEBVIEW_WIDGET_PDF_VIEWER_PRIVATE (self);

   	MxAdjustment *hadjustment, *vadjustment;
	mx_scrollable_get_adjustments(MX_SCROLLABLE(self) ,&hadjustment,&vadjustment);
	gfloat cur_x = mx_adjustment_get_value(hadjustment)/priv->m_scale_factor;
	gfloat cur_y = mx_adjustment_get_value(vadjustment)/priv->m_scale_factor;
	priv->m_scale_factor = scale_factor;

	g_print("\n trying to open %s %f",pdf_uri,scale_factor);
	if(pdf_uri != NULL)
	{
		GError* error = NULL;
		priv->m_pdf_document = poppler_document_new_from_file (pdf_uri, NULL, &error);
	}

	g_assert(priv->m_pdf_document != NULL);
	gint num_pages = poppler_document_get_n_pages(priv->m_pdf_document);
	g_print("\n num_pages %d",num_pages);
	gint i=0;
	double docWidth=0, docHeight=0;
	for(i=0;i<num_pages;i++)
	{
		double page_width,page_height;
		poppler_page_get_size (poppler_document_get_page(priv->m_pdf_document,i), &page_width, &page_height);
		docWidth  =  MAX(docWidth,page_width);
		docHeight += page_height;
	}

	gdouble canvas_width = docWidth*priv->m_scale_factor;
	gdouble canvas_height = docHeight*priv->m_scale_factor;
	//gdouble canvas_height = MIN(docHeight*priv->m_scale_factor,6000.0);
	clutter_canvas_set_size(priv->m_pdf_canvas,ceilf(canvas_width),ceilf(canvas_height));
    g_signal_connect(priv->m_pdf_canvas,"draw",(GCallback)_lightwood_webview_widget_pdf_viewer_render_pdf_cb,self);
    clutter_content_invalidate(CLUTTER_CONTENT(priv->m_pdf_canvas));
    g_signal_handlers_disconnect_by_data(priv->m_pdf_canvas,self);
    clutter_actor_set_size(priv->m_pdf_actor,canvas_width,canvas_height);

	gdouble viewportWidth,viewportHeight;
	viewportHeight = clutter_actor_get_height(clutter_actor_get_parent(priv->m_pdf_actor));
	viewportWidth = clutter_actor_get_width(clutter_actor_get_parent(priv->m_pdf_actor));

	mx_adjustment_set_elastic(hadjustment,TRUE);
	mx_adjustment_set_elastic(vadjustment,TRUE);
	g_object_set(G_OBJECT(hadjustment),
	                     "lower", 0.0,
	                     "upper", (gdouble)(canvas_width > viewportWidth?canvas_width:viewportWidth),
	                     "page-size", (gdouble)viewportWidth,
	                     "page-increment", (gdouble)canvas_width / 3,
	                     "step-increment", (gdouble)canvas_width / 12,
	                     NULL);
    g_object_set(G_OBJECT(vadjustment),
                  "lower", 0.0,
                  "upper", (gdouble)(canvas_height > viewportHeight?canvas_height:viewportHeight),
                  "page-size", (gdouble)viewportHeight,
                  "page-increment", (gdouble)canvas_height / 3,
                  "step-increment", (gdouble)canvas_height / 12,
                  NULL);
    mx_adjustment_set_value(vadjustment,cur_y*priv->m_scale_factor);
    mx_adjustment_set_value(hadjustment,cur_x*priv->m_scale_factor);

    clutter_actor_show(CLUTTER_ACTOR(self));


}

void lightwood_webview_widget_pdf_viewer_hide(LightwoodWebViewWidgetPdfViewer* self)
{
	clutter_actor_hide(CLUTTER_ACTOR(self));
}

gfloat lightwood_webview_widget_pdf_viewer_get_zoom_level(LightwoodWebViewWidgetPdfViewer* self)
{
	return self->priv->m_scale_factor;
}
