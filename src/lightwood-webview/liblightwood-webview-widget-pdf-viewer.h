/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* liblightwood-webview-widget-pdf-viewer.h */

#ifndef _LIGHTWOOD_WEBVIEW_WIDGET_PDF_VIEWER_H
#define _LIGHTWOOD_WEBVIEW_WIDGET_PDF_VIEWER_H

#include <glib-object.h>
#include <clutter/clutter.h>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-prototypes"
#include <mx/mx.h>
#pragma GCC diagnostic pop

G_BEGIN_DECLS

#define LIGHTWOOD_TYPE_WEBVIEW_WIDGET_PDF_VIEWER lightwood_webview_widget_pdf_viewer_get_type()

#define LIGHTWOOD_WEBVIEW_WIDGET_PDF_VIEWER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  LIGHTWOOD_TYPE_WEBVIEW_WIDGET_PDF_VIEWER, LightwoodWebViewWidgetPdfViewer))

#define LIGHTWOOD_WEBVIEW_WIDGET_PDF_VIEWER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  LIGHTWOOD_TYPE_WEBVIEW_WIDGET_PDF_VIEWER, LightwoodWebViewWidgetPdfViewerClass))

#define LIGHTWOOD_IS_WEBVIEW_WIDGET_PDF_VIEWER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  LIGHTWOOD_TYPE_WEBVIEW_WIDGET_PDF_VIEWER))

#define LIGHTWOOD_IS_WEBVIEW_WIDGET_PDF_VIEWER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  LIGHTWOOD_TYPE_WEBVIEW_WIDGET_PDF_VIEWER))

#define LIGHTWOOD_WEBVIEW_WIDGET_PDF_VIEWER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  LIGHTWOOD_TYPE_WEBVIEW_WIDGET_PDF_VIEWER, LightwoodWebViewWidgetPdfViewerClass))

typedef struct _LightwoodWebViewWidgetPdfViewer LightwoodWebViewWidgetPdfViewer;
typedef struct _LightwoodWebViewWidgetPdfViewerClass LightwoodWebViewWidgetPdfViewerClass;
typedef struct _LightwoodWebViewWidgetPdfViewerPrivate LightwoodWebViewWidgetPdfViewerPrivate;

struct _LightwoodWebViewWidgetPdfViewer
{
  MxKineticScrollView parent;

  LightwoodWebViewWidgetPdfViewerPrivate *priv;
};

struct _LightwoodWebViewWidgetPdfViewerClass
{
  MxKineticScrollViewClass parent_class;
};

GType lightwood_webview_widget_pdf_viewer_get_type (void) G_GNUC_CONST;

LightwoodWebViewWidgetPdfViewer *lightwood_webview_widget_pdf_viewer_new (void);
void lightwood_webview_widget_pdf_viewer_show(LightwoodWebViewWidgetPdfViewer* self,const gfloat scale_factor,const gchar* pdf_uri);
void lightwood_webview_widget_pdf_viewer_hide(LightwoodWebViewWidgetPdfViewer* self);
gfloat lightwood_webview_widget_pdf_viewer_get_zoom_level(LightwoodWebViewWidgetPdfViewer* self);


G_END_DECLS

#endif /* _LIGHTWOOD_WEBVIEW_WIDGET_PDF_VIEWER_H */
