/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION:liblightwood-webview-widget
 * @Title:LightwoodWebViewWidget
 * @short_description: Renders the web data using webki clutter
 * @see_also: #ClutterActor #Webkit-Clutter
 *
 * #LightwoodWebViewWidget is a widget will create a mx-webview to render data.
 * lightwood_webview_widget_init_session() will initializes webkit.
 * lightwood_webview_widget_new() will create the instance of the widget.Before calling this 
 * #LightwoodWebviewAssistant fileds has to be filled.
 * This widget can also be used to load the url using lightwood_webview_widget_load_url()
 * resizing a widget using lightwood_webview_widget_resize(),Lightwood audio management can 
 * also be handled like pause,lightwood_webview_widget_pause_audio(),play lightwood_webview_widget_play_audio
 * ,stop,lightwood_webview_widget_stop_audio() etc. Zoom level of the page can be controlled by 
 * lightwood_webview_widget_set_zoom_level(),lightwood_webview_widget_context_sensitive_zoom(),refresh by i
 * lightwood_webview_widget_refresh().Loaded page can be saved using lightwood_webview_widget_save_as_pdf().
 * in the widget can be navigated to any position by lightwood_webview_widget_scroll_to().
 * 
 *
 * ## freeing the widget
 *      call g_object_unref() to free the widget.
 *
 *
 */
#include "liblightwood-webview-widget.h"
#include "liblightwood-webview.h"
#include "liblightwood-webview-marshal.h"
#include "liblightwood-webview-widget-fsvideo-player.h"
#include "liblightwood-webview-widget-audio-manager.h"
#include "liblightwood-webview-widget-pdf-viewer.h"
//#include "liblightwood-webview-widget-zoom-handler.h"
#include "liblightwood-webview-widget-gesture-handler.h"
#include "lightwood-enumtypes.h"

#include <webkit/webkitenumtypes.h>
#include <webkit/webkitwebpolicydecision.h>
#include <webkit/webkitwebwindowfeatures.h>
#include <webkit/webkitnetworkresponse.h>
//#include <webkit/webkitmarshal.h>
#include <webkit/webkitgwebframe.h>
#include <webkit/webkitgeolocationpolicydecision.h>
#include <webkit/webkitwebinspector.h>

#include <math.h>
#include <stdlib.h>

#define LIBSOUP_USE_UNSTABLE_REQUEST_API 1
#include <libsoup/soup-cache.h>

#include <libsoup/soup.h>
#include <libsoup/soup-cookie-jar.h>
#include <libsoup/soup-gnome.h>

#include <glib/gi18n.h>
#include <errno.h>

G_DEFINE_TYPE (LightwoodWebViewWidget, lightwood_webview_widget, CLUTTER_TYPE_ACTOR)

enum
{
	PROP_0,

	PROP_SOUP_SESSION,
	PROP_FORM_ASSISTANCE,
	PROP_LOAD_STATUS,
	PROP_GESTURE_HANDLING,
	PROP_URI,
	PROP_TITLE,
	PROP_AUDIO_STATUS,
	PROP_ZOOM_LEVEL,
	PROP_THUMBNAIL,
	PROP_SHOW_THUMBNAIL
};

enum
{
  SINGLE_TAP_ALERT,
  DOUBLE_TAP_ALERT,
  LONG_PRESS_ALERT,
  NEW_PAGE_REQUESTED,
  NEW_VIEW_REQUESTED,
  SAVE_IMAGE_REQUESTED,
  MIME_ALERT,
  RSS_FEED_ALERT,
  VKB_ALERT,
  ENABLE_WEBPAGE_SCROLL,
  WEBVIEW_SPELLER_ALERT,
  AUDIO_STATUS_CHANGE,
  LAST_SIGNAL
};

#define WEBVIEW_WIDGET_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), LIGHTWOOD_TYPE_WEBVIEW_WIDGET, LightwoodWebViewWidgetPrivate))



struct _LightwoodWebViewWidgetPrivate
{
	ClutterActor* 		m_webview;


	gboolean 			m_default_event_handling;
	gboolean			m_scroll_enabled;
	gboolean 			m_double_tap_enabled;
	gboolean			m_long_press_enabled;
	gchar *app_name;
	gboolean show_thumbnail;


	LightwoodWebViewWidgetAudioManager*		m_audioManager;
	LightwoodWebViewWidgetFSVideoPlayer*  	m_fsvideoplayer;
//	LightwoodWebViewWidgetZoomHandler*    	m_zoom_request_handler;
	LightwoodWebViewWidgetPdfViewer* 		m_pdf_viewer;
	LightwoodWebViewWidgetGestureHandler* 	m_gesture_handler;

	GTimer* m_timer;
	ClutterActor* m_thumbnail;
};

static guint signals[LAST_SIGNAL] = { 0, };

static gboolean
_lightwood_webview_widget_mime_type_policy_decision_requested_cb(WebKitWebPage* page, WebKitWebFrame* frame,
                                                       WebKitNetworkRequest* request, const char* mime_type,
                                                       WebKitWebPolicyDecision* decision, gpointer data);
/*
static gboolean
_lightwood_webview_widget_navigation_policy_decision_requested_cb(WebKitWebPage           *web_page,
															WebKitWebFrame            *frame,
															WebKitNetworkRequest      *request,
															WebKitWebNavigationAction *navigation_action,
															WebKitWebPolicyDecision   *policy_decision,
															gpointer                   user_data);
*/

static gboolean
_lightwood_webview_widget_new_window_policy_decision_requested_cb(WebKitWebPage* page, WebKitWebFrame* frame,
                                                       WebKitNetworkRequest* request,  WebKitWebNavigationAction *navigation_action,
                                                       WebKitWebPolicyDecision* decision, gpointer data);
static gboolean
_lightwood_webview_widget_download_decision_requested_cb(WebKitWebPage* page, WebKitDownload* download,LightwoodWebViewWidget *self);

//static gboolean
//_lightwood_webview_widget_console_message_decision_requested_cb(WebKitWebPage* page,gchar* message,gint lineno, gchar* source);
static void
_lightwood_webview_widget_speller_alert_cb(LightwoodWebView* webview,gboolean is_shown,LightwoodWebViewWidget *self);

//static void
//_lightwood_webview_widget_vkb_alert_cb(LightwoodWebViewSpeller* vkb,gboolean is_visible,LightwoodWebViewWidget *self);

static void
_lightwood_webview_widget_audio_status_cb(LightwoodWebViewWidgetAudioManager* audio_manager,ClutterActor* self);

static void
lightwood_webview_widget_initialize (LightwoodWebViewWidget *self);


//static void
//_lightwood_webview_widget_audio_requested_cb(LightwoodWebViewWidgetAudioManager* audio_manager,LightwoodWebViewWidget *self);

static void
lightwood_webview_widget_get_property (GObject    *object,
                                  guint       property_id,
                                  GValue     *value,
                                  GParamSpec *pspec)
{
	LightwoodWebViewWidget *self = LIGHTWOOD_WEBVIEW_WIDGET(object);
  switch (property_id)
	{

		case PROP_FORM_ASSISTANCE:
		{
			g_value_set_boolean (value, lightwood_webview_widget_get_form_assistance (self));
		}
		  break;

		case PROP_ZOOM_LEVEL:
		{
			g_value_set_double(value,lightwood_webview_widget_get_zoom_level (self));
		}
		  break;

		case PROP_SOUP_SESSION:
		{
			g_value_set_object (value, lightwood_webview_widget_get_soup_session (self));
		}
		  break;

		case PROP_GESTURE_HANDLING:
		{
			g_value_set_boolean (value, lightwood_webview_widget_get_gesture_handling (self));
		}
		  break;

		case PROP_LOAD_STATUS:
		{
			g_value_set_enum (value, lightwood_webview_widget_get_load_status (self));
		}
            break;

        case PROP_URI:
		{
			g_value_set_string (value, lightwood_webview_widget_get_uri (self));
		}
		  break;

        case PROP_TITLE:
		{
			g_value_set_string (value, lightwood_webview_widget_get_title (self));
		}
		  break;

		case PROP_AUDIO_STATUS:
		{
			g_value_set_enum (value, lightwood_webview_widget_get_audio_status (self));
		}
		  break;

        case PROP_SHOW_THUMBNAIL:
                  g_value_set_boolean (value, lightwood_webview_widget_get_show_thumbnail (self));
                  break;

	default:
	  G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
	}
}

static void
lightwood_webview_widget_set_property (GObject      *object,
                                  guint         property_id,
                                  const GValue *value,
                                  GParamSpec   *pspec)
{
	LightwoodWebViewWidget *self = LIGHTWOOD_WEBVIEW_WIDGET(object);
  switch (property_id)
    {

		case PROP_FORM_ASSISTANCE:
		{
			lightwood_webview_widget_set_form_assistance (self, g_value_get_boolean (value));
		}
		  break;

		case PROP_ZOOM_LEVEL:
		{
			if(CLUTTER_IS_ACTOR(self->priv->m_webview))
					lightwood_webview_widget_set_zoom_level (self, g_value_get_double (value));
		}
		  break;

		case PROP_GESTURE_HANDLING:
		{
			if(CLUTTER_IS_ACTOR(self->priv->m_webview))
				lightwood_webview_widget_set_gesture_handling (self, g_value_get_boolean (value));
		}
		  break;

	case PROP_THUMBNAIL:
	{
		self->priv->m_thumbnail = g_value_get_object(value);
		g_assert(CLUTTER_IS_ACTOR(self->priv->m_thumbnail));
		clutter_actor_set_name(self->priv->m_thumbnail,"thumbnail");
		g_object_set(self->priv->m_thumbnail,
				"x-expand",TRUE,
				"y-expand",TRUE,
				"x-align",CLUTTER_ACTOR_ALIGN_FILL,
				"y-align",CLUTTER_ACTOR_ALIGN_FILL,
				NULL);
		g_print("\nadding thumnail....");
//		clutter_actor_add_child(CLUTTER_ACTOR(self),self->priv->m_thumbnail);
		clutter_actor_insert_child_at_index(CLUTTER_ACTOR(self),self->priv->m_thumbnail,(clutter_actor_get_n_children(CLUTTER_ACTOR(self))+1));
		if (self->priv->show_thumbnail)
			clutter_actor_show(self->priv->m_thumbnail);
		else
			clutter_actor_hide(self->priv->m_thumbnail);
	}
	break;
	case PROP_SHOW_THUMBNAIL:
		lightwood_webview_widget_set_show_thumbnail (self, g_value_get_boolean (value));
		break;


		default:
		  G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
lightwood_webview_widget_dispose (GObject *object)
{
	LightwoodWebViewWidget* self= LIGHTWOOD_WEBVIEW_WIDGET(object);
	g_return_if_fail(LIGHTWOOD_IS_WEBVIEW_WIDGET(self));

	lightwood_webview_widget_stop_audio (self);
/*	if(self->priv->m_alerthandler)
	{
		g_object_unref(self->priv->m_alerthandler);
		g_signal_handlers_disconnect_by_func(self->priv->m_vkb,_lightwood_webview_widget_vkb_alert_cb,self);
		g_object_unref(self->priv->m_fsvideoplayer);
		self->priv->m_fsvideoplayer = NULL;
		G_OBJECT_CLASS (lightwood_webview_widget_parent_class)->dispose (object);
		g_print("\n lightwood_webview_widget_dispose");

	}
	self->priv->m_alerthandler = NULL;*/
	G_OBJECT_CLASS (lightwood_webview_widget_parent_class)->dispose (object);

}

static void
lightwood_webview_widget_finalize (GObject *object)
{
  LightwoodWebViewWidget *self = LIGHTWOOD_WEBVIEW_WIDGET (object);

  g_free (self->priv->app_name);

  G_OBJECT_CLASS (lightwood_webview_widget_parent_class)->finalize (object);
}

static GObject *
lightwood_webview_widget_constructor (GType gtype,
								 guint n_properties,
								 GObjectConstructParam *properties)
{
	GObject *obj;

  {
	/* Always chain up to the parent constructor */
	obj = G_OBJECT_CLASS (lightwood_webview_widget_parent_class)->constructor (gtype, n_properties, properties);
  }

  /* update the object state depending on constructor properties */
//  LightwoodWebViewWidgetPrivate* priv =  WEBVIEW_WIDGET_PRIVATE (LIGHTWOOD_WEBVIEW_WIDGET(obj));
  //g_print("\nconstructor.....%d,%s",priv->m_app_name,priv->m_app_name);

  lightwood_webview_widget_initialize(LIGHTWOOD_WEBVIEW_WIDGET(obj));

   return obj;
}

static void
lightwood_webview_widget_class_init (LightwoodWebViewWidgetClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  g_type_class_add_private (klass, sizeof (LightwoodWebViewWidgetPrivate));

  object_class->constructor = lightwood_webview_widget_constructor;
  object_class->get_property = lightwood_webview_widget_get_property;
  object_class->set_property = lightwood_webview_widget_set_property;
  object_class->dispose = lightwood_webview_widget_dispose;
  object_class->finalize = lightwood_webview_widget_finalize;


  GParamSpec *pspec;

	/**
	 * LightwoodWebViewWidget:form_assistance:
	 *
	 * whether the form assistance should be enabled.
	 * if set to FALSE the virtual keyboard will not be shown for focus on text entry elements.
	 **/
  pspec = g_param_spec_boolean( "form_assistance",
								"form_assistance",
								"form_assistance",
								true,
								G_PARAM_CONSTRUCT | G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_FORM_ASSISTANCE, pspec);



	/**
	 * LightwoodWebViewWidget:soup_session:
	 *
	 * gives the webkit's soup session instance.
	 * can be used by the app to set/overide network preferences.ex:cookies,soup cache,proxy.
	 *
	 **/
  pspec = g_param_spec_object(  "soup_session",
								"webkit soup session",
								"webkit soup session",
								SOUP_TYPE_SESSION,
								G_PARAM_READABLE);
  g_object_class_install_property (object_class, PROP_SOUP_SESSION, pspec);

	/**
	 * LightwoodWebViewWidget:load_status:
	 *
	 * gives the load status  #WebKitLoadStatus of the current webpage.
	 *
	 **/
  pspec = g_param_spec_enum( "load_status",
							"load_status",
							"load_status",
							WEBKIT_TYPE_LOAD_STATUS,
							WEBKIT_LOAD_PROVISIONAL,
							G_PARAM_READABLE);
  g_object_class_install_property (object_class, PROP_LOAD_STATUS, pspec);

	/**
	 * LightwoodWebViewWidget:uri:
	 *
	 * gives the uri of the displayed webpage.
	 **/
  pspec = g_param_spec_string("url",
							"url",
							"url",
							NULL,
							G_PARAM_READABLE);
  g_object_class_install_property (object_class, PROP_URI, pspec);

  pspec = g_param_spec_string("title",
							"title",
							"title",
							NULL,
							G_PARAM_READABLE);
  g_object_class_install_property (object_class, PROP_TITLE, pspec);

  pspec = g_param_spec_object(  "thumbnail",
  								"thumbnail",
  								"thumbnail",
  								CLUTTER_TYPE_ACTOR,
  								G_PARAM_READWRITE);
    g_object_class_install_property (object_class, PROP_THUMBNAIL, pspec);


	/**
	 * LightwoodWebViewWidget:show_thumbnail:
	 *
	 * if %TRUE grabs keyboard focus to the displayed page.
	 *
	 **/
	pspec = g_param_spec_boolean( "show_thumbnail",
			"show_thumbnail",
			"show_thumbnail",
			true,
			G_PARAM_CONSTRUCT | G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_SHOW_THUMBNAIL, pspec);





	/**
	 * LightwoodWebViewWidget:gesture_handling:
	 *
	 * enable/disable gesture handling.
	 *
	 * if %TRUE then the webview widget intercepts all touch events to detect predefined gestures and if not only then it lets  go to webkit engine.
	 * that means say if user moves finger on the page its treated as scroll gesture and the page is scrolled .
	 *
	 * if %FALSE the webviewwidget will not intercept the touch events and all events shall go through to the webkit engine.
	 *
	 **/
  pspec = g_param_spec_boolean( "gesture_handling",
								"gesture handling",
								"gesture handling",
								true,
								G_PARAM_CONSTRUCT | G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_GESTURE_HANDLING, pspec);

  pspec = g_param_spec_double( "zoom_level",
								"zoom_level",
								"zoom_level",
								0.1,
								100.0,
								1.0,
								G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_ZOOM_LEVEL, pspec);

 	/**
 	 * LightwoodWebViewWidget:audio_status:
 	 *
 	 * whether the form assistance should be enabled.
 	 * if set to FALSE the virtual keyboard will not be shown for focus on text entry elements.
 	 **/
   pspec = g_param_spec_enum ("audio_status",
 								"audio_progress",
 								"audio_progress",
								LIGHTWOOD_TYPE_WEB_VIEW_AUDIO_STATUS,
								LIGHTWOOD_WEBVIEW_AUDIO_STATUS_PAUSED,
 								G_PARAM_READABLE);
   g_object_class_install_property (object_class, PROP_AUDIO_STATUS, pspec);

   signals[ENABLE_WEBPAGE_SCROLL] =
     g_signal_new ("enable_webpage_scroll",
                   G_TYPE_FROM_CLASS (klass),
                   G_SIGNAL_RUN_LAST,
                   0,
                   NULL, NULL,
                   lightwood_webview_marshal_BOOLEAN__BOOLEAN,
                   G_TYPE_BOOLEAN, 1,G_TYPE_BOOLEAN);

   signals[AUDIO_STATUS_CHANGE] =
     g_signal_new ("audio_status_change",
                   G_TYPE_FROM_CLASS (klass),
                   G_SIGNAL_RUN_LAST,
                   0,
                   NULL, NULL,
                   g_cclosure_marshal_VOID__VOID,
                   G_TYPE_NONE,0);





   /**
    * LightwoodWebViewWidget::new_view_requested:
    * @LightwoodWebviewWidget: the #LightwoodWebViewWidget which emitted the signal
    * @url: the request url to be loaded.
    *
    * The ::new_view_requested signal is emitted when a user clicks on a link
    * and that needs to be loaded in a new window .
    * Its upto the application to load the uri in the same webkit instance or in a different tab.
    *
    **/
  signals[NEW_VIEW_REQUESTED] =
    g_signal_new ("new_view_requested",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL,
                  g_cclosure_marshal_VOID__STRING,
                  G_TYPE_NONE, 1, G_TYPE_STRING);



  /**
   * LightwoodWebViewWidget::save_image_requested:
   * @LightwoodWebviewWidget: the #LightwoodWebViewWidget which emitted the signal
   * @image_uri: the uri of the image resource to be saved
   *
   * The ::save_image_requested signal is emitted when a user does a long press on an image with an intent to save it.
   *
   *Its upto the application to save the image using #WebKitDownload.
   **/
  signals[SAVE_IMAGE_REQUESTED] =
    g_signal_new ("save_image_requested",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL,
                  g_cclosure_marshal_VOID__STRING,
                  G_TYPE_NONE, 1, G_TYPE_STRING);

  /**
   * LightwoodWebViewWidget::single_tap_alert:
   * @LightwoodWebviewWidget: the #LightwoodWebViewWidget which emitted the signal
   * @event: the touch event  #ClutterEvent info
   * Returns: %TRUE if the event was handled
   *
   * The ::single_tap_alert signal is emitted when a user taps on the page.
   *  the application can override and return %TRUE then the default single tap handler shall not be invoked.
   **/
  signals[SINGLE_TAP_ALERT] =
    g_signal_new ("single_tap_alert",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL,
                  lightwood_webview_marshal_BOOLEAN__OBJECT,
                  G_TYPE_BOOLEAN, 1,CLUTTER_TYPE_EVENT);

  /**
   * LightwoodWebViewWidget::double_tap_alert:
   * @LightwoodWebviewWidget: the #LightwoodWebViewWidget which emitted the signal
   * @event: the touch event  #ClutterEvent info.
   * Returns: %TRUE if the event was handled.
   *
   * The ::double_tap_alert signal is emitted when a user does a double tap on the page.
   * the application can overide and return %TRUE then the default double_tap_alert handler shall not be invoked.
   **/
  signals[DOUBLE_TAP_ALERT] =
    g_signal_new ("double_tap_alert",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL,
                  lightwood_webview_marshal_BOOLEAN__OBJECT,
                  G_TYPE_BOOLEAN, 1,CLUTTER_TYPE_EVENT);

  /**
   * LightwoodWebViewWidget::long_press_alert:
   * @LightwoodWebviewWidget: the #LightwoodWebViewWidget which emitted the signal
   * @event: the touch event  #ClutterEvent info
   * Returns: %TRUE if the event was handled
   *
   * The ::long_press_alert signal is emitted when a user does a double tap on the page.
   * the application can overide and return %TRUE then the default long press handler shall not be invoked.
   **/
  signals[LONG_PRESS_ALERT] =
    g_signal_new ("long_press_alert",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL,
                  lightwood_webview_marshal_BOOLEAN__OBJECT,
                  G_TYPE_BOOLEAN, 1,CLUTTER_TYPE_EVENT);

  /**
   * LightwoodWebViewWidget::mime_alert:
   * @LightwoodWebviewWidget: the #LightwoodWebViewWidget which emitted the signal
   * @mime_object: the #WebkitDownload instance having the downloadable mime uri.
   *
   * The ::mime_alert signal is emitted when a user click on a mime link that cannot be shown in webpage.
   * the application's mime handler can use the interface on the mime object to download the mime uri and open it using the relevant application.
   **/
  signals[MIME_ALERT] =
    g_signal_new ("mime_alert",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL,
                  g_cclosure_marshal_VOID__POINTER,
                  G_TYPE_NONE, 1, G_TYPE_POINTER);

  /**
   * LightwoodWebViewWidget::rss_feed_alert:
   * @LightwoodWebviewWidget: the #LightwoodWebViewWidget which emitted the signal
   * @mime_object: the #WebkitDownload instance having the downloadable mime uri.
   *
   * The ::rss_feed_alert signal is emitted when a user opens a feed url.
   * the application's mime handler can retrieve the feed title and url and open it in the feeds app.
   **/
	signals[RSS_FEED_ALERT] =
		g_signal_new("rss_feed_alert",
					G_TYPE_FROM_CLASS (klass),
					G_SIGNAL_RUN_LAST,
					0,
					NULL, NULL,
					g_cclosure_marshal_VOID__UINT,
					G_TYPE_NONE, 1,G_TYPE_UINT);

   /**
	* LightwoodWebViewWidget::new_page_requested:
	* @LightwoodWebviewWidget: the #LightwoodWebViewWidget which emitted the signal
	* @url: the request url to be loaded.
	* Returns: if the webkit should load the page or not.
	*
	* The ::new_page_requested signal is emitted whenever webkit tries to load a page in the same webview
	* when a user clicked on a link or there was a request using the api thereby allowing the user to
	* decide whether to load the webppage or not.
	*
	* if the application returns %TRUE then the page shall not be loaded.
	**/
  signals[NEW_PAGE_REQUESTED] =
    g_signal_new ("new_page_requested",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL,
                  lightwood_webview_marshal_BOOLEAN__OBJECT,
                  G_TYPE_BOOLEAN, 1, G_TYPE_STRING);

  /**
	* LightwoodWebViewWidget::vkb_alert:
	* @LightwoodWebviewWidget: the #LightwoodWebViewWidget which emitted the signal
	* @is_shown: whether the form virtual keyboard is shown or hidden
	*
	* This signal is emitted whenever the form assistance virtual keyboard is shown or hidden.
	**/
  signals[VKB_ALERT] =
		 g_signal_new ("vkb_alert",
			  G_TYPE_FROM_CLASS (klass),
			  G_SIGNAL_RUN_LAST,
			  0,
			  NULL, NULL,
			  g_cclosure_marshal_VOID__BOOLEAN,
			  G_TYPE_NONE, 1,
			  G_TYPE_BOOLEAN);

  signals[WEBVIEW_SPELLER_ALERT] =
      g_signal_new ("webview_speller_alert",
                    G_TYPE_FROM_CLASS (klass),
                    G_SIGNAL_RUN_LAST,
                    0,
                    NULL, NULL,
                    g_cclosure_marshal_VOID__BOOLEAN,
                    G_TYPE_NONE, 1,G_TYPE_BOOLEAN);


/*  signals[AUDIO_REQUESTED] =
		 g_signal_new ("audio_requested",
			  G_TYPE_FROM_CLASS (klass),
			  G_SIGNAL_RUN_LAST,
			  0,
			  NULL, NULL,
			  g_cclosure_marshal_VOID__VOID,
			  G_TYPE_NONE, 0);*/

}

/*static void
lightwood_webview_size_changed_cb(ClutterActor*  webview,GParamSpec* pspec)
{

	//LightwoodWebViewWidgetPrivate* priv =  WEBVIEW_WIDGET_PRIVATE (self);
	gfloat viewport_width,viewport_height=0.0;

	WebKitWebPage* webPage = webkit_iweb_view_get_page(WEBKIT_IWEB_VIEW(webview));
	clutter_actor_get_size(CLUTTER_ACTOR(webview),&viewport_width,&viewport_height);
	g_print("\n lightwood_webview_widget_size_changed_cb %f,%f",viewport_width,viewport_height);
	g_assert(viewport_width < 700);
	//clutter_actor_queue_redraw(priv->m_webview);


	//clutter_actor_set_size(CLUTTER_ACTOR(priv->m_webview),viewport_width,viewport_height);
	webkit_web_page_set_use_fixed_layout(webPage, FALSE);
	webkit_web_page_set_viewport_size(webPage,viewport_width,viewport_height);
	webkit_web_page_set_use_fixed_layout(webPage, TRUE);
	//webkit_web_page_set_fixed_layout_width(webPage,viewport_width/webkit_web_page_get_zoom_level(webPage));
	//webkit_web_page_set_fixed_layout_height(webPage,(viewport_width*3/4)/webkit_web_page_get_zoom_level(webPage));
	webkit_web_page_set_fixed_layout_width(webPage,viewport_width);
	webkit_web_page_set_fixed_layout_height(webPage,viewport_height);


}*/

//static uint counter=0;
static void
lightwood_webview_widget_zoom_level_changed_cb(WebKitWebPage* webPage,GParamSpec* pspec,LightwoodWebViewWidget *self)
{
/*	counter++;
	if(counter > 1)
		g_assert(0);*/
	g_print("\n lightwood_webview_widget_zoom_level_changed_cb=%f",webkit_web_page_get_zoom_level(webPage));
	g_signal_emit_by_name(G_OBJECT(self),"notify::zoom_level", pspec,NULL);

}

static WebKitWebPage* _lightwood_webview_widget_inspection_requested_cb(WebKitWebPage* page, WebKitWebInspector* inspector, LightwoodWebViewWidget *self)
{
	g_print("\n _lightwood_webview_widget_inspection_requested_cb");

	MxWindow* inspectorWindow = mx_window_new();
	ClutterActor* scrollView = mx_scroll_view_new();
	mx_window_set_child(inspectorWindow, scrollView);

	ClutterActor* webView = mx_web_view_new();
	clutter_actor_add_child(scrollView, webView);

	ClutterActor* stage = CLUTTER_ACTOR(mx_window_get_clutter_stage(inspectorWindow));
	clutter_actor_set_size(stage, clutter_actor_get_width(CLUTTER_ACTOR(self)),clutter_actor_get_height(CLUTTER_ACTOR(self)));
	clutter_actor_show(stage);

	return webkit_iweb_view_get_page(WEBKIT_IWEB_VIEW(webView));
    //return webkit_iweb_view_get_page(WEBKIT_IWEB_VIEW(self->priv->m_webview));
}

static gboolean
_lightwood_webview_widget_initialize_audio_management_cb(LightwoodWebViewWidget *self)
{
	 WebKitWebPage* webPage =
			 webkit_iweb_view_get_page(WEBKIT_IWEB_VIEW(self->priv->m_webview));

	 gboolean audio_element_found =
			 lightwood_webview_widget_audio_manager_initialize(self->priv->m_audioManager,webPage);
	 if(audio_element_found)
		 g_print("\n !!!!!!!!!media element found!!!!!!!!!!!!");

	return FALSE;
}

static void
_lightwood_webview_widget_speller_alert_cb(LightwoodWebView* webview,gboolean is_shown,LightwoodWebViewWidget *self)
{
	g_signal_emit(self,signals[WEBVIEW_SPELLER_ALERT],0,is_shown);
}
static void
_lightwood_webview_widget_uri_change_cb(WebKitWebPage* webPage,GParamSpec* pspec,LightwoodWebViewWidget *self)
{
	g_print("\n _lightwood_webview_widget_uri_change_cb");
	g_signal_emit_by_name(G_OBJECT(self),"notify::url", pspec, NULL);
    /*if(webkit_web_page_get_load_status(webPage) == WEBKIT_LOAD_FINISHED)
     {
		 gboolean audio_element_found =
				 lightwood_webview_widget_audio_manager_initialize(self->priv->m_audioManager,webPage);
		 if(audio_element_found)
			 g_print("\n !!!!!!!!!media element found!!!!!!!!!!!!");

     }*/
	//g_idle_add((GSourceFunc)_lightwood_webview_widget_initialize_audio_management_cb,self);
	g_timeout_add(2500,(GSourceFunc)_lightwood_webview_widget_initialize_audio_management_cb,self);
}

static void
_lightwood_webview_widget_load_status_cb(WebKitWebPage* webPage,GParamSpec* pspec,LightwoodWebViewWidget *self)
{
	//g_object_notify(G_OBJECT(self),"load_status");
    g_signal_emit_by_name(G_OBJECT(self),"notify::load_status", pspec, NULL);

	if ( webkit_web_page_get_load_status(webPage) == WEBKIT_LOAD_FINISHED)
	{
		g_timer_stop(self->priv->m_timer);
		g_print("\n elapsed time in webkit for handling link=%fsec.",g_timer_elapsed(self->priv->m_timer,NULL));

		//g_timeout_add_full(G_PRIORITY_DEFAULT_IDLE,200,(GSourceFunc)_lightwood_webbrowser_tab_invalidate,self,NULL);
		 WebKitWebFrame* frame = webkit_web_page_get_main_frame(webPage);

		 WebKitNetworkResponse* response = webkit_web_frame_get_network_response(frame);
		 if(response == NULL) return;
		 SoupMessage* message = webkit_network_response_get_message(response);
		 if(message == NULL) return;
		 const char* mime_type =
		 soup_message_headers_get_content_type(message->response_headers,NULL);
		 g_print("\n liblightwood webview widget::mime_type=%s,%d\n",mime_type, __LINE__);
		// if( g_strrstr(mime_type,"xml") != NULL)
		 {
			 WebKitWebDataSource* src = webkit_web_frame_get_data_source(frame);
			 gchar* page_src = webkit_web_data_source_get_data(src)->str;
			// g_print("\n ------------PAGE SOURCES--------\n%.300s",page_src);
			 gboolean rss_detected =    (g_strstr_len(page_src,500,"<rss") != NULL)
								     || (g_strstr_len(page_src,500,"<rdf") != NULL)
								     || (g_strstr_len(page_src,500,"<feed") != NULL) ;
			 if(rss_detected)
			 {
				 gboolean is_podcast = (g_strstr_len(webkit_network_response_get_uri(response),100,"podcast") != NULL);
				 gboolean is_video_podcast = (g_strstr_len(webkit_network_response_get_uri(response),100,"videopodcast") != NULL);
				 //mime_type = g_strdup("application/xml");
				 //soup_message_headers_set_content_type(message->response_headers,g_strdup("text/xml"),NULL);
				 if(is_video_podcast)
					 g_print("\n ****video podcast detected......****");
				 else if(is_podcast)
					 g_print("\n ****podcast detected......****");
				 else
					 g_print("\n *****rss feed detected......*******");
				g_signal_emit(self,signals[RSS_FEED_ALERT],0,is_podcast);
			 }
		 }

		 g_print("\n !!!!!!!!!finding media element !!!!!!!!!!!!");
		 gboolean audio_element_found =
				 lightwood_webview_widget_audio_manager_initialize(self->priv->m_audioManager,webPage);
		 if(audio_element_found)
			 g_print("\n !!!!!!!!!media element found!!!!!!!!!!!!");

	}

}
/*static void
_lightwood_webview_widget_vkb_alert_cb(LightwoodWebViewSpeller* vkb,gboolean is_visible,LightwoodWebViewWidget *self)
{
	g_signal_emit(self,signals[VKB_ALERT],0,is_visible);
}*/


static void
_lightwood_webview_widget_audio_status_cb(LightwoodWebViewWidgetAudioManager *audio_manager,ClutterActor* self)
{

	g_signal_emit(G_OBJECT(self),signals[AUDIO_STATUS_CHANGE],0);

}


static void
lightwood_webview_widget_init(LightwoodWebViewWidget *self)
{
	self->priv = WEBVIEW_WIDGET_PRIVATE (self);

	clutter_actor_set_layout_manager(CLUTTER_ACTOR(self),
			clutter_bin_layout_new(CLUTTER_BIN_ALIGNMENT_FILL,CLUTTER_BIN_ALIGNMENT_FILL)
			);

}
static gulong idle_resize_id =0;

static gboolean _lightwood_webview_widget_transition_completed_cb(ClutterActor* webview)
{
	WebKitWebPage* webPage = webkit_iweb_view_get_page(WEBKIT_IWEB_VIEW(webview));
	WebKitWebFrame* mainFrame = webkit_web_page_get_main_frame(webPage);
	if(!idle_resize_id)
	{
		webkit_web_frame_set_contents_frozen(mainFrame, FALSE);
		g_print("\n\n WEBVIEW CONTENT FREE !!!!!!!!!!!");
	}

	return G_SOURCE_REMOVE;
}

static gboolean
idle_on_allocate_cb (ClutterActor* webview)
{
	//LightwoodWebViewWidgetPrivate* priv =  WEBVIEW_WIDGET_PRIVATE (self);


	gfloat viewport_width,viewport_height=0.0;
	WebKitWebPage* webPage = webkit_iweb_view_get_page(WEBKIT_IWEB_VIEW(webview));
	WebKitWebFrame* mainFrame = webkit_web_page_get_main_frame(webPage);
	viewport_width = clutter_actor_get_width(CLUTTER_ACTOR(webview));
	viewport_height = clutter_actor_get_height(CLUTTER_ACTOR(webview));
	//g_print("\n idle_on_allocate_cb %f,%f",viewport_width,viewport_height);

	{
		g_print("\n\n WEBVIEW CONTENT RESIZED !!!!!!!!!!!");
	   gdouble zoomFactor = webkit_web_frame_get_contents_scale(mainFrame);
		webkit_web_frame_set_contents_frozen(mainFrame, false);
		webkit_web_frame_set_contents_scale(mainFrame, zoomFactor);
		webkit_web_frame_set_contents_frozen(mainFrame, true);
	}
	idle_resize_id =0;
	static gulong completion_id =0;
	if(completion_id)
		g_source_remove (completion_id);
	completion_id = clutter_threads_add_timeout_full(G_PRIORITY_HIGH_IDLE,200,(GSourceFunc)_lightwood_webview_widget_transition_completed_cb,webview,NULL);
	return G_SOURCE_REMOVE;


/*		//clutter_actor_set_size(CLUTTER_ACTOR(priv->m_webview),viewport_width,viewport_height);
		webkit_web_page_set_use_fixed_layout(webPage, FALSE);
		webkit_web_page_set_viewport_size(webPage,viewport_width,viewport_height);
		webkit_web_page_set_use_fixed_layout(webPage, TRUE);
		//webkit_web_page_set_fixed_layout_width(webPage,viewport_width/webkit_web_page_get_zoom_level(webPage));
		//webkit_web_page_set_fixed_layout_height(webPage,(viewport_width*3/4)/webkit_web_page_get_zoom_level(webPage));
		webkit_web_page_set_fixed_layout_width(webPage,viewport_width);
		webkit_web_page_set_fixed_layout_height(webPage,viewport_height);
		idle_resize_id =0;
		return G_SOURCE_REMOVE;*/
}


static void
_lightwood_webview_widget_on_webview_allocate_cb(ClutterActor* webview,const ClutterActorBox *allocation,
													ClutterAllocationFlags  flags,LightwoodWebViewWidget* self)
{
	if(idle_resize_id)
		return ;
	gfloat viewport_width,viewport_height=0.0;
	viewport_width = clutter_actor_get_width(CLUTTER_ACTOR(webview));
	viewport_height = clutter_actor_get_height(CLUTTER_ACTOR(webview));
	gfloat boundry_width = clutter_actor_get_width(clutter_actor_get_parent(CLUTTER_ACTOR(self)));
	gfloat boundry_height = clutter_actor_get_height(clutter_actor_get_parent(CLUTTER_ACTOR(self)));
	WebKitWebPage* webPage = webkit_iweb_view_get_page(WEBKIT_IWEB_VIEW(webview));
	WebKitWebFrame* mainFrame = webkit_web_page_get_main_frame(webPage);
	gfloat content_width =webkit_web_frame_get_contents_width(mainFrame);
	int page_width=0,page_height=0;
	webkit_web_page_get_viewport_size(webPage,&page_width,&page_height);

	if(
		((gfloat)page_width 	>=	viewport_width ) ||
		(viewport_width >= (boundry_width+10)) ||
		(viewport_height >= (boundry_height+10))
	  )
	{
		g_print("\n\n WEBVIEW CONTENT FROZEN********");
		webkit_web_frame_set_contents_frozen(mainFrame, TRUE);

		//g_assert(0);
		return;
	}
	g_print("\n content width %f page width %d viewport width %f",
			content_width,page_width,viewport_width);
  idle_resize_id= clutter_threads_add_timeout_full (G_PRIORITY_HIGH_IDLE,30, (GSourceFunc)idle_on_allocate_cb, webview,NULL);

}

static void
lightwood_webview_widget_initialize (LightwoodWebViewWidget *self)
{
	LightwoodWebViewWidgetPrivate* priv =  WEBVIEW_WIDGET_PRIVATE (self);

	priv->m_webview = lightwood_webview_new();
   	g_assert(CLUTTER_IS_ACTOR(priv->m_webview));
   	g_object_set(priv->m_webview,
   				"x-expand",TRUE,
   				"y-expand",TRUE,
   				"x-align",CLUTTER_ACTOR_ALIGN_FILL,
   				"y-align",CLUTTER_ACTOR_ALIGN_FILL,
   				NULL);
   	//clutter_actor_add_constraint(priv->m_webview,clutter_bind_constraint_new(self,CLUTTER_BIND_SIZE,0));
   	g_signal_connect_after (CLUTTER_ACTOR(priv->m_webview), "allocation-changed", G_CALLBACK (_lightwood_webview_widget_on_webview_allocate_cb), self);
   	clutter_actor_add_child(CLUTTER_ACTOR(self),priv->m_webview);
   	clutter_actor_show(priv->m_webview);
   clutter_actor_set_reactive(CLUTTER_ACTOR(self),TRUE);


   priv->m_gesture_handler = NULL;






	priv->m_default_event_handling = TRUE;

	/*mx_bin_set_child (self,CLUTTER_ACTOR(priv->m_webview));
	mx_bin_set_fill(self, TRUE, TRUE);

	g_object_set(self,
			"layout-manager",g_object_new (CLUTTER_TYPE_BOX_LAYOUT,"vertical", TRUE,NULL),
			NULL);
	clutter_actor_add_child(CLUTTER_ACTOR(self),CLUTTER_ACTOR(priv->m_webview));

	 clutter_box_pack (CLUTTER_BOX (self), CLUTTER_ACTOR(priv->m_webview),
	                    "x-fill", TRUE,
	                    "y-fill", TRUE,
	                    "expand", TRUE,
	                    NULL);*/

	clutter_actor_show(CLUTTER_ACTOR(self));



   /*
//    mx_kinetic_scroll_view_set_scroll_policy(MX_KINETIC_SCROLL_VIEW(self),MX_SCROLL_POLICY_ALIGNED);
	mx_kinetic_scroll_view_set_use_captured(MX_KINETIC_SCROLL_VIEW(self),TRUE);
	mx_kinetic_scroll_view_set_deceleration(MX_KINETIC_SCROLL_VIEW(self),1.03f);

	MxAdjustment *hadjust, *vadjust;
	mx_scrollable_get_adjustments (MX_SCROLLABLE (self),&hadjust, &vadjust);
	mx_adjustment_set_elastic (hadjust, TRUE);
	mx_adjustment_set_elastic (vadjust, TRUE);
	g_signal_connect(vadjust,"interpolation-completed",
			(GCallback)_lightwood_webview_widget_interpolation_completed_cb,self);*/

    WebKitWebPage* webPage = webkit_iweb_view_get_page(WEBKIT_IWEB_VIEW(priv->m_webview));
	WebKitWebFrame* mainFrame = webkit_web_page_get_main_frame(webPage);
    g_object_connect (webPage,
    		   	   	   //"signal::navigation-policy-decision-requested",G_CALLBACK(_lightwood_webview_widget_navigation_policy_decision_requested_cb),self,
					   "signal::mime-type-policy-decision-requested",G_CALLBACK(_lightwood_webview_widget_mime_type_policy_decision_requested_cb),self,
					   "signal::new-window-policy-decision-requested",G_CALLBACK(_lightwood_webview_widget_new_window_policy_decision_requested_cb),self,
					   //"signal::geolocation-policy-decision-requested",G_CALLBACK(_lightwood_webview_widget_geolocation_policy_decision_requested_cb),self,
					   "signal::download-requested",G_CALLBACK(_lightwood_webview_widget_download_decision_requested_cb),self,
					   "signal::notify::uri",G_CALLBACK(_lightwood_webview_widget_uri_change_cb),self,
					   "signal::notify::load-status",G_CALLBACK(_lightwood_webview_widget_load_status_cb),self,
					   //"signal::console-message",G_CALLBACK(_lightwood_webview_widget_console_message_decision_requested_cb),self,
					   "signal::notify::zoom-level",G_CALLBACK(lightwood_webview_widget_zoom_level_changed_cb),self,
					   "signal::inspection-requested",G_CALLBACK(_lightwood_webview_widget_inspection_requested_cb), self,
					   NULL);
    g_signal_connect(priv->m_webview,"webview_speller_alert",G_CALLBACK(_lightwood_webview_widget_speller_alert_cb), self);

    webkit_web_page_set_zoom_level(webkit_iweb_view_get_page(WEBKIT_IWEB_VIEW(priv->m_webview)),1.0);
    webkit_web_frame_set_contents_frozen(mainFrame, false);
    webkit_web_frame_set_contents_scale(mainFrame, 1.0);

    //    g_signal_connect(self->priv->m_webview_assistant->m_iwebview_speller,
  //  							"webview_speller_visible",(GCallback)_lightwood_webview_widget_vkb_alert_cb,self);

/*    	g_object_connect (priv->m_webview,
						  "signal::button-press-event",G_CALLBACK(_lightwood_webview_widget_button_press_event_cb),self,
						  "signal::button-release-event",G_CALLBACK(_lightwood_webview_widget_button_release_event_cb),self,
						  NULL);*/







/*gchar* user_agent = webkit_dom_navigator_get_user_agent(
								webkit_dom_dom_window_get_navigator(
										webkit_dom_document_get_default_view(
												webkit_web_page_get_dom_document(webPage))));
		g_print("\n user agent from naviagtor-%s",user_agent);*/



	priv->m_scroll_enabled = TRUE;
	priv->m_double_tap_enabled = TRUE;
	priv->m_long_press_enabled = TRUE;
    priv->m_timer = g_timer_new();

    //create alert handler
    //priv->m_alerthandler=lightwood_webview_widget_alert_handler_new();
    //lightwood_webview_widget_alert_handler_initialize(priv->m_alerthandler,priv->m_webview);
    priv->m_fsvideoplayer=lightwood_webview_widget_fsvideo_player_new();
    lightwood_webview_widget_fsvideo_player_initialize(priv->m_fsvideoplayer,priv->m_webview);
    //priv->m_zoom_request_handler = lightwood_webview_widget_zoom_handler_new();
	//g_signal_connect(priv->m_zoom_request_handler,"notify::is-zoom-inprogress",G_CALLBACK(_lightwood_webview_widget_zoom_status_cb),self);
	priv->m_audioManager = lightwood_webview_widget_audio_manager_new();
	//g_signal_connect(priv->m_audioManager,"audio_requested",G_CALLBACK(_lightwood_webview_widget_audio_requested_cb),self);
	g_signal_connect(priv->m_audioManager,"audio_mgr_status_change",G_CALLBACK(_lightwood_webview_widget_audio_status_cb),self);

	priv->m_gesture_handler =lightwood_webview_widget_gesture_handler_new (self);
	priv->m_pdf_viewer =lightwood_webview_widget_pdf_viewer_new();
	clutter_actor_add_child(CLUTTER_ACTOR(self),CLUTTER_ACTOR(priv->m_pdf_viewer));
   	g_object_set(priv->m_pdf_viewer,
   				"x-expand",TRUE,
   				"y-expand",TRUE,
   				"x-align",CLUTTER_ACTOR_ALIGN_FILL,
   				"y-align",CLUTTER_ACTOR_ALIGN_FILL,
   				NULL);
   	//clutter_actor_add_constraint(priv->m_pdf_viewer,clutter_bind_constraint_new (self,CLUTTER_BIND_SIZE,0));
   	clutter_actor_hide(CLUTTER_ACTOR(priv->m_pdf_viewer));


}

/*static void _lightwood_webview_widget_audio_requested_cb(LightwoodWebViewWidgetAudioManager *audio_manager,LightwoodWebViewWidget* self)
{
	//g_timeout_add(1000,lightwood_webview_widget_resume_audio,self);
	g_print("\n _lightwood_webview_widget_audio_requested_cb");
	g_signal_emit(G_OBJECT(self),signals[AUDIO_REQUESTED],0,NULL);
}*/

/**
 * lightwood_webview_widget_init_session:
 * @self: a #LightwoodWebViewWidget
 * @app_name: an app name
 * @assistant:handler for top level widget creation 
 *
 * This will initailize the web view session. This has to be called to 
 * use this widget.
 **/
void lightwood_webview_widget_init_session (LightwoodWebViewWidget *self,
                                            const gchar* app_name,
                                            LightwoodWebviewAssistant* assistant)
{
  g_return_if_fail (LIGHTWOOD_IS_WEBVIEW_WIDGET (self));

  g_free (self->priv->app_name);
  self->priv->app_name = g_strdup (app_name);

  lightwood_webview_init_session (app_name, assistant);
}
/**
 * lightwood_webview_widget_get_app_name:
 * @self: a #LightwoodWebViewWidget
 *
 * This function will give the application name which is given while 
 * lightwood_webview_widget_init_session() calling this.
 * 
 * Returns: Name of the application.
 */
const gchar *
lightwood_webview_widget_get_app_name (LightwoodWebViewWidget *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_WEBVIEW_WIDGET (self), NULL);

  return self->priv->app_name;
}

/**
 * lightwood_webview_widget_new:
 *
 * lightwood_webview_widget_new() creates a new instance of #LightwoodWebViewWidget
 *
 * Returns: (transfer full): a newly created #LightwoodWebViewWidget instance
 */
LightwoodWebViewWidget *
lightwood_webview_widget_new (void)
{
  return g_object_new (LIGHTWOOD_TYPE_WEBVIEW_WIDGET, NULL);
}


/**
 * lightwood_webview_widget_set_form_assistance:
 * @self: a #LightwoodWebViewWidget
 * @is_enabled: should form assistance be enabled.
 *
 * lightwood_webview_widget_set_form_assistance () should form assistance be enabled
 */
void
lightwood_webview_widget_set_form_assistance (LightwoodWebViewWidget *self,
                                              gboolean is_enabled)
{
	if(self->priv->m_webview)
		g_object_set(G_OBJECT(LIGHTWOOD_WEBVIEW_WIDGET(self)->priv->m_webview),
				"form-assistance",is_enabled,
				NULL);
	//lightwood_webview_set_enable_focus(
		//		LIGHTWOOD_WEBVIEW(LIGHTWOOD_WEBVIEW_WIDGET(self)->priv->m_webview),is_enabled);
}

/**
 * lightwood_webview_widget_set_focus:
 * @self: a #LightwoodWebViewWidget
 * @isfocussed: should the page be focussed.
 *
 * grab or loose focus for the webpage.
 *
 */
void
lightwood_webview_widget_set_focus (LightwoodWebViewWidget *self,
                                    gboolean isfocussed)
{
  lightwood_webview_set_focus (LIGHTWOOD_WEBVIEW (self->priv->m_webview), isfocussed);
}


/**
 * lightwood_webview_widget_set_gesture_handling:
 * @self: a #LightwoodWebViewWidget
 * @is_enabled : set it to %FALSE to disable all gesture detection so that all touch events go to the webkit
 *
 * enable or disable gesture detection .
 *
 */
void
lightwood_webview_widget_set_gesture_handling (LightwoodWebViewWidget *self,
                                               gboolean is_enabled)
{
	LightwoodWebViewWidgetPrivate* priv = WEBVIEW_WIDGET_PRIVATE (self);
	if(priv->m_gesture_handler)
		lightwood_webview_widget_gesture_handler_enable_gesture(priv->m_gesture_handler,is_enabled);
	/*if(priv->m_default_event_handling != is_enabled)
	{
		if(!is_enabled)
		{
			g_signal_handlers_block_by_func(priv->m_webview,_lightwood_webview_widget_button_press_event_cb,self);
			g_signal_handlers_block_by_func(priv->m_webview,_lightwood_webview_widget_button_release_event_cb,self);
		}
		else
		{
			g_signal_handlers_unblock_by_func(priv->m_webview,_lightwood_webview_widget_button_press_event_cb,self);
			g_signal_handlers_unblock_by_func(priv->m_webview,_lightwood_webview_widget_button_release_event_cb,self);
		}

		mx_kinetic_scroll_view_set_use_captured(MX_KINETIC_SCROLL_VIEW(self),is_enabled);
		g_object_set(G_OBJECT(self),
					"scroll-policy",is_enabled?MX_SCROLL_POLICY_BOTH:MX_SCROLL_POLICY_NONE,
					NULL);
	}
	priv->m_default_event_handling = is_enabled;*/

}

/**
 * lightwood_webview_widget_set_double_tap_alert:
 * @self: self #ClutterActor
 * @is_enabled: set it to  %FALSE to disable double tap gesture detection.then the double taps shall go to webkit.
 *
 * enable or disable double tap gesture detection.
 */
/*void lightwood_webview_widget_set_double_tap_alert(ClutterActor* self,gboolean is_enabled)

{
	LightwoodWebViewWidgetPrivate* priv = WEBVIEW_WIDGET_PRIVATE(LIGHTWOOD_WEBVIEW_WIDGET(self));
	if(priv->m_double_tap_enabled != is_enabled)
	{
		ClutterSettings* settings = clutter_settings_get_default();
		g_object_set(settings,
						"double-click-distance",(is_enabled?LIGHTWOOD_WEBVIEW_DOUBLE_TAP_DISTANCE:5),
						"double-click-time",(is_enabled?LIGHTWOOD_WEBVIEW_DOUBLE_TAP_DELAY:250),
						NULL);
		priv->m_double_tap_enabled = is_enabled;
	}
}*/

/**
 * lightwood_webview_widget_set_long_press_alert:
 * @self: self #ClutterActor
 * @is_enabled: if %FALSE the widget shall not detect long press gestures.
 *
 * enable or disable long press gesture detection .
 */

void lightwood_webview_widget_set_long_press_alert(ClutterActor* self,gboolean is_enabled)
{
	LightwoodWebViewWidgetPrivate* priv = WEBVIEW_WIDGET_PRIVATE(LIGHTWOOD_WEBVIEW_WIDGET(self));
	priv->m_long_press_enabled = is_enabled;
}
/*static ClutterActor* currentToplevelCallback(WebKitSoupAuthDialog* feature, SoupMessage* message, gpointer userData)
{
}*/

/**
 * lightwood_webview_widget_get_webview:
 * @self: a #LightwoodWebViewWidget
 *
 * lightwood_webview_widget_get_webview() provides access to the webkit's (rendering engine) APIs.
 * 
 * Returns: (transfer none): its #LightwoodWebView instance.
 */
ClutterActor *
lightwood_webview_widget_get_webview (LightwoodWebViewWidget *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_WEBVIEW_WIDGET (self), NULL);

  return CLUTTER_ACTOR(self->priv->m_webview);
}

/**
 * lightwood_webview_widget_load_url:
 * @self: a #LightwoodWebViewWidget
 * @url: request url
 *
 * load the reuqested url into the webview.
 */
void lightwood_webview_widget_load_url (LightwoodWebViewWidget *self,
                                        const gchar* url)
{
	LightwoodWebViewWidgetPrivate* priv = self->priv;
	webkit_iweb_view_load_uri(
			WEBKIT_IWEB_VIEW(priv->m_webview),url);
      if(priv->m_thumbnail)
        clutter_actor_remove_child (CLUTTER_ACTOR (self), priv->m_thumbnail);
      priv->m_thumbnail = NULL;
}
/**
 * lightwood_webview_widget_load_string:
 * @self: a #LightwoodWebViewWidget
 * @content:An URI string.
 * @mime_type: Standard mime_type.
 * @encoding: encoding/%NULL
 * @base_uri:uri for relative location.
 *
 * It simply delegates to webkit_iweb_view_load_string().
 */

void lightwood_webview_widget_load_string (LightwoodWebViewWidget *self,
                                           const gchar *content,
                                           const gchar *mime_type,
                                           const gchar *encoding,
                                           const gchar *base_uri)
{
	LightwoodWebViewWidgetPrivate* priv = self->priv;
	webkit_iweb_view_load_string(WEBKIT_IWEB_VIEW(priv->m_webview),content,mime_type,encoding,base_uri);
	
}

/**
 * lightwood_webview_widget_get_zoom_level:
 * @self: a #LightwoodWebViewWidget
 *
 * It gets the zoom level of the normal page or pdf page. It uses webkit APIs 
 * internally.
 * 
 * Returns: zoom level between 0-1.
 **/
gdouble
lightwood_webview_widget_get_zoom_level (LightwoodWebViewWidget *self)
{
	LightwoodWebViewWidgetPrivate* priv = self->priv;
/*
	gdouble zoom_level = webkit_web_page_get_zoom_level(
							webkit_iweb_view_get_page(WEBKIT_IWEB_VIEW(priv->m_webview)));
*/
	gdouble zoom_level = 1.0;
	if(CLUTTER_ACTOR_IS_VISIBLE(priv->m_webview))
	{
		WebKitWebPage* webPage = webkit_iweb_view_get_page(WEBKIT_IWEB_VIEW(priv->m_webview));
		zoom_level = webkit_web_page_get_zoom_level(webPage);

	}
	else
	{
		zoom_level =lightwood_webview_widget_pdf_viewer_get_zoom_level(priv->m_pdf_viewer);
	}
	g_print("\n !!!!!!!!!zoom level %f",zoom_level);
	return zoom_level;
}
/**
 * lightwood_webview_widget_set_zoom_level:
 * @self: a #LightwoodWebViewWidget
 * @request_zoom_level: zoom level between 0-1.
 *
 * It will zoom-in /zoom-out normal page/pdf page according to this value.
 **/
void
lightwood_webview_widget_set_zoom_level (LightwoodWebViewWidget *self,
                                         gdouble request_zoom_level)
{
	LightwoodWebViewWidgetPrivate* priv = LIGHTWOOD_WEBVIEW_WIDGET(self)->priv;

	if(CLUTTER_ACTOR_IS_VISIBLE (priv->m_webview))
	{
		lightwood_webview_normal_page_zoom(priv->m_webview,request_zoom_level);
	}
	else
	{
		lightwood_webview_widget_pdf_viewer_show(priv->m_pdf_viewer,request_zoom_level,NULL);
	}
}

/**
 * lightwood_webview_widget_context_sensitive_zoom:
 * @self: a #LightwoodWebViewWidget
 * @is_zoomin: If %TRUE zooms-in else zoom-out.
 * @zoom_posx:x-position
 * @zoom_posy:y-position
 *
 * It will zoom-in /zoom-out normal page/pdf page according to this value.
 */
void
lightwood_webview_widget_context_sensitive_zoom (LightwoodWebViewWidget *self,
                                                 gboolean is_zoomin,
                                                 gfloat zoom_posx,
                                                 gfloat zoom_posy)
{
	LightwoodWebViewWidgetPrivate* priv = self->priv;
	if(CLUTTER_ACTOR_IS_VISIBLE (priv->m_webview))
	{
		lightwood_webview_context_sensitive_zoom(priv->m_webview,is_zoomin,zoom_posx,zoom_posy);
	}
}
/**
 * lightwood_webview_widget_save_as_pdf:
 * @self: a #LightwoodWebViewWidget
 * @file_name: If %TRUE zooms-in else zoom-out.
 *
 * saves the page as pdf.  
 *
 */


void
lightwood_webview_widget_save_as_pdf (LightwoodWebViewWidget *self,
                                      const gchar* file_name)
{
    LightwoodWebViewWidgetPrivate* priv = LIGHTWOOD_WEBVIEW_WIDGET(self)->priv;
    ClutterActor *actor = CLUTTER_ACTOR (self);

    gchar* absolute = NULL;
    if (g_path_is_absolute(file_name))
    {
        absolute = g_strdup (file_name);
    }
    else
    {
        gchar *dir = g_get_current_dir ();
        absolute = g_build_filename (dir, file_name, (gchar *) 0);
        free (dir);
    }

    GError* error = NULL;
    gchar *pdf_uri = g_filename_to_uri (absolute, NULL, &error);
    g_assert(pdf_uri != NULL);

    WebKitWebPage* webPage = webkit_iweb_view_get_page(WEBKIT_IWEB_VIEW(priv->m_webview));
    WebKitWebFrame* mainFrame = webkit_web_page_get_main_frame(webPage);
    webkit_web_frame_save_as_pdf (mainFrame,absolute, clutter_actor_get_width (actor), clutter_actor_get_height (actor));
    //webkit_iweb_view_load_uri(WEBKIT_IWEB_VIEW(priv->m_webview),pdf_uri);
    g_free (absolute);
}

/**
 * lightwood_webview_widget_scroll_to:
 * @self: self #ClutterActor
 * @abs_scroll_offset_x:offset value of x 
 * @abs_scroll_offset_y:offset value of y
 *
 * this will allow to navigate to the certain postion in the widget.
 *
 */

void
lightwood_webview_widget_scroll_to (LightwoodWebViewWidget *self,
                                    gint abs_scroll_offset_x,
                                    gint abs_scroll_offset_y)
{
	LightwoodWebViewWidgetPrivate* priv = self->priv;

    WebKitWebPage* webPage = webkit_iweb_view_get_page(WEBKIT_IWEB_VIEW(priv->m_webview));
    WebKitWebFrame* mainFrame = webkit_web_page_get_main_frame(webPage);
    clutter_actor_set_easing_duration(priv->m_webview,2000);
    webkit_web_frame_set_scroll_offset(mainFrame,abs_scroll_offset_x,abs_scroll_offset_y);
    //mx_web_view_set_x_scroll_offset(priv->m_webview,abs_scroll_offset_x);
    //mx_web_view_set_y_scroll_offset(priv->m_webview,abs_scroll_offset_y);
    clutter_actor_set_easing_duration(priv->m_webview,0);
}
/**
 * lightwood_webview_widget_pause_audio:
 * @self: a #LightwoodWebViewWidget
 *
 * This will handle the audio management in the widget.It will pause the HTML audio.
 * 
 *
 */

void
lightwood_webview_widget_pause_audio (LightwoodWebViewWidget *self)
{
	LightwoodWebViewWidgetPrivate* priv = self->priv;
	lightwood_webview_widget_audio_manager_pause_audio(priv->m_audioManager);
}
/**
 * lightwood_webview_widget_play_audio:
 * @self: a #LightwoodWebViewWidget
 *
 * this will play the HTML audio.
 *
 **/



void
lightwood_webview_widget_play_audio (LightwoodWebViewWidget *self)
{
	LightwoodWebViewWidgetPrivate* priv = self->priv;
	lightwood_webview_widget_audio_manager_play_audio(priv->m_audioManager);

}
/**
 * lightwood_webview_widget_stop_audio:
 * @self: a #LightwoodWebViewWidget
 *
 * this will stop the HTML audio.
 *
 */


void
lightwood_webview_widget_stop_audio (LightwoodWebViewWidget *self)
{
	LightwoodWebViewWidgetPrivate* priv = self->priv;
	lightwood_webview_widget_audio_manager_stop_audio(priv->m_audioManager);
}
/**
 * lightwood_webview_widget_create_clone:
 * @self: a #LightwoodWebViewWidget
 * @width:width of the clone widget
 * @height:height of the clone widget
 * @is_deep_copy:%TRUE for deep_COPY, else shallow copy.
 *
 * this will creates the clone.
 *
 * Returns: (transfer full): cloned actor.
 */

ClutterActor *
lightwood_webview_widget_create_clone (LightwoodWebViewWidget *self,
                                       gfloat width,
                                       gfloat height,
                                       gboolean is_deep_copy)
{
//	return lightwood_webview_widget_zoom_handler_create_clone(LIGHTWOOD_WEBVIEW_WIDGET(self)->priv->m_webview,
	//													width,height,is_deep_copy);
	return NULL;
}
/**
 * lightwood_webview_widget_get_thumbnail:
 * @self: a #LightwoodWebViewWidget
 * @width: width of the clone widget
 * @height: height of the clone widget
 *
 * this will gets the Thumbnail.
 *
 * Returns: thumnail string
 */


/* FIXME: this method doesn't make much sense. 'data' is never written and the semantic
 * of the return value isn't clear. It it supposed to be the path of the thumnail?
 * Or the actual data of the thumnail? Shouldn't this be self->priv->m_thumbnail
 * as that's what the PROP_THUMBNAIL accessor returns?
 */
unsigned char *
lightwood_webview_widget_get_thumbnail (LightwoodWebViewWidget *self,
                                        gint width,
                                        gint height)
{
    LightwoodWebViewWidgetPrivate* priv = self->priv;
    ClutterActor *actor = CLUTTER_ACTOR (self);
    WebKitWebPage* webPage = 	webkit_iweb_view_get_page(WEBKIT_IWEB_VIEW(priv->m_webview));
    WebKitWebFrame* mainFrame = webkit_web_page_get_main_frame(webPage);
    cairo_format_t format =  CAIRO_FORMAT_ARGB32;
    gint view_width = (gint) clutter_actor_get_width (actor);
    gint view_height = (gint) clutter_actor_get_height (actor);

    int stride = cairo_format_stride_for_width (format, width);
    unsigned char* data = malloc (stride * height);
    cairo_surface_t* surface = cairo_image_surface_create(format,width,height);
	cairo_t*   cr = cairo_create (surface);
    cairo_scale(cr,width/view_width,height/view_height);

    webkit_web_frame_render(mainFrame,cr,0,0,view_width,view_height);
    cairo_surface_flush(surface);
    cairo_status_t result = cairo_surface_write_to_png(surface,"webview.png");
    g_assert(result == CAIRO_STATUS_SUCCESS);
    cairo_surface_destroy(surface);

    return data;
}



//-----------------------------webview policy handlers and notifications----------------------------

/*
static gboolean
_lightwood_webview_widget_navigation_policy_decision_requested_cb(WebKitWebPage           *web_page,
															WebKitWebFrame            *frame,
															WebKitNetworkRequest      *request,
															WebKitWebNavigationAction *navigation_action,
															WebKitWebPolicyDecision   *policy_decision,
															gpointer                   user_data)
{
	LightwoodWebViewWidget* self = LIGHTWOOD_WEBVIEW_WIDGET(user_data);
	LightwoodWebViewWidgetPrivate* priv = WEBVIEW_WIDGET_PRIVATE(self);
	gboolean handled = FALSE;
	if(g_strrstr(webkit_network_request_get_uri(request),"pdf"))
	{
		g_print("\n open pdf!!!!!!........");
		clutter_actor_hide(priv->m_webview);
		lightwood_webview_widget_pdf_viewer_show(priv->m_pdf_viewer,1.0,webkit_network_request_get_uri(request));
		handled = TRUE;

	}
	else
	{
		lightwood_webview_widget_pdf_viewer_hide(priv->m_pdf_viewer);
		clutter_actor_show(priv->m_webview);
	}
	g_print("\n !!!!!!!! notify zoom level.............");
	g_signal_emit_by_name(G_OBJECT(self),"notify::zoom_level", NULL, NULL);
	return handled;
}
*/

/*callback when a mime is detected.decide how to handle the different mime types.
 * 1. if its an attachment just download it.
 * 2. else if possible display in the webpage itself
 * 3. else download it.
 * */
static gboolean
_lightwood_webview_widget_mime_type_policy_decision_requested_cb(WebKitWebPage* page, WebKitWebFrame* frame,
                                                       WebKitNetworkRequest* request, const char* mime_type,
                                                       WebKitWebPolicyDecision* decision, gpointer data)
{
//	g_printf("\n mime type = %s",mime_type);

	gboolean mime_handled=FALSE;

    if( !webkit_web_page_can_show_mime_type(page, mime_type)
    		//&& !g_strrstr(mime_type,"pdf")
      )
	{
   		webkit_web_policy_decision_download(decision);
	    mime_handled = TRUE;
	}
    else
    {
		WebKitNetworkResponse* response = webkit_web_frame_get_network_response(frame);
		g_assert(response != NULL);
		SoupMessage* message = webkit_network_response_get_message(response);
		char* disposition = NULL;
		if(message != NULL)
		{
			soup_message_headers_get_content_disposition(message->response_headers,
													 &disposition, NULL);

			g_object_unref(response);

			//if its an attachment download it regardless of the mime_type

			if((NULL != disposition) && g_str_equal(disposition, "attachment"))
			{
			  webkit_web_policy_decision_download(decision);
			  mime_handled = TRUE;
			}
			else
			{
				g_signal_emit (G_OBJECT(data), signals[NEW_PAGE_REQUESTED], 0, g_strdup(webkit_network_request_get_uri(request)),&mime_handled);
				if(!mime_handled && g_strrstr(mime_type,"pdf"))
				{
					g_print("\n open pdf please......");
					mime_handled = TRUE;
				}

			}

		}
    }
    /*if( g_str_equal(mime_type, "text/html") ||
		g_str_equal(mime_type, "text/plain") ||
		g_str_equal(mime_type, "audio/ogg"))
	{
		//let's check and display the content
    	//g_print("\n display mime type= %s",mime_type);
		g_assert(webkit_web_page_can_show_mime_type(page, mime_type));
	}
	else*/

    return mime_handled;
}
/*callback when a there is a request to open a new window for hyperlink.
 * here we decide to open in the current window itself*/
static gboolean
_lightwood_webview_widget_new_window_policy_decision_requested_cb(WebKitWebPage* page, WebKitWebFrame* frame,
                                                       WebKitNetworkRequest* request,  WebKitWebNavigationAction *navigation_action,
                                                       WebKitWebPolicyDecision* decision, gpointer data)
{
	//webkit_web_page_load_request(page,request);
	LightwoodWebViewWidgetPrivate* priv = LIGHTWOOD_WEBVIEW_WIDGET(data)->priv;
	gboolean flag=FALSE;
	g_object_get(priv->m_gesture_handler,"new-view-enable",&flag,NULL);
	if(!flag)
	{
         g_signal_emit ((LightwoodWebViewWidget*)data, signals[NEW_VIEW_REQUESTED], 0,webkit_network_request_get_uri(request));
        }
        g_object_set(priv->m_gesture_handler,"new-view-enable",FALSE,NULL);	
       return TRUE;
}



/*
static GMainLoop* event_loop = NULL;
static MmdLib_MsgBoxProp _popup = {MMD_MSG_BOX_TYPE_INFO,PKGDATADIR"/mmd-apps/icons/icon_browser.png",NULL,NULL,NULL,10};

static gboolean _lightwood_webview_widget_geolocation_policy_decision_confirm_cb(MmdLib_MsgBoxResp resp,gpointer user_data)
{
	WebKitGeolocationPolicyDecision* policy_decision = (WebKitGeolocationPolicyDecision*)user_data;
	if(resp == MMD_MSG_BOX_RESP_OK)
	{
		webkit_geolocation_policy_allow(policy_decision);
	}
	else
	{
		webkit_geolocation_policy_deny(policy_decision);
	}
	g_main_loop_quit(event_loop);
	return FALSE;
}
static gboolean
_lightwood_webview_widget_geolocation_policy_decision_requested_cb(WebKitWebView *webView,
																WebKitWebFrame                  *frame,
																WebKitGeolocationPolicyDecision *policy_decision,
																gpointer                         user_data)
{
	event_loop = g_main_loop_new(NULL,FALSE);
	_popup.enType = MMD_MSG_BOX_TYPE_CONFIRM_OC;
	_popup.enMsgDisplay = MMD_MSG_DOUBLE_LINE;
	const gchar* uri = webkit_web_frame_get_uri(frame);
	//_popup.pMessage = g_strdup_printf("%.35s%s?wants to know your physical location.",uri,strlen(uri) > 35?"...":"");
	_popup.pMessage = g_strdup_printf("%s?wants to know your physical location.",uri);
	_popup.pCallBk = _lightwood_webview_widget_geolocation_policy_decision_confirm_cb; 	_popup.pUserData = policy_decision;
	_popup.timeout = 50;
	 mmdlib_message_box_create_and_show(0xff, _popup);


	g_main_loop_run(event_loop);

	g_main_loop_unref(event_loop);
	event_loop = NULL;

	return TRUE;
}
*/

/*callback when an attachment is detected.
 * emit a signal with the download details to inform the download manager to handle it*/
static gboolean
_lightwood_webview_widget_download_decision_requested_cb(WebKitWebPage* page, WebKitDownload* download,LightwoodWebViewWidget *self)
{
	//ENTRY_TRACE
	g_signal_emit (self, signals[MIME_ALERT], 0,download);
	return TRUE;

	/*
	GError *error = NULL;
	gchar* filename=NULL;

	gchar* uri  = g_strdup(webkit_network_request_get_uri(
				webkit_download_get_network_request(download)));
	filename = g_strdup_printf("%s/Downloads/%s",g_get_home_dir(),g_strrstr(uri,"/")+1);
	g_print("uri=%s,filename =%s",uri,filename);
	g_free(uri);
   uri = g_filename_to_uri(filename, NULL, NULL);
   webkit_download_set_destination_uri(download, uri);
   g_free(filename);
   g_free(uri);
	return TRUE;
	*/
}

/*
static gboolean
_lightwood_webview_widget_console_message_decision_requested_cb(WebKitWebPage* page,gchar* message,gint lineno, gchar* source)
{
	//lets suppress it...
	return TRUE;
}
*/


//---------------------------------end of policy handlers------------------------------------------------------------------------------------

/**
 * lightwood_webview_widget_get_soup_session:
 * @self: a #LightwoodWebViewWidget
 *
 * Return the #LightwoodWebViewWidget:soup_session property
 *
 * Returns: (transfer none): the value of #LightwoodWebViewWidget:soup_session property
 */
SoupSession *
lightwood_webview_widget_get_soup_session (LightwoodWebViewWidget *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_WEBVIEW_WIDGET (self), NULL);

  return webkit_get_default_session();
}

/**
 * lightwood_webview_widget_get_form_assistance:
 * @self: a #LightwoodWebViewWidget
 *
 * Return the #LightwoodWebViewWidget:form_assistance property
 *
 * Returns: the value of #LightwoodWebViewWidget:form_assistance property
 */
gboolean
lightwood_webview_widget_get_form_assistance (LightwoodWebViewWidget *self)
{
  gboolean state;

  g_return_val_if_fail (LIGHTWOOD_IS_WEBVIEW_WIDGET (self), FALSE);

  g_object_get (self->priv->m_webview, "form-assistance", &state, NULL);

  return state;
}

/**
 * lightwood_webview_widget_get_load_status:
 * @self: a #LightwoodWebViewWidget
 *
 * Return the #LightwoodWebViewWidget:load_status property
 *
 * Returns: the value of #LightwoodWebViewWidget:load_status property
 */
WebKitLoadStatus
lightwood_webview_widget_get_load_status (LightwoodWebViewWidget *self)
{
  WebKitWebPage *webPage;

  g_return_val_if_fail (LIGHTWOOD_IS_WEBVIEW_WIDGET (self), WEBKIT_LOAD_FAILED);

  webPage = webkit_iweb_view_get_page (WEBKIT_IWEB_VIEW (self->priv->m_webview));

  return webkit_web_page_get_load_status (webPage);
}

/**
 * lightwood_webview_widget_get_gesture_handling:
 * @self: a #LightwoodWebViewWidget
 *
 * Return the #LightwoodWebViewWidget:gesture_handling property
 *
 * Returns: the value of #LightwoodWebViewWidget:gesture_handling property
 */
gboolean
lightwood_webview_widget_get_gesture_handling (LightwoodWebViewWidget *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_WEBVIEW_WIDGET (self), FALSE);

  return self->priv->m_default_event_handling;
}

/**
 * lightwood_webview_widget_get_uri:
 * @self: a #LightwoodWebViewWidget
 *
 * Return the #LightwoodWebViewWidget:uri property
 *
 * Returns: (nullable): the value of #LightwoodWebViewWidget:uri property
 */
const gchar *
lightwood_webview_widget_get_uri (LightwoodWebViewWidget *self)
{
  WebKitWebPage* webPage;

  g_return_val_if_fail (LIGHTWOOD_IS_WEBVIEW_WIDGET (self), NULL);

  webPage = webkit_iweb_view_get_page (WEBKIT_IWEB_VIEW (self->priv->m_webview));
  return webkit_web_page_get_uri (webPage);
}

/**
 * lightwood_webview_widget_get_title:
 * @self: a #LightwoodWebViewWidget
 *
 * Return the #LightwoodWebViewWidget:title property
 *
 * Returns: (nullable): the value of #LightwoodWebViewWidget:title property
 */
const gchar *
lightwood_webview_widget_get_title (LightwoodWebViewWidget *self)
{
  WebKitWebPage* webPage;

  g_return_val_if_fail (LIGHTWOOD_IS_WEBVIEW_WIDGET (self), NULL);

  webPage = webkit_iweb_view_get_page (WEBKIT_IWEB_VIEW (self->priv->m_webview));
  return webkit_web_page_get_title (webPage);
}

/**
 * lightwood_webview_widget_get_audio_status:
 * @self: a #LightwoodWebViewWidget
 *
 * Return the #LightwoodWebViewWidget:audio_status property
 *
 * Returns: the value of #LightwoodWebViewWidget:audio_status property
 */
LightwoodWebViewAudioStatus
lightwood_webview_widget_get_audio_status (LightwoodWebViewWidget *self)
{
  LightwoodWebViewAudioStatus state;

  g_return_val_if_fail (LIGHTWOOD_IS_WEBVIEW_WIDGET (self), LIGHTWOOD_WEBVIEW_AUDIO_STATUS_ENDED);

  g_object_get (self->priv->m_audioManager, "audio_status", &state,NULL);

  return state;
}

/**
 * lightwood_webview_widget_get_show_thumbnail:
 * @self: a #LightwoodWebViewWidget
 *
 * Return the #LightwoodWebViewWidget:show_thumbnail property
 *
 * Returns: the value of #LightwoodWebViewWidget:show_thumbnail property
 */
gboolean
lightwood_webview_widget_get_show_thumbnail (LightwoodWebViewWidget *self)
{
  g_return_val_if_fail (LIGHTWOOD_IS_WEBVIEW_WIDGET (self), FALSE);

  return self->priv->show_thumbnail;
}

/**
 * lightwood_webview_widget_set_show_thumbnail:
 * @self: a #LightwoodWebViewWidget
 * @show: %TRUE to show thumbnail
 *
 * Update the #LightwoodWebViewWidget:show_thumbnail property
 */
void
lightwood_webview_widget_set_show_thumbnail (LightwoodWebViewWidget *self,
                                             gboolean show)
{
  g_return_if_fail (LIGHTWOOD_IS_WEBVIEW_WIDGET (self));

  self->priv->show_thumbnail = show;

  if (self->priv->m_thumbnail == NULL)
    goto out;

  if (show)
    clutter_actor_show (self->priv->m_thumbnail);
  else
    clutter_actor_hide (self->priv->m_thumbnail);

out:
  g_object_notify (G_OBJECT (self), "show_thumbnail");
}
