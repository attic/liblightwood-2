/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* liblightwood-web-view-widget.h */

#ifndef _LIGHTWOOD_WEBVIEW_WIDGET_H
#define _LIGHTWOOD_WEBVIEW_WIDGET_H

#include <glib-object.h>
#include <clutter/clutter.h>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-prototypes"
#include <mx/mx.h>
#include <webkit/webkitclutter.h>
#pragma GCC diagnostic pop
#include "liblightwood-webview-assistant.h"

G_BEGIN_DECLS

#define LIGHTWOOD_TYPE_WEBVIEW_WIDGET lightwood_webview_widget_get_type()

#define LIGHTWOOD_WEBVIEW_WIDGET(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  LIGHTWOOD_TYPE_WEBVIEW_WIDGET, LightwoodWebViewWidget))

#define LIGHTWOOD_WEBVIEW_WIDGET_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  LIGHTWOOD_TYPE_WEBVIEW_WIDGET, LightwoodWebViewWidgetClass))

#define LIGHTWOOD_IS_WEBVIEW_WIDGET(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  LIGHTWOOD_TYPE_WEBVIEW_WIDGET))

#define LIGHTWOOD_IS_WEBVIEW_WIDGET_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  LIGHTWOOD_TYPE_WEBVIEW_WIDGET))

#define LIGHTWOOD_WEBVIEW_WIDGET_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  LIGHTWOOD_TYPE_WEBVIEW_WIDGET, LightwoodWebViewWidgetClass))

typedef struct _LightwoodWebViewWidget LightwoodWebViewWidget;
typedef struct _LightwoodWebViewWidgetClass LightwoodWebViewWidgetClass;
typedef struct _LightwoodWebViewWidgetPrivate LightwoodWebViewWidgetPrivate;

struct _LightwoodWebViewWidget
{
  ClutterActor parent;

  LightwoodWebViewWidgetPrivate *priv;
};

struct _LightwoodWebViewWidgetClass
{
  ClutterActorClass parent_class;
};


typedef enum
{
	LIGHTWOOD_WEBVIEW_AUDIO_REQUEST_NONE,
	LIGHTWOOD_WEBVIEW_AUDIO_REQUEST_PLAY,
	LIGHTWOOD_WEBVIEW_AUDIO_REQUEST_PAUSE,
	LIGHTWOOD_WEBVIEW_AUDIO_REQUEST_STOP
} LightwoodWebViewAudioRequest;

typedef enum
{
	LIGHTWOOD_WEBVIEW_AUDIO_STATUS_ENDED = 0,
	LIGHTWOOD_WEBVIEW_AUDIO_STATUS_PLAYING,
	LIGHTWOOD_WEBVIEW_AUDIO_STATUS_PAUSED
} LightwoodWebViewAudioStatus;

GType lightwood_webview_widget_get_type (void) G_GNUC_CONST;


void lightwood_webview_widget_init_session (LightwoodWebViewWidget *self,
                                            const gchar* app_name,
                                            LightwoodWebviewAssistant* assistant);

const gchar *lightwood_webview_widget_get_app_name (LightwoodWebViewWidget *self);

LightwoodWebViewWidget * lightwood_webview_widget_new (void);
ClutterActor* lightwood_webview_widget_get_webview (LightwoodWebViewWidget *self);
void lightwood_webview_widget_load_url (LightwoodWebViewWidget *self,
                                        const gchar* url);
void lightwood_webview_widget_load_string (LightwoodWebViewWidget *self,
										const gchar* content,
										const gchar* mime_type,
										const gchar* encoding,
										const gchar* base_uri);

void lightwood_webview_widget_set_focus (LightwoodWebViewWidget * self,
                                         gboolean isfocussed);

ClutterActor* lightwood_webview_widget_create_clone (LightwoodWebViewWidget *self,
                                                     gfloat width,
                                                     gfloat height,
                                                     gboolean is_deep_copy);

//audio management api
void lightwood_webview_widget_pause_audio (LightwoodWebViewWidget *self);
void lightwood_webview_widget_play_audio (LightwoodWebViewWidget *self);
void lightwood_webview_widget_stop_audio (LightwoodWebViewWidget *self);

void lightwood_webview_widget_context_sensitive_zoom (LightwoodWebViewWidget *self,
                                                      gboolean is_zoomin,
                                                      gfloat zoom_posx,
                                                      gfloat zoom_posy);
void lightwood_webview_widget_save_as_pdf (LightwoodWebViewWidget *self,
                                           const gchar* file_name);
void lightwood_webview_widget_scroll_to (LightwoodWebViewWidget *self,
                                         gint abs_scroll_offset_x,
                                         gint abs_scroll_offset_y);
unsigned char* lightwood_webview_widget_get_thumbnail (LightwoodWebViewWidget *self,
                                                       gint width,
                                                       gint height);

/* Properties */

SoupSession *lightwood_webview_widget_get_soup_session (LightwoodWebViewWidget *self);

gboolean lightwood_webview_widget_get_form_assistance (LightwoodWebViewWidget *self);
void lightwood_webview_widget_set_form_assistance (LightwoodWebViewWidget *self,
                                                   gboolean is_enabled);

WebKitLoadStatus lightwood_webview_widget_get_load_status (LightwoodWebViewWidget *self);

//enable/disable custom gesture handling-double tap,long press
gboolean lightwood_webview_widget_get_gesture_handling (LightwoodWebViewWidget * self);
void lightwood_webview_widget_set_gesture_handling (LightwoodWebViewWidget * self,
                                                    gboolean is_enabled);

const gchar * lightwood_webview_widget_get_uri (LightwoodWebViewWidget * self);

const gchar * lightwood_webview_widget_get_title (LightwoodWebViewWidget * self);

LightwoodWebViewAudioStatus lightwood_webview_widget_get_audio_status (LightwoodWebViewWidget *self);

gdouble lightwood_webview_widget_get_zoom_level (LightwoodWebViewWidget *self);
void lightwood_webview_widget_set_zoom_level (LightwoodWebViewWidget *self,
                                              gdouble request_zoom_level);

gboolean lightwood_webview_widget_get_show_thumbnail (LightwoodWebViewWidget *self);
void lightwood_webview_widget_set_show_thumbnail (LightwoodWebViewWidget *self,
                                                  gboolean show);

G_END_DECLS

#endif /* _LIGHTWOOD_WEBVIEW_WIDGET_H */
