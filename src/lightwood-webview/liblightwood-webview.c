/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* liblightwood-web-view.c */

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-prototypes"
#include <mx/mx.h>
#pragma GCC diagnostic pop
#include "liblightwood-webview.h"

#include <webkit/webkitiwebview.h>
#include <webkit/webkitgwebframe.h>
#include <math.h>
#include <errno.h>
#include <string.h>

#define LIBSOUP_USE_UNSTABLE_REQUEST_API 1
#include <libsoup/soup-cache.h>
#include <libsoup/soup-gnome.h>
#include <libsoup/soup.h>
#include <libsoup/soup-cookie-jar.h>

#define UA_PLATFORM "liblightwood"
#define UA_BROWSER 	"BLUE"
#define USER_STYLESHEET "file://"PKGDATADIR"/mmd-internet/mmd-browser/browser.css"

extern void webkit_init (void);

G_DEFINE_TYPE (LightwoodWebView, lightwood_webview, MX_TYPE_WEB_VIEW)
/*G_DEFINE_TYPE_WITH_CODE(LightwoodWebView, lightwood_webview, MX_TYPE_WEBVIEW,
                        G_IMPLEMENT_INTERFACE(WEBKIT_TYPE_IWEBVIEW,
                                              lightwood_webkit_iwebview_iface_init))*/

#define WEBVIEW_PRIVATE(o) \
		(G_TYPE_INSTANCE_GET_PRIVATE ((o), LIGHTWOOD_TYPE_WEBVIEW, LightwoodWebViewPrivate))

enum
{
PROP_0,

	PROP_WEBVIEW_ASSISTANT,
	PROP_FORM_ASSISTANCE
};

enum
{
	WEBVIEW_SPELLER_ALERT,
	LAST_SIGNAL
};


static gchar* 				  s_app_name;
static LightwoodWebviewAssistant* s_webview_assistant;

struct _LightwoodWebViewPrivate
{
	gboolean 	m_is_enabled;
	gboolean 	m_recieved_editable_notification;
	gdouble 	m_scroll_y;
	LightwoodWebViewSpeller* m_speller;
	GObject* m_alert_handler;
	gboolean isZoomOut;
	gboolean ToggleZoom;
	gfloat ZoomLevel;
	gdouble current_content_scale;
	gdouble zoomFactor;
	WebKitDOMElement* node;
};

static guint signals[LAST_SIGNAL] = { 0, };



static WebKitIPopupMenu*
_lightwood_webview_create_popup_menu(WebKitIWebView* webView);
static void
_lightwood_webview_input_method_state_cb(
		WebKitWebPage* page,WebKitDOMElement* node,gboolean isEditable,LightwoodWebView *self);
static void
_lightwood_webview_remove_focus_cb(LightwoodWebView *self);
static void _lightwood_webview_zoom_transitions_completed_cb(ClutterActor* webview, gpointer data);

static void
lightwood_webview_get_property (GObject    *object,
                           guint       property_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
	LightwoodWebView *self = LIGHTWOOD_WEBVIEW(object);
  switch (property_id)
    {
	case PROP_FORM_ASSISTANCE:
	  g_value_set_boolean(value,self->priv->m_is_enabled);
	 break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
lightwood_webview_set_property (GObject      *object,
                           guint         property_id,
                           const GValue *value,
                           GParamSpec   *pspec)
{
	LightwoodWebView *self = LIGHTWOOD_WEBVIEW(object);
  switch (property_id)
    {
	case PROP_FORM_ASSISTANCE:
		if(self->priv->m_is_enabled != g_value_get_boolean(value))
		{
			self->priv->m_is_enabled = g_value_get_boolean(value);
			g_object_notify(G_OBJECT(self),"form-assistance");
			if(!self->priv->m_is_enabled)
			{
				//_lightwood_webview_remove_focus_cb(self);
				lightwood_webview_set_focus(self,FALSE);
			}


		}
		break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
lightwood_webview_dispose (GObject *object)
{
	LightwoodWebView *self = LIGHTWOOD_WEBVIEW(object);

	if (self->priv)
	{
		if(self->priv->m_alert_handler)
			g_object_unref(self->priv->m_alert_handler);
/*		if(self->priv->m_virtualKeyboard)
		{
			//clutter_container_remove_actor(CLUTTER_CONTAINER(self),CLUTTER_ACTOR(self->priv->m_virtualKeyboard->m_speller));
			g_object_unref(self->priv->m_virtualKeyboard);
			self->priv->m_virtualKeyboard = NULL;
		}*/
	}

  G_OBJECT_CLASS (lightwood_webview_parent_class)->dispose (object);

}

/*static gboolean _lightwood_webview_ignored_motion_event(ClutterActor* actor, ClutterMotionEvent* event)
{
    return FALSE;
}*/

static void
lightwood_webview_class_init (LightwoodWebViewClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  g_type_class_add_private (klass, sizeof (LightwoodWebViewPrivate));

  object_class->get_property = lightwood_webview_get_property;
  object_class->set_property = lightwood_webview_set_property;
  object_class->dispose = lightwood_webview_dispose;

  GParamSpec *pspec;
  pspec = g_param_spec_boolean( "form-assistance",
								"form-assistance",
								"form-assistance",
								true,
								G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_FORM_ASSISTANCE, pspec);

  signals[WEBVIEW_SPELLER_ALERT] =
    g_signal_new ("webview_speller_alert",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL,
                  g_cclosure_marshal_VOID__BOOLEAN,
                  G_TYPE_NONE, 1,G_TYPE_BOOLEAN);

  //ClutterActorClass* actor_class = CLUTTER_ACTOR_CLASS (klass);
  //actor_class->motion_event = _lightwood_webview_ignored_motion_event;
  //MxWebViewClass *mx_webview_class = MX_WEBVIEW_CLASS (klass);
 // mx_webview_class->create_popup_menu =_lightwood_webview_create_popup_menu;
}


void lightwood_webview_init_session (const gchar* app_name,LightwoodWebviewAssistant* webview_assistant)
{
	//should be called only once per process
	static gboolean is_initialized = FALSE;
	if(is_initialized) return;
	is_initialized = TRUE;

	webkit_init();
	s_app_name = g_strdup(app_name);
	s_webview_assistant = webview_assistant;

	if(webview_assistant &&	webview_assistant->m_renderTheme){
		g_print("\n custom theme updated...");
		webkit_irender_theme_set_default(webview_assistant->m_renderTheme);
	}


	SoupSession* session = webkit_get_default_session();

	//set timeout
	//g_object_set(G_OBJECT(session),	SOUP_SESSION_TIMEOUT,40,NULL);
	/* Store cookies in moz-compatible SQLite format */


	char* cacheDir = g_build_filename(g_get_user_cache_dir(), "webkitclutter", NULL);
	SoupCache* cache = soup_cache_new(cacheDir, SOUP_CACHE_SHARED);
	soup_session_add_feature(session, SOUP_SESSION_FEATURE(cache));
	soup_cache_load(cache);
	g_object_unref(cache);

	gchar* cookie_file = g_build_filename (cacheDir,g_strdup_printf("%s-cookies.sqlitedb",app_name), NULL);
	 g_print("\n cookie file=%s",cookie_file);
	 SoupCookieJar*  cookie_jar = soup_cookie_jar_db_new (cookie_file, FALSE);
	 soup_cookie_jar_set_accept_policy(cookie_jar,SOUP_COOKIE_JAR_ACCEPT_ALWAYS);
	 soup_session_add_feature (session, SOUP_SESSION_FEATURE (cookie_jar));
	 g_object_unref (cookie_jar);

	 g_free (cookie_file);
	 g_free (cacheDir);



	    if(webview_assistant && webview_assistant->m_soupAuth_handler)
	    	soup_session_add_feature(session, webview_assistant->m_soupAuth_handler);



/*		const gchar* user_agent = webkit_web_settings_get_user_agent(webkit_settings);
		//Mozilla/5.0 (Unknown; Linux i686) AppleWebKit/534.7+ (KHTML, like Gecko) Version/5.0 Safari/534.7+
		GRegex * ua_regex = g_regex_new ("(.*)(Unknown)(.*)(Safari)(.*)",0,0,NULL);
		gchar* replace_text = g_strdup_printf("\\1%s\\3\\4\\5",UA_PLATFORM);
		gchar * new_user_agent = g_regex_replace(ua_regex,user_agent,-1,0,replace_text,0,NULL);
		g_object_set(webkit_settings, "user-agent", new_user_agent, NULL);
		g_print("\n user agent-%s",webkit_web_settings_get_user_agent(webkit_settings));

    	const gchar* css_uri = USER_STYLESHEET;
    	g_print("user css =%s",css_uri);
    	g_object_set(webkit_settings, "user-stylesheet-uri", css_uri, NULL);*/

	  //proxy settings
//	  SoupURI* proxy_uri = NULL;
//	  pxProxyFactory* proxy_factory = px_proxy_factory_new();
//	  gchar** proxies = px_proxy_factory_get_proxies(proxy_factory,"http://www.google.com");
//	  if(proxies)
//	  {
//		  guint8 i=0;
//		  for (i = 0; proxies[i]; i++)
//		  {
//			if (!strcmp (proxies[i], "direct://"))
//				  break;
//			 if (strncmp (proxies[i], "http://", 7) == 0)
//			 {
//			    proxy_uri = soup_uri_new (proxies[i]);
//			    break;
//			}
//		  }
//
//	      for (i = 0; proxies[i]; i++)
//	            g_free (proxies[i]);
//	      g_free (proxies);
//	  }
//	  px_proxy_factory_free(proxy_factory);
//
//	  if(proxy_uri)
//	  g_print("\n webkit session proxy uri = %s",soup_uri_to_string(proxy_uri,FALSE));
//	  g_object_set(session,SOUP_SESSION_PROXY_URI,proxy_uri,NULL);


#ifdef SOUP_TYPE_PASSWORD_MANAGER
  /* Use GNOME keyring to store passwords. Only add the manager if we
     are not using a private session, otherwise we want any new
     password to expire when we exit *and* we don't want to use any
     existing password in the keyring */
    soup_session_add_feature_by_type (session, SOUP_TYPE_PASSWORD_MANAGER_GNOME);
#endif

}


static void
_lightwood_webview_on_allocate_cb(ClutterActor* actor,const ClutterActorBox *allocation,
													ClutterAllocationFlags  flags,LightwoodWebView* self)
{
		  if(s_webview_assistant && s_webview_assistant->alert_handler_factory && !self->priv->m_alert_handler)
		  		self->priv->m_alert_handler =s_webview_assistant->alert_handler_factory(CLUTTER_ACTOR(clutter_actor_get_stage(CLUTTER_ACTOR(self))),s_app_name,CLUTTER_ACTOR(self));
		  g_signal_handlers_disconnect_by_func (actor,G_CALLBACK(_lightwood_webview_on_allocate_cb), self);

}

static void
lightwood_webview_init (LightwoodWebView *self)
{
	LightwoodWebViewPrivate* priv = self->priv = WEBVIEW_PRIVATE (self);
	WEBKIT_IWEB_VIEW_GET_INTERFACE(self)->create_popup_menu = _lightwood_webview_create_popup_menu;

	g_signal_connect (CLUTTER_ACTOR(self), "allocation-changed", G_CALLBACK (_lightwood_webview_on_allocate_cb), self);

	priv->m_alert_handler = NULL;
	//if(s_webview_assistant && s_webview_assistant->alert_handler_factory)
		//priv->m_alert_handler =s_webview_assistant->alert_handler_factory(s_app_name,CLUTTER_ACTOR(self));
	priv->m_recieved_editable_notification = FALSE;

	//if(!clutter_actor_get_parent(CLUTTER_ACTOR(priv->m_virtualKeyboard)))
		//clutter_container_add_actor(self,CLUTTER_ACTOR(priv->m_virtualKeyboard));

	//clutter_container_add_actor(CLUTTER_CONTAINER((CLUTTER_ACTOR(self))),priv->m_popupMenu->m_vPopupMenuRoller);

	WebKitWebPage* webPage = webkit_iweb_view_get_page(WEBKIT_IWEB_VIEW(self));
    WebKitWebSettings* webkit_settings = webkit_web_page_get_settings(webPage);
    g_object_set(webkit_settings,
				"enable-scripts",TRUE,
				"enable-plugins",TRUE,
				"enable-java-applet",TRUE,
				"javascript-can-open-windows-automatically", TRUE,
                "enable-developer-extras",TRUE,
                "enable-universal-access-from-file-uris",TRUE,
                "enable-webgl",TRUE,
             //   "user-agent","AppNumber=23000 DEV=liblightwood APIVersion=2.1.0.0",
                 NULL);
    g_object_set(webkit_settings,"enable-tiled-backing-store",TRUE,NULL);
    ClutterAction *pan_action=NULL;
    g_object_get(self,"pan-action",&pan_action,NULL);

    if(pan_action)
    g_object_set(pan_action,"acceleration-factor",3.0,"deceleration",3.0,NULL);

    g_object_connect (webPage,
					   "signal::input-method-state-set",G_CALLBACK(_lightwood_webview_input_method_state_cb),self,
					   //"signal::microfocus-changed",G_CALLBACK(_lightwood_webview_tab_focus_changed_cb),self,
					   NULL);

    priv->m_is_enabled = TRUE;

    priv->m_speller = NULL;
    priv->isZoomOut = FALSE;
    priv->ToggleZoom=FALSE;
    priv->ZoomLevel=1.0;
    priv->current_content_scale=1.0;
    priv->zoomFactor=1.0;

}

ClutterActor*
lightwood_webview_new (void)
{
	g_assert(s_app_name != NULL);
	LightwoodWebView* self = g_object_new (LIGHTWOOD_TYPE_WEBVIEW,NULL);
	return CLUTTER_ACTOR(self);
}

void lightwood_webview_set_enable_focus(LightwoodWebView* self,gboolean is_enabled)
{
	if(s_webview_assistant && s_webview_assistant->alert_handler_factory && !self->priv->m_alert_handler)
			self->priv->m_alert_handler =s_webview_assistant->alert_handler_factory(
												CLUTTER_ACTOR(clutter_actor_get_stage(CLUTTER_ACTOR(self))),s_app_name,CLUTTER_ACTOR(self));

	if(self->priv->m_is_enabled != is_enabled)
	{
		self->priv->m_is_enabled = is_enabled;
		g_object_notify(G_OBJECT(self),"form-assistance");
	}
	if(!self->priv->m_is_enabled)
		_lightwood_webview_remove_focus_cb(self);

}

void lightwood_webview_set_focus(LightwoodWebView* self,gboolean is_focussed)
{
	g_assert(CLUTTER_IS_ACTOR(self));

	if(!is_focussed)
	{
		_lightwood_webview_remove_focus_cb(self);
		g_print("\n lightwood_webview_set_focus - disable");
		if(self->priv->m_speller)
			lightwood_webview_speller_hide(self->priv->m_speller);
	}
	else
	{
		g_print("\n lightwood_webview_set_focus - enable");
		clutter_actor_grab_key_focus (CLUTTER_ACTOR(self));
	}
}

static void _lightwood_webview_zoom_transitions_completed_cb(ClutterActor* webview, gpointer data)
{
    WebKitWebPage* webPage = webkit_iweb_view_get_page(WEBKIT_IWEB_VIEW(webview));
    WebKitWebFrame* mainFrame = webkit_web_page_get_main_frame(webPage);
    double zoomFactor=1.0;
    g_object_get(webview, "fast-zoom-level", &zoomFactor, NULL);
    g_print("\n _lightwood_webview_zoom_transitions_completed_cb ");
    g_signal_handlers_disconnect_by_func(webview,_lightwood_webview_zoom_transitions_completed_cb,data);
    if(WEBVIEW_PRIVATE(LIGHTWOOD_WEBVIEW(webview))->isZoomOut)
    	{
    	webkit_web_page_set_zoom_level(webPage,WEBVIEW_PRIVATE(LIGHTWOOD_WEBVIEW(webview))->ZoomLevel);
    	}
    zoomFactor *= webkit_web_frame_get_contents_scale(mainFrame);

    webkit_web_frame_set_contents_frozen(mainFrame, FALSE);
    webkit_web_frame_set_contents_scale(mainFrame, zoomFactor);
   // g_object_notify(G_OBJECT(webPage), "zoom-level");
    //webkit_web_frame_set_zoom_level(mainFrame,zoomFactor);
    //webkit_web_page_set_zoom_level(webPage,zoomFactor);

   mx_web_view_set_fast_zoom_level(MX_WEB_VIEW(webview),1.0);
}


void lightwood_webview_normal_page_zoom(ClutterActor* self,gdouble request_zoom_level)
{

	WebKitWebPage* pWebPage = webkit_iweb_view_get_page(WEBKIT_IWEB_VIEW(self));
	if(WEBVIEW_PRIVATE(LIGHTWOOD_WEBVIEW(self))->current_content_scale == request_zoom_level)
	 return;
	webkit_web_page_set_zoom_level(pWebPage,request_zoom_level);
	WEBVIEW_PRIVATE(LIGHTWOOD_WEBVIEW(self))->current_content_scale=request_zoom_level;

}



gdouble lightwood_webview_get_zoom_factor(ClutterActor* self)
{
return WEBVIEW_PRIVATE(LIGHTWOOD_WEBVIEW(self))->zoomFactor;
}

void lightwood_webview_page_zoom(ClutterActor* self,gdouble request_zoom_level)
{
	WebKitWebPage* pWebPage = webkit_iweb_view_get_page(WEBKIT_IWEB_VIEW(self));
	WebKitWebFrame* pMainFrame = webkit_web_page_get_main_frame(pWebPage);
	if(webkit_web_frame_get_contents_scale(pMainFrame) == request_zoom_level)
		return;

				g_signal_connect(self, "transitions-completed",
						G_CALLBACK(_lightwood_webview_zoom_transitions_completed_cb), NULL);
				webkit_web_frame_set_contents_frozen(pMainFrame, true);
	clutter_actor_save_easing_state (self);
	//clutter_actor_set_easing_mode (self, CLUTTER_LINEAR);
	clutter_actor_set_easing_duration (self, 600);
	float contentsScale = webkit_web_frame_get_contents_scale(pMainFrame);
	int xZoomPosition, yZoomPosition;
	        xZoomPosition = webkit_web_frame_get_x_scroll_offset(pMainFrame) / contentsScale;
	        yZoomPosition = webkit_web_frame_get_y_scroll_offset(pMainFrame) / contentsScale;

	        float maxX, maxY;
	        webkit_web_page_get_max_offsets_for_zoom_level(pWebPage, 1.0, &maxX, &maxY);

	        xZoomPosition = MIN(maxX, xZoomPosition);
	        yZoomPosition = MIN(maxY, yZoomPosition);

	        mx_web_view_set_x_scroll_offset(MX_WEB_VIEW(self), xZoomPosition*(WEBVIEW_PRIVATE(LIGHTWOOD_WEBVIEW(self))->ZoomLevel));
	        mx_web_view_set_y_scroll_offset(MX_WEB_VIEW(self), yZoomPosition*(WEBVIEW_PRIVATE(LIGHTWOOD_WEBVIEW(self))->ZoomLevel));

	if(contentsScale > 1.0)
	{
		mx_web_view_set_fast_zoom_level(MX_WEB_VIEW(self),
			request_zoom_level/contentsScale);
	}
	clutter_actor_restore_easing_state (self);

}

static WebKitDOMElement* _lightwood_Choose_element_to_zoom_cb(GSList* pCandidatesList, void* pUserData)
{
	if (pCandidatesList == NULL)
		return NULL;
	GSList* pIter;

	for (pIter = pCandidatesList; pIter; pIter = g_slist_next(pIter))
	{
		//WebKitDOMElement* pElement = WEBKIT_DOM_ELEMENT(pIter->data);
	}
	return WEBKIT_DOM_ELEMENT(pCandidatesList->data);
}

void lightwood_webview_context_sensitive_zoom(ClutterActor* pWebActor,gboolean is_zoomin,gfloat zoom_posx,gfloat zoom_posy)
{
	WebKitWebPage* pWebPage = webkit_iweb_view_get_page(WEBKIT_IWEB_VIEW(pWebActor));
	WebKitWebFrame* pWebMainFrame = webkit_web_page_get_main_frame(pWebPage);
	 WEBVIEW_PRIVATE(LIGHTWOOD_WEBVIEW(pWebActor))->ZoomLevel = webkit_web_page_get_zoom_level(pWebPage);
	if(!is_zoomin)
	{
		 WEBVIEW_PRIVATE(LIGHTWOOD_WEBVIEW(pWebActor))->isZoomOut=TRUE;
		 lightwood_webview_page_zoom(pWebActor,1.0);
		return;
	}
 if( WEBVIEW_PRIVATE(LIGHTWOOD_WEBVIEW(pWebActor))->ToggleZoom == FALSE)
 {
	 WEBVIEW_PRIVATE(LIGHTWOOD_WEBVIEW(pWebActor))->ToggleZoom=TRUE;
	float xCoOrdinate;
	float yCoOrdinate;

	clutter_actor_transform_stage_point(pWebActor, zoom_posx, zoom_posy, &xCoOrdinate, &yCoOrdinate);

	int inxZoomPos;
	double dbZoomFactor;
	int inyZoomPos;

	if (!webkit_web_page_get_zoom_target(pWebPage, xCoOrdinate, yCoOrdinate, &inxZoomPos,
			&inyZoomPos,&dbZoomFactor, _lightwood_Choose_element_to_zoom_cb, pWebActor))
	{
		return ;
	}

	g_signal_connect(pWebActor, "transitions-completed",
							G_CALLBACK(_lightwood_webview_zoom_transitions_completed_cb), NULL);
	webkit_web_frame_set_contents_frozen(pWebMainFrame, TRUE);

	dbZoomFactor /= webkit_web_frame_get_contents_scale(pWebMainFrame);
	WEBVIEW_PRIVATE(LIGHTWOOD_WEBVIEW(pWebActor))->isZoomOut=FALSE;
	clutter_actor_save_easing_state(pWebActor);
	//clutter_actor_set_easing_mode (pWebActor, CLUTTER_LINEAR);
	clutter_actor_set_easing_duration(pWebActor, 600);
	//Set x and y scroll offset
	mx_web_view_set_y_scroll_offset(MX_WEB_VIEW(pWebActor), ((WEBVIEW_PRIVATE(LIGHTWOOD_WEBVIEW(pWebActor))->ZoomLevel)*inyZoomPos));
	mx_web_view_set_x_scroll_offset(MX_WEB_VIEW(pWebActor), ((WEBVIEW_PRIVATE(LIGHTWOOD_WEBVIEW(pWebActor))->ZoomLevel)*inxZoomPos));
		//Set the fast zoom level to pWebActor.
	if(dbZoomFactor >= 1.0)
	{
		mx_web_view_set_fast_zoom_level(MX_WEB_VIEW(pWebActor), dbZoomFactor);
	}
	clutter_actor_restore_easing_state(pWebActor);
 }
 else
 {
	 WEBVIEW_PRIVATE(LIGHTWOOD_WEBVIEW(pWebActor))->ToggleZoom=FALSE;
	 WEBVIEW_PRIVATE(LIGHTWOOD_WEBVIEW(pWebActor))->isZoomOut=TRUE;
	 lightwood_webview_page_zoom(pWebActor,1.0);
	 return;
 }
}

static WebKitIPopupMenu* _lightwood_webview_create_popup_menu(WebKitIWebView* webView)
{

	g_print("\n _lightwood_webview_create_popup_menu");
	g_assert(s_webview_assistant && s_webview_assistant->popupmenu_factory);


	WebKitIPopupMenu* popup_menu = s_webview_assistant->popupmenu_factory(275.0,clutter_actor_get_height(CLUTTER_ACTOR(webView)));

	g_object_set(CLUTTER_ACTOR(popup_menu),
			"x-expand",TRUE,
			"y-expand",TRUE,
			//"x",0.0,
			"x-align",CLUTTER_ACTOR_ALIGN_END,
			"y-align",CLUTTER_ACTOR_ALIGN_FILL,
			NULL);
	clutter_actor_add_child(clutter_actor_get_parent(CLUTTER_ACTOR(MX_WEB_VIEW(webView))),CLUTTER_ACTOR(popup_menu));
	return (WebKitIPopupMenu*)popup_menu;
}

static gboolean
_lightwood_webview_blur_cb(WebKitDOMElement* input_element)
{
	webkit_dom_element_blur(input_element);
	return FALSE;
}

static void
_lightwood_webview_remove_focus_cb(LightwoodWebView *self)
{
	WebKitWebPage* page = webkit_iweb_view_get_page(WEBKIT_IWEB_VIEW(self));
	WebKitDOMDocument* doc = webkit_web_page_get_dom_document(page);
	WebKitDOMElement* input_element =
			webkit_dom_html_document_get_active_element(WEBKIT_DOM_HTML_DOCUMENT(doc));

	if(!input_element) return;
	//webkit_dom_element_blur(input_element);
	g_timeout_add(20,(GSourceFunc)_lightwood_webview_blur_cb,input_element);
}

static gboolean _lightwood_webview_virtualkeyboard_show(LightwoodWebView *self)
{
	if(self->priv->m_is_enabled)
	{
		//lightwood_webview_set_focus(self,TRUE);
		lightwood_webview_speller_show(self->priv->m_speller,NULL,NULL);

	}
	return G_SOURCE_REMOVE;
}

static void
_lightwood_webview_virtualkeyboard_transition_cb(LightwoodWebViewSpeller* m_speller,gboolean is_shown,LightwoodWebView *self)
{
	//g_signal_emit(self,signals[WEBVIEW_SPELLER_ALERT],0,is_shown);
	LightwoodWebViewPrivate* priv = WEBVIEW_PRIVATE (self);
	WebKitWebPage* webPage = webkit_iweb_view_get_page(WEBKIT_IWEB_VIEW(self));
	WebKitWebFrame* webFrame = webkit_web_page_get_main_frame(webPage);
	if(!is_shown){
			_lightwood_webview_remove_focus_cb(self);
	}
	else
	{
		gfloat zoom_level = webkit_web_page_get_zoom_level(webkit_iweb_view_get_page(WEBKIT_IWEB_VIEW(self)))*webkit_web_frame_get_contents_scale(webFrame);
				glong abs_x,abs_y=0;
				lightwood_webview_get_absolute_offset_for_element(priv->node,zoom_level,&abs_x,&abs_y);

				gfloat rel_x,rel_y,rel_speller_y;
				clutter_actor_get_transformed_position(CLUTTER_ACTOR(self),&rel_x,&rel_y);
				clutter_actor_get_transformed_position(CLUTTER_ACTOR(m_speller),&rel_x,&rel_speller_y);

				gfloat final_y=abs_y+rel_y+20;
				//g_print("\n FINAL Y  %f rel_speller_y %f \n",final_y,rel_speller_y);
				if(final_y < rel_speller_y)
					priv->m_scroll_y=0;
				else
				{

					priv->m_scroll_y = abs_y;
				}
				priv->m_recieved_editable_notification  = TRUE;
				if(priv->m_scroll_y > 0)
				{
					//g_print("\n >>>>>>>>>>>>>>>>..scroll_y=%f",priv->m_scroll_y);
				    clutter_actor_set_easing_duration(CLUTTER_ACTOR(self),1000);
				    webkit_web_frame_set_y_scroll_offset(webFrame,priv->m_scroll_y);
				    clutter_actor_set_easing_duration(CLUTTER_ACTOR(self),0);


				}
	}

}


static void _lightwood_webview_virtual_keyboard_show_and_alert(LightwoodWebView *self)
{
	LightwoodWebViewPrivate* priv = WEBVIEW_PRIVATE (self);
	if(!priv->m_speller)
	{
		ClutterActor* view = clutter_actor_get_stage(CLUTTER_ACTOR(self));
		priv->m_speller = s_webview_assistant->speller_factory(FALSE,FALSE);
		g_object_set(priv->m_speller,
				"x-expand",TRUE,
				"y-expand",TRUE,
				"x",6.0,
				"y",0.0,
				"x-align",CLUTTER_ACTOR_ALIGN_START,
				"y-align",CLUTTER_ACTOR_ALIGN_END,
				NULL);
		clutter_actor_hide(CLUTTER_ACTOR(priv->m_speller));
		g_signal_connect(priv->m_speller,"webview_speller_visible",G_CALLBACK(_lightwood_webview_virtualkeyboard_transition_cb),self);
		clutter_actor_add_child(view,CLUTTER_ACTOR(priv->m_speller));

		g_timeout_add(500,(GSourceFunc)_lightwood_webview_virtualkeyboard_show,self);
	}
	else
		lightwood_webview_speller_show(self->priv->m_speller,NULL,NULL);


}


/*callback when an input/dropdown element receives/looses focus.
 * emit a signal to let the view use  virtual keyboard or the dropdown roller*/
static void
_lightwood_webview_input_method_state_cb(WebKitWebPage* page,WebKitDOMElement* node,gboolean isEditable,LightwoodWebView *self)
{
	//g_print("\n recieved input notification....");
	LightwoodWebViewPrivate* priv = WEBVIEW_PRIVATE (self);

	priv->node=node;
	WebKitWebPage* webPage = webkit_iweb_view_get_page(WEBKIT_IWEB_VIEW(self));
	WebKitWebFrame* webFrame = webkit_web_page_get_main_frame(webPage);

	//WebKitDOMDocument* document = webkit_web_page_get_dom_document(webkit_iwebview_get_page(WEBKIT_IWEBVIEW(self)));
    //WebKitDOMDOMWindow* dom_window = webkit_dom_document_get_default_view(document);
	WebKitLoadStatus load_status = webkit_web_page_get_load_status(page);

	static gdouble initial_offset_x=0,initial_offset_y=0;
	//webkit_web_frame_get_scroll_offset(webFrame,&initial_offset_x,&initial_offset_y);
	gchar* node_name = NULL;
	if(node != NULL){
		node_name = webkit_dom_node_get_node_name(WEBKIT_DOM_NODE(node));

	}
	if(isEditable)
	{
		if(!s_webview_assistant || !s_webview_assistant->speller_factory)
		{
			return;
		}
		if(!priv->m_is_enabled
		  || ((load_status !=  WEBKIT_LOAD_FINISHED) &&  (load_status !=  WEBKIT_LOAD_FAILED) && (load_status !=  WEBKIT_LOAD_FIRST_VISUALLY_NON_EMPTY_LAYOUT))
		  || !CLUTTER_ACTOR_IS_VISIBLE(self))
		{
			_lightwood_webview_remove_focus_cb(self);
			return;
		}
		gfloat zoom_level = webkit_web_page_get_zoom_level(webkit_iweb_view_get_page(WEBKIT_IWEB_VIEW(self)));
				glong abs_x,abs_y=0;
				lightwood_webview_get_absolute_offset_for_element(node,zoom_level,&abs_x,&abs_y);

		//_lightwood_webview_virtual_keyboard_show_and_alert()
		if(self->priv->m_speller)
		{
		gboolean speller_is_shown = lightwood_webview_speller_is_shown(self->priv->m_speller);
		 if(!speller_is_shown)
		{
		_lightwood_webview_virtual_keyboard_show_and_alert(self);
		g_signal_emit(self,signals[WEBVIEW_SPELLER_ALERT],0,TRUE);
		}
		 else
		 {
			 gfloat rel_x,rel_y,rel_speller_y;
			 				clutter_actor_get_transformed_position(CLUTTER_ACTOR(self),&rel_x,&rel_y);
			 				clutter_actor_get_transformed_position(CLUTTER_ACTOR(self->priv->m_speller),&rel_x,&rel_speller_y);
			 				gfloat final_y=abs_y+rel_y+20;
			 				if(final_y < rel_speller_y)
			 					priv->m_scroll_y=0;
			 				else
			 				{
			 					priv->m_scroll_y = abs_y-40;
			 				}
			 				priv->m_recieved_editable_notification  = TRUE;
			 				if(priv->m_scroll_y > 0)
			 				{
			 				//	g_print("\n >>>>>>>>>>>>>>>>..scroll_y=%f",priv->m_scroll_y);
			 				    clutter_actor_set_easing_duration(CLUTTER_ACTOR(self),1000);
			 				    webkit_web_frame_set_y_scroll_offset(webFrame,priv->m_scroll_y);
			 				    clutter_actor_set_easing_duration(CLUTTER_ACTOR(self),0);


			 				}
		 }
	}
		else
		{
			_lightwood_webview_virtual_keyboard_show_and_alert(self);
			g_signal_emit(self,signals[WEBVIEW_SPELLER_ALERT],0,TRUE);
		}
		}
	else
	{
		if(!((node != NULL) && g_str_equal(node_name,"SELECT")))
		{
			//_lightwood_webview_virtual_keyboard_hide_and_alert()
			if(priv->m_recieved_editable_notification )
			{
				priv->m_recieved_editable_notification  = FALSE;
				clutter_actor_set_easing_duration(CLUTTER_ACTOR(self),1000);
				webkit_web_frame_set_y_scroll_offset(webFrame,initial_offset_y);
				clutter_actor_set_easing_duration(CLUTTER_ACTOR(self),0);
				initial_offset_x = initial_offset_y = 0;

				//g_signal_emit (self, signals[INPUT_ALERT], 0,FALSE,NULL);
				g_signal_emit(self,signals[WEBVIEW_SPELLER_ALERT],0,FALSE);
				lightwood_webview_speller_hide(priv->m_speller);

			}
		}
	}
}

/*a utility api that returns the absolute offset(x,y coordinates) of a node wrt the tab origin*/
void lightwood_webview_get_absolute_offset_for_element(WebKitDOMElement* element,gfloat zoom_level, glong *x, glong *y)
{
    WebKitDOMElement* offset_parent;
    glong offset_top=0, offset_left=0;

    if (NULL == element){
        return;
    }

    /*g_object_get(element,
                 "offset-left", &offset_left,
                 "offset-top", &offset_top,
                 "offset-parent", &offset_parent,
                 NULL);*/
    offset_left = webkit_dom_element_get_offset_left(element);
    offset_top = webkit_dom_element_get_offset_top(element);
    offset_parent = webkit_dom_element_get_offset_parent(element);

    *x = offset_left*zoom_level;
    *y = offset_top*zoom_level;

    if (NULL == offset_parent){
        return;
    }

    /* If there's a parent, we keep going. */
    glong parent_x=0, parent_y=0;
    lightwood_webview_get_absolute_offset_for_element(offset_parent,zoom_level, &parent_x, &parent_y);

    *x += parent_x;
    *y += parent_y;
}

WebKitDOMElement* lightwood_webview_get_clickable_node(LightwoodWebView* self,ClutterButtonEvent* evt,
												guint topPadding,guint bottomPadding,guint leftPadding,guint rightPadding,
												gfloat* x_adj,gfloat* y_adj)
{
//	LightwoodWebViewPrivate* priv = WEBVIEW_PRIVATE(self);
	WebKitWebPage* webPage = webkit_iweb_view_get_page(WEBKIT_IWEB_VIEW(self));
	WebKitWebFrame* webFrame = webkit_web_page_get_main_frame(webPage);

	int page_x_offset,page_y_offset;
	webkit_web_frame_get_scroll_offset(webFrame, &page_x_offset, &page_y_offset);
	/*glong page_x_offset = webkit_dom_dom_window_get_page_x_offset(
						webkit_dom_document_get_default_view(
									webkit_web_page_get_dom_document(webkit_iwebview_get_page(WEBKIT_IWEBVIEW(self)))));
	glong page_y_offset = webkit_dom_dom_window_get_page_y_offset(
						webkit_dom_document_get_default_view(
								webkit_web_page_get_dom_document(webkit_iwebview_get_page(WEBKIT_IWEBVIEW(self)))));*/

    /* Hit test a rectangle to try to find links close to the click. */
	float x, y;
	clutter_actor_transform_stage_point(CLUTTER_ACTOR(self), evt->x, evt->y, &x, &y);
    WebKitWebPage* page = webkit_iweb_view_get_page(WEBKIT_IWEB_VIEW(self));
    WebKitDOMNode* node = NULL;
    WebKitDOMElement* hit_element = NULL;
    WebKitHitTestResultContext hit_context;

    WebKitHitTestResult* result = webkit_web_page_hit_test(page, x, y);
    g_object_get(result, "context", &hit_context, NULL);
    g_object_get(result, "inner-node", &node, NULL);

    if ( (hit_context & WEBKIT_HIT_TEST_RESULT_CONTEXT_LINK) 	 ||
         (hit_context & WEBKIT_HIT_TEST_RESULT_CONTEXT_EDITABLE) ||
         (hit_context & WEBKIT_HIT_TEST_RESULT_CONTEXT_MEDIA)    ||
         (hit_context & WEBKIT_HIT_TEST_RESULT_CONTEXT_IMAGE)    ||
          WEBKIT_DOM_IS_HTML_INPUT_ELEMENT(node)
		  /*WEBKIT_DOM_IS_HTML_ANCHOR_ELEMENT(node)				 ||
		  WEBKIT_DOM_IS_HTML_SELECT_ELEMENT(node)				 ||
		  WEBKIT_DOM_IS_HTML_TEXT_AREA_ELEMENT(node)			 ||
		  WEBKIT_DOM_IS_HTML_IMAGE_ELEMENT(node)                 ||
		  WEBKIT_DOM_IS_HTML_MEDIA_ELEMENT(node)*/
       )
    {
    	//g_print("\n point hit_context=%d",hit_context);
    	*x_adj=0.0;*y_adj=0.0;
    	if(node && (hit_context & WEBKIT_HIT_TEST_RESULT_CONTEXT_LINK))
    	{
    		hit_element = webkit_dom_node_get_parent_element(node);
    		if(!WEBKIT_DOM_IS_HTML_ANCHOR_ELEMENT(hit_element))
    			hit_element = webkit_dom_node_get_parent_element((WebKitDOMNode*)hit_element);
    	}
    	else
    		hit_element = WEBKIT_DOM_ELEMENT(node);

		//g_print("clicked node=%s",webkit_dom_element_get_tag_name(WEBKIT_DOM_ELEMENT(node)));
		return hit_element;

    	/*WebKitDOMElement* parent = webkit_dom_node_get_parent_element(node);
    	parent = webkit_dom_node_get_parent_element(parent);
    	g_print("clciked node=%s",webkit_dom_element_get_tag_name(parent));
		if ( webkit_dom_node_has_attributes(parent) 	&&
			 (WEBKIT_DOM_IS_HTML_ANCHOR_ELEMENT(parent)
			   ||  WEBKIT_DOM_IS_HTML_INPUT_ELEMENT(parent)
			   ||  WEBKIT_DOM_IS_HTML_SELECT_ELEMENT(parent)
			   ||  WEBKIT_DOM_IS_HTML_TEXT_AREA_ELEMENT(parent)
			 )
		   )
		{*/

			//g_print("clciked node=%s",webkit_dom_element_get_tag_name(parent));
			 //return (WebKitDOMElement*)parent;

    }

    return (WebKitDOMElement*)node;     
}


WebKitDOMElement* lightwood_webview_get_touchable_node(LightwoodWebView* self,ClutterTouchEvent* evt,
												guint topPadding,guint bottomPadding,guint leftPadding,guint rightPadding,
												gfloat* x_adj,gfloat* y_adj)
{
//	LightwoodWebViewPrivate* priv = WEBVIEW_PRIVATE(self);
	WebKitWebPage* webPage = webkit_iweb_view_get_page(WEBKIT_IWEB_VIEW(self));
	WebKitWebFrame* webFrame = webkit_web_page_get_main_frame(webPage);

	int page_x_offset,page_y_offset;
	webkit_web_frame_get_scroll_offset(webFrame, &page_x_offset, &page_y_offset);
	/*glong page_x_offset = webkit_dom_dom_window_get_page_x_offset(
						webkit_dom_document_get_default_view(
									webkit_web_page_get_dom_document(webkit_iwebview_get_page(WEBKIT_IWEBVIEW(self)))));
	glong page_y_offset = webkit_dom_dom_window_get_page_y_offset(
						webkit_dom_document_get_default_view(
								webkit_web_page_get_dom_document(webkit_iwebview_get_page(WEBKIT_IWEBVIEW(self)))));*/

    /* Hit test a rectangle to try to find links close to the click. */
	float x, y;
	clutter_actor_transform_stage_point(CLUTTER_ACTOR(self), evt->x, evt->y, &x, &y);
    WebKitWebPage* page = webkit_iweb_view_get_page(WEBKIT_IWEB_VIEW(self));
    WebKitDOMNode* node = NULL;
    WebKitDOMElement* hit_element = NULL;
    WebKitHitTestResultContext hit_context;

    WebKitHitTestResult* result = webkit_web_page_hit_test(page, x, y);
    g_object_get(result, "context", &hit_context, NULL);
    g_object_get(result, "inner-node", &node, NULL);

    if ( (hit_context & WEBKIT_HIT_TEST_RESULT_CONTEXT_LINK) 	 ||
         (hit_context & WEBKIT_HIT_TEST_RESULT_CONTEXT_EDITABLE) ||
         (hit_context & WEBKIT_HIT_TEST_RESULT_CONTEXT_MEDIA)    ||
         (hit_context & WEBKIT_HIT_TEST_RESULT_CONTEXT_IMAGE)    ||
          WEBKIT_DOM_IS_HTML_INPUT_ELEMENT(node)
		  /*WEBKIT_DOM_IS_HTML_ANCHOR_ELEMENT(node)				 ||
		  WEBKIT_DOM_IS_HTML_SELECT_ELEMENT(node)				 ||
		  WEBKIT_DOM_IS_HTML_TEXT_AREA_ELEMENT(node)			 ||
		  WEBKIT_DOM_IS_HTML_IMAGE_ELEMENT(node)                 ||
		  WEBKIT_DOM_IS_HTML_MEDIA_ELEMENT(node)*/
       )
    {
    	//g_print("\n point hit_context=%d",hit_context);
    	*x_adj=0.0;*y_adj=0.0;
    	if(node && (hit_context & WEBKIT_HIT_TEST_RESULT_CONTEXT_LINK))
    	{
    		hit_element = webkit_dom_node_get_parent_element(node);
    		if(!WEBKIT_DOM_IS_HTML_ANCHOR_ELEMENT(hit_element))
    			hit_element = webkit_dom_node_get_parent_element((WebKitDOMNode*)hit_element);
    	}
    	else
    		hit_element = WEBKIT_DOM_ELEMENT(node);

		//g_print("clicked node=%s",webkit_dom_element_get_tag_name(WEBKIT_DOM_ELEMENT(node)));
		return hit_element;

    	/*WebKitDOMElement* parent = webkit_dom_node_get_parent_element(node);
    	parent = webkit_dom_node_get_parent_element(parent);
    	g_print("clciked node=%s",webkit_dom_element_get_tag_name(parent));
		if ( webkit_dom_node_has_attributes(parent) 	&&
			 (WEBKIT_DOM_IS_HTML_ANCHOR_ELEMENT(parent)
			   ||  WEBKIT_DOM_IS_HTML_INPUT_ELEMENT(parent)
			   ||  WEBKIT_DOM_IS_HTML_SELECT_ELEMENT(parent)
			   ||  WEBKIT_DOM_IS_HTML_TEXT_AREA_ELEMENT(parent)
			 )
		   )
		{*/

			//g_print("clciked node=%s",webkit_dom_element_get_tag_name(parent));
			 //return (WebKitDOMElement*)parent;

    }

    return (WebKitDOMElement*)node;     
}
