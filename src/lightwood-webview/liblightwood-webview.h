/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* liblightwood-web-view.h */

#ifndef _LIGHTWOOD_WEBVIEW_H
#define _LIGHTWOOD_WEBVIEW_H

#include <glib-object.h>
#include <clutter/clutter.h>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-prototypes"
#include <webkit/webkitiwebview.h>
#include <webkit/mx-web-view.h>
#pragma GCC diagnostic pop
#include <webkitdom/webkitdom.h>
#include <webkit/webkitirendertheme.h>

#include "liblightwood-iwebview-speller.h"
#include "liblightwood-webview-assistant.h"

G_BEGIN_DECLS

#define LIGHTWOOD_TYPE_WEBVIEW lightwood_webview_get_type()

#define LIGHTWOOD_WEBVIEW(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  LIGHTWOOD_TYPE_WEBVIEW, LightwoodWebView))

#define LIGHTWOOD_WEBVIEW_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  LIGHTWOOD_TYPE_WEBVIEW, LightwoodWebViewClass))

#define LIGHTWOOD_IS_WEBVIEW(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  LIGHTWOOD_TYPE_WEBVIEW))

#define LIGHTWOOD_IS_WEBVIEW_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  LIGHTWOOD_TYPE_WEBVIEW))

#define LIGHTWOOD_WEBVIEW_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  LIGHTWOOD_TYPE_WEBVIEW, LightwoodWebViewClass))

typedef struct _LightwoodWebView LightwoodWebView;
typedef struct _LightwoodWebViewClass LightwoodWebViewClass;
typedef struct _LightwoodWebViewPrivate LightwoodWebViewPrivate;

struct _LightwoodWebView
{
  MxWebView parent;
  LightwoodWebViewPrivate *priv;
};

struct _LightwoodWebViewClass
{
  MxWebViewClass parent_class;
};



GType lightwood_webview_get_type (void) G_GNUC_CONST;

void lightwood_webview_init_session(const gchar* app_name,LightwoodWebviewAssistant* webview_assistant);
ClutterActor *lightwood_webview_new (void);
void lightwood_webview_set_enable_focus(LightwoodWebView* self,gboolean is_enabled);
void lightwood_webview_set_focus(LightwoodWebView* self,gboolean is_focussed);
void lightwood_webview_page_zoom(ClutterActor* self,gdouble request_zoom_level);
void lightwood_webview_normal_page_zoom(ClutterActor* self,gdouble request_zoom_level);
gdouble lightwood_webview_get_zoom_factor(ClutterActor* self);
void lightwood_webview_context_sensitive_zoom(ClutterActor* self,gboolean is_zoomin,gfloat zoom_posx,gfloat zoom_posy);

WebKitDOMElement* lightwood_webview_get_clickable_node(LightwoodWebView* self,
												ClutterButtonEvent* evt,
												guint topPadding,guint bottomPadding,guint leftPadding,guint rightPadding,
												gfloat* x_adj,gfloat* y_adj);
WebKitDOMElement* lightwood_webview_get_touchable_node(LightwoodWebView* self,
												ClutterTouchEvent* evt,
												guint topPadding,guint bottomPadding,guint leftPadding,guint rightPadding,
												gfloat* x_adj,gfloat* y_adj);
void lightwood_webview_get_absolute_offset_for_element(WebKitDOMElement* element,gfloat zoom_level, glong *x, glong *y);

G_END_DECLS

#endif /* _LIGHTWOOD_WEBVIEW_H */
