/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <clutter/clutter.h>
#include <clutter/gdk/clutter-gdk.h>
#include "liblightwood-widget.h"

/**
 * SECTION:liblightwood-widget
 * @Title:LightwoodWidget
 * @short_description: Base class for stylable widgets.
 * @See_Also: #ClutterActor
 *
 * The LightwoodWidget is a base class for stylable widgets which implements
 * style properties from
 * #GtkApertisStylable like background-image, background-color, border, margin etc.
 * All the widgets which are stylable in lightwood are inherited from this base
 * class.
 *
 * ## Sample example to specify the properties in CSS file
 *
 * ```
 * LightwoodButton {
 *	background-image: url("resource:///org/apertis/lightwood/tests/mrs_normal.png");
 *	font: Monospace,Normal Normal Normal Extra-Expanded 14;
 * }
 * *:active {
 *	background-image: url("resource:///org/apertis/lightwood/tests/mrs_pressed.png");
 *	color: rgba(0,0,0,1.0);
 * }
 * *:insensitive{
 *	background-color: rgba(255,255,255,0.30);
 * }
 * *:hover{
 *	border-color: #a1a1a1;
 *	border-radius: 3px;
 *	border: 1px solid;
 *	background-image: url("resource:///org/apertis/lightwood/tests/mrs_pressed.png");
 *	color: rgba(0,0,0,1.0);
 * }
 * ```
 *
 * Since: UNRELEASED
 */

static void lightwood_widget_finalize (GObject *obj);

static void
lightwood_widget_dispose (GObject *obj);

static void lightwood_widget_parent_set (ClutterActor *actor,
                                         ClutterActor *old_parent);

static void
lightwood_widget_stylable_interface_init (GtkApertisStylableInterface *iface);
static void lightwood_widget_stylable_style_context_invalidated (
    GtkApertisStylable *stylable);

static GtkWidgetPath *
lightwood_widget_stylable_get_path (GtkApertisStylable *stylable);

static GdkScreen *
lightwood_widget_stylable_get_screen (GtkApertisStylable *stylable);

static void lightwood_widget_stylable_reset_style (GtkApertisStylable *stylable);

static const gchar *
lightwood_widget_stylable_get_name (GtkApertisStylable *stylable);

static gboolean
lightwood_widget_stylable_should_animate (GtkApertisStylable *stylable);

static GtkStyleContext *
lightwood_widget_stylable_get_style_context (GtkApertisStylable *stylable);

static void
lightwood_widget_allocation_changed_cb (GObject *object, GParamSpec *pspec,
					gpointer data);

static gboolean
lightwood_widget_draw_canvas (ClutterCanvas *canvas, cairo_t *cr, guint width,
			      guint height, gpointer user_data);

typedef struct
{
  GtkStyleContext *context; /* owned */
  GtkWidgetPath *path;
} LightwoodWidgetPrivate;

G_DEFINE_ABSTRACT_TYPE_WITH_CODE (LightwoodWidget, lightwood_widget,
				  CLUTTER_TYPE_ACTOR,
				  G_ADD_PRIVATE (LightwoodWidget)
				  G_IMPLEMENT_INTERFACE (GTK_TYPE_APERTIS_STYLABLE,
							 lightwood_widget_stylable_interface_init));

static void
lightwood_widget_class_init (LightwoodWidgetClass *klass)
{
  GObjectClass *gobject_class = NULL;
  ClutterActorClass *actor_class = NULL;

  gobject_class = G_OBJECT_CLASS (klass);
  actor_class = CLUTTER_ACTOR_CLASS (klass);

  gobject_class->dispose = lightwood_widget_dispose;
  gobject_class->finalize = lightwood_widget_finalize;
  actor_class->parent_set = lightwood_widget_parent_set;
}

static void
gtk_border_to_clutter_margin (const GtkBorder *in,
                              ClutterMargin *out)
{
  out->left = in->left;
  out->right = in->right;
  out->top = in->top;
  out->bottom = in->bottom;
}

static void
update_style_properties (LightwoodWidget *self)
{
  ClutterActor *actor = NULL;
  GtkStyleContext *context = NULL;
  GtkStateFlags flags;
  GtkBorder gtk_border;
  ClutterMargin clutter_margin;

  g_return_if_fail (LIGHTWOOD_IS_WIDGET (self));
  g_return_if_fail (CLUTTER_IS_ACTOR (self));

  actor = CLUTTER_ACTOR (self);
  flags = GTK_STATE_FLAG_NORMAL;
  context = gtk_apertis_stylable_get_style_context (GTK_APERTIS_STYLABLE (self));

  /* Margin */
  gtk_style_context_get_margin (context, flags, &gtk_border);
  gtk_border_to_clutter_margin (&gtk_border, &clutter_margin);
  clutter_actor_set_margin (actor, &clutter_margin);
}

static void
lightwood_widget_stylable_interface_init (GtkApertisStylableInterface *iface)
{
  iface->style_context_invalidated =
      lightwood_widget_stylable_style_context_invalidated;
  iface->get_path = lightwood_widget_stylable_get_path;
  iface->get_screen = lightwood_widget_stylable_get_screen;
  iface->reset_style = lightwood_widget_stylable_reset_style;
  iface->get_name = lightwood_widget_stylable_get_name;
  iface->should_animate = lightwood_widget_stylable_should_animate;
  iface->get_style_context = lightwood_widget_stylable_get_style_context;
}

static void
lightwood_widget_stylable_style_context_invalidated (
    GtkApertisStylable *stylable)
{
  LightwoodWidget *self = NULL;
  LightwoodWidgetPrivate *priv = NULL;
  ClutterActor *actor = NULL;

  g_return_if_fail (LIGHTWOOD_IS_WIDGET (stylable));

  self = LIGHTWOOD_WIDGET (stylable);
  priv = lightwood_widget_get_instance_private (self);
  actor = CLUTTER_ACTOR (stylable);

  g_clear_pointer (&priv->path, (GDestroyNotify) gtk_widget_path_free);
  update_style_properties (self);
  clutter_actor_queue_relayout (actor);
}

static gint
gtk_widget_path_append_for_actor (GtkWidgetPath *path,
                                  LightwoodWidget *actor)
{
  gint pos;
  LightwoodWidgetPrivate *priv = NULL;

  g_return_val_if_fail (LIGHTWOOD_IS_WIDGET (actor), 0);

  priv = lightwood_widget_get_instance_private (actor);
  pos = gtk_widget_path_append_type (path, G_OBJECT_TYPE (actor));

  if (clutter_actor_get_name (CLUTTER_ACTOR (actor)) != NULL)
    {
      gtk_widget_path_iter_set_name (path, pos,
                                     clutter_actor_get_name (CLUTTER_ACTOR (actor)));
    }

  if (priv->context != NULL)
    {
      GList *classes = NULL;
      GList *l = NULL;

      /* Also add any persistent classes in the style context. */
      classes = gtk_style_context_list_classes (priv->context);

      for (l = classes; l != NULL; l = l->next)
        gtk_widget_path_iter_add_class (path, pos, l->data);

      g_list_free (classes);
    }

  return pos;
}

static GtkWidgetPath *
lightwood_widget_stylable_get_path (GtkApertisStylable *stylable)
{
  LightwoodWidget *self = NULL;
  LightwoodWidgetPrivate *priv = NULL;

  g_return_val_if_fail (LIGHTWOOD_IS_WIDGET (stylable), NULL);
  self = LIGHTWOOD_WIDGET (stylable);
  priv = lightwood_widget_get_instance_private (self);

  if (priv->path == NULL)
    {
      ClutterActor *parent = NULL;
      GtkWidgetPath *result = NULL;

      /* Build a new path. */
      parent = clutter_actor_get_parent (CLUTTER_ACTOR (stylable));

      if (parent != NULL && LIGHTWOOD_IS_WIDGET (parent))
        {
          result = gtk_widget_path_copy (
              gtk_apertis_stylable_get_path (GTK_APERTIS_STYLABLE (parent)));
        }
      else
        {
          /* Top-level or unparented widget. */
          result = gtk_widget_path_new ();
        }

      gtk_widget_path_append_for_actor (result, self);

      priv->path = result;
    }

  return priv->path;
}

static GdkScreen *
lightwood_widget_stylable_get_screen (GtkApertisStylable *stylable)
{
  ClutterActor *stage = NULL;
  GdkWindow *window = NULL;

  g_return_val_if_fail (LIGHTWOOD_IS_WIDGET (stylable), NULL);

  stage = clutter_actor_get_stage (CLUTTER_ACTOR (stylable));

  if (stage == NULL)
    {
      return NULL;
    }

  window = clutter_gdk_get_stage_window (CLUTTER_STAGE (stage));

  if (window == NULL)
    {
      return NULL;
    }

  return gdk_window_get_screen (window);
}

static void
lightwood_widget_stylable_reset_style (GtkApertisStylable *stylable)
{
  LightwoodWidgetPrivate *priv = NULL;

  g_return_if_fail (LIGHTWOOD_IS_WIDGET (stylable));

  priv = lightwood_widget_get_instance_private (LIGHTWOOD_WIDGET (stylable));

  if (priv->context != NULL)
    gtk_style_context_invalidate (priv->context);
}

static const gchar *
lightwood_widget_stylable_get_name (GtkApertisStylable *stylable)
{
  g_return_val_if_fail (LIGHTWOOD_IS_WIDGET (stylable), NULL);

  return clutter_actor_get_name (CLUTTER_ACTOR (stylable));
}

static gboolean
lightwood_widget_stylable_should_animate (GtkApertisStylable *stylable)
{
  g_return_val_if_fail (LIGHTWOOD_IS_WIDGET (stylable), FALSE);

  /* Do not support animation for now. */
  return FALSE;
}

static GtkStyleContext *
lightwood_widget_stylable_get_style_context (GtkApertisStylable *stylable)
{
  LightwoodWidget *self = NULL;
  LightwoodWidgetPrivate *priv = NULL;

  g_return_val_if_fail (LIGHTWOOD_IS_WIDGET (stylable), NULL);
  self = LIGHTWOOD_WIDGET (stylable);
  priv = lightwood_widget_get_instance_private (self);

  if (G_UNLIKELY (priv->context == NULL))
    {
      ClutterStage *stage = NULL;
      GdkWindow *window = NULL;
      GdkScreen *screen = NULL;
      ClutterActor *parent = NULL;

      /* Screen. */
      stage = CLUTTER_STAGE (clutter_actor_get_stage (CLUTTER_ACTOR (self)));
      if (stage != NULL)
        window = clutter_gdk_get_stage_window (stage);
      if (window != NULL)
        screen = gdk_window_get_screen (window);

      priv->context = gtk_style_context_new_for_stylable (stylable);

      if (screen != NULL)
        gtk_style_context_set_screen (priv->context, screen);

      /* Parent. */
      parent = clutter_actor_get_parent (CLUTTER_ACTOR (self));

      if (parent != NULL && GTK_IS_APERTIS_STYLABLE (parent))
        {
          gtk_style_context_set_parent (
              priv->context,
              gtk_apertis_stylable_get_style_context (GTK_APERTIS_STYLABLE (parent)));
        }

      /* ID and stylable. */
      gtk_style_context_set_apertis_id (
          priv->context, clutter_actor_get_name (CLUTTER_ACTOR (self)));
    }

  return priv->context;
}

static void
lightwood_widget_init (LightwoodWidget *self)
{
  g_debug ("%s", __FUNCTION__);
}

static void
lightwood_widget_finalize (GObject *obj)
{
  LightwoodWidgetPrivate *priv = NULL;

  priv = lightwood_widget_get_instance_private (LIGHTWOOD_WIDGET (obj));

  if (priv->path != NULL)
    {
      gtk_widget_path_free (priv->path);
    }
  G_OBJECT_CLASS (lightwood_widget_parent_class)->finalize (obj);
}

static void
lightwood_widget_dispose (GObject *obj)
{
  LightwoodWidgetPrivate *priv = NULL;

  priv = lightwood_widget_get_instance_private (LIGHTWOOD_WIDGET (obj));

  g_clear_object (&priv->context);

  G_OBJECT_CLASS (lightwood_widget_parent_class)->dispose (obj);
}

static void
lightwood_widget_parent_set (ClutterActor *actor,
                             ClutterActor *old_parent)
{
  LightwoodWidgetPrivate *priv = NULL;
  ClutterActor *new_parent = NULL;

  g_return_if_fail (LIGHTWOOD_IS_WIDGET (actor));
  priv = lightwood_widget_get_instance_private (LIGHTWOOD_WIDGET (actor));
  new_parent = clutter_actor_get_parent (actor);

  if (priv->context != NULL && GTK_IS_APERTIS_STYLABLE (new_parent))
    {
      gtk_style_context_set_parent (priv->context,
                                    gtk_apertis_stylable_get_style_context (
                                        GTK_APERTIS_STYLABLE (new_parent)));
    }
  else if (priv->context != NULL)
    {
      gtk_style_context_set_parent (priv->context, NULL);
    }
}

/**
 * lightwood_widget_update_background:
 * @self: A #LightwoodWidget
 * @context: a #GtkStyleContext, context to retrieve the background
 * @flags: a #GtkStateFlags state to retrieve the background for
 *
 * Updates the @actor with background image/color as per the info from @context and
 * @flags
 *
 * Since: UNRELEASED
 */
void
lightwood_widget_update_background (LightwoodWidget *self,
				    GtkStyleContext *context,
				    GtkStateFlags flags)
{
  ClutterContent *canvas = NULL;

  g_return_if_fail (LIGHTWOOD_IS_WIDGET (self));
  g_return_if_fail (GTK_IS_STYLE_CONTEXT (context));

  g_debug (G_STRLOC "background-update");

  gtk_style_context_set_state (context, flags);
  canvas = clutter_actor_get_content (CLUTTER_ACTOR (self));

  if (canvas == NULL)
    {
      canvas = clutter_canvas_new ();
      clutter_actor_set_content (CLUTTER_ACTOR (self), canvas);
      g_signal_connect (self, "notify::allocation",
                        G_CALLBACK (lightwood_widget_allocation_changed_cb), NULL);
      g_signal_connect (canvas, "draw",
			G_CALLBACK (lightwood_widget_draw_canvas), self);
    }
  else
    {
      g_object_ref (canvas);
    }

  clutter_content_invalidate (canvas);
  g_object_unref (canvas);

}

/**
 * lightwood_widget_update_font:
 * @actor: ClutterActor to which font description need to apply.
 * @context: a #GtkStyleContext, context to retrieve the font
 * @flags: a #GtkStateFlags, state to retrieve the font for
 *
 * Updates the @actor with font description from @context.
 *
 * Since: UNRELEASED
 */
void
lightwood_widget_update_font (ClutterActor *actor,
			      GtkStyleContext *context,
			      GtkStateFlags flags)
{
  PangoFontDescription *font_desc = NULL;

  g_return_if_fail (CLUTTER_IS_ACTOR (actor));
  g_return_if_fail (GTK_IS_STYLE_CONTEXT (context));

  g_debug (G_STRLOC "update-font");

  gtk_style_context_get (context, flags, GTK_STYLE_PROPERTY_FONT, &font_desc,
                         NULL);
  if (font_desc != NULL)
    {
      clutter_text_set_font_description (CLUTTER_TEXT (actor), font_desc);
      g_clear_pointer (&font_desc, pango_font_description_free);
    }
  else
    {
      /* The font_name string must be NULL,
       * which means that the font name from the default ClutterBackend will be used*/
      clutter_text_set_font_name (CLUTTER_TEXT (actor), NULL);
    }
}

/**
 * lightwood_widget_update_margin:
 * @self: A #LightwoodWidget
 * @context: a #GtkStyleContext, context to retrieve the margin
 * @flags: a #GtkStateFlags, state to retrieve the margin for
 *
 * Updates the @actor with margin from @context.
 *
 * Since: UNRELEASED
 */
void
lightwood_widget_update_margin (LightwoodWidget *self, GtkStyleContext *context,
				GtkStateFlags flags)
{
  GtkBorder *margin_info = NULL;

  g_debug ("%s,%d", __FUNCTION__, __LINE__);
  g_return_if_fail (LIGHTWOOD_IS_WIDGET (self));
  g_return_if_fail (GTK_IS_STYLE_CONTEXT (context));

  gtk_style_context_get_margin (context, flags, margin_info);

  if (margin_info != NULL)
    {
      clutter_actor_set_margin_bottom (CLUTTER_ACTOR (self),
				       margin_info->bottom);
      clutter_actor_set_margin_top (CLUTTER_ACTOR (self), margin_info->top);
      clutter_actor_set_margin_left (CLUTTER_ACTOR (self), margin_info->left);
      clutter_actor_set_margin_right (CLUTTER_ACTOR (self), margin_info->right);
    }
  else
    {
      clutter_actor_set_margin_bottom (CLUTTER_ACTOR (self), 0);
      clutter_actor_set_margin_top (CLUTTER_ACTOR (self), 0);
      clutter_actor_set_margin_left (CLUTTER_ACTOR (self), 0);
      clutter_actor_set_margin_right (CLUTTER_ACTOR (self), 0);
    }
}

/**
 * lightwood_lookup_icon:
 * @icon_name: the name of the icon to lookup
 *
 * Looks up a named icon based on the theme which was set.
 *
 * Returns: (nullable) (transfer full): the exact path of the icon
 *
 * Since: UNRELEASED
 */
gchar *
lightwood_lookup_icon (const gchar *icon_name)
{
  GtkIconInfo *gtk_icon_info = NULL;
  gchar *file_path = NULL;

  g_debug (G_STRLOC "icon_name %s", icon_name);
  g_return_val_if_fail (icon_name != NULL, NULL);

  gtk_icon_info = gtk_icon_theme_lookup_icon (
      gtk_icon_theme_get_default (), icon_name, 0, GTK_ICON_LOOKUP_DIR_LTR);
  if (gtk_icon_info == NULL)
    {
      g_debug ("Failed to look up icon for \"%s\"", icon_name);
    }
  else
    {
      file_path = g_strdup (gtk_icon_info_get_filename (gtk_icon_info));
      g_object_unref (gtk_icon_info);
    }
  return file_path;
}

static void
lightwood_widget_allocation_changed_cb (GObject *object, GParamSpec *pspec,
					gpointer data)
{
  ClutterActor *actor = NULL;
  ClutterContent *content = NULL;

  actor = CLUTTER_ACTOR (object);
  content = clutter_actor_get_content (actor);

  if (content != NULL)
    {
      ClutterActorBox box;
      clutter_actor_get_allocation_box (actor, &box);
      clutter_canvas_set_size (CLUTTER_CANVAS (content), (box.x2 - box.x1),
			       (box.y2 - box.y1));
    }
}

static gboolean
lightwood_widget_draw_canvas (ClutterCanvas *canvas, cairo_t *cr, guint width,
			      guint height, gpointer user_data)
{
  g_debug (G_STRLOC "draw-canvas");

  gtk_render_background (gtk_apertis_stylable_get_style_context (GTK_APERTIS_STYLABLE (user_data)),
			 cr, 0, 0, width, height);

  return TRUE;
}
