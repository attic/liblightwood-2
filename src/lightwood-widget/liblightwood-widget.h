/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _LIGHTWOOD_WIDGET_H
#define _LIGHTWOOD_WIDGET_H

#include <clutter/clutter.h>
#include <glib-object.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

/**
 * LIGHTWOOD_TYPE_WIDGET:
 *
 * Get the #GType of the #LightwoodWidget
 *
 * Since: UNRELEASED
 */
#define LIGHTWOOD_TYPE_WIDGET lightwood_widget_get_type ()
G_DECLARE_DERIVABLE_TYPE (LightwoodWidget, lightwood_widget, LIGHTWOOD, WIDGET, ClutterActor)

/**
 * LightwoodWidgetClass:
 * @parent_class: Parent of #LightwoodWidgetClass
 *
 * Base class structure for #LightwoodWidget
 *
 * Since: UNRELEASED
 */
struct _LightwoodWidgetClass
{
  ClutterActorClass parent_class;
  /*< private >*/
  gpointer padding[10];
};

void
lightwood_widget_update_background (LightwoodWidget *self,
				    GtkStyleContext *context,
				    GtkStateFlags flags);
void
lightwood_widget_update_font (ClutterActor *actor, GtkStyleContext *context,
			      GtkStateFlags flags);
void
lightwood_widget_update_margin (LightwoodWidget *self, GtkStyleContext *context,
				GtkStateFlags flags);

gchar *
lightwood_lookup_icon (const gchar *icon_name);

G_END_DECLS
#endif /* _LIGHTWOOD_WIDGET_H */
