/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* test_lightwood_flipper.c */
#include "liblightwood-flipper.h"
#include "libthornbury-model.h"

#define TEST_FLIP_ANIMATION_PRINT( a ...)			g_print(a);

#define PARENT_XY 50.0 , 50.0
#define STAGE_WH  1000, 500
#define ACTOR_WIDTH 400
#define ACTOR_HEIGHT 400
#define ACTOR_X 50.0f
#define ACTOR_Y 50.0f
#define BOUND_DEPTH 2.0

#define LINE_HEIGHT 4.0

#define TRANSITION_TIME 2000

static  gfloat fltXPivotPoint = 0.5 ;
//static gfloat fltYPivotPoint = 0.5 ;

static ThornburyModel *pFlipperModel = NULL ;
static ClutterActor *pSecondActor , *pFirstActor ;
static ClutterActor *pStage = NULL ;
static gboolean bModelSwaped = FALSE ;
static gboolean bDirectionSwaped = FALSE ;
static LightwoodFlipper *pFlipper = NULL ;
static gboolean bSignalConnected1 = FALSE ;
static gboolean bSignalConnected2 = FALSE ;
static gboolean bDiscreteSet = FALSE ;
static gfloat fltDiscrete = 0.0 ;
static guint bDiscreteStartDirection  ;
static gfloat fltStartX , fltLastX;


static gboolean v_test_flipper_change_model(gpointer pUserData)
{
	thornbury_model_remove(pFlipperModel ,0);
	if(!bModelSwaped)
	{
		thornbury_model_append (pFlipperModel, LIGHTWOOD_FLIPPER_COLUMN_FIRST_ACTOR,pSecondActor,
				LIGHTWOOD_FLIPPER_COLUMN_SECOND_ACTOR, pFirstActor,
				-1);
	}
	else
	{
		thornbury_model_append (pFlipperModel, LIGHTWOOD_FLIPPER_COLUMN_FIRST_ACTOR,pFirstActor ,
				LIGHTWOOD_FLIPPER_COLUMN_SECOND_ACTOR, pSecondActor,
				-1);
	}
	bModelSwaped = !bModelSwaped ;
	return FALSE ;
}

static gboolean v_test_flipper_bring_actor_in_reverse(gpointer pUserData)
{
	/* Interchange the Model data */
	v_test_flipper_change_model(pUserData);

	/* Toggle the Flip direction */
	g_object_set(CLUTTER_ACTOR(pUserData),
			"flip-direction" , bDirectionSwaped ? CLUTTER_TIMELINE_FORWARD : CLUTTER_TIMELINE_BACKWARD,
					NULL);
	bDirectionSwaped = !bDirectionSwaped ;
	return FALSE ;
}


/* button state changed callback, if multi state */
static void v_test_flipper_animation_started(GObject *flipper, ClutterActor *pFlipActor , gpointer PUserData)
{
	TEST_FLIP_ANIMATION_PRINT("%s \n",__FUNCTION__);
}

static gboolean
b_test_flipper_unhide_first_actor_for_half_flip (gpointer pUserData)
{
	gboolean bAnimationInProgress ;
	g_object_get(pFlipper , "animation-in-progress" , &bAnimationInProgress , NULL);
	if(FALSE == bAnimationInProgress)
	{
//		thornbury_model_remove(pFlipperModel , 0);
		clutter_actor_show(CLUTTER_ACTOR(pUserData));
	}
	return FALSE ;
}

static void v_test_flipper_animation_completed(GObject *flipper, ClutterActor *pFlipActor , gpointer PUserData)
{
	guint uinFlipCycle ;
	TEST_FLIP_ANIMATION_PRINT("%s \n",__FUNCTION__);
	g_object_get (flipper , "flip-cycle" , &uinFlipCycle , NULL);
	bDiscreteSet = FALSE ;
	fltDiscrete = 0.0;

	if( LIGHTWOOD_ENABLE_FIRST_ACTOR_FLIP_ONLY == uinFlipCycle)
	{
		g_timeout_add_seconds(1 , b_test_flipper_unhide_first_actor_for_half_flip , pFlipActor);
	}

}
static void
v_test_flipper_animation_reversed (LightwoodFlipper *flipper,
                                   ClutterActor *pFlipActor,
                                   ClutterTimelineDirection uinFlipDirection,
                                   gpointer PUserData)
{
	const gchar *pActor = clutter_actor_get_name(pFlipActor);
	TEST_FLIP_ANIMATION_PRINT("%s \n",__FUNCTION__);
	if (CLUTTER_TIMELINE_FORWARD == uinFlipDirection)
	{
		TEST_FLIP_ANIMATION_PRINT("%s actor  is being animated in CLUTTER_TIMELINE_FORWARD \n" , pActor);
	}
	else if (CLUTTER_TIMELINE_BACKWARD == uinFlipDirection)
	{
		TEST_FLIP_ANIMATION_PRINT("%s actor  is being animated in CLUTTER_TIMELINE_BACKWARD \n" , pActor);
	}
}


static void
v_test_flipper_animation_half_mark (LightwoodFlipper *flipper,
                                    ClutterTimelineDirection uinFlipDirection)
{
	TEST_FLIP_ANIMATION_PRINT("%s \n",__FUNCTION__);
	if(CLUTTER_TIMELINE_FORWARD == uinFlipDirection)
	{
		TEST_FLIP_ANIMATION_PRINT(" CLUTTER_TIMELINE_FORWARD \n" );
	}
	else if(CLUTTER_TIMELINE_BACKWARD == uinFlipDirection)
	{
		TEST_FLIP_ANIMATION_PRINT("  CLUTTER_TIMELINE_BACKWARD \n" );
	}
}


static void
v_test_flipper_animation_marker_reached (LightwoodFlipper *flipper,
                                         ClutterActor *pActorBeingAnimated,
                                         ClutterTimelineDirection uinFlipDirection,
                                         gpointer PUserData)
{
	const gchar *pActor = clutter_actor_get_name(pActorBeingAnimated);
	TEST_FLIP_ANIMATION_PRINT("%s \n",__FUNCTION__);
	if(CLUTTER_TIMELINE_FORWARD == uinFlipDirection)
	{
		TEST_FLIP_ANIMATION_PRINT("Marker reached for %s actor: Flip direction: CLUTTER_TIMELINE_FORWARD \n" , pActor);
	}
	else if(CLUTTER_TIMELINE_BACKWARD == uinFlipDirection)
	{
		TEST_FLIP_ANIMATION_PRINT("Marker reached for %s actor: Flip direction: CLUTTER_TIMELINE_BACKWARD \n" , pActor);
	}
}




/* creation of model */
static ThornburyModel *v_test_flipper_create_model (void)
{
	ThornburyModel *model = NULL;

	model = (ThornburyModel*)thornbury_list_model_new (LIGHTWOOD_FLIPPER_COLUUMN_LAST,
			G_TYPE_OBJECT, NULL,
			G_TYPE_OBJECT, NULL,
			-1);
	return model;
}

static void v_test_flipper_create_line(gfloat fltWidth ,gfloat fltHeight ,gfloat fltX , gfloat fltY , ClutterActor *stage )
{
	ClutterActor *pNewActor = clutter_actor_new ();
	clutter_actor_set_background_color ( pNewActor, CLUTTER_COLOR_DarkYellow);
	clutter_actor_add_child (stage,  pNewActor);
	clutter_actor_set_size( pNewActor, fltWidth , fltHeight );
	clutter_actor_set_position( pNewActor , fltX , fltY);
}

static void  v_test_flipper_create_text(gfloat fltY , const gchar *pText)
{
	ClutterActor *pInfoLabel  = clutter_text_new();
	clutter_text_set_single_line_mode (CLUTTER_TEXT (pInfoLabel), TRUE);
	clutter_text_set_color(CLUTTER_TEXT(pInfoLabel),CLUTTER_COLOR_Yellow);
	clutter_text_set_font_name(CLUTTER_TEXT(pInfoLabel),"Sans Bold 14");
	clutter_actor_add_child( pStage,	pInfoLabel);
	clutter_text_set_ellipsize(CLUTTER_TEXT(pInfoLabel),PANGO_ELLIPSIZE_END);
	clutter_actor_set_position((pInfoLabel ), ACTOR_WIDTH + ACTOR_X + 20 , fltY);
	clutter_text_set_text(CLUTTER_TEXT(pInfoLabel) , pText);
}


static gboolean v_test_flipper_on_key_press_event (ClutterStage *stage,
		ClutterEvent *event,
		gpointer      pUserData)
{
	LightwoodFlipper *flipper = pUserData;

	switch (clutter_event_get_key_symbol (event))
	{
	case CLUTTER_KEY_x:
	{
		lightwood_flipper_stop_animation (flipper);
	}
	return CLUTTER_EVENT_STOP;
	case CLUTTER_KEY_r:
	{
		lightwood_flipper_restart_animation (flipper);
	}
	return CLUTTER_EVENT_STOP;

	case CLUTTER_KEY_s:
	{
		gboolean bAnimationInProgress ;
		g_object_get(CLUTTER_ACTOR(pUserData) , "animation-in-progress" , &bAnimationInProgress , NULL);
		if(TRUE == bAnimationInProgress)
		{
			return CLUTTER_EVENT_STOP;
		}
		if(TRUE == bDiscreteSet)
		{
			if(thornbury_model_get_n_rows(pFlipperModel) == 0)
			{
				thornbury_model_append (pFlipperModel, LIGHTWOOD_FLIPPER_COLUMN_FIRST_ACTOR,pFirstActor,
					LIGHTWOOD_FLIPPER_COLUMN_SECOND_ACTOR, pSecondActor,
					-1);
			}
//			guint uinFlipDirection = ( bDiscreteStartDirection == TRUE ? CLUTTER_TIMELINE_FORWARD :CLUTTER_TIMELINE_BACKWARD );
			lightwood_flipper_proceed_flip_anim (flipper, bDiscreteStartDirection);
//			bDiscreteStartDirection = !bDiscreteStartDirection ;
		}
		else
		{
			v_test_flipper_bring_actor_in_reverse(pUserData);
			lightwood_flipper_start_animation (flipper);
		}
	}
	return CLUTTER_EVENT_STOP;
	case CLUTTER_KEY_b:
	{
		lightwood_flipper_reverse_animation (flipper);
	}
	return CLUTTER_EVENT_STOP;
	case CLUTTER_KEY_w:
	{
		lightwood_flipper_rewind_animation (flipper);
	}
	return CLUTTER_EVENT_STOP;
	case CLUTTER_KEY_m:
	{
		gboolean bAnimationInProgress ;
		g_object_get(CLUTTER_ACTOR(pUserData) , "animation-in-progress" , &bAnimationInProgress , NULL);
		if(FALSE == bAnimationInProgress)
		{
			v_test_flipper_change_model(pUserData);
			TEST_FLIP_ANIMATION_PRINT("model data swaped \n");
		}
		else
		{
			TEST_FLIP_ANIMATION_PRINT("change of model has to be done when animation is not in progress \n");
		}
	}
	return CLUTTER_EVENT_STOP;
	case CLUTTER_KEY_c:
	{

		gboolean bAnimationInProgress ;
		g_object_get(CLUTTER_ACTOR(pUserData) , "animation-in-progress" , &bAnimationInProgress , NULL);
		if(FALSE == bAnimationInProgress)
		{
//			g_object_set(CLUTTER_ACTOR(pUserData),
//					"transition-direction" , (bDirectionSwaped ? BACKWARD_FLIP_DIRECTION : FORWARD_FLIP_DIRECTION),
//					 NULL);

			g_object_get(CLUTTER_ACTOR(pUserData) , "animation-in-progress" , &bAnimationInProgress , NULL);
			if(FALSE == bAnimationInProgress)
			{
				g_object_set(CLUTTER_ACTOR(pUserData),
						"flip-direction" , bDirectionSwaped ? CLUTTER_TIMELINE_FORWARD : CLUTTER_TIMELINE_BACKWARD ,
								NULL);
				bDirectionSwaped = !bDirectionSwaped ;
			}

			TEST_FLIP_ANIMATION_PRINT("flip direction changed \n");
		}
		else
		{
			TEST_FLIP_ANIMATION_PRINT("change of property is effective only when animation is not in progress \n");
		}
	}
	return CLUTTER_EVENT_STOP ;
	case CLUTTER_KEY_D:
	{
		TEST_FLIP_ANIMATION_PRINT("%s %d destroying \n" , __FUNCTION__ , __LINE__);
		g_object_unref(CLUTTER_ACTOR(pUserData));
		v_test_flipper_create_text(ACTOR_Y   + ACTOR_HEIGHT - 20.0 , "Note : Flipper Instance Destroyed");
	}
	return CLUTTER_EVENT_STOP ;
	case CLUTTER_KEY_0:
	{
		g_object_set(CLUTTER_ACTOR(pUserData),"flip-cycle",LIGHTWOOD_ENABLE_FULL_FLIP_CYCLE,NULL);
	}
	return CLUTTER_EVENT_STOP;
	case CLUTTER_KEY_1:
	{
		g_object_set(CLUTTER_ACTOR(pUserData),"flip-cycle",LIGHTWOOD_ENABLE_FIRST_ACTOR_FLIP_ONLY,NULL);
	}
	return CLUTTER_EVENT_STOP;
	case CLUTTER_KEY_2:
	{
		g_object_set(CLUTTER_ACTOR(pUserData),"flip-cycle",LIGHTWOOD_ENABLE_SECOND_ACTOR_FLIP_ONLY,NULL);
	}
	return CLUTTER_EVENT_STOP;
	case CLUTTER_KEY_p:
	{
		fltXPivotPoint = fltXPivotPoint - 0.1 ;
		if(fltXPivotPoint <= 1.0 && fltXPivotPoint >= 0.0)
			g_object_set(CLUTTER_ACTOR(pUserData),"x-pivot-point",fltXPivotPoint,NULL);
	}
	return CLUTTER_EVENT_STOP;
	case CLUTTER_KEY_P:
	{
		fltXPivotPoint = fltXPivotPoint + 0.1 ;
		if(fltXPivotPoint <= 1.0 && fltXPivotPoint >= 0.0)
			g_object_set(CLUTTER_ACTOR(pUserData),"x-pivot-point",fltXPivotPoint,NULL);
	}
	return CLUTTER_EVENT_STOP;
	case CLUTTER_KEY_F:
	{
		gboolean bAnimationInProgress ;
		g_object_get(CLUTTER_ACTOR(pUserData) , "animation-in-progress" , &bAnimationInProgress , NULL);
		if(FALSE == bAnimationInProgress)
		{
			g_object_set(CLUTTER_ACTOR(pUserData),"first-actor-set-mark",
					bSignalConnected1 ? 0 :TRANSITION_TIME/2,NULL);

			bSignalConnected1 = !bSignalConnected1;
		}
	}
	return CLUTTER_EVENT_STOP;
	case CLUTTER_KEY_f:
	{
		gboolean bAnimationInProgress ;
		g_object_get(CLUTTER_ACTOR(pUserData) , "animation-in-progress" , &bAnimationInProgress , NULL);
		if(FALSE == bAnimationInProgress)
		{
			g_object_set(CLUTTER_ACTOR(pUserData),"second-actor-set-mark",
					bSignalConnected2 ? 0 :TRANSITION_TIME/2,NULL);
			bSignalConnected2 = !bSignalConnected2  ;
		}
	}
	return CLUTTER_EVENT_STOP;
	case CLUTTER_KEY_d:
	{
		gboolean bAnimationInProgress ;
		TEST_FLIP_ANIMATION_PRINT("%s %d \n",__FUNCTION__ , __LINE__);
		g_object_get(CLUTTER_ACTOR(pUserData) , "animation-in-progress" , &bAnimationInProgress , NULL);
		if(FALSE == bAnimationInProgress)
		{
			bDiscreteSet = !bDiscreteSet;
			g_object_set(CLUTTER_ACTOR(pUserData),"discrete-factor",
					bDiscreteSet ? fltDiscrete :0.0,NULL);
		}
	}
	return CLUTTER_EVENT_STOP;

	case CLUTTER_KEY_plus:
	{
		if(TRUE == bDiscreteSet)
		{
			gboolean bAnimationInProgress ;
			TEST_FLIP_ANIMATION_PRINT("%s %d \n",__FUNCTION__ , __LINE__);
			g_object_get(CLUTTER_ACTOR(pUserData) , "animation-in-progress" , &bAnimationInProgress , NULL);
			if(FALSE == bAnimationInProgress)
			{
				fltDiscrete = fltDiscrete+0.05;

				TEST_FLIP_ANIMATION_PRINT("%s %d \n",__FUNCTION__ , __LINE__);

				g_object_set(CLUTTER_ACTOR(pUserData),"discrete-factor",
						bDiscreteSet ? fltDiscrete :0.0,NULL);
			}
		}
	}
	return CLUTTER_EVENT_STOP;

	case CLUTTER_KEY_minus:
	{
		if(TRUE == bDiscreteSet)
		{
			gboolean bAnimationInProgress ;
			TEST_FLIP_ANIMATION_PRINT("%s %d \n",__FUNCTION__ , __LINE__);
			g_object_get(CLUTTER_ACTOR(pUserData) , "animation-in-progress" , &bAnimationInProgress , NULL);
			if(FALSE == bAnimationInProgress)
			{
				fltDiscrete = fltDiscrete-0.05;
				g_object_set(CLUTTER_ACTOR(pUserData),"discrete-factor",
						bDiscreteSet ? fltDiscrete :0.0,NULL);
			}
		}
	}
	return CLUTTER_EVENT_STOP;



	default:
		break;
	}

	return CLUTTER_EVENT_PROPAGATE;
}



static LightwoodFlipper *
p_test_flipper_create_flipper_instance (void)
{
	pFlipperModel = v_test_flipper_create_model();

	thornbury_model_append (pFlipperModel, LIGHTWOOD_FLIPPER_COLUMN_FIRST_ACTOR, pFirstActor,
			LIGHTWOOD_FLIPPER_COLUMN_SECOND_ACTOR, pSecondActor,
			-1);



	pFlipper =  g_object_new(LIGHTWOOD_TYPE_FLIPPER, "transition_time", TRANSITION_TIME,
		/* 	"flip-cycle" , LIGHTWOOD_ENABLE_FIRST_ACTOR_FLIP_ONLY, */
			"model",(ThornburyModel*)pFlipperModel,
			/*  "forward-flip-end-angle" , 90.0 , */NULL);
	g_signal_connect(pFlipper, "flip-animation-started",  G_CALLBACK(v_test_flipper_animation_started), NULL);
	g_signal_connect(pFlipper, "flip-animation-completed",  G_CALLBACK(v_test_flipper_animation_completed), NULL);
	g_signal_connect(pFlipper, "flip-animation-reversed",  G_CALLBACK(v_test_flipper_animation_reversed), NULL);
	g_signal_connect(pFlipper, "flip-animation-half-mark",  G_CALLBACK(v_test_flipper_animation_half_mark), NULL);
	g_signal_connect(pFlipper, "flip-animation-mark-reached",  G_CALLBACK(v_test_flipper_animation_marker_reached), NULL);

	return pFlipper ;
}

static void  lightwood_flipper_drag_end_cb                      (ClutterDragAction  *pAction,
                                                        ClutterActor       *pActor,
                                                        gfloat              fltEvent_x,
                                                        gfloat              fltEvent_y,
                                                        ClutterModifierType modifiers,
                                                        gpointer            pUserData)
{

	if(TRUE == bDiscreteSet)
	{
		gboolean bAnimationInProgress ;
		g_object_get(CLUTTER_ACTOR(pUserData) , "animation-in-progress" , &bAnimationInProgress , NULL);
		if(FALSE == bAnimationInProgress)
		{
//			guint uinFlipDirection = ( bDiscreteStartDirection == TRUE ? CLUTTER_TIMELINE_FORWARD :CLUTTER_TIMELINE_BACKWARD );
			g_object_set(CLUTTER_ACTOR(pUserData),"proceed-flip",bDiscreteStartDirection,NULL);
		}
	}
	fltStartX = 0.0;
	fltLastX = 0.0 ;
}



static gboolean            lightwood_flipper_drag_progress_cb (ClutterDragAction *pAction,
        ClutterActor      *pActor,
        gfloat             fltDelta_x,
        gfloat             fltDelta_y,
        gpointer           pUserData)
{
	gfloat fltX,fltY;
	clutter_drag_action_get_motion_coords(pAction,&fltX,&fltY);

	if(fltX < ACTOR_X  || fltX > (ACTOR_X+ACTOR_WIDTH) )
		return TRUE ;

//	if(fltX > fltLastX  && fltLastX + 10 > fltX )
//		return ;
//	if( fltX < fltLastX && fltLastX - 10 <  fltX)
//		return;

	if(TRUE == bDiscreteSet)
	{
		gboolean bAnimationInProgress ;
		TEST_FLIP_ANIMATION_PRINT("%s %d \n",__FUNCTION__ , __LINE__);
		g_object_get(CLUTTER_ACTOR(pUserData) , "animation-in-progress" , &bAnimationInProgress , NULL);
		if(FALSE == bAnimationInProgress)
		{

			guint uintDirection ;
			g_object_get(CLUTTER_ACTOR(pUserData),
					"flip-direction" , &uintDirection  , NULL);
			fltDiscrete = ( fltX - ACTOR_X)/ACTOR_WIDTH;
			if(uintDirection == CLUTTER_TIMELINE_BACKWARD)
			{
				fltDiscrete = 1.0 - fltDiscrete ;
				TEST_FLIP_ANIMATION_PRINT("%s %d \n",__FUNCTION__ , __LINE__);
				if(fltDiscrete < fltLastX )
				{
					//				fltDiscrete = fltDiscrete+0.05;
					bDiscreteStartDirection = CLUTTER_TIMELINE_FORWARD ;
				}
				else
				{
					//				fltDiscrete = fltDiscrete-0.05;
					bDiscreteStartDirection = CLUTTER_TIMELINE_BACKWARD;
				}
			}
			else
			{
				TEST_FLIP_ANIMATION_PRINT("%s %d \n",__FUNCTION__ , __LINE__);
				if(fltDiscrete > fltLastX )
				{
					//				fltDiscrete = fltDiscrete+0.05;
					bDiscreteStartDirection = CLUTTER_TIMELINE_FORWARD ;
				}
				else
				{
					//				fltDiscrete = fltDiscrete-0.05;
					bDiscreteStartDirection = CLUTTER_TIMELINE_BACKWARD;
				}
			}
			fltLastX = fltDiscrete ;
			g_object_set(CLUTTER_ACTOR(pUserData),"discrete-factor",
					bDiscreteSet ? fltDiscrete :0.0,NULL);
		}
	}
	return TRUE ;
}




static void    lightwood_flipper_drag_begin_cb                      (ClutterDragAction  *pAction,
                                                        ClutterActor       *pActor,
                                                        gfloat              fltEvent_x,
                                                        gfloat              fltEvent_y,
                                                        ClutterModifierType modifiers,
                                                        gpointer            pUserData)
{
	fltStartX = fltEvent_x;
	fltLastX = fltEvent_x ;

}



static void v_test_flipper_create_sample_clutter_actors (void)
{
	ClutterActor *pInfoLabel;

	pFirstActor = clutter_actor_new ();
	clutter_actor_add_child (pStage,  pFirstActor);
	clutter_actor_set_background_color ( pFirstActor, CLUTTER_COLOR_White);
	clutter_actor_set_size( pFirstActor , ACTOR_WIDTH , ACTOR_HEIGHT );
	clutter_actor_set_position( pFirstActor ,ACTOR_X , ACTOR_Y) ;
	clutter_actor_set_name(pFirstActor , "FIRST");
//	clutter_actor_add_constraint (pFirstActor, clutter_bind_constraint_new (pFirstActor, CLUTTER_BIND_ALL, 0.f));

//	clutter_actor_add_constraint (pFirstActor, clutter_snap_constraint_new (pFirstActor,
//                                 CLUTTER_SNAP_EDGE_TOP,
//                                 CLUTTER_SNAP_EDGE_TOP,
//                                 0.f));


	pInfoLabel  = clutter_text_new();
	clutter_text_set_single_line_mode (CLUTTER_TEXT (pInfoLabel), TRUE);
	clutter_text_set_color(CLUTTER_TEXT(pInfoLabel),CLUTTER_COLOR_DarkBlue);
	clutter_text_set_font_name(CLUTTER_TEXT(pInfoLabel),"Sans Bold 50");
	clutter_actor_add_child( pFirstActor,	pInfoLabel);
	clutter_text_set_ellipsize(CLUTTER_TEXT(pInfoLabel),PANGO_ELLIPSIZE_END);
	clutter_actor_set_position((pInfoLabel ), 50 , ACTOR_HEIGHT/2);
	clutter_text_set_text(CLUTTER_TEXT(pInfoLabel) , "FIRST 1");

	pSecondActor = clutter_actor_new ();
	clutter_actor_add_child (pStage,  pSecondActor);
	clutter_actor_set_background_color ( pSecondActor, CLUTTER_COLOR_DarkChameleon);
	clutter_actor_set_size( pSecondActor , ACTOR_WIDTH , ACTOR_HEIGHT );
	clutter_actor_set_position( pSecondActor , ACTOR_X , ACTOR_Y);
	clutter_actor_set_name(pSecondActor , "SECOND");

	clutter_actor_hide(pSecondActor);

//	clutter_actor_add_constraint (pSecondActor, clutter_bind_constraint_new (pSecondActor, CLUTTER_BIND_ALL, 0.f));

//	clutter_actor_add_constraint (pSecondActor, clutter_snap_constraint_new (pSecondActor,
//                                 CLUTTER_SNAP_EDGE_TOP,
//                                 CLUTTER_SNAP_EDGE_TOP,
//                                 0.f));

	pInfoLabel  = clutter_text_new();
	clutter_text_set_single_line_mode (CLUTTER_TEXT (pInfoLabel), TRUE);
	clutter_text_set_color(CLUTTER_TEXT(pInfoLabel),CLUTTER_COLOR_DarkBlue);
	clutter_text_set_font_name(CLUTTER_TEXT(pInfoLabel),"Sans Bold 50");
	clutter_actor_add_child( pSecondActor,	pInfoLabel);
	clutter_text_set_ellipsize(CLUTTER_TEXT(pInfoLabel),PANGO_ELLIPSIZE_END);
	clutter_actor_set_position((pInfoLabel ), 50 , ACTOR_HEIGHT/2);
	clutter_text_set_text(CLUTTER_TEXT(pInfoLabel) , "SECOND");

	v_test_flipper_create_line(ACTOR_WIDTH ,BOUND_DEPTH , ACTOR_X , ACTOR_Y , pStage);
	v_test_flipper_create_line(ACTOR_WIDTH ,BOUND_DEPTH  , ACTOR_X  ,ACTOR_Y + ACTOR_HEIGHT  , pStage );
	v_test_flipper_create_line(BOUND_DEPTH  ,ACTOR_HEIGHT  , ACTOR_X , ACTOR_Y  , pStage);
	v_test_flipper_create_line(BOUND_DEPTH  ,ACTOR_HEIGHT  , ACTOR_X + ACTOR_WIDTH  , ACTOR_Y , pStage );


	v_test_flipper_create_line(ACTOR_WIDTH ,LINE_HEIGHT , 0.0 , 0.0 , pFirstActor);
	v_test_flipper_create_line(ACTOR_WIDTH ,LINE_HEIGHT , 0.0 ,ACTOR_HEIGHT/2   , pFirstActor);
	v_test_flipper_create_line(ACTOR_WIDTH ,LINE_HEIGHT , 0.0 , ACTOR_HEIGHT , pFirstActor);
	v_test_flipper_create_line(LINE_HEIGHT ,ACTOR_HEIGHT , 0.0 , 0.0 , pFirstActor);
	v_test_flipper_create_line(LINE_HEIGHT ,ACTOR_HEIGHT ,  ACTOR_WIDTH/2 ,0.0 , pFirstActor);
	v_test_flipper_create_line(LINE_HEIGHT ,ACTOR_HEIGHT ,  ACTOR_WIDTH ,0.0, pFirstActor);


	v_test_flipper_create_line(ACTOR_WIDTH ,LINE_HEIGHT , 0.0 , 0.0 , pSecondActor);
	v_test_flipper_create_line(ACTOR_WIDTH ,LINE_HEIGHT ,  0.0 ,ACTOR_HEIGHT/2  , pSecondActor);
	v_test_flipper_create_line(ACTOR_WIDTH ,LINE_HEIGHT , 0.0 , ACTOR_HEIGHT , pSecondActor);
	v_test_flipper_create_line(LINE_HEIGHT ,ACTOR_HEIGHT , 0.0 , 0.0 , pSecondActor);
	v_test_flipper_create_line(LINE_HEIGHT ,ACTOR_HEIGHT , ACTOR_WIDTH/2 ,0.0, pSecondActor);
	v_test_flipper_create_line(LINE_HEIGHT ,ACTOR_HEIGHT , ACTOR_WIDTH ,0.0, pSecondActor);



	v_test_flipper_create_text(ACTOR_Y   + 20.0 , "press x to stop");
	v_test_flipper_create_text(ACTOR_Y   + 40.0 , "press r to restart");
	v_test_flipper_create_text(ACTOR_Y   + 60.0 , "press w to rewind");
	v_test_flipper_create_text(ACTOR_Y   + 80.0 , "press b to reverse animation");
	v_test_flipper_create_text(ACTOR_Y   + 100.0 , "press s to start");
	v_test_flipper_create_text(ACTOR_Y   + 120.0 , "press m to swap model");
	v_test_flipper_create_text(ACTOR_Y   + 140.0 , "press c to change flip direction");
	v_test_flipper_create_text(ACTOR_Y   + 160.0 , "press \t 0 to flip both actor");
	v_test_flipper_create_text(ACTOR_Y   + 180.0 , "\t\t 1 to flip First actor Only");
	v_test_flipper_create_text(ACTOR_Y   + 200.0 , "\t\t 2 to flip Second actor Only");
	v_test_flipper_create_text(ACTOR_Y   + 220.0 , "press P to increment X pivot point by 0.1");
	v_test_flipper_create_text(ACTOR_Y   + 240.0 , "press p to decrement X pivot point by 0.1");
	v_test_flipper_create_text(ACTOR_Y   + 260.0 , "press F/f to set/unset Marker for 1/2 actor ");
	v_test_flipper_create_text(ACTOR_Y   + 280.0 , "press d to set/unset discrete value. ");
	v_test_flipper_create_text(ACTOR_Y   + 300.0 , "  use +/- or Drag to increase/decrease scaling factor");
	v_test_flipper_create_text(ACTOR_Y   + 340.0 , "press D to destroy flipper instance");
}

int main (int argc, char **argv)
{
	int clInErr = clutter_init(&argc, &argv);
	ClutterColor black = { 0x00, 0x00, 0x00, 0x00 };
	ClutterAction *pDragAction;

	TEST_FLIP_ANIMATION_PRINT("%s \n",__FUNCTION__);
	if (clInErr != CLUTTER_INIT_SUCCESS)
		return -1;

	pStage = clutter_stage_new ();
	clutter_stage_set_title (CLUTTER_STAGE (pStage), "Threading");
	clutter_actor_set_background_color (pStage, &black);
	//	clutter_actor_set_size (pStage, 500 , 500 ) ;  /*728, 480); */
	clutter_actor_set_size (pStage, STAGE_WH );
	g_signal_connect (pStage, "destroy", G_CALLBACK (clutter_main_quit), NULL);

	v_test_flipper_create_sample_clutter_actors();

	/* creates the Flipper Instance */
	pFlipper = p_test_flipper_create_flipper_instance();


	g_signal_connect (pStage,"key-press-event", G_CALLBACK (v_test_flipper_on_key_press_event),
			pFlipper);

	/* creation of drag action */
	pDragAction = clutter_drag_action_new ();
	clutter_actor_add_action (pStage,  pDragAction);
	clutter_drag_action_set_drag_axis (CLUTTER_DRAG_ACTION ( pDragAction), CLUTTER_DRAG_X_AXIS);
	clutter_drag_action_set_drag_threshold (CLUTTER_DRAG_ACTION ( pDragAction), 10, 10);
	clutter_actor_set_reactive(pFirstActor , TRUE);
	g_signal_connect ( pDragAction, "drag-progress", G_CALLBACK (lightwood_flipper_drag_progress_cb), pFlipper);
	g_signal_connect ( pDragAction, "drag-begin", G_CALLBACK (lightwood_flipper_drag_begin_cb), pFlipper);
	g_signal_connect ( pDragAction, "drag-end", G_CALLBACK (lightwood_flipper_drag_end_cb), pFlipper);

	clutter_actor_show (pStage);

	clutter_main();

	return 0;

}
