/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2011 Robert Bosch Car Multimedia GmbH
 * Author: Tomeu Vizoso <tomeu.vizoso@collabora.co.uk>
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <string.h>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-prototypes"
#include <mx/mx.h>
#pragma GCC diagnostic pop
#include <clutter-gst/clutter-gst.h>
#include <lightwood-roller/liblightwood-roller.h>
#include <lightwood-roller/liblightwood-fixedroller.h>

#include "sample-item.h"
#include "sample-item-toggle.h"
#include "sample-item-video.h"
#include "sample-item-factory.h"
#include "sample-ui-elements.h"

#define WIDTH                   800
#define HEIGHT                  480
#define BACKGROUND_FILE_NAME    "background.png"
#define MASK_FILE_NAME          "content_mask_alpha.png"
#define OVERLAY_FILE_NAME       "content_overlay.png"
#define SPINNER_FILE_NAME       "spinner.png"
#define SCROLL_BAR_WIDTH        24
#define AUTOSCROLL_FREQUENCY    10000
#define NUM_OF_ITEMS            100000

#define COMBO_ADD_ITEM     0
#define COMBO_SLIDE_WIDTH  1
#define COMBO_SLIDE_FOOTER 2

struct Options
{
  gboolean video;
  gboolean toggle;;
  gboolean auto_scroll;
  gboolean auto_focus;
  gboolean scroll_back;
  gboolean no_background;
  gboolean motion_blur;
};

struct _AppData {
  struct Options options;
  ClutterActor *roller;
  ClutterModel *model;
  ClutterActor *footer;
  ClutterActor *content;
};

typedef struct _AppData AppData;

static gboolean
scroll_timeout_cb (MxAdjustment *vadjustment)
{
  static gboolean direction_down = TRUE;
  gfloat position;

  position = mx_adjustment_get_page_size (vadjustment) * 10;
  if (!direction_down)
    position = -position;

  mx_adjustment_interpolate (vadjustment, position, AUTOSCROLL_FREQUENCY,
                             CLUTTER_EASE_IN_OUT_QUAD);

  if (direction_down)
    direction_down = FALSE;
  else
    direction_down = TRUE;

  return TRUE;
}

static guint idle_id = 0;

static gboolean
callback_hide_cb (MxScrollBar *scroll_bar)
{
  idle_id = 0;

  clutter_actor_hide (CLUTTER_ACTOR (scroll_bar));

  return FALSE;
}

static void
combo_index_changed_cb (MxComboBox *combo_box,
                        GParamSpec *pspec,
                        AppData  *data)
{
  switch (mx_combo_box_get_index (combo_box))
    {
    case COMBO_ADD_ITEM:
      {
        ClutterModel *model = data->model;
        static guint new_item_count = 0;
        g_autofree gchar *icon1_filename = NULL, *video1_filename = NULL;

        icon1_filename = g_test_build_filename (G_TEST_DIST, "data",
                                                ICON1_FILE_NAME, NULL);
        video1_filename = g_test_build_filename (G_TEST_DIST, "data",
                                                 VIDEO1_FILE_NAME, NULL);

        CoglHandle icon = cogl_texture_new_from_file (icon1_filename,
                                                      COGL_TEXTURE_NONE,
                                                      COGL_PIXEL_FORMAT_ANY,
                                                      NULL);

        clutter_model_insert (model, 3, 0, g_strdup_printf ("new item number %i",
                                                            new_item_count),
                                        1, icon,
                                        2, g_strdup_printf ("new item number %i",
                                                            new_item_count),
                                        3, FALSE,
                                        4, video1_filename,
                                        -1);

        new_item_count++;
        break;
      }
    case COMBO_SLIDE_WIDTH:
      {
        ClutterActor *roller = data->roller;
        clutter_actor_animate (roller, CLUTTER_LINEAR, 1000,
                               "width", 200.0,
                               NULL);
        break;
      }
    case COMBO_SLIDE_FOOTER:
      {
        ClutterActor *footer = data->footer;
        clutter_actor_animate (CLUTTER_ACTOR (footer), CLUTTER_LINEAR, 1000,
                               "height", 0.0,
                               NULL);
        break;
      }
    default:
      return;
    }

  mx_combo_box_set_active_text (MX_COMBO_BOX (combo_box), "Choose an action");
}

static void
adjustment_value_notify_cb (MxAdjustment *adjustment,
                            GParamSpec *pspec,
                            MxScrollBar *scroll_bar)
{
  if (!CLUTTER_ACTOR_IS_VISIBLE (scroll_bar))
    clutter_actor_show (CLUTTER_ACTOR (scroll_bar));

  if (idle_id != 0)
    g_source_remove (idle_id);

  idle_id = g_timeout_add (1000, (GSourceFunc) callback_hide_cb, scroll_bar);
}

static void
adjustment_value_notify2_cb (MxAdjustment *adjustment,
                             GParamSpec   *pspec,
                             LightwoodRoller    *roller)
{
  gfloat position = mx_adjustment_get_value (adjustment);
  g_debug ("Row at the top of the roller is %d",
           lightwood_roller_get_row_at (roller, position));
}

static guint scroll_back_idle_id = 0;
static gdouble last_activated_position = 0.;

static void
roller_item_activated_cb (LightwoodRoller *roller, guint row, gpointer data)
{
  MxAdjustment *vadjust;

  mx_scrollable_get_adjustments (MX_SCROLLABLE (roller), NULL, &vadjust);
  last_activated_position = mx_adjustment_get_value (vadjust);

  if (scroll_back_idle_id != 0)
    {
      g_source_remove (scroll_back_idle_id);
      scroll_back_idle_id = 0;
    }
}

static gboolean
scroll_back_cb (LightwoodRoller *roller)
{
  MxAdjustment *vadjust;

  mx_scrollable_get_adjustments (MX_SCROLLABLE (roller), NULL, &vadjust);

  mx_adjustment_interpolate (vadjust,
                             last_activated_position,
                             1250,
                             get_elastic_mode ());

  scroll_back_idle_id = 0;
  return FALSE;
}

static void
adjustment_value_notify3_cb (MxAdjustment *adjustment,
                             GParamSpec   *pspec,
                             LightwoodRoller    *roller)
{
  if (scroll_back_idle_id != 0)
    {
      g_source_remove (scroll_back_idle_id);
      scroll_back_idle_id = 0;
    }

  scroll_back_idle_id = g_timeout_add (2000, (GSourceFunc) scroll_back_cb, roller);
}

static void
locking_finished_cb (LightwoodRoller *roller,
                     gpointer   user_data)
{
  gint focused_row;

  g_object_get (G_OBJECT (roller), "focused-row", &focused_row, NULL);
  g_object_set (G_OBJECT (roller), "focused-row", focused_row + 10, NULL);
}

typedef struct _DragState DragState;
struct _DragState
{
  gboolean      in_press;
  gboolean      in_drag;
  gboolean      in_pause;

  /* In stage coordinates */
  gfloat        press_x, press_y;
  gfloat        last_y;

  guint         press_timeout;
  LightwoodRoller    *roller;
  ClutterActor *item;
  ClutterActor *faded_actor;
};

static DragState state = { 0, };

static gboolean
long_press_cb (gpointer user_data)
{
  MxAdjustment *vadjust;
  ClutterModel *model;
  ClutterActor *stage;
  gint row;
  gdouble position;
  gfloat y;

  state.in_pause = FALSE;
  state.press_timeout = 0;

  mx_scrollable_get_adjustments (MX_SCROLLABLE (state.roller),
                                 NULL, &vadjust);
  position = mx_adjustment_get_value (vadjust);

  clutter_actor_transform_stage_point (CLUTTER_ACTOR (state.roller), 0, state.press_y, NULL, &y);
  row = lightwood_roller_get_row_at (state.roller, position + y);

  g_object_get (state.roller, "model", &model, NULL);

  lightwood_roller_set_animation_mode (state.roller, TRUE);

  stage = clutter_actor_get_stage (CLUTTER_ACTOR (state.roller));
  state.item = clutter_stage_get_actor_at_pos (CLUTTER_STAGE (stage),
                                               CLUTTER_PICK_REACTIVE,
                                               state.press_x,
                                               state.press_y);

  if (row > 0)
    g_object_set (state.item, "arrow-up", ARROW_GREY, NULL);

  if (row < clutter_model_get_n_rows (model) - 1)
    g_object_set (state.item, "arrow-down", ARROW_GREY, NULL);

  return FALSE;
}

static gboolean
handle_press (LightwoodRoller *roller, ClutterEvent *event)
{
  memset (&state, 0, sizeof (DragState));

  state.roller = roller;
  state.in_press = TRUE;
  state.in_pause = TRUE;
  state.press_timeout = g_timeout_add (1000, long_press_cb, NULL);

  clutter_event_get_coords (event, &state.press_x, &state.press_y);
  state.last_y = state.press_y;

  return FALSE;
}

static gboolean
handle_release (LightwoodRoller *roller, ClutterEvent *event)
{
  lightwood_roller_set_animation_mode (state.roller, FALSE);
  state.in_press = FALSE;
  state.in_pause = FALSE;
  if (state.in_drag)
    {
      guint i, origin_row, target_row, n_columns;
      gfloat y, release_y, position;
      MxAdjustment *vadjust;
      ClutterModel *model;
      ClutterModelIter *origin_iter, *target_iter;

      state.in_drag = FALSE;

      mx_scrollable_get_adjustments (MX_SCROLLABLE (state.roller),
                                     NULL, &vadjust);
      position = mx_adjustment_get_value (vadjust);
      clutter_actor_transform_stage_point (CLUTTER_ACTOR (state.roller), 0, state.press_y, NULL, &y);
      origin_row = lightwood_roller_get_row_at (state.roller, position + y);

      clutter_event_get_coords (event, NULL, &y);
      clutter_actor_transform_stage_point (CLUTTER_ACTOR (roller), 0, y, NULL, &release_y);
      target_row = lightwood_roller_get_row_at (state.roller, position + release_y);

      g_object_get (roller, "model", &model, NULL);
      n_columns = clutter_model_get_n_columns (model);
      origin_iter = clutter_model_get_iter_at_row (model, origin_row);
      target_iter = clutter_model_get_iter_at_row (model, target_row);

      for (i = 0; i < n_columns; i++)
        {
          GValue origin_value = { 0, };
          GValue target_value = { 0, };

          clutter_model_iter_get_value (origin_iter, i, &origin_value);
          clutter_model_iter_get_value (target_iter, i, &target_value);

          clutter_model_iter_set_value (origin_iter, i, &target_value);
          clutter_model_iter_set_value (target_iter, i, &origin_value);

          g_value_unset (&origin_value);
          g_value_unset (&target_value);
        }

      g_object_set (state.item, "arrow-up", ARROW_HIDDEN, NULL);
      g_object_set (state.item, "arrow-down", ARROW_HIDDEN, NULL);

      if (state.faded_actor != NULL)
        clutter_actor_animate (state.faded_actor, CLUTTER_LINEAR, 500,
                               "opacity", 255,
                               NULL);

      return TRUE;
    }
  else
    {
      if (state.press_timeout > 0)
        {
          g_source_remove (state.press_timeout);
          state.press_timeout = 0;
        }
      state.in_drag = FALSE;
      return FALSE;
    }
}

static gboolean
handle_motion (LightwoodRoller *roller, ClutterEvent *event)
{
  MxSettings *settings;
  gint drag_threshold;
  gfloat motion_x, motion_y;
  ClutterMotionEvent *motion_event = (ClutterMotionEvent *)event;

  if (!state.in_press)
    return FALSE;

  if (!(motion_event->modifier_state & CLUTTER_BUTTON1_MASK))
    return handle_release (roller, event);

  settings = mx_settings_get_default ();
  g_object_get (G_OBJECT (settings), "drag-threshold", &drag_threshold, NULL);

  clutter_event_get_coords (event, &motion_x, &motion_y);

  if (state.in_drag || ((ABS (motion_x - state.press_x) >= drag_threshold) ||
                        (ABS (motion_y - state.press_y) >= drag_threshold)))
    {
      if (!state.in_pause)
        {
          gint i;
          guint len;
          GSList *items, *iter;
          ClutterActorBox *allocations;

          state.in_drag = TRUE;

          if (motion_y < state.press_y)
            g_object_set (state.item, "arrow-up", ARROW_WHITE, NULL);
          else
            g_object_set (state.item, "arrow-up", ARROW_GREY, NULL);

          if (motion_y > state.press_y)
            g_object_set (state.item, "arrow-down", ARROW_WHITE, NULL);
          else
            g_object_set (state.item, "arrow-down", ARROW_GREY, NULL);

          items = lightwood_roller_get_actors (state.roller);
          allocations = lightwood_roller_get_allocations (state.roller, -1, -1, -1, FALSE, &len);
          for (iter = items, i = 0; iter; iter = iter->next, i++)
            {
              ClutterActor *child_actor = (ClutterActor *) iter->data;
              gfloat y, new_y;

              if (child_actor == state.item)
                {
                  gfloat dy;

                  dy = motion_y - state.last_y;
                  state.last_y = motion_y;

                  new_y = clutter_actor_get_y (state.item) + dy;
                }
              else
                new_y = allocations[i].y1;

              clutter_actor_set_y (child_actor, new_y);

              clutter_actor_transform_stage_point (CLUTTER_ACTOR (state.roller), 0, motion_y, NULL, &y);
              if (y > allocations[i].y1 &&
                  y < allocations[i].y2 &&
                  state.faded_actor != child_actor &&
                  state.item != child_actor)
                {
                  if (state.faded_actor != NULL)
                    clutter_actor_animate (state.faded_actor, CLUTTER_LINEAR, 500,
                                           "opacity", 255,
                                           NULL);

                  clutter_actor_animate (child_actor, CLUTTER_LINEAR, 500,
                                         "opacity", 0,
                                         NULL);
                  state.faded_actor = child_actor;
                }
            }
          g_slice_free1 (sizeof(ClutterActorBox) * g_slist_length (items),
                         allocations);
          g_slist_free (items);

          return TRUE;
        }
      else
        {
          g_source_remove (state.press_timeout);
          state.press_timeout = 0;
          state.in_drag = FALSE;
          state.in_press = FALSE;
          return FALSE;
        }
    }

  return FALSE;
}

static gboolean
captured_event_cb (LightwoodRoller *roller, ClutterEvent *event, gpointer data)
{
  switch (event->type)
    {
    case CLUTTER_BUTTON_PRESS:
      return handle_press (roller, event);
      break;
    case CLUTTER_BUTTON_RELEASE:
      return handle_release (roller, event);
      break;
    case CLUTTER_MOTION:
      return handle_motion (roller, event);
      break;
    default:
      return FALSE;
      break;
    }
}

static ClutterActor *
create_roller (gboolean video, gboolean toggle, gboolean no_background,
               gboolean motion_blur, ClutterModel *model)
{
  ClutterActor *roller;
  SampleItemFactory *item_factory;
  GType item_type;

  if (video)
    item_type = TYPE_SAMPLE_ITEM_VIDEO;
  else if (toggle)
    item_type = TYPE_SAMPLE_ITEM_TOGGLE;
  else
    item_type = TYPE_SAMPLE_ITEM;

  item_factory = g_object_new (TYPE_SAMPLE_ITEM_FACTORY,
                               "item-type", item_type,
                               "model", model,
                               NULL);

  roller = g_object_new (LIGHTWOOD_TYPE_FIXED_ROLLER,
                         "roll-over", TRUE,
                         "motion-blur", motion_blur,
                         "model", model,
                         "factory", item_factory,
                         "name", "roller",
                         "reactive", TRUE,
                         "clamp-duration", 1250,
                         "clamp-mode", get_elastic_mode (),
                         NULL);

  lightwood_roller_add_attribute (LIGHTWOOD_ROLLER (roller), "name", COLUMN_NAME);
  lightwood_roller_add_attribute (LIGHTWOOD_ROLLER (roller), "label", COLUMN_LABEL);

  if (video)
    {
      lightwood_roller_add_attribute (LIGHTWOOD_ROLLER (roller), "video", COLUMN_VIDEO);
    }
  else if (toggle)
    {
      lightwood_roller_add_attribute (LIGHTWOOD_ROLLER (roller), "icon", COLUMN_ICON);
      lightwood_roller_add_attribute (LIGHTWOOD_ROLLER (roller), "toggle", COLUMN_TOGGLE);
    }
  else
    {
      lightwood_roller_add_attribute (LIGHTWOOD_ROLLER (roller), "icon", COLUMN_ICON);
    }

  return roller;
}

static ClutterActor *
create_content (ClutterActor *roller, ClutterActor *footer, gboolean no_background)
{
  ClutterActor *content;
  ClutterActor *texture;
  ClutterActor *scroll_view;
  ClutterActor *vbox;
  MxSettings *settings;
  g_autofree gchar *background_filename = NULL, *overlay_filename = NULL;
  g_autofree gchar *mask_filename = NULL;

  background_filename = g_test_build_filename (G_TEST_DIST, "data",
                                               BACKGROUND_FILE_NAME, NULL);
  overlay_filename = g_test_build_filename (G_TEST_DIST, "data",
                                            OVERLAY_FILE_NAME, NULL);
  mask_filename = g_test_build_filename (G_TEST_DIST, "data",
                                         MASK_FILE_NAME, NULL);

  content = mx_stack_new ();

  if (!no_background)
    {
      texture = clutter_texture_new_from_file (background_filename, NULL);
      clutter_actor_add_child (content, texture);
      clutter_actor_show (texture);

      texture = clutter_texture_new_from_file (overlay_filename, NULL);
      clutter_actor_add_child (content, texture);
      clutter_actor_show (texture);
    }

  vbox = clutter_box_new (g_object_new (CLUTTER_TYPE_BOX_LAYOUT,
                                        "vertical", TRUE,
                                        NULL));
  clutter_actor_add_child (content, vbox);

  scroll_view = create_scroll_view ();
  clutter_box_pack (CLUTTER_BOX (vbox), scroll_view,
                    "x-fill", FALSE,
                    "x-align", CLUTTER_BOX_ALIGNMENT_START,
                    "y-fill", TRUE,
                    "expand", TRUE,
                    NULL);
  clutter_actor_show (scroll_view);

  settings = mx_settings_get_default ();
  g_object_set (G_OBJECT (settings), "drag-threshold", 20, NULL);

  clutter_actor_add_child (scroll_view, roller);
  clutter_actor_show (roller);

  clutter_box_pack (CLUTTER_BOX (vbox), footer,
                    "x-align", CLUTTER_BOX_ALIGNMENT_START,
                    "expand", TRUE,
                    NULL);
  clutter_actor_show (footer);

  if (!no_background)
    {
      texture = clutter_texture_new_from_file (mask_filename, NULL);
      clutter_actor_set_opacity (texture, 0x11);
      clutter_actor_add_child (content, texture);
      clutter_actor_show (texture);
    }

  return content;
}

static ClutterActor *
create_scroll_bar (MxAdjustment *vadjustment)
{
  ClutterActor *scroll_bar;

  scroll_bar = mx_scroll_bar_new_with_adjustment (vadjustment);
  mx_scroll_bar_set_orientation (MX_SCROLL_BAR (scroll_bar),
                                 MX_ORIENTATION_VERTICAL);
  clutter_actor_set_position (scroll_bar, WIDTH - SCROLL_BAR_WIDTH, 0);
  clutter_actor_set_size (scroll_bar, SCROLL_BAR_WIDTH, HEIGHT);

  return scroll_bar;
}

static void
startup_cb (MxApplication *application, AppData *app_data)
{
  MxWindow       *window;
  ClutterColor    black = { 0, 0, 0, 255 };
  ClutterActor   *stage;
  ClutterActor   *vbox;
  ClutterActor   *status_bar;
  ClutterActor   *content;
  ClutterActor   *scroll_bar;
  ClutterActor   *combo_box;
  ClutterActor   *roller;
  ClutterActor   *footer;
  ClutterModel   *model;
  MxAdjustment   *vadjustment;

  window = mx_application_create_window (application, "Roller Test with MX");
  mx_window_set_has_toolbar (window, FALSE);
  stage = CLUTTER_ACTOR (mx_window_get_clutter_stage (window));

  clutter_actor_set_size (stage, WIDTH, HEIGHT);
  clutter_stage_set_color (CLUTTER_STAGE (stage), &black);
  clutter_actor_show (stage);

  vbox = clutter_box_new (g_object_new (CLUTTER_TYPE_BOX_LAYOUT,
                                        "vertical", TRUE,
                                        NULL));
  mx_window_set_child (window, vbox);

  status_bar = create_status_bar ();
  clutter_box_pack (CLUTTER_BOX (vbox), status_bar,
                    "expand", FALSE,
                    NULL);
  clutter_actor_show (status_bar);

  model = create_model (NUM_OF_ITEMS);
  roller = create_roller (app_data->options.video,
                          app_data->options.toggle,
                          app_data->options.no_background,
                          app_data->options.motion_blur,
                          model);
  g_signal_connect (roller, "captured-event", G_CALLBACK (captured_event_cb), NULL);

  footer = g_object_new (CLUTTER_TYPE_TEXT, "text", "Footer",
                                            "font-name", "Sans 28px",
                                            "color", CLUTTER_COLOR_White,
                                            NULL);

  content = create_content (roller, footer, app_data->options.no_background);
  clutter_box_pack (CLUTTER_BOX (vbox), content,
                    "x-fill", TRUE,
                    "y-fill", TRUE,
                    "expand", TRUE,
                    NULL);
  clutter_actor_show (content);

  mx_scrollable_get_adjustments (MX_SCROLLABLE (roller), NULL, &vadjustment);

  scroll_bar = create_scroll_bar (vadjustment);
  clutter_actor_add_child (stage, scroll_bar);
  clutter_actor_hide (scroll_bar);

  combo_box = mx_combo_box_new ();
  clutter_actor_add_child (status_bar, combo_box);
  clutter_actor_set_position (combo_box, 400, 16);
  clutter_actor_show (combo_box);

  mx_combo_box_set_active_text (MX_COMBO_BOX (combo_box), "Choose an action");
  mx_combo_box_insert_text (MX_COMBO_BOX (combo_box), COMBO_ADD_ITEM, "Add item");
  mx_combo_box_insert_text (MX_COMBO_BOX (combo_box), COMBO_SLIDE_WIDTH, "Slide width");
  mx_combo_box_insert_text (MX_COMBO_BOX (combo_box), COMBO_SLIDE_FOOTER, "Slide footer");

  app_data->model = model;
  app_data->roller = roller;
  app_data->footer = footer;
  app_data->content = content;

  g_signal_connect (combo_box, "notify::index",
                    G_CALLBACK (combo_index_changed_cb), app_data);

  g_signal_connect (vadjustment, "notify::value",
                    G_CALLBACK (adjustment_value_notify_cb),
                    scroll_bar);

  //g_signal_connect (vadjustment, "notify::value",
  //                  G_CALLBACK (adjustment_value_notify2_cb),
  //                  roller);

  if (app_data->options.auto_scroll)
    g_timeout_add_full (G_PRIORITY_LOW, AUTOSCROLL_FREQUENCY,
                        (GSourceFunc) scroll_timeout_cb, vadjustment, NULL);

  if (app_data->options.scroll_back)
    {
      g_signal_connect (G_OBJECT (roller), "item-activated",
                        G_CALLBACK (roller_item_activated_cb), NULL);

      g_signal_connect (vadjustment, "notify::value",
                        G_CALLBACK (adjustment_value_notify3_cb),
                        roller);
    }

  if (app_data->options.auto_focus)
    g_signal_connect (roller, "locking-finished",
                      G_CALLBACK (locking_finished_cb),
                      NULL);
}

int main(int argc, char *argv[])
{
  MxApplication  *application;
  GOptionContext *context;
  GError         *error = NULL;
  AppData         app_data = { 0, };
  GOptionEntry   entries[] =
    {
      {"video", 0, 0, G_OPTION_ARG_NONE, &app_data.options.video, "Display a video in each item", NULL},
      {"toggle", 0, 0, G_OPTION_ARG_NONE, &app_data.options.toggle, "Display a toggle in each item", NULL},
      {"autoscroll", 0, 0, G_OPTION_ARG_NONE, &app_data.options.auto_scroll, "Scroll automatically", NULL},
      {"scrollback", 0, 0, G_OPTION_ARG_NONE, &app_data.options.scroll_back, "Scroll back to the focused line if idle", NULL},
      {"autofocus", 0, 0, G_OPTION_ARG_NONE, &app_data.options.auto_focus, "Move focus automatically", NULL},
      {"nobackground", 0, 0, G_OPTION_ARG_NONE, &app_data.options.no_background, "Don't use background images", NULL},
      {"motionblur", 0, 0, G_OPTION_ARG_NONE, &app_data.options.motion_blur, "Use motion blur while scrolling", NULL},
      {NULL}
    };

  g_test_init (&argc, &argv, NULL);

  context = g_option_context_new ("- LightwoodRoller example");
  g_option_context_add_main_entries (context, entries, NULL);
  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      g_print ("%s\n", error->message);
      g_print ("Use --help for more information.\n");
      exit (0);
    }

  if (app_data.options.video && app_data.options.toggle)
    {
      g_print ("Only one of --video and --toggle can be specified.\n");
      exit (0);
    }

  clutter_gst_init (&argc, &argv);

  application = mx_application_new ("lightwoodroller.basic", 0);
  g_signal_connect (application, "startup", (GCallback) startup_cb, &app_data);

  g_application_run (G_APPLICATION (application), argc, argv);

  return EXIT_SUCCESS;
}
