/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2011 Robert Bosch Car Multimedia GmbH
 * Author: Tomeu Vizoso <tomeu.vizoso@collabora.co.uk>
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "sample-ui-elements.h"
#include "sample-item-toggle.h"

G_DEFINE_TYPE_WITH_CODE (SampleItemToggle, sample_item_toggle, CLUTTER_TYPE_GROUP, );

enum
{
  PROP_0,

  PROP_ICON,
  PROP_LABEL,
  PROP_TOGGLE,
  PROP_MODEL,
  PROP_ROW,
};

static void
sample_item_toggle_get_property (GObject    *object,
                                 guint       property_id,
                                 GValue     *value,
                                 GParamSpec *pspec)
{
  SampleItemToggle *item = SAMPLE_ITEM_TOGGLE (object);
  CoglHandle cogl_texture;

  switch (property_id)
    {
    case PROP_ICON:
      cogl_texture = clutter_texture_get_cogl_texture (CLUTTER_TEXTURE (item->icon));
      cogl_handle_ref (cogl_texture);
      g_value_set_boxed (value, cogl_texture);
      break;
    case PROP_LABEL:
      g_value_set_string (value,
          g_strdup (clutter_text_get_text (CLUTTER_TEXT (item->label))));
      break;
    case PROP_TOGGLE:
      g_value_set_boolean (value,
                           mx_toggle_get_active (MX_TOGGLE (item->toggle)));
      break;
    case PROP_MODEL:
      g_value_set_object (value, item->model);
      break;
    case PROP_ROW:
      g_value_set_uint (value, item->row);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
sample_item_toggle_set_property (GObject      *object,
                                 guint         property_id,
                                 const GValue *value,
                                 GParamSpec   *pspec)
{
  SampleItemToggle *item = SAMPLE_ITEM_TOGGLE (object);

  switch (property_id)
    {
    case PROP_ICON:
      clutter_texture_set_cogl_texture (CLUTTER_TEXTURE (item->icon),
                                        g_value_get_boxed (value));
      break;
    case PROP_LABEL:
      clutter_text_set_text (CLUTTER_TEXT (item->label),
                             g_value_get_string (value));
      break;
    case PROP_TOGGLE:
      clutter_actor_hide (item->toggle);
      mx_toggle_set_active (MX_TOGGLE (item->toggle),
                            g_value_get_boolean (value));
      clutter_actor_show (item->toggle);
      break;
    case PROP_MODEL:
      item->model = g_value_get_object (value);
      break;
    case PROP_ROW:
      item->row = g_value_get_uint (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
notify_active_cb (MxToggle         *toggle,
                  GParamSpec       *pspec,
                  SampleItemToggle *item)
{
  ClutterModelIter *iter;

  if (item->model == NULL)
    return;

  iter = clutter_model_get_iter_at_row (item->model, item->row);
  clutter_model_iter_set (iter, COLUMN_TOGGLE, mx_toggle_get_active (toggle), -1);
  g_object_unref (iter);
}

static void
sample_item_toggle_class_init (SampleItemToggleClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GParamSpec *pspec;

  object_class->get_property = sample_item_toggle_get_property;
  object_class->set_property = sample_item_toggle_set_property;

  pspec = g_param_spec_boxed ("icon",
                              "icon",
                              "COGL texture handle for the icon",
                              COGL_TYPE_HANDLE,
                              G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_ICON, pspec);

  pspec = g_param_spec_string ("label",
                               "label",
                               "Text for the label",
                               NULL,
                               G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_LABEL, pspec);

  pspec = g_param_spec_boolean ("toggle",
                                "toggle",
                                "Value of the MxToggle",
                                FALSE,
                                G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_TOGGLE, pspec);

  pspec = g_param_spec_object ("model",
                               "ClutterModel",
                               "ClutterModel",
                               CLUTTER_TYPE_MODEL,
                               G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_MODEL, pspec);

  pspec = g_param_spec_uint ("row",
                             "row number",
                             "row number",
                             0, G_MAXUINT,
                             0,
                             G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_ROW, pspec);
}

static void
sample_item_toggle_init (SampleItemToggle *sample_item_toggle)
{
  ClutterActor *line;
  ClutterColor text_color = { 255, 255, 255, 255 };
  ClutterColor line_color = {0xFF, 0xFF, 0xFF, 0x33};

  sample_item_toggle->icon = clutter_texture_new ();
  clutter_container_add_actor (CLUTTER_CONTAINER (sample_item_toggle), sample_item_toggle->icon);
  clutter_actor_set_position (sample_item_toggle->icon, 18, 14);
  clutter_actor_show (sample_item_toggle->icon);

  sample_item_toggle->label = clutter_text_new ();
  clutter_text_set_color (CLUTTER_TEXT (sample_item_toggle->label), &text_color);
  clutter_text_set_font_name (CLUTTER_TEXT (sample_item_toggle->label), "Sans 28px");
  clutter_actor_set_position (sample_item_toggle->label, 79, 17);
  clutter_container_add_actor (CLUTTER_CONTAINER (sample_item_toggle), sample_item_toggle->label);
  clutter_actor_show (sample_item_toggle->label);

  sample_item_toggle->toggle = mx_toggle_new ();
  g_signal_connect (sample_item_toggle->toggle, "notify::active",
                    G_CALLBACK (notify_active_cb), sample_item_toggle);
  clutter_actor_set_position (sample_item_toggle->toggle, 350, 20);
  clutter_container_add_actor (CLUTTER_CONTAINER (sample_item_toggle), sample_item_toggle->toggle);
  clutter_actor_show (sample_item_toggle->toggle);

  line = clutter_rectangle_new_with_color (&line_color);
  clutter_actor_set_size (line, 52, 1);
  clutter_actor_set_position (line, 11, 63);
  clutter_container_add_actor (CLUTTER_CONTAINER (sample_item_toggle), line);
  clutter_actor_show (line);

  line = clutter_rectangle_new_with_color (&line_color);
  clutter_actor_set_size (line, 709, 1);
  clutter_actor_set_position (line, 67, 63);
  clutter_container_add_actor (CLUTTER_CONTAINER (sample_item_toggle), line);
  clutter_actor_show (line);
}

ClutterActor*
sample_item_toggle_new (void)
{
  return g_object_new (TYPE_SAMPLE_ITEM_TOGGLE, NULL);
}
