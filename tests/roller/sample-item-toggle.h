/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2011 Robert Bosch Car Multimedia GmbH
 * Author: Tomeu Vizoso <tomeu.vizoso@collabora.co.uk>
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __SAMPLE_ITEM_TOGGLE_H__
#define __SAMPLE_ITEM_TOGGLE_H__

#include <clutter/clutter.h>
#include <cogl/cogl.h>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-prototypes"
#include <mx/mx.h>
#pragma GCC diagnostic pop

G_BEGIN_DECLS

#define TYPE_SAMPLE_ITEM_TOGGLE             (sample_item_toggle_get_type ())
#define SAMPLE_ITEM_TOGGLE(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_SAMPLE_ITEM_TOGGLE, SampleItemToggle))
#define SAMPLE_ITEM_TOGGLE_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_SAMPLE_ITEM_TOGGLE, SampleItemToggleClass))
#define IS_SAMPLE_ITEM_TOGGLE(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_SAMPLE_ITEM_TOGGLE))
#define IS_SAMPLE_ITEM_TOGGLE_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_SAMPLE_ITEM_TOGGLE))
#define SAMPLE_ITEM_TOGGLE_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_SAMPLE_ITEM_TOGGLE, SampleItemToggleClass))

typedef struct _SampleItemToggle        SampleItemToggle;
typedef struct _SampleItemToggleClass   SampleItemToggleClass;

struct _SampleItemToggle
{
  ClutterGroup group;

  ClutterActor *icon;
  ClutterActor *label;
  ClutterActor *toggle;
  ClutterModel *model;
  guint row;
};

struct _SampleItemToggleClass
{
  ClutterGroupClass        parent_class;
};


GType          sample_item_toggle_get_type         (void) G_GNUC_CONST;
ClutterActor*  sample_item_toggle_new              (void);

G_END_DECLS

#endif /* __SAMPLE_ITEM_TOGGLE_H__ */

