/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <lightwood-roller/liblightwood-roller.h>
#include <lightwood-roller/liblightwood-fixedroller.h>
#include <lightwood-roller/liblightwood-expandable.h>

#include "sample-item.h"
#include "sample-glow-shader.h"

static void sample_item_expandable_iface_init (LightwoodExpandableIface *iface);

G_DEFINE_TYPE_WITH_CODE (SampleItem, sample_item, CLUTTER_TYPE_GROUP,
                         G_IMPLEMENT_INTERFACE (LIGHTWOOD_TYPE_EXPANDABLE,
                                                sample_item_expandable_iface_init));

enum
{
  PROP_0,

  PROP_ICON,
  PROP_LABEL,
  PROP_ARROW_UP,
  PROP_ARROW_DOWN,
  PROP_MODEL,
  PROP_ROW,
  PROP_FOCUSED,
};

static void
sample_item_get_property (GObject    *object,
                          guint       property_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  SampleItem *item = SAMPLE_ITEM (object);
  CoglHandle cogl_texture;

  switch (property_id)
    {
    case PROP_ICON:
      cogl_texture = clutter_texture_get_cogl_texture (CLUTTER_TEXTURE (item->icon));
      cogl_handle_ref (cogl_texture);
      g_value_set_boxed (value, cogl_texture);
      break;
    case PROP_LABEL:
      g_value_set_string (value,
          g_strdup (clutter_text_get_text (CLUTTER_TEXT (item->label))));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
sample_item_set_property (GObject      *object,
                          guint         property_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  SampleItem *item = SAMPLE_ITEM (object);

  switch (property_id)
    {
    case PROP_ICON:
      clutter_texture_set_cogl_texture (CLUTTER_TEXTURE (item->icon),
                                        g_value_get_boxed (value));
      break;
    case PROP_LABEL:
      clutter_text_set_text (CLUTTER_TEXT (item->label),
                             g_value_get_string (value));
      break;
    case PROP_ARROW_UP:
      if (g_value_get_int (value) == ARROW_HIDDEN)
        clutter_actor_hide (item->arrow_up);
      else
        {
          if (g_value_get_int (value) == ARROW_WHITE)
            clutter_actor_set_opacity (item->arrow_up, 255);
          else
            clutter_actor_set_opacity (item->arrow_up, 180);

          clutter_actor_show (item->arrow_up);
        }
      break;
    case PROP_ARROW_DOWN:
      if (g_value_get_int (value) == ARROW_HIDDEN)
        clutter_actor_hide (item->arrow_down);
      else
        {
          if (g_value_get_int (value) == ARROW_WHITE)
            clutter_actor_set_opacity (item->arrow_down, 255);
          else
            clutter_actor_set_opacity (item->arrow_down, 180);

          clutter_actor_show (item->arrow_down);
        }
      break;
    case PROP_MODEL:
      item->model = g_value_get_object (value);
      break;
    case PROP_ROW:
      {
        ClutterActor *parent = clutter_actor_get_parent (CLUTTER_ACTOR (object));

        if (parent != NULL &&
            item->model != NULL &&
            g_value_get_uint (value) == clutter_model_get_n_rows (item->model) - 1 &&
            lightwood_roller_get_effective_rollover_enabled (LIGHTWOOD_ROLLER (parent)))
          clutter_actor_show (item->extra_separator);
        else
          clutter_actor_hide (item->extra_separator);
        break;
      }
    case PROP_FOCUSED:
      if (g_value_get_boolean (value))
        {
          SampleItemClass *klass = SAMPLE_ITEM_GET_CLASS (object);

          if (clutter_actor_get_effect (item->icon, "glow") != NULL)
            return;

          clutter_actor_add_effect_with_name (item->icon, "glow", item->glow_effect_1);
          clutter_actor_add_effect_with_name (item->label, "glow", item->glow_effect_2);
        }
      else
        {
          if (clutter_actor_get_effect (item->icon, "glow") == NULL)
            return;

          clutter_actor_remove_effect_by_name (item->icon, "glow");
          clutter_actor_remove_effect_by_name (item->label, "glow");
        }
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

/* LightwoodExpandable implementation */

static void
sample_item_set_expanded (LightwoodExpandable *expandable,
                          gboolean             expanded,
                          gboolean             animated)
{
  if (expanded == SAMPLE_ITEM (expandable)->expanded)
    return;

  if (expanded)
    {
      if (animated)
        clutter_actor_animate (CLUTTER_ACTOR (expandable), CLUTTER_LINEAR, 1000,
                               "height", 100.0,
                               NULL);
      else
        clutter_actor_set_height (CLUTTER_ACTOR (expandable), 100);
    }
  else
    {
      if (animated)
        clutter_actor_animate (CLUTTER_ACTOR (expandable), CLUTTER_LINEAR, 1000,
                               "height", 64.0,
                               NULL);
      else
        clutter_actor_set_height (CLUTTER_ACTOR (expandable), -1);
    }

  SAMPLE_ITEM (expandable)->expanded = expanded;
}

static void
sample_item_expandable_iface_init (LightwoodExpandableIface *iface)
{
  iface->set_expanded = sample_item_set_expanded;
}

static gboolean
button_release_event (ClutterActor       *actor,
                      ClutterButtonEvent *event)
{
  ClutterActor *parent = clutter_actor_get_parent (actor);

  g_return_val_if_fail (LIGHTWOOD_IS_FIXED_ROLLER (parent), FALSE);

  g_debug ("actor '%s' has been activated, (un)expanding",
           clutter_actor_get_name (actor));

  if (SAMPLE_ITEM (actor)->expanded)
    lightwood_fixed_roller_set_expanded_item (LIGHTWOOD_FIXED_ROLLER (parent), NULL);
  else
    lightwood_fixed_roller_set_expanded_item (LIGHTWOOD_FIXED_ROLLER (parent), actor);

  return FALSE;
}

static void
sample_item_class_init (SampleItemClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  ClutterActorClass *actor_class = CLUTTER_ACTOR_CLASS (klass);
  GParamSpec *pspec;

  object_class->get_property = sample_item_get_property;
  object_class->set_property = sample_item_set_property;

  actor_class->button_release_event = button_release_event;

  pspec = g_param_spec_boxed ("icon",
                              "icon",
                              "COGL texture handle for the icon",
                              COGL_TYPE_HANDLE,
                              G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_ICON, pspec);

  pspec = g_param_spec_string ("label",
                               "label",
                               "Text for the label",
                               NULL,
                               G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_LABEL, pspec);

  pspec = g_param_spec_int ("arrow-up",
                            "arrow up",
                            "Controls the display of the upwards arrow",
                            ARROW_WHITE,
                            ARROW_LAST,
                            ARROW_HIDDEN,
                            G_PARAM_WRITABLE);
  g_object_class_install_property (object_class, PROP_ARROW_UP, pspec);

  pspec = g_param_spec_int ("arrow-down",
                            "arrow down",
                            "Controls the display of the downwards arrow",
                            ARROW_WHITE,
                            ARROW_LAST,
                            ARROW_HIDDEN,
                            G_PARAM_WRITABLE);
  g_object_class_install_property (object_class, PROP_ARROW_DOWN, pspec);

  pspec = g_param_spec_object ("model",
                               "ClutterModel",
                               "ClutterModel",
                               CLUTTER_TYPE_MODEL,
                               G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_MODEL, pspec);

  pspec = g_param_spec_uint ("row",
                             "row number",
                             "row number",
                             0, G_MAXUINT,
                             0,
                             G_PARAM_WRITABLE);
  g_object_class_install_property (object_class, PROP_ROW, pspec);

  pspec = g_param_spec_boolean ("focused",
                                "focused",
                                "Whether this actor should be rendered as focused",
                                FALSE,
                                G_PARAM_WRITABLE);
  g_object_class_install_property (object_class, PROP_FOCUSED, pspec);
}

static void
sample_item_init (SampleItem *sample_item)
{
  ClutterActor *line;
  ClutterColor text_color = { 255, 255, 255, 255 };
  ClutterColor line_color = {0xFF, 0xFF, 0xFF, 0x33};
  g_autofree gchar *arrow_up_filename = NULL, *arrow_down_filename = NULL;

  arrow_up_filename = g_test_build_filename (G_TEST_DIST, "data",
                                             ARROW_UP_FILE_NAME, NULL);
  arrow_down_filename = g_test_build_filename (G_TEST_DIST, "data",
                                               ARROW_DOWN_FILE_NAME, NULL);

  sample_item->icon = clutter_texture_new ();
  clutter_container_add_actor (CLUTTER_CONTAINER (sample_item), sample_item->icon);
  clutter_actor_set_position (sample_item->icon, 18, 14);
  clutter_actor_show (sample_item->icon);

  sample_item->arrow_up = clutter_texture_new_from_file (arrow_up_filename, NULL);
  clutter_actor_set_position (sample_item->arrow_up, 34, 10);
  clutter_container_add_actor (CLUTTER_CONTAINER (sample_item), sample_item->arrow_up);
  clutter_actor_hide (sample_item->arrow_up);

  sample_item->arrow_down = clutter_texture_new_from_file (arrow_down_filename, NULL);
  clutter_actor_set_position (sample_item->arrow_down, 34, 53);
  clutter_container_add_actor (CLUTTER_CONTAINER (sample_item), sample_item->arrow_down);
  clutter_actor_hide (sample_item->arrow_down);

  sample_item->label = clutter_text_new ();
  clutter_text_set_color (CLUTTER_TEXT (sample_item->label), &text_color);
  clutter_text_set_font_name (CLUTTER_TEXT (sample_item->label), "Sans 28px");
  clutter_actor_set_position (sample_item->label, 79, 17);
  clutter_container_add_actor (CLUTTER_CONTAINER (sample_item), sample_item->label);
  clutter_actor_show (sample_item->label);

  /* Vertical separators */
  line = clutter_rectangle_new_with_color (&line_color);
  clutter_actor_set_size (line, 1, 64);
  clutter_actor_set_position (line, 11, 0);
  clutter_container_add_actor (CLUTTER_CONTAINER (sample_item), line);
  clutter_actor_show (line);

  line = clutter_rectangle_new_with_color (&line_color);
  clutter_actor_set_size (line, 1, 64);
  clutter_actor_set_position (line, 64, 0);
  clutter_container_add_actor (CLUTTER_CONTAINER (sample_item), line);
  clutter_actor_show (line);

  /* Horizontal separators */
  line = clutter_rectangle_new_with_color (&line_color);
  clutter_actor_set_size (line, 52, 1);
  clutter_actor_set_position (line, 11, 63);
  clutter_container_add_actor (CLUTTER_CONTAINER (sample_item), line);
  clutter_actor_show (line);

  line = clutter_rectangle_new_with_color (&line_color);
  clutter_actor_set_size (line, 709, 1);
  clutter_actor_set_position (line, 67, 63);
  clutter_container_add_actor (CLUTTER_CONTAINER (sample_item), line);
  clutter_actor_show (line);

  /* Extra horizontal separators */
  sample_item->extra_separator = clutter_group_new ();
  clutter_actor_set_position (sample_item->extra_separator, 0, 60);
  clutter_container_add_actor (CLUTTER_CONTAINER (sample_item),
                               sample_item->extra_separator);

  line = clutter_rectangle_new_with_color (&line_color);
  clutter_actor_set_size (line, 52, 1);
  clutter_actor_set_position (line, 11, 0);
  clutter_container_add_actor (CLUTTER_CONTAINER (sample_item->extra_separator),
                               line);
  clutter_actor_show (line);

  line = clutter_rectangle_new_with_color (&line_color);
  clutter_actor_set_size (line, 709, 1);
  clutter_actor_set_position (line, 67, 0);
  clutter_container_add_actor (CLUTTER_CONTAINER (sample_item->extra_separator),
                               line);
  clutter_actor_show (line);

  clutter_actor_set_reactive (CLUTTER_ACTOR (sample_item), TRUE);

  sample_item->glow_effect_1 = g_object_ref (sample_glow_shader_new ());
  sample_item->glow_effect_2 = g_object_ref (sample_glow_shader_new ());
}

ClutterActor*
sample_item_new (void)
{
    return g_object_new (TYPE_SAMPLE_ITEM, NULL);
}
