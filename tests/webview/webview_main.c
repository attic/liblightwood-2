/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/*
 * webview_main.c
 *
 *  Created on: Aug 1, 2012
 *      Author: chaiwala
 */
#include <mx/mx.h>
#include <liblightwood-webview-widget.h>
//#include "lightwood_webview_rendertheme.h"
#include <cairo/cairo.h>
#include <cairo/cairo-pdf.h>
#include <glib/poppler.h>
#include <cairo.h>
#include <math.h>

static MxWindow* window=NULL;
LightwoodWebViewWidget *webview_widget = NULL;
ClutterActor* view=NULL;
//static void _open_pdf(const gchar* pdf_uri);
//static gboolean _render_pdf_cb(ClutterCanvas* texture,cairo_t* cr,gint childWidth,gint childHeight,PopplerDocument* document);

static void
scrolltoTopCallback (ClutterActor *actor,
                     LightwoodWebViewWidget *webview_widget)
{
	lightwood_webview_widget_scroll_to(webview_widget,0,0);
}

static void
zoominoutCallback (ClutterActor *actor,
                   LightwoodWebViewWidget *view)
{
	static gfloat zoom_level=1.0;
	if( g_strcmp0(mx_button_get_label((MxButton *)actor),"+") == 0 )
		zoom_level += 0.2;
	else
		zoom_level -= 0.2;

	zoom_level = MAX(zoom_level,0.2);
	g_print("\n trying zoom %f",zoom_level);
	lightwood_webview_widget_set_zoom_level (webview_widget, zoom_level);
}

static void
thumbnailCallback (ClutterActor *actor,
                   LightwoodWebViewWidget *view)
{
	gchar* data = lightwood_webview_widget_get_thumbnail(webview_widget,150,150);
	g_assert(data != NULL);
	free(data);
}

static void
pdfCallback (ClutterActor *actor,
             LightwoodWebViewWidget *view)
{
	lightwood_webview_widget_save_as_pdf(webview_widget,"sample.pdf");
}
static void locationChangeCallback(ClutterActor* actor, MxWebView* view)
{
    webkit_iweb_view_load_uri(WEBKIT_IWEB_VIEW(view), clutter_text_get_text(CLUTTER_TEXT(actor)));
}
static void
uriChanged(WebKitWebPage* page, GParamSpec* pspec, ClutterText* text)
{
    clutter_text_set_text(text, webkit_web_page_get_uri(page));
}
static void backCallback(ClutterActor* actor, MxWebView* view)
{
    WebKitWebPage* webPage = webkit_iweb_view_get_page(WEBKIT_IWEB_VIEW(view));
    webkit_web_page_go_back(webPage);
}

static gboolean alertCallback(WebKitWebPage* webPage, WebKitWebFrame* frame, const char* message, gpointer data)
{
    g_message("ALERT: %s", message);
    return TRUE;
}
static gboolean scrollEventCallback(ClutterActor* actor, ClutterEvent* event, WebKitWebPage* page)
{
	g_print("\n scrollEventCallback");
    ClutterScrollEvent* scroll = (ClutterScrollEvent*)event;

    if (scroll->direction != CLUTTER_SCROLL_DOWN && scroll->direction != CLUTTER_SCROLL_UP)
        return FALSE;

/*    if (!(scroll->modifier_state & CLUTTER_CONTROL_MASK))
        return FALSE;*/

    static float zoomFactor = 1.0;

    if (scroll->direction == CLUTTER_SCROLL_UP)
        zoomFactor += 0.2;
    else if (scroll->direction == CLUTTER_SCROLL_DOWN)
        zoomFactor -= 0.2;

    zoomFactor = MAX(zoomFactor,0.2);
    	g_print("\n trying zoom %f",zoomFactor);
    lightwood_webview_widget_set_zoom_level (webview_widget, zoomFactor);

    return TRUE;
}

static void create_toolbar()
{
    mx_window_set_has_toolbar(window, TRUE);
    // The toolbar.
      MxToolbar* toolbar = mx_window_get_toolbar(window);

      ClutterLayoutManager* hboxManager = clutter_box_layout_new();
      clutter_box_layout_set_vertical(CLUTTER_BOX_LAYOUT(hboxManager), FALSE);
      clutter_box_layout_set_spacing(CLUTTER_BOX_LAYOUT(hboxManager), 6);

      ClutterActor* hbox = clutter_box_new(hboxManager);
      clutter_container_add_actor(CLUTTER_CONTAINER(toolbar), hbox);
      clutter_actor_set_width(hbox, 400);

      WebKitWebPage* page = webkit_iweb_view_get_page(WEBKIT_IWEB_VIEW(view));
      // The back button.
         ClutterActor* backText = mx_button_new_with_label("Back");
         g_signal_connect(backText, "clicked",
                          G_CALLBACK(backCallback), view);
         clutter_box_pack(CLUTTER_BOX(hbox), backText,
             "expand", TRUE,
             "y-fill", TRUE,
             NULL);

         // The location bar.
         ClutterActor* locationBar = mx_entry_new();
         ClutterActor* locationTextActor = mx_entry_get_clutter_text(MX_ENTRY(locationBar));

         g_signal_connect(locationTextActor, "activate",
                          G_CALLBACK(locationChangeCallback), view);

         g_signal_connect(page, "notify::uri",
                          G_CALLBACK(uriChanged), locationTextActor);

         clutter_box_pack(CLUTTER_BOX(hbox), locationBar,
             "expand", TRUE,
             "x-fill", TRUE,
             "y-fill", TRUE,
             NULL);


      ClutterActor* pdf_save = mx_button_new_with_label("PDF");
      g_signal_connect(pdf_save, "clicked",
                       G_CALLBACK(pdfCallback), view);
      clutter_box_pack(CLUTTER_BOX(hbox), pdf_save,
          "expand", TRUE,
          "y-fill", TRUE,
          NULL);

      ClutterActor* scrolltotp = mx_button_new_with_label("^");
      g_signal_connect(scrolltotp, "clicked",
                       G_CALLBACK(scrolltoTopCallback), view);
      clutter_box_pack(CLUTTER_BOX(hbox), scrolltotp,
          "expand", TRUE,
          "y-fill", TRUE,
          NULL);

      ClutterActor* zoomin = mx_button_new_with_label("+");
      g_signal_connect(zoomin, "clicked",
                       G_CALLBACK(zoominoutCallback), view);
      clutter_box_pack(CLUTTER_BOX(hbox), zoomin,
          "expand", TRUE,
          "y-fill", TRUE,
          NULL);

      ClutterActor* zoomout = mx_button_new_with_label("-");
      g_signal_connect(zoomout, "clicked",
                       G_CALLBACK(zoominoutCallback), view);
      clutter_box_pack(CLUTTER_BOX(hbox), zoomout,
          "expand", TRUE,
          "y-fill", TRUE,
          NULL);

      ClutterActor* thumbnail = mx_button_new_with_label("PNG");
      g_signal_connect(thumbnail, "clicked",
                       G_CALLBACK(thumbnailCallback), view);
      clutter_box_pack(CLUTTER_BOX(hbox), thumbnail,
          "expand", TRUE,
          "y-fill", TRUE,
          NULL);

      g_signal_connect(view, "scroll-event",
                        G_CALLBACK(scrollEventCallback), page);

}

static gboolean blockButtonPressEvent(ClutterActor* actor, ClutterButtonEvent* event, gpointer data)
{
    // Prevent clicks to propagate to the stage as they're used to scroll and
    // should not reach the default MxWindow handler (which otherwise moves
    // the window if it has a toolbar)
    return event->button == 1;
}

int main(int argc, char** argv)
{
	clutter_x11_enable_xinput();

  //  g_type_init();

    webkit_init();

    if (clutter_get_accessibility_enabled()) {
        char* bridgePath = g_module_build_path("/usr/lib/gtk-2.0/modules", "libatk-bridge");
        GModule* module = g_module_open(bridgePath, G_MODULE_BIND_LAZY);

        void (*symbol) (void);
        if (module != NULL && g_module_symbol(module, "gnome_accessibility_module_init", (gpointer*)&symbol))
            symbol();
        else
            g_warning("Failed to initialize atk-bridge.");
    }

    MxApplication* app = mx_application_new("Test_Web.View", 0);

    ClutterActor* stage;
     //stage = CLUTTER_ACTOR(graphics_layer_stage_new());
    stage = CLUTTER_ACTOR(clutter_stage_new());
    clutter_stage_set_user_resizable(CLUTTER_STAGE(stage), TRUE);
    window = mx_window_new_with_clutter_stage(CLUTTER_STAGE(stage));
    mx_application_add_window(app, window);
    gdouble width = 800, height = 480;
    clutter_actor_set_size(stage, width, height);
    clutter_stage_set_color(CLUTTER_STAGE(stage), CLUTTER_COLOR_Black);

/*  	
    	g_print("\n trying to open %s",pdf_uri);
    	GError* error = NULL;
    	PopplerDocument *document = poppler_document_new_from_file (pdf_uri, NULL, &error);
    	g_assert(document != NULL);
    	PopplerPage* pdf_page = poppler_document_get_page (document, 0);
    	double childWidth, childHeight;
    	poppler_page_get_size (pdf_page, &childWidth, &childHeight);
    	childHeight = MIN(childHeight,500);
    	childWidth = MIN(childWidth,500);
    	g_object_unref (pdf_page);

	ClutterContent* cairo_canvas = clutter_canvas_new();
	ClutterActor* pdfActor = clutter_actor_new();
	clutter_actor_set_content(pdfActor,cairo_canvas);
	mx_window_set_child(window,pdfActor);

	clutter_canvas_set_size(cairo_canvas,ceilf(childWidth),ceilf(childHeight));
	clutter_actor_set_size(pdfActor,childWidth,childHeight);
	g_signal_connect(cairo_canvas,"draw",(GCallback)_render_pdf_cb,document);
	clutter_content_invalidate(cairo_canvas);
	clutter_actor_set_size(pdfActor,childWidth,childHeight);

    clutter_actor_show(stage);
    mx_application_run(app);
    return 0;*/

    // The view.
    //LightwoodWebviewAssistant* hook = g_new(LightwoodWebviewAssistant,1);
    //webkit_irender_theme_set_default(NULL);
    //hook->m_renderTheme = (WebKitIRenderTheme*)lightwood_webview_render_theme_new(webkit_irender_theme_get_default());
   // hook->m_renderTheme = NULL;
    //hook->m_iwebview_speller = NULL;
    //hook->m_soupAuth_handler = NULL;
    webview_widget = lightwood_webview_widget_new();
    lightwood_webview_widget_init_session(webview_widget, "testwebview",NULL);
	//mx_bin_set_fill(MX_BIN(webview_widget), TRUE, TRUE);
	view= lightwood_webview_widget_get_webview(webview_widget);
    //view = mx_web_view_new();
	mx_window_set_child(window, CLUTTER_ACTOR (webview_widget));
/*   	g_object_set(webview_widget,
   				"x-expand",TRUE,
   				"y-expand",TRUE,
   				"x-align",CLUTTER_ACTOR_ALIGN_FILL,
   				"y-align",CLUTTER_ACTOR_ALIGN_FILL,
   				NULL);*/

	mx_window_set_has_toolbar(window, TRUE);
	create_toolbar();
	g_signal_connect_after(webview_widget, "button-press-event",
	                     G_CALLBACK(blockButtonPressEvent), 0);

    WebKitWebPage* page = webkit_iweb_view_get_page(WEBKIT_IWEB_VIEW(view));
    g_signal_connect(page, "script-alert",
                     G_CALLBACK(alertCallback), window);
    WebKitWebSettings* webkit_settings = webkit_web_page_get_settings(page);
    g_object_set(webkit_settings,
 				//"enable-scripts",TRUE,
 				//"enable-plugins",TRUE,
 				//"enable-java-applet",TRUE,
 				//"javascript-can-open-windows-automatically", TRUE,
                 "enable-tiled-backing-store", TRUE,
                 "enable-developer-extras",TRUE,
                 //"enable-universal-access-from-file-uris",TRUE,
                 //"enable-webgl",TRUE,
                  NULL);

    clutter_actor_show(stage);


    if (argc > 1)
    	//lightwood_webview_widget_load_url(webview_widget, argv[1]);
    	webkit_iweb_view_load_uri(WEBKIT_IWEB_VIEW(view),argv[1]);
    else
    	//lightwood_webview_widget_load_url(webview_widget, "http://www.google.com/");
    	webkit_iweb_view_load_uri(WEBKIT_IWEB_VIEW(view),"http://www.google.com/");

//    mx_application_run(app);

    return 0;
}
#if 0
static gboolean _render_pdf_cb(ClutterCanvas* texture,cairo_t* cr,gint childWidth,gint childHeight,PopplerDocument* document)
{
	//clutter_cairo_texture_set_auto_resize(texture,TRUE);
	cairo_save(cr);
	g_assert(document != NULL);
	PopplerPage* pdf_page = poppler_document_get_page (document, 0);
	g_print("\n page rendered");
/*	cairo_surface_t* surface = cairo_image_surface_create (CAIRO_FORMAT_ARGB32,
			150*childWidth/72.0,
			150*childHeight/72.0);
    cairo_set_source_surface(cr,surface,0,0);*/
	poppler_page_render (pdf_page, cr);
	cairo_paint(cr);
	cairo_set_operator (cr, CAIRO_OPERATOR_DEST_OVER);
	cairo_set_source_rgb (cr, 1, 1, 1);
	cairo_paint (cr);
	cairo_restore(cr);
	 g_object_unref (pdf_page);
	 g_object_unref (document);
	return TRUE;
}
#endif
/*static void _open_pdf(const gchar* pdf_uri)
{

	g_print("\n trying to open %s",pdf_uri);
	GError* error = NULL;
	PopplerDocument *document = poppler_document_new_from_file (pdf_uri, NULL, &error);
	g_assert(document != NULL);
	PopplerPage* pdf_page = poppler_document_get_page (document, 0);
	double childWidth, childHeight;
	poppler_page_get_size (pdf_page, &childWidth, &childHeight);
	g_object_unref (pdf_page);

	//g_print("\n scroll_view=%f,%f  texture=%d,%d",width,height,childWidth,childHeight);

	ClutterActor* m_pdfView = mx_kinetic_scroll_view_new();
	clutter_actor_show(m_pdfView);
	//clutter_actor_set_size(priv->m_pdfActor,clutter_actor_get_width(self),clutter_actor_get_height(self));
	clutter_actor_set_size(m_pdfView,800,480);
   	//clutter_actor_add_child(self,m_pdfView);
	//mx_window_set_child(window,m_pdfView);
   	//clutter_actor_add_constraint(priv->m_pdfActor,clutter_bind_constraint_new (self,CLUTTER_BIND_SIZE,0));
   	//g_print("\n pdf view height = %f",clutter_actor_get_height(priv->m_pdfActor));


	ClutterActor* view_port = mx_viewport_new();
	//clutter_actor_set_size(view_port,clutter_actor_get_width(self),clutter_actor_get_height(self));
	clutter_actor_set_size(view_port,800,480);
   	//clutter_actor_add_child(priv->m_pdfActor,view_port);
	 mx_bin_set_child(m_pdfView,view_port);
    	mx_bin_set_fill(m_pdfView,TRUE,TRUE);
   	//clutter_actor_add_constraint(view_port,clutter_bind_constraint_new (priv->m_pdfActor,CLUTTER_BIND_SIZE,0));
	//clutter_actor_set_x_align(view_port,CLUTTER_ACTOR_ALIGN_START);
	//clutter_actor_set_y_align(view_port,CLUTTER_ACTOR_ALIGN_START);

	ClutterContent* cairo_canvas = clutter_canvas_new();
	clutter_canvas_set_size(cairo_canvas,(gint)childWidth,(gint)childHeight);
	ClutterActor* pdfActor = clutter_actor_new();
	clutter_actor_set_content(pdfActor,cairo_canvas);
	clutter_actor_set_size(pdfActor,(gint)childWidth,(gint)childHeight);
	//mx_bin_set_child(view_port,pdfActor);
	mx_window_set_child(window,pdfActor);
	mx_bin_set_fill(view_port,TRUE,TRUE);
	clutter_actor_show(view_port);
	clutter_actor_show(pdfActor);
	//clutter_actor_set_x_align(priv->m_pdfActor,CLUTTER_ACTOR_ALIGN_FILL);
	//clutter_actor_set_y_align(priv->m_pdfActor,CLUTTER_ACTOR_ALIGN_FILL);
	g_signal_connect(cairo_canvas,"draw",(GCallback)_render_pdf_cb,document);
	clutter_content_invalidate(cairo_canvas);
   //clutter_actor_queue_relayout(_render_pdf_cb);

	MxAdjustment *hadjustment, *vadjustment;
	mx_scrollable_get_adjustments(view_port ,&hadjustment,&vadjustment);
	mx_adjustment_set_elastic(hadjustment,TRUE);
	mx_adjustment_set_elastic(vadjustment,TRUE);
	g_object_set(G_OBJECT(hadjustment),
	                     "lower", 0.0,
	                     "upper", (gdouble)childWidth,
	                     "page-size", (gdouble)800.0,
	                     "page-increment", (gdouble)childWidth / 3,
	                     "step-increment", (gdouble)childWidth / 12,
	                     NULL);
    g_object_set(G_OBJECT(vadjustment),
                  "lower", 0.0,
                  "upper", (gdouble)childHeight,
                  "page-size", (gdouble)480.0,
                  "page-increment", (gdouble)childHeight / 3,
                  "step-increment", (gdouble)childHeight / 12,
                  NULL);


}*/
